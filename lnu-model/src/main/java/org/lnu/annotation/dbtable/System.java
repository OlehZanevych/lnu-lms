package org.lnu.annotation.dbtable;

/**
 * Annotation, that is used to describe system table.
 * @author OlehZanevych
 */
public @interface System {

}
