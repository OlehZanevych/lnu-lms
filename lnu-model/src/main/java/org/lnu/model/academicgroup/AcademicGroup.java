package org.lnu.model.academicgroup;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Formula;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.lnu.annotation.dbtable.Essence;
import org.lnu.model.APIModel;
import org.lnu.model.department.Department;
import org.lnu.model.enumtype.capability.Capability;
import org.lnu.model.lecturer.Lecturer;
import org.lnu.model.specialty.Specialty;
import org.lnu.model.student.Student;

/**
 * Entity, that describes academic group.
 * @author OlehZanevych
 */
@Essence
@Entity
@Table(name = "t_essence_academic_groups")
@AssociationOverrides({
	@AssociationOverride(
		name = "responsibleOwnUsers",
		joinTable = @JoinTable(name = "t_relationship_academic_groups_responsible_own_users",
			joinColumns = @JoinColumn(name = "academic_group_id"),
			inverseJoinColumns = @JoinColumn(name = "user_id")
		)
	),
	@AssociationOverride(
		name = "responsibleGroups",
		joinTable = @JoinTable(name = "t_relationship_academic_groups_responsible_groups",
			joinColumns = @JoinColumn(name = "academic_group_id"),
			inverseJoinColumns = @JoinColumn(name = "group_id")
		)
	),
	@AssociationOverride(
		name = "responsibleUsers",
		joinTable = @JoinTable(name = "v_relationship_academic_groups_responsible_users",
			joinColumns = @JoinColumn(name = "academic_group_id", insertable = false, updatable = false),
			inverseJoinColumns = @JoinColumn(name = "user_id", insertable = false, updatable = false)
		)
	)
})
public final class AcademicGroup extends APIModel<Long> {
	
	private static final long serialVersionUID = 1L;
	
	@Type(type = "org.lnu.model.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",
					value = "org.lnu.model.enumtype.capability.Capability"
			)
	)
	@Formula("get_academic_group_capability(:user.id, id, is_private)")
	@Enumerated(EnumType.STRING)
	private Capability capability;
	
	@NotNull
	@Column(name = "name")
	private String name;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "department_id")
	private Department department;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "specialty_id")
	private Specialty specialty;
	
	@NotNull
	@Column(name = "year")
	private Byte year;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "mentor_id")
	private Lecturer mentor;
	
	@ManyToOne
	@JoinColumn(name = "leader_id")
	private Student leader;
	
	/**
	 * Default constructor with no parameters.
	 */
	public AcademicGroup() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public AcademicGroup(final Long id) {
		super(id);
	}
	
	@Override
	public Capability getCapability() {
		return capability;
	}

	@Override
	public void setCapability(final Capability capability) {
		this.capability = capability;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(final Department department) {
		this.department = department;
	}

	public Specialty getSpecialty() {
		return specialty;
	}

	public void setSpecialty(final Specialty specialty) {
		this.specialty = specialty;
	}

	public Byte getYear() {
		return year;
	}

	public void setYear(final Byte year) {
		this.year = year;
	}

	public Lecturer getMentor() {
		return mentor;
	}

	public void setMentor(final Lecturer mentor) {
		this.mentor = mentor;
	}

	public Student getLeader() {
		return leader;
	}

	public void setLeader(final Student leader) {
		this.leader = leader;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((capability == null) ? 0 : capability.hashCode());
		result = prime * result + ((department == null) ? 0 : department.hashCode());
		result = prime * result + ((leader == null) ? 0 : leader.hashCode());
		result = prime * result + ((mentor == null) ? 0 : mentor.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((specialty == null) ? 0 : specialty.hashCode());
		result = prime * result + ((year == null) ? 0 : year.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AcademicGroup other = (AcademicGroup) obj;
		if (capability != other.capability) {
			return false;
		}
		if (department == null) {
			if (other.department != null) {
				return false;
			}
		} else if (!department.equals(other.department)) {
			return false;
		}
		if (leader == null) {
			if (other.leader != null) {
				return false;
			}
		} else if (!leader.equals(other.leader)) {
			return false;
		}
		if (mentor == null) {
			if (other.mentor != null) {
				return false;
			}
		} else if (!mentor.equals(other.mentor)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (specialty == null) {
			if (other.specialty != null) {
				return false;
			}
		} else if (!specialty.equals(other.specialty)) {
			return false;
		}
		if (year == null) {
			if (other.year != null) {
				return false;
			}
		} else if (!year.equals(other.year)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AcademicGroup [capability=");
		builder.append(capability);
		builder.append(", name=");
		builder.append(name);
		builder.append(", department=");
		builder.append(department);
		builder.append(", specialty=");
		builder.append(specialty);
		builder.append(", year=");
		builder.append(year);
		builder.append(", mentor=");
		builder.append(mentor);
		builder.append(", leader=");
		builder.append(leader);
		builder.append(", isPrivate=");
		builder.append(isPrivate);
		builder.append(", responsibleOwnUsers=");
		builder.append(responsibleOwnUsers);
		builder.append(", responsibleGroups=");
		builder.append(responsibleGroups);
		builder.append(", responsibleUsers=");
		builder.append(responsibleUsers);
		builder.append(", id=");
		builder.append(id);
		builder.append(", parentId=");
		builder.append(parentId);
		builder.append("]");
		return builder.toString();
	}

}
