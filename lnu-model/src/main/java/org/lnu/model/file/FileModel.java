package org.lnu.model.file;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.lnu.model.APIModel;

/**
 * Base model for file API methods.
 * @author OlehZanevych
 */
@MappedSuperclass
public abstract class FileModel extends APIModel<Long> {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name = "file_uri")
	protected String fileURI;
	
	/**
	 * Default constructor with no parameters.
	 */
	public FileModel() {
		
	}

	/**
	 * Constructor with id.
	 * @param id id.
	 */
	public FileModel(final Long id) {
		super(id);
	}

	public String getFileURI() {
		return fileURI;
	}

	public void setFileURI(final String fileURI) {
		this.fileURI = fileURI;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((fileURI == null) ? 0 : fileURI.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		FileModel other = (FileModel) obj;
		if (fileURI == null) {
			if (other.fileURI != null) {
				return false;
			}
		} else if (!fileURI.equals(other.fileURI)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FileModel [fileURI=");
		builder.append(fileURI);
		builder.append(", isPrivate=");
		builder.append(isPrivate);
		builder.append(", responsibleOwnUsers=");
		builder.append(responsibleOwnUsers);
		builder.append(", responsibleGroups=");
		builder.append(responsibleGroups);
		builder.append(", responsibleUsers=");
		builder.append(responsibleUsers);
		builder.append(", id=");
		builder.append(id);
		builder.append(", parentId=");
		builder.append(parentId);
		builder.append("]");
		return builder.toString();
	}

}
