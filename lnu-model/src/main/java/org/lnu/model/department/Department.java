package org.lnu.model.department;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Formula;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.lnu.annotation.dbtable.Essence;
import org.lnu.model.APIModel;
import org.lnu.model.building.map.BuildingMap;
import org.lnu.model.enumtype.capability.Capability;
import org.lnu.model.unit.Unit;
import org.lnu.model.lecturer.Lecturer;

/**
 * Entity, that describes department.
 * @author OlehZanevych
 */
@Essence
@Entity
@Table(name = "t_essence_departments")
@AssociationOverrides({
	@AssociationOverride(
		name = "responsibleOwnUsers",
		joinTable = @JoinTable(name = "t_relationship_departments_responsible_own_users",
			joinColumns = @JoinColumn(name = "department_id"),
			inverseJoinColumns = @JoinColumn(name = "user_id")
		)
	),
	@AssociationOverride(
		name = "responsibleGroups",
		joinTable = @JoinTable(name = "t_relationship_departments_responsible_groups",
			joinColumns = @JoinColumn(name = "department_id"),
			inverseJoinColumns = @JoinColumn(name = "group_id")
		)
	),
	@AssociationOverride(
		name = "responsibleUsers",
		joinTable = @JoinTable(name = "v_relationship_departments_responsible_users",
			joinColumns = @JoinColumn(name = "department_id", insertable = false, updatable = false),
			inverseJoinColumns = @JoinColumn(name = "user_id", insertable = false, updatable = false)
		)
	)
})
public final class Department extends APIModel<Long> {
	
	private static final long serialVersionUID = 1L;
	
	@Type(type = "org.lnu.model.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",
					value = "org.lnu.model.enumtype.capability.Capability"
			)
	)
	@Formula("get_department_capability(:user.id, id, is_private)")
	@Enumerated(EnumType.STRING)
	private Capability capability;
	
	@NotNull
	@Column(name = "name")
	private String name;
	
	@NotNull
	@Column(name = "abbreviation", unique = true)
	private String abbreviation;
	
	@ManyToOne
	@JoinColumn(name = "unit_id")
	private Unit unit;
	
	@NotNull
	@Column(name = "email")
	private String email;
	
	@NotNull
	@Column(name = "phone")
	private String phone;
	
	@ManyToOne
	@JoinColumn(name = "head_id")
	private Lecturer head;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "building_map_id")
	private BuildingMap buildingMap;
	
	@NotNull
	@Column(name = "on_map_id")
	private Short onMapId;
	
	@NotNull
	@Column(name = "room")
	private String room;
	
	@NotNull
	@Column(name = "info")
	private String info;
	
	/**
	 * Default constructor with no parameters.
	 */
	public Department() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public Department(final Long id) {
		super(id);
	}
	
	@Override
	public Capability getCapability() {
		return capability;
	}

	@Override
	public void setCapability(final Capability capability) {
		this.capability = capability;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(final String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(final Unit unit) {
		this.unit = unit;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(final String phone) {
		this.phone = phone;
	}

	public Lecturer getHead() {
		return head;
	}

	public void setHead(final Lecturer head) {
		this.head = head;
	}

	public BuildingMap getBuildingMap() {
		return buildingMap;
	}

	public void setBuildingMap(final BuildingMap buildingMap) {
		this.buildingMap = buildingMap;
	}

	public Short getOnMapId() {
		return onMapId;
	}

	public void setOnMapId(final Short onMapId) {
		this.onMapId = onMapId;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(final String room) {
		this.room = room;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(final String info) {
		this.info = info;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((abbreviation == null) ? 0 : abbreviation.hashCode());
		result = prime * result + ((buildingMap == null) ? 0 : buildingMap.hashCode());
		result = prime * result + ((capability == null) ? 0 : capability.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		result = prime * result + ((head == null) ? 0 : head.hashCode());
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((onMapId == null) ? 0 : onMapId.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((room == null) ? 0 : room.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Department other = (Department) obj;
		if (abbreviation == null) {
			if (other.abbreviation != null) {
				return false;
			}
		} else if (!abbreviation.equals(other.abbreviation)) {
			return false;
		}
		if (buildingMap == null) {
			if (other.buildingMap != null) {
				return false;
			}
		} else if (!buildingMap.equals(other.buildingMap)) {
			return false;
		}
		if (capability != other.capability) {
			return false;
		}
		if (email == null) {
			if (other.email != null) {
				return false;
			}
		} else if (!email.equals(other.email)) {
			return false;
		}
		if (unit == null) {
			if (other.unit != null) {
				return false;
			}
		} else if (!unit.equals(other.unit)) {
			return false;
		}
		if (head == null) {
			if (other.head != null) {
				return false;
			}
		} else if (!head.equals(other.head)) {
			return false;
		}
		if (info == null) {
			if (other.info != null) {
				return false;
			}
		} else if (!info.equals(other.info)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (onMapId == null) {
			if (other.onMapId != null) {
				return false;
			}
		} else if (!onMapId.equals(other.onMapId)) {
			return false;
		}
		if (phone == null) {
			if (other.phone != null) {
				return false;
			}
		} else if (!phone.equals(other.phone)) {
			return false;
		}
		if (room == null) {
			if (other.room != null) {
				return false;
			}
		} else if (!room.equals(other.room)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Department [capability=");
		builder.append(capability);
		builder.append(", name=");
		builder.append(name);
		builder.append(", abbreviation=");
		builder.append(abbreviation);
		builder.append(", unit=");
		builder.append(unit);
		builder.append(", email=");
		builder.append(email);
		builder.append(", phone=");
		builder.append(phone);
		builder.append(", head=");
		builder.append(head);
		builder.append(", buildingMap=");
		builder.append(buildingMap);
		builder.append(", onMapId=");
		builder.append(onMapId);
		builder.append(", room=");
		builder.append(room);
		builder.append(", info=");
		builder.append(info);
		builder.append(", isPrivate=");
		builder.append(isPrivate);
		builder.append(", responsibleOwnUsers=");
		builder.append(responsibleOwnUsers);
		builder.append(", responsibleGroups=");
		builder.append(responsibleGroups);
		builder.append(", responsibleUsers=");
		builder.append(responsibleUsers);
		builder.append(", id=");
		builder.append(id);
		builder.append(", parentId=");
		builder.append(parentId);
		builder.append("]");
		return builder.toString();
	}

}
