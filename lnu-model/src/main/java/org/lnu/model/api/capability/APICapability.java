package org.lnu.model.api.capability;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.lnu.annotation.dbtable.System;
import org.lnu.model.ModelWithSimpleId;
import org.lnu.model.enumtype.api.API;
import org.lnu.model.enumtype.api.capability.level.APICapabilityLevel;
import org.lnu.model.role.Role;

/**
 * Entity, that describes API capability of Role.
 * @author OlehZanevych
 */
@System
@Entity
@Table(name = "t_system_api_capabilities")
public final class APICapability extends ModelWithSimpleId<Long> {
	
	private static final long serialVersionUID = 1L;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "role_id")
	private Role role;
	
	@NotNull
	@Type(type = "org.lnu.model.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",
					value = "org.lnu.model.enumtype.api.API"
			)
	)
	@Column(name = "api", columnDefinition = "e_api")
	@Enumerated(EnumType.STRING)
	private API api;
	
	@NotNull
	@Type(type = "org.lnu.model.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",
					value = "org.lnu.model.enumtype.api.capability.level.APICapabilityLevel"
			)
	)
	@Column(name = "api_capability_level", columnDefinition = "e_api_capability_level")
	@Enumerated(EnumType.STRING)
	private APICapabilityLevel apiCapabilityLevel;
	
	/**
	 * Default constructor with no parameters.
	 */
	public APICapability() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public APICapability(final Long id) {
		super(id);
	}

	public API getApi() {
		return api;
	}

	public void setApi(final API api) {
		this.api = api;
	}

	public APICapabilityLevel getApiCapabilityLevel() {
		return apiCapabilityLevel;
	}

	public void setApiCapabilityLevel(final APICapabilityLevel apiCapabilityLevel) {
		this.apiCapabilityLevel = apiCapabilityLevel;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((api == null) ? 0 : api.hashCode());
		result = prime * result + ((apiCapabilityLevel == null) ? 0 : apiCapabilityLevel.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		APICapability other = (APICapability) obj;
		if (api != other.api) {
			return false;
		}
		if (apiCapabilityLevel != other.apiCapabilityLevel) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("APICapability [role=");
		builder.append(role);
		builder.append(", api=");
		builder.append(api);
		builder.append(", apiCapabilityLevel=");
		builder.append(apiCapabilityLevel);
		builder.append(", id=");
		builder.append(id);
		builder.append(", parentId=");
		builder.append(parentId);
		builder.append("]");
		return builder.toString();
	}
	
}
