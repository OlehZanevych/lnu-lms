package org.lnu.model.load;

import java.util.List;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Formula;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.lnu.annotation.dbtable.Essence;
import org.lnu.model.APIModel;
import org.lnu.model.atomicload.AtomicLoad;
import org.lnu.model.course.Course;
import org.lnu.model.curriculum.semester.hours.CurriculumSemesterHours;
import org.lnu.model.department.Department;
import org.lnu.model.enumtype.capability.Capability;

/**
 * Entity, that describes Lecturers Load.
 * @author OlehZanevych
 */
@Essence
@Entity
@Table(name = "t_essence_load")
@AssociationOverrides({
	@AssociationOverride(
		name = "responsibleOwnUsers",
		joinTable = @JoinTable(name = "t_relationship_load_responsible_own_users",
			joinColumns = @JoinColumn(name = "load_id"),
			inverseJoinColumns = @JoinColumn(name = "user_id")
		)
	),
	@AssociationOverride(
		name = "responsibleGroups",
		joinTable = @JoinTable(name = "t_relationship_load_responsible_groups",
			joinColumns = @JoinColumn(name = "load_id"),
			inverseJoinColumns = @JoinColumn(name = "group_id")
		)
	),
	@AssociationOverride(
		name = "responsibleUsers",
		joinTable = @JoinTable(name = "v_relationship_load_responsible_users",
			joinColumns = @JoinColumn(name = "load_id", insertable = false, updatable = false),
			inverseJoinColumns = @JoinColumn(name = "user_id", insertable = false, updatable = false)
		)
	)
})
public class Load extends APIModel<Long> {

	private static final long serialVersionUID = 1L;
	
	@Type(type = "org.lnu.model.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",
					value = "org.lnu.model.enumtype.capability.Capability"
			)
	)
	@Formula("get_load_capability(:user.id, id, is_private)")
	@Enumerated(EnumType.STRING)
	private Capability capability;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "curriculum_semester_hours_id", insertable = false, updatable = false)
	private CurriculumSemesterHours curriculumSemesterHours;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "course_id", insertable = false, updatable = false)
	private Course course;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "department_id", insertable = false, updatable = false)
	private Department department;
	
	@OneToMany(mappedBy = "load", cascade = CascadeType.ALL)
	private List<AtomicLoad> atomicLoads;
	
	/**
	 * Default constructor with no parameters.
	 */
	public Load() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public Load(final Long id) {
		super(id);
	}
	
	@Override
	public Capability getCapability() {
		return capability;
	}

	@Override
	public void setCapability(final Capability capability) {
		this.capability = capability;
	}

	public CurriculumSemesterHours getCurriculumSemesterHours() {
		return curriculumSemesterHours;
	}

	public void setCurriculumSemesterHours(final CurriculumSemesterHours curriculumSemesterHours) {
		this.curriculumSemesterHours = curriculumSemesterHours;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(final Course course) {
		this.course = course;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(final Department department) {
		this.department = department;
	}

	public List<AtomicLoad> getAtomicLoads() {
		return atomicLoads;
	}

	public void setAtomicLoads(final List<AtomicLoad> atomicLoads) {
		this.atomicLoads = atomicLoads;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((atomicLoads == null) ? 0 : atomicLoads.hashCode());
		result = prime * result + ((capability == null) ? 0 : capability.hashCode());
		result = prime * result + ((course == null) ? 0 : course.hashCode());
		result = prime * result + ((curriculumSemesterHours == null) ? 0 : curriculumSemesterHours.hashCode());
		result = prime * result + ((department == null) ? 0 : department.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Load other = (Load) obj;
		if (atomicLoads == null) {
			if (other.atomicLoads != null) {
				return false;
			}
		} else if (!atomicLoads.equals(other.atomicLoads)) {
			return false;
		}
		if (capability != other.capability) {
			return false;
		}
		if (course == null) {
			if (other.course != null) {
				return false;
			}
		} else if (!course.equals(other.course)) {
			return false;
		}
		if (curriculumSemesterHours == null) {
			if (other.curriculumSemesterHours != null) {
				return false;
			}
		} else if (!curriculumSemesterHours.equals(other.curriculumSemesterHours)) {
			return false;
		}
		if (department == null) {
			if (other.department != null) {
				return false;
			}
		} else if (!department.equals(other.department)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Load [capability=");
		builder.append(capability);
		builder.append(", curriculumSemesterHours=");
		builder.append(curriculumSemesterHours);
		builder.append(", course=");
		builder.append(course);
		builder.append(", department=");
		builder.append(department);
		builder.append(", atomicLoads=");
		builder.append(atomicLoads);
		builder.append(", isPrivate=");
		builder.append(isPrivate);
		builder.append(", responsibleOwnUsers=");
		builder.append(responsibleOwnUsers);
		builder.append(", responsibleGroups=");
		builder.append(responsibleGroups);
		builder.append(", responsibleUsers=");
		builder.append(responsibleUsers);
		builder.append(", id=");
		builder.append(id);
		builder.append(", parentId=");
		builder.append(parentId);
		builder.append("]");
		return builder.toString();
	}

}
