package org.lnu.model.id.user.api;

import java.io.Serializable;

import org.lnu.model.enumtype.api.API;
import org.lnu.model.user.User;

/**
 * Primary key with values of User and Api.
 * @author OlehZanevych
 */
public class UserApiId implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private User user;
	
	private API api;
	
	/**
	 * Default constructor with no parameters.
	 */
	public UserApiId() {
		
	}

	/**
	 * Constructor with user and api.
	 * @param user user
	 * @param api api
	 */
	public UserApiId(final User user, final API api) {
		this.user = user;
		this.api = api;
	}

	public User getUser() {
		return user;
	}

	public void setUser(final User user) {
		this.user = user;
	}

	public API getApi() {
		return api;
	}

	public void setApi(final API api) {
		this.api = api;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((api == null) ? 0 : api.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		UserApiId other = (UserApiId) obj;
		if (api != other.api) {
			return false;
		}
		if (user == null) {
			if (other.user != null) {
				return false;
			}
		} else if (!user.equals(other.user)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserApiId [user=");
		builder.append(user);
		builder.append(", api=");
		builder.append(api);
		builder.append("]");
		return builder.toString();
	}

}
