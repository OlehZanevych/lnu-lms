package org.lnu.model.geometry;

import java.io.Serializable;

/**
 * Class, that describes 2D point.
 * @author OlehZanevych
 */
public class Point implements Serializable, Cloneable {
	
	private static final long serialVersionUID = 1L;

	protected double x;
	
	protected double y;
	
	/**
	 * Default constructor with no parameters.
	 */
	public Point() {
		
	}
	
	/**
	 * Constructor.
	 * @param x
	 * @param y
	 */
	public Point(final double x, final double y) {
		this.x = x;
		this.y = y;
	}
	
	public double getX() {
		return x;
	}

	public void setX(final double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(final double y) {
		this.y = y;
	}
	
	@Override
	public Point clone() {
		Point clonedPoint = null;
		try {
			clonedPoint = (Point) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new IllegalArgumentException(e.getMessage());
		}
		clonedPoint.x = this.x;
		clonedPoint.y = this.y;
		return clonedPoint;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Point other = (Point) obj;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x)) {
			return false;
		}
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Point [x=");
		builder.append(x);
		builder.append(", y=");
		builder.append(y);
		builder.append("]");
		return builder.toString();
	}

}
