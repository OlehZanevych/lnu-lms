package org.lnu.model.geometry;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Class, that describes 2D polygon.
 * @author OlehZanevych
 */
public class Polygon implements Serializable, Cloneable {
	
	private static final long serialVersionUID = 1L;
	
	protected Point[] points;

	/**
	 * Default constructor with no parameters.
	 */
	public Polygon() {
		
	}

	/**
	 * Constructor.
	 * @param points
	 */
	public Polygon(final Point[] points) {
		this.points = points;
	}
	
	public Point[] getPoints() {
		return points;
	}

	public void setPoints(final Point[] points) {
		this.points = points;
	}
	
	@Override
	public Polygon clone() {
		Polygon clonedPolygon = null;
		try {
			clonedPolygon = (Polygon) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new IllegalArgumentException(e.getMessage());
		}
		clonedPolygon.points = Arrays.stream(this.points).map(i -> i.clone())
				.toArray(Point[]::new);
		return clonedPolygon;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(points);
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Polygon other = (Polygon) obj;
		if (!Arrays.equals(points, other.points)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Polygon [points=");
		builder.append(Arrays.toString(points));
		builder.append("]");
		return builder.toString();
	}

}
