package org.lnu.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Base model with simple id.
 * @author OlehZanevych
 * 
 * @param <KEY> Identifier class, that extends Number.
 */
@MappedSuperclass
public abstract class ModelWithSimpleId<KEY extends Number> extends Model {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	protected KEY id;

	/**
	 * Default constructor with no parameters.
	 */
	public ModelWithSimpleId() {
		
	}

	/**
	 * Constructor with id.
	 * @param id id.
	 */
	public ModelWithSimpleId(final KEY id) {
		this.id = id;
	}

	public KEY getId() {
		return id;
	}

	public void setId(final KEY id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ModelWithSimpleId<?> other = (ModelWithSimpleId<?>) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ModelWithSimpleId [id=");
		builder.append(id);
		builder.append(", parentId=");
		builder.append(parentId);
		builder.append("]");
		return builder.toString();
	}

}
