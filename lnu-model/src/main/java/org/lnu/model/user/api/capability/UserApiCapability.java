package org.lnu.model.user.api.capability;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Formula;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.lnu.annotation.dbtable.System;
import org.lnu.model.Model;
import org.lnu.model.enumtype.api.API;
import org.lnu.model.enumtype.api.capability.level.APICapabilityLevel;
import org.lnu.model.enumtype.api.createcapability.APICreateCapability;
import org.lnu.model.id.user.api.UserApiId;
import org.lnu.model.user.User;

/**
 * Entity, that describes API capability of user.
 * @author OlehZanevych
 */
@System
@Entity
@Table(name = "v_system_user_api_capabilities")
@IdClass(UserApiId.class)
public class UserApiCapability extends Model {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;
	
	@Id
	@Type(type = "org.lnu.model.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",
					value = "org.lnu.model.enumtype.api.API"
			)
	)
	@Column(name = "api", columnDefinition = "e_api")
	@Enumerated(EnumType.STRING)
	private API api;
	
	@Type(type = "org.lnu.model.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",
					value = "org.lnu.model.enumtype.api.capability.level.APICapabilityLevel"
			)
	)
	@Column(name = "api_capability_level", columnDefinition = "e_api_capability_level")
	@Enumerated(EnumType.STRING)
	private APICapabilityLevel apiCapabilityLevel;
	
	@Type(type = "org.lnu.model.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",
					value = "org.lnu.model.enumtype.api.createcapability.APICreateCapability"
			)
	)
	@Formula("get_api_create_capability(api_capability_level)")
	@Enumerated(EnumType.STRING)
	private APICreateCapability apiCreateCapability;

	/**
	 * Default constructor with no parameters.
	 */
	public UserApiCapability() {
		
	}
	
	public UserApiId getId() {
		return new UserApiId(user, api);
	}

	public User getUser() {
		return user;
	}

	public void setUser(final User user) {
		this.user = user;
	}

	public API getApi() {
		return api;
	}

	public void setApi(final API api) {
		this.api = api;
	}

	public APICapabilityLevel getApiCapabilityLevel() {
		return apiCapabilityLevel;
	}

	public void setApiCapabilityLevel(final APICapabilityLevel apiCapabilityLevel) {
		this.apiCapabilityLevel = apiCapabilityLevel;
	}

	public APICreateCapability getApiCreateCapability() {
		return apiCreateCapability;
	}

	public void setApiCreateCapability(final APICreateCapability apiCreateCapability) {
		this.apiCreateCapability = apiCreateCapability;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((api == null) ? 0 : api.hashCode());
		result = prime * result + ((apiCapabilityLevel == null) ? 0 : apiCapabilityLevel.hashCode());
		result = prime * result + ((apiCreateCapability == null) ? 0 : apiCreateCapability.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		UserApiCapability other = (UserApiCapability) obj;
		if (api != other.api) {
			return false;
		}
		if (apiCapabilityLevel != other.apiCapabilityLevel) {
			return false;
		}
		if (apiCreateCapability != other.apiCreateCapability) {
			return false;
		}
		if (user == null) {
			if (other.user != null) {
				return false;
			}
		} else if (!user.equals(other.user)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserApiCapability [user=");
		builder.append(user);
		builder.append(", api=");
		builder.append(api);
		builder.append(", apiCapabilityLevel=");
		builder.append(apiCapabilityLevel);
		builder.append(", apiCreateCapability=");
		builder.append(apiCreateCapability);
		builder.append(", parentId=");
		builder.append(parentId);
		builder.append("]");
		return builder.toString();
	}

}
