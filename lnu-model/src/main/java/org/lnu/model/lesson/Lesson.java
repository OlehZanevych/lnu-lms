package org.lnu.model.lesson;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.lnu.annotation.dbtable.Essence;
import org.lnu.model.ModelWithSimpleId;
import org.lnu.model.atomicload.AtomicLoad;
import org.lnu.model.enumtype.day.Day;
import org.lnu.model.enumtype.weekperiodicity.WeekPeriodicity;
import org.lnu.model.learningplace.LearningPlace;

/**
 * Entity, that describes lesson.
 * @author OlehZanevych
 */
@Essence
@Entity
@Table(name = "t_essence_lessons")
public class Lesson extends ModelWithSimpleId<Long> {
	
	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name = "atomic_load_id")
	private AtomicLoad atomicLoad;
	
	@Type(type = "org.lnu.model.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",
					value = "org.lnu.model.enumtype.day.Day"
			)
	)
	@Column(name = "day", columnDefinition = "e_day")
	@Enumerated(EnumType.STRING)
	private Day day;
	
	@Column(name = "hour")
	private Byte hour;
	
	@Column(name = "minute")
	private Byte minute;
	
	@Column(name = "duration")
	private Byte duration;
	
	@Type(type = "org.lnu.model.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",	
					value = "org.lnu.model.enumtype.weekperiodicity.WeekPeriodicity"
			)
	)
	@Column(name = "week_periodicity", columnDefinition = "e_week_periodicity")
	@Enumerated(EnumType.STRING)
	private WeekPeriodicity weekPeriodicity;
	
	@ManyToMany
    @JoinTable(name = "t_relationship_lessons_places",
		joinColumns = @JoinColumn(name = "lesson_id"),
		inverseJoinColumns = @JoinColumn(name = "place_id"))
	private List<LearningPlace> places;
	
	/**
	 * Default constructor with no parameters.
	 */
	public Lesson() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public Lesson(final Long id) {
		super(id);
	}

	public AtomicLoad getAtomicLoad() {
		return atomicLoad;
	}

	public void setAtomicLoad(final AtomicLoad atomicLoad) {
		this.atomicLoad = atomicLoad;
	}

	public Day getDay() {
		return day;
	}

	public void setDay(final Day day) {
		this.day = day;
	}

	public Byte getHour() {
		return hour;
	}

	public void setHour(final Byte hour) {
		this.hour = hour;
	}

	public Byte getMinute() {
		return minute;
	}

	public void setMinute(final Byte minute) {
		this.minute = minute;
	}

	public Byte getDuration() {
		return duration;
	}

	public void setDuration(final Byte duration) {
		this.duration = duration;
	}

	public WeekPeriodicity getWeekPeriodicity() {
		return weekPeriodicity;
	}

	public void setWeekPeriodicity(final WeekPeriodicity weekPeriodicity) {
		this.weekPeriodicity = weekPeriodicity;
	}

	public List<LearningPlace> getPlaces() {
		return places;
	}

	public void setPlaces(final List<LearningPlace> places) {
		this.places = places;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((atomicLoad == null) ? 0 : atomicLoad.hashCode());
		result = prime * result + ((day == null) ? 0 : day.hashCode());
		result = prime * result + ((duration == null) ? 0 : duration.hashCode());
		result = prime * result + ((hour == null) ? 0 : hour.hashCode());
		result = prime * result + ((minute == null) ? 0 : minute.hashCode());
		result = prime * result + ((places == null) ? 0 : places.hashCode());
		result = prime * result + ((weekPeriodicity == null) ? 0 : weekPeriodicity.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Lesson other = (Lesson) obj;
		if (atomicLoad == null) {
			if (other.atomicLoad != null) {
				return false;
			}
		} else if (!atomicLoad.equals(other.atomicLoad)) {
			return false;
		}
		if (day != other.day) {
			return false;
		}
		if (duration == null) {
			if (other.duration != null) {
				return false;
			}
		} else if (!duration.equals(other.duration)) {
			return false;
		}
		if (hour == null) {
			if (other.hour != null) {
				return false;
			}
		} else if (!hour.equals(other.hour)) {
			return false;
		}
		if (minute == null) {
			if (other.minute != null) {
				return false;
			}
		} else if (!minute.equals(other.minute)) {
			return false;
		}
		if (places == null) {
			if (other.places != null) {
				return false;
			}
		} else if (!places.equals(other.places)) {
			return false;
		}
		if (weekPeriodicity != other.weekPeriodicity) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Lesson [atomicLoad=");
		builder.append(atomicLoad);
		builder.append(", day=");
		builder.append(day);
		builder.append(", hour=");
		builder.append(hour);
		builder.append(", minute=");
		builder.append(minute);
		builder.append(", duration=");
		builder.append(duration);
		builder.append(", weekPeriodicity=");
		builder.append(weekPeriodicity);
		builder.append(", places=");
		builder.append(places);
		builder.append(", id=");
		builder.append(id);
		builder.append(", parentId=");
		builder.append(parentId);
		builder.append("]");
		return builder.toString();
	}
	
}
