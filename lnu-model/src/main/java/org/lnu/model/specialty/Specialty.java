package org.lnu.model.specialty;

import java.util.List;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Formula;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.lnu.annotation.dbtable.Essence;
import org.lnu.model.APIModel;
import org.lnu.model.department.Department;
import org.lnu.model.enumtype.capability.Capability;
import org.lnu.model.enumtype.educationaldegree.EducationalDegree;
import org.lnu.model.unit.Unit;

/**
 * Entity, that describes specialty.
 * @author OlehZanevych
 */
@Essence
@Entity
@Table(name = "t_essence_specialties")
@AssociationOverrides({
	@AssociationOverride(
		name = "responsibleOwnUsers",
		joinTable = @JoinTable(name = "t_relationship_specialties_responsible_own_users",
			joinColumns = @JoinColumn(name = "specialty_id"),
			inverseJoinColumns = @JoinColumn(name = "user_id")
		)
	),
	@AssociationOverride(
		name = "responsibleGroups",
		joinTable = @JoinTable(name = "t_relationship_specialties_responsible_groups",
			joinColumns = @JoinColumn(name = "specialty_id"),
			inverseJoinColumns = @JoinColumn(name = "group_id")
		)
	),
	@AssociationOverride(
		name = "responsibleUsers",
		joinTable = @JoinTable(name = "v_relationship_specialties_responsible_users",
			joinColumns = @JoinColumn(name = "specialty_id", insertable = false, updatable = false),
			inverseJoinColumns = @JoinColumn(name = "user_id", insertable = false, updatable = false)
		)
	)
})
public final class Specialty extends APIModel<Long> {
	
	private static final long serialVersionUID = 1L;
	
	@Type(type = "org.lnu.model.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",
					value = "org.lnu.model.enumtype.capability.Capability"
			)
	)
	@Formula("get_specialty_capability(:user.id, id, is_private)")
	@Enumerated(EnumType.STRING)
	private Capability capability;
	
	@NotNull
	@Column(name = "name")
	private String name;
	
	@NotNull
	@Column(name = "code", unique = true)
	private String code;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "unit_id")
	private Unit unit;
	
	@ManyToMany
    @JoinTable(name = "t_relationship_departments_specialties",
		joinColumns = @JoinColumn(name = "specialty_id"),
		inverseJoinColumns = @JoinColumn(name = "department_id"))
    private List<Department> departments;

	@NotNull
	@Type(type = "org.lnu.model.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",
					value = "org.lnu.model.enumtype.educationaldegree.EducationalDegree"
			)
	)
	@Column(name = "educational_degree", columnDefinition = "e_educational_degree")
	@Enumerated(EnumType.STRING)
	private EducationalDegree educationalDegree;
	
	/**
	 * Default constructor with no parameters.
	 */
	public Specialty() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public Specialty(final Long id) {
		super(id);
	}

	@Override
	public Capability getCapability() {
		return capability;
	}

	@Override
	public void setCapability(final Capability capability) {
		this.capability = capability;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(final String code) {
		this.code = code;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(final Unit unit) {
		this.unit = unit;
	}

	public List<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(final List<Department> departments) {
		this.departments = departments;
	}

	public EducationalDegree getEducationalDegree() {
		return educationalDegree;
	}

	public void setEducationalDegree(final EducationalDegree educationalDegree) {
		this.educationalDegree = educationalDegree;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((capability == null) ? 0 : capability.hashCode());
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((departments == null) ? 0 : departments.hashCode());
		result = prime * result + ((educationalDegree == null) ? 0 : educationalDegree.hashCode());
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Specialty other = (Specialty) obj;
		if (capability != other.capability) {
			return false;
		}
		if (code == null) {
			if (other.code != null) {
				return false;
			}
		} else if (!code.equals(other.code)) {
			return false;
		}
		if (departments == null) {
			if (other.departments != null) {
				return false;
			}
		} else if (!departments.equals(other.departments)) {
			return false;
		}
		if (educationalDegree != other.educationalDegree) {
			return false;
		}
		if (unit == null) {
			if (other.unit != null) {
				return false;
			}
		} else if (!unit.equals(other.unit)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Specialty [capability=");
		builder.append(capability);
		builder.append(", name=");
		builder.append(name);
		builder.append(", code=");
		builder.append(code);
		builder.append(", unit=");
		builder.append(unit);
		builder.append(", departments=");
		builder.append(departments);
		builder.append(", educationalDegree=");
		builder.append(educationalDegree);
		builder.append(", isPrivate=");
		builder.append(isPrivate);
		builder.append(", responsibleOwnUsers=");
		builder.append(responsibleOwnUsers);
		builder.append(", responsibleGroups=");
		builder.append(responsibleGroups);
		builder.append(", responsibleUsers=");
		builder.append(responsibleUsers);
		builder.append(", id=");
		builder.append(id);
		builder.append(", parentId=");
		builder.append(parentId);
		builder.append("]");
		return builder.toString();
	}

}
