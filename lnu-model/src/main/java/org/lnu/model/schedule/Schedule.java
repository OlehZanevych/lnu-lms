package org.lnu.model.schedule;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.lnu.annotation.dbtable.Essence;
import org.lnu.model.ModelWithSimpleId;
import org.lnu.model.academicgroup.AcademicGroup;
import org.lnu.model.course.Course;
import org.lnu.model.enumtype.classtype.ClassType;
import org.lnu.model.lecturer.Lecturer;
import org.lnu.model.lesson.Lesson;

/**
 * Entity, that describes schedule.
 * @author OlehZanevych
 */
@Essence
@Entity
@Table(name = "v_essence_schedule")
public class Schedule extends ModelWithSimpleId<Long> {
	
	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name = "course_id", insertable = false, updatable = false)
	private Course course;
	
	@Type(type = "org.lnu.model.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",
					value = "org.lnu.model.enumtype.classtype.ClassType"
			)
	)
	@Column(name = "class_type", columnDefinition = "e_class_type", insertable = false, updatable = false)
	@Enumerated(EnumType.STRING)
	private ClassType classType;
	
	@Column(name = "hours", insertable = false, updatable = false)
	private Short hours;
	
	@ManyToOne
	@JoinColumn(name = "academic_group_id", insertable = false, updatable = false)
	private AcademicGroup academicGroup;
	
	@Column(name = "semester", insertable = false, updatable = false)
	private Byte semester;
	
	@ManyToMany
    @JoinTable(name = "t_relationship_lecturers_atomic_load",
		joinColumns = @JoinColumn(name = "atomic_load_id"),
		inverseJoinColumns = @JoinColumn(name = "lecturer_id"))
	private List<Lecturer> lecturers;
	
	@OneToMany(mappedBy = "atomicLoad")
	private List<Lesson> lessons;
	
	/**
	 * Default constructor with no parameters.
	 */
	public Schedule() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public Schedule(final Long id) {
		super(id);
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(final Course course) {
		this.course = course;
	}

	public ClassType getClassType() {
		return classType;
	}

	public void setClassType(final ClassType classType) {
		this.classType = classType;
	}

	public Short getHours() {
		return hours;
	}

	public void setHours(final Short hours) {
		this.hours = hours;
	}

	public AcademicGroup getAcademicGroup() {
		return academicGroup;
	}

	public void setAcademicGroup(final AcademicGroup academicGroup) {
		this.academicGroup = academicGroup;
	}

	public Byte getSemester() {
		return semester;
	}

	public void setSemester(final Byte semester) {
		this.semester = semester;
	}

	public List<Lecturer> getLecturers() {
		return lecturers;
	}

	public void setLecturers(final List<Lecturer> lecturers) {
		this.lecturers = lecturers;
	}

	public List<Lesson> getLessons() {
		return lessons;
	}

	public void setLessons(final List<Lesson> lessons) {
		this.lessons = lessons;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((academicGroup == null) ? 0 : academicGroup.hashCode());
		result = prime * result + ((classType == null) ? 0 : classType.hashCode());
		result = prime * result + ((course == null) ? 0 : course.hashCode());
		result = prime * result + ((hours == null) ? 0 : hours.hashCode());
		result = prime * result + ((lecturers == null) ? 0 : lecturers.hashCode());
		result = prime * result + ((lessons == null) ? 0 : lessons.hashCode());
		result = prime * result + ((semester == null) ? 0 : semester.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Schedule other = (Schedule) obj;
		if (academicGroup == null) {
			if (other.academicGroup != null) {
				return false;
			}
		} else if (!academicGroup.equals(other.academicGroup)) {
			return false;
		}
		if (classType != other.classType) {
			return false;
		}
		if (course == null) {
			if (other.course != null) {
				return false;
			}
		} else if (!course.equals(other.course)) {
			return false;
		}
		if (hours == null) {
			if (other.hours != null) {
				return false;
			}
		} else if (!hours.equals(other.hours)) {
			return false;
		}
		if (lecturers == null) {
			if (other.lecturers != null) {
				return false;
			}
		} else if (!lecturers.equals(other.lecturers)) {
			return false;
		}
		if (lessons == null) {
			if (other.lessons != null) {
				return false;
			}
		} else if (!lessons.equals(other.lessons)) {
			return false;
		}
		if (semester == null) {
			if (other.semester != null) {
				return false;
			}
		} else if (!semester.equals(other.semester)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Schedule [course=");
		builder.append(course);
		builder.append(", classType=");
		builder.append(classType);
		builder.append(", hours=");
		builder.append(hours);
		builder.append(", academicGroup=");
		builder.append(academicGroup);
		builder.append(", semester=");
		builder.append(semester);
		builder.append(", lecturers=");
		builder.append(lecturers);
		builder.append(", lessons=");
		builder.append(lessons);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}
	
}
