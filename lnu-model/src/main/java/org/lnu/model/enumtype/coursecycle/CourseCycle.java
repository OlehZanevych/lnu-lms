package org.lnu.model.enumtype.coursecycle;

/**
 * Enum, that describes course cycle.
 * @author OlehZanevych
 */
public enum CourseCycle {
	NULL,
	PROFESSIONAL_AND_PRACTICAL,
	HUMANITARIAN_AND_SOCIOECONOMIC,
	MATHEMATICAL_AND_NATURALSCIENCE,
	PROFESSIONALLYORIENTED_HUMANITARIAN_AND_SOCIOECONOMIC
}
