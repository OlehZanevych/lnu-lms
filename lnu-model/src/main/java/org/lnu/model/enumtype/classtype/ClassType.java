package org.lnu.model.enumtype.classtype;

/**
 * Enum, that describes class type.
 * @author OlehZanevych
 */
public enum ClassType {
	NULL,
	LECTURE,
	PRACTICAL,
	LABORATORY
}
