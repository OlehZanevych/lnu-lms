package org.lnu.model.enumtype.toilettype;

/**
 * Enum, that describes toilet type.
 * @author OlehZanevych
 */
public enum ToiletType {
	NULL,
	MAN,
	WOMAN,
	GENERAL
}
