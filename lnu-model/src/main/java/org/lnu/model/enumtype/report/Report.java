package org.lnu.model.enumtype.report;

/**
 * Enum, that describes report.
 * @author OlehZanevych
 */
public enum Report {
	NULL,
	ELECTIVE,
	SETOFF,
	DIFFERENTIATED_SETOFF,
	EXAM,
	COURSE_PROJECT,
	COURSEWORK
}
