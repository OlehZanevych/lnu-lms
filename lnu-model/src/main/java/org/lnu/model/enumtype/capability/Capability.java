package org.lnu.model.enumtype.capability;

/**
 * Enum for storing access capability levels.
 * @author OlehZanevych
 */
public enum Capability {
	READ,
	MODIFY_CONTENT,
	MODIFY_ALL
}
