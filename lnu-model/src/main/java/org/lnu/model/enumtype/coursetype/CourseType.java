package org.lnu.model.enumtype.coursetype;

/**
 * Enum, that describes course type.
 * @author OlehZanevych
 */
public enum CourseType {
	NORMATIVE,
	CHOICE
}
