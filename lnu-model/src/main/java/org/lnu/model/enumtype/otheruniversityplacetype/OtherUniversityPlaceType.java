package org.lnu.model.enumtype.otheruniversityplacetype;

/**
 * Enum, that describes other University place type.
 * @author OlehZanevych
 */
public enum OtherUniversityPlaceType {
	NULL,
	LIBRARY,
	BOOKSTORE,
	STATIONERY_SHOP,
	ASSEMBLY_HALL,
	SHOWER,
	KITCHEN
}
