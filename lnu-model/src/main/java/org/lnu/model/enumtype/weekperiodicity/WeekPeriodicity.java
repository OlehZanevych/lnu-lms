package org.lnu.model.enumtype.weekperiodicity;

/**
 * Enum, that describes week periodicity.
 * @author OlehZanevych
 */
public enum WeekPeriodicity {
	ALWAYS,
	NUMERATOR,
	DENOMINATOR,
	NUMERATOR_OF_NUMERATOR,
	DENOMINATOR_OF_NUMERATOR,
	NUMERATOR_OF_DENOMINATOR,
	DENOMINATOR_OF_DENOMINATOR
}
