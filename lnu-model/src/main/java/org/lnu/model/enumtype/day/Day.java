package org.lnu.model.enumtype.day;

/**
 * Enum, that describes day.
 * @author OlehZanevych
 */
public enum Day {
	SUNDAY,
	MONDAY,
	TUESDAY,
	WEDNESDAY,
	THURSDAY,
	FRIDAY,
	SATURDAY
}
