package org.lnu.model.enumtype.academicstatus;

/**
 * Enum, that describes academic degree.
 * @author OlehZanevych
 */
public enum AcademicStatus {
	NULL,
	SENIOR_RESEARCHER,
	ASSOCIATE_PROFESSOR,
	PROFESSOR
}
