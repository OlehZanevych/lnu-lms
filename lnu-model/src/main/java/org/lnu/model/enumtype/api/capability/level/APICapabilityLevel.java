package org.lnu.model.enumtype.api.capability.level;

/**
 * Enum for storing API capability levels.
 * @author OlehZanevych
 */
public enum APICapabilityLevel {
	NULL,
	MODIFY_OWN_CONTENT,
	MODIFY_OWN_AND_PUBLIC_CONTENT,
	MODIFY_PUBLIC_CONTENT_AND_ALL_OWN,
	MODIFY_ALL_OWN_AND_PUBLIC,
	MODIFY_CONTENT_AND_ALL_OWN_AND_PUBLIC,
	MODIFY_ALL
}
