package org.lnu.model.enumtype.learningplacetype;

/**
 * Enum, that describes Learning Place type.
 * @author OlehZanevych
 */
public enum LearningPlaceType {
	NULL,
	SIMPLE_AUDIENCE,
	COMPUTER_LAB,
	SPORT_PLACE
}
