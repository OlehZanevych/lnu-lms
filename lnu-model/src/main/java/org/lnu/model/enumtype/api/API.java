package org.lnu.model.enumtype.api;

/**
 * Enum for storing API names.
 * @author OlehZanevych
 */
public enum API {
	NULL,
	academicgroups,
	apicapabilities,
	buildings,
	buildingmaps,
	courses,
	curricula,
	departments,
	dormitories,
	units,
	groups,
	learningplacegroups,
	learningplaces,
	lecturers,
	load,
	otheruniversityplaces,
	roles,
	schedule,
	scientificdegrees,
	specialties,
	students,
	toilets,
	users
}
