package org.lnu.model.enumtype.educationaldegree;

/**
 * Enum, that describes educational degree.
 * @author OlehZanevych
 */
public enum EducationalDegree {
	NULL,
	BACHELOR,
	SPECIALIST,
	MASTER
}
