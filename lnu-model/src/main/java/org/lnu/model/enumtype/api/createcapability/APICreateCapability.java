package org.lnu.model.enumtype.api.createcapability;

/**
 * Enum for storing API create capability levels.
 * @author OlehZanevych
 */
public enum APICreateCapability {
	CONTENT,
	ALL
}
