package org.lnu.model.enumtype.unit.type;

/**
 * Enum, that describes Unit type.
 * @author OlehZanevych
 */
public enum UnitType {
	NULL,
	COLLEGE,
	FACULTY
}
