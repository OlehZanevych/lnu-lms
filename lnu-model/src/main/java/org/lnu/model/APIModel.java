package org.lnu.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.ManyToMany;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;

import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.lnu.model.enumtype.capability.Capability;
import org.lnu.model.group.Group;
import org.lnu.model.user.User;

/**
 * Base model for API methods.
 * @author OlehZanevych
 * 
 * @param <KEY> Identifier class, that extends Number.
 */
@MappedSuperclass
@FilterDef(
		name = "user",
		parameters = @ParamDef(name = "id", type = "long")
)
public abstract class APIModel<KEY extends Number> extends ModelWithSimpleId<KEY> {

	private static final long serialVersionUID = 1L;
	
	@Column(name = "is_private")
	protected Boolean isPrivate;
	
	@ManyToMany
	protected List<User> responsibleOwnUsers;
	
	@ManyToMany
    protected List<Group> responsibleGroups;
	
	@ManyToMany
	protected List<User> responsibleUsers;
	
	/**
	 * Default constructor with no parameters.
	 */
	public APIModel() {
		
	}

	/**
	 * Constructor with id.
	 * @param id id.
	 */
	public APIModel(final KEY id) {
		super(id);
	}
	
	/**
	 * Method to initialize default values.
	 */
	@PrePersist
	private void initializeDefaultValues() {
		if (isPrivate == null) {
			isPrivate = false;
		}
	}

	/**
	 * Method for getting access capability.
	 * @return access capability
	 */
	public abstract Capability getCapability();
	
	/**
	 * Method for setting access capability.
	 * @param capability access capability
	 */
	public abstract void setCapability(final Capability capability);

	public Boolean getIsPrivate() {
		return isPrivate;
	}

	public void setIsPrivate(final Boolean isPrivate) {
		this.isPrivate = isPrivate;
	}

	public List<User> getResponsibleOwnUsers() {
		return responsibleOwnUsers;
	}

	public void setResponsibleOwnUsers(final List<User> responsibleOwnUsers) {
		this.responsibleOwnUsers = responsibleOwnUsers;
	}

	public List<Group> getResponsibleGroups() {
		return responsibleGroups;
	}

	public void setResponsibleGroups(final List<Group> responsibleGroups) {
		this.responsibleGroups = responsibleGroups;
	}

	public List<User> getResponsibleUsers() {
		return responsibleUsers;
	}

	public void setResponsibleUsers(final List<User> responsibleUsers) {
		this.responsibleUsers = responsibleUsers;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((isPrivate == null) ? 0 : isPrivate.hashCode());
		result = prime * result + ((responsibleGroups == null) ? 0 : responsibleGroups.hashCode());
		result = prime * result + ((responsibleOwnUsers == null) ? 0 : responsibleOwnUsers.hashCode());
		result = prime * result + ((responsibleUsers == null) ? 0 : responsibleUsers.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		APIModel<?> other = (APIModel<?>) obj;
		if (isPrivate == null) {
			if (other.isPrivate != null) {
				return false;
			}
		} else if (!isPrivate.equals(other.isPrivate)) {
			return false;
		}
		if (responsibleGroups == null) {
			if (other.responsibleGroups != null) {
				return false;
			}
		} else if (!responsibleGroups.equals(other.responsibleGroups)) {
			return false;
		}
		if (responsibleOwnUsers == null) {
			if (other.responsibleOwnUsers != null) {
				return false;
			}
		} else if (!responsibleOwnUsers.equals(other.responsibleOwnUsers)) {
			return false;
		}
		if (responsibleUsers == null) {
			if (other.responsibleUsers != null) {
				return false;
			}
		} else if (!responsibleUsers.equals(other.responsibleUsers)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("APIModel [isPrivate=");
		builder.append(isPrivate);
		builder.append(", responsibleOwnUsers=");
		builder.append(responsibleOwnUsers);
		builder.append(", responsibleGroups=");
		builder.append(responsibleGroups);
		builder.append(", responsibleUsers=");
		builder.append(responsibleUsers);
		builder.append("]");
		return builder.toString();
	}
}
