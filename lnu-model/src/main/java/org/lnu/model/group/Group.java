package org.lnu.model.group;

import java.util.List;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Formula;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.lnu.annotation.dbtable.System;
import org.lnu.model.APIModel;
import org.lnu.model.enumtype.capability.Capability;
import org.lnu.model.user.User;

/**
 * Group table.
 * @author OlehZanevych
 */
@System
@Entity
@Table(name = "t_system_groups")
@AssociationOverrides({
	@AssociationOverride(
		name = "responsibleOwnUsers",
		joinTable = @JoinTable(name = "t_relationship_groups_responsible_own_users",
			joinColumns = @JoinColumn(name = "group_id"),
			inverseJoinColumns = @JoinColumn(name = "user_id")
		)
	),
	@AssociationOverride(
		name = "responsibleGroups",
		joinTable = @JoinTable(name = "t_relationship_groups_responsible_groups",
			joinColumns = @JoinColumn(name = "group_id"),
			inverseJoinColumns = @JoinColumn(name = "responsible_group_id")
		)
	),
	@AssociationOverride(
		name = "responsibleUsers",
		joinTable = @JoinTable(name = "v_relationship_groups_responsible_users",
			joinColumns = @JoinColumn(name = "group_id", insertable = false, updatable = false),
			inverseJoinColumns = @JoinColumn(name = "user_id", insertable = false, updatable = false)
		)
	)
})
public final class Group extends APIModel<Long> {
	
	private static final long serialVersionUID = 1L;
	
	@Type(type = "org.lnu.model.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",
					value = "org.lnu.model.enumtype.capability.Capability"
			)
	)
	@Formula("get_group_capability(:user.id, id, is_private)")
	@Enumerated(EnumType.STRING)
	private Capability capability;

	@NotNull
    @Column(name = "name", unique = true)
    private String name;
    
	@NotNull
    @Column(name = "abbreviation", unique = true)
    private String abbreviation;
    
	@Column(name = "description")
    private String description;
    
    @ManyToMany
    @JoinTable(name = "t_relationship_groups_users",
		joinColumns = @JoinColumn(name = "group_id"),
		inverseJoinColumns = @JoinColumn(name = "user_id"))
	private List<User> users;
    
    /**
	 * Default constructor with no parameters.
	 */
	public Group() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public Group(final Long id) {
		super(id);
	}
	
	@Override
	public Capability getCapability() {
		return capability;
	}

	@Override
	public void setCapability(final Capability capability) {
		this.capability = capability;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(final String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(final List<User> users) {
		this.users = users;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((abbreviation == null) ? 0 : abbreviation.hashCode());
		result = prime * result + ((capability == null) ? 0 : capability.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((users == null) ? 0 : users.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Group other = (Group) obj;
		if (abbreviation == null) {
			if (other.abbreviation != null) {
				return false;
			}
		} else if (!abbreviation.equals(other.abbreviation)) {
			return false;
		}
		if (capability != other.capability) {
			return false;
		}
		if (description == null) {
			if (other.description != null) {
				return false;
			}
		} else if (!description.equals(other.description)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (users == null) {
			if (other.users != null) {
				return false;
			}
		} else if (!users.equals(other.users)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Group [capability=");
		builder.append(capability);
		builder.append(", name=");
		builder.append(name);
		builder.append(", abbreviation=");
		builder.append(abbreviation);
		builder.append(", description=");
		builder.append(description);
		builder.append(", users=");
		builder.append(users);
		builder.append(", isPrivate=");
		builder.append(isPrivate);
		builder.append(", responsibleOwnUsers=");
		builder.append(responsibleOwnUsers);
		builder.append(", responsibleGroups=");
		builder.append(responsibleGroups);
		builder.append(", responsibleUsers=");
		builder.append(responsibleUsers);
		builder.append(", id=");
		builder.append(id);
		builder.append(", parentId=");
		builder.append(parentId);
		builder.append("]");
		return builder.toString();
	}
	
}
