package org.lnu.model.otheruniversityplace;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Formula;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.lnu.annotation.dbtable.Essence;
import org.lnu.model.APIModel;
import org.lnu.model.building.map.BuildingMap;
import org.lnu.model.enumtype.capability.Capability;
import org.lnu.model.enumtype.otheruniversityplacetype.OtherUniversityPlaceType;

/**
 * Entity, that describes other University place.
 * @author OlehZanevych
 */
@Essence
@Entity
@Table(name = "t_essence_other_university_places")
@AssociationOverrides({
	@AssociationOverride(
		name = "responsibleOwnUsers",
		joinTable = @JoinTable(name = "t_relationship_other_university_places_responsible_own_users",
			joinColumns = @JoinColumn(name = "other_university_place_id"),
			inverseJoinColumns = @JoinColumn(name = "user_id")
		)
	),
	@AssociationOverride(
		name = "responsibleGroups",
		joinTable = @JoinTable(name = "t_relationship_other_university_places_responsible_groups",
			joinColumns = @JoinColumn(name = "other_university_place_id"),
			inverseJoinColumns = @JoinColumn(name = "group_id")
		)
	),
	@AssociationOverride(
		name = "responsibleUsers",
		joinTable = @JoinTable(name = "v_relationship_other_university_places_responsible_users",
			joinColumns = @JoinColumn(name = "other_university_place_id", insertable = false, updatable = false),
			inverseJoinColumns = @JoinColumn(name = "user_id", insertable = false, updatable = false)
		)
	)
})
public final class OtherUniversityPlace extends APIModel<Long> {
	
	private static final long serialVersionUID = 1L;
	
	@Type(type = "org.lnu.model.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",
					value = "org.lnu.model.enumtype.capability.Capability"
			)
	)
	@Formula("get_other_university_place_capability(:user.id, id, is_private)")
	@Enumerated(EnumType.STRING)
	private Capability capability;
	
	@NotNull
	@Column(name = "name")
	private String name;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "building_map_id")
	private BuildingMap buildingMap;
	
	@NotNull
	@Column(name = "on_map_id")
	private Short onMapId;
	
	@NotNull
	@Type(type = "org.lnu.model.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",	
					value = "org.lnu.model.enumtype.otheruniversityplacetype.OtherUniversityPlaceType"
			)
	)
	@Column(name = "other_university_place_type", columnDefinition = "e_other_university_place_type")
	@Enumerated(EnumType.STRING)
	private OtherUniversityPlaceType otherUniversityPlaceType;
	
	@Column(name = "square")
	private Float square;
	
	@Column(name = "rooms_count")
	private Byte roomsCount;
	
	@Column(name = "info")
	private String info;

	/**
	 * Default constructor with no parameters.
	 */
	public OtherUniversityPlace() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public OtherUniversityPlace(final Long id) {
		super(id);
	}
	
	@Override
	public Capability getCapability() {
		return capability;
	}

	@Override
	public void setCapability(final Capability capability) {
		this.capability = capability;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public BuildingMap getBuildingMap() {
		return buildingMap;
	}

	public void setBuildingMap(final BuildingMap buildingMap) {
		this.buildingMap = buildingMap;
	}

	public Short getOnMapId() {
		return onMapId;
	}

	public void setOnMapId(final Short onMapId) {
		this.onMapId = onMapId;
	}

	public OtherUniversityPlaceType getOtherUniversityPlaceType() {
		return otherUniversityPlaceType;
	}

	public void setOtherUniversityPlaceType(final OtherUniversityPlaceType otherUniversityPlaceType) {
		this.otherUniversityPlaceType = otherUniversityPlaceType;
	}

	public Float getSquare() {
		return square;
	}

	public void setSquare(final Float square) {
		this.square = square;
	}

	public Byte getRoomsCount() {
		return roomsCount;
	}

	public void setRoomsCount(final Byte roomsCount) {
		this.roomsCount = roomsCount;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(final String info) {
		this.info = info;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((buildingMap == null) ? 0 : buildingMap.hashCode());
		result = prime * result + ((capability == null) ? 0 : capability.hashCode());
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((onMapId == null) ? 0 : onMapId.hashCode());
		result = prime * result + ((otherUniversityPlaceType == null) ? 0 : otherUniversityPlaceType.hashCode());
		result = prime * result + ((roomsCount == null) ? 0 : roomsCount.hashCode());
		result = prime * result + ((square == null) ? 0 : square.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		OtherUniversityPlace other = (OtherUniversityPlace) obj;
		if (buildingMap == null) {
			if (other.buildingMap != null) {
				return false;
			}
		} else if (!buildingMap.equals(other.buildingMap)) {
			return false;
		}
		if (capability != other.capability) {
			return false;
		}
		if (info == null) {
			if (other.info != null) {
				return false;
			}
		} else if (!info.equals(other.info)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (onMapId == null) {
			if (other.onMapId != null) {
				return false;
			}
		} else if (!onMapId.equals(other.onMapId)) {
			return false;
		}
		if (otherUniversityPlaceType != other.otherUniversityPlaceType) {
			return false;
		}
		if (roomsCount == null) {
			if (other.roomsCount != null) {
				return false;
			}
		} else if (!roomsCount.equals(other.roomsCount)) {
			return false;
		}
		if (square == null) {
			if (other.square != null) {
				return false;
			}
		} else if (!square.equals(other.square)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OtherUniversityPlace [capability=");
		builder.append(capability);
		builder.append(", name=");
		builder.append(name);
		builder.append(", buildingMap=");
		builder.append(buildingMap);
		builder.append(", onMapId=");
		builder.append(onMapId);
		builder.append(", otherUniversityPlaceType=");
		builder.append(otherUniversityPlaceType);
		builder.append(", square=");
		builder.append(square);
		builder.append(", roomsCount=");
		builder.append(roomsCount);
		builder.append(", info=");
		builder.append(info);
		builder.append(", isPrivate=");
		builder.append(isPrivate);
		builder.append(", responsibleOwnUsers=");
		builder.append(responsibleOwnUsers);
		builder.append(", responsibleGroups=");
		builder.append(responsibleGroups);
		builder.append(", responsibleUsers=");
		builder.append(responsibleUsers);
		builder.append(", id=");
		builder.append(id);
		builder.append(", parentId=");
		builder.append(parentId);
		builder.append("]");
		return builder.toString();
	}
	
}
