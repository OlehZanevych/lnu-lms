package org.lnu.model.toilet;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Formula;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.lnu.annotation.dbtable.Essence;
import org.lnu.model.APIModel;
import org.lnu.model.building.map.BuildingMap;
import org.lnu.model.enumtype.capability.Capability;
import org.lnu.model.enumtype.toilettype.ToiletType;

/**
 * Entity, that describes toilet table.
 * @author OlehZanevych
 */
@Essence
@Entity
@Table(name = "t_essence_toilets")
@AssociationOverrides({
	@AssociationOverride(
		name = "responsibleOwnUsers",
		joinTable = @JoinTable(name = "t_relationship_toilets_responsible_own_users",
			joinColumns = @JoinColumn(name = "toilet_id"),
			inverseJoinColumns = @JoinColumn(name = "user_id")
		)
	),
	@AssociationOverride(
		name = "responsibleGroups",
		joinTable = @JoinTable(name = "t_relationship_toilets_responsible_groups",
			joinColumns = @JoinColumn(name = "toilet_id"),
			inverseJoinColumns = @JoinColumn(name = "group_id")
		)
	),
	@AssociationOverride(
		name = "responsibleUsers",
		joinTable = @JoinTable(name = "v_relationship_toilets_responsible_users",
			joinColumns = @JoinColumn(name = "toilet_id", insertable = false, updatable = false),
			inverseJoinColumns = @JoinColumn(name = "user_id", insertable = false, updatable = false)
		)
	)
})
public final class Toilet extends APIModel<Long> {
	
	private static final long serialVersionUID = 1L;
	
	@Type(type = "org.lnu.model.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",
					value = "org.lnu.model.enumtype.capability.Capability"
			)
	)
	@Formula("get_toilet_capability(:user.id, id, is_private)")
	@Enumerated(EnumType.STRING)
	private Capability capability;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "building_map_id")
	private BuildingMap buildingMap;
	
	@NotNull
	@Column(name = "on_map_id")
	private Short onMapId;
	
	@NotNull
	@Type(type = "org.lnu.model.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",	
					value = "org.lnu.model.enumtype.toilettype.ToiletType"
			)
	)
	@Column(name = "toilet_type", columnDefinition = "e_toilet_type")
	@Enumerated(EnumType.STRING)
	private ToiletType toiletType;
	
	@Column(name = "info")
	private String info;
	
	/**
	 * Default constructor with no parameters.
	 */
	public Toilet() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public Toilet(final Long id) {
		super(id);
	}
	
	@Override
	public Capability getCapability() {
		return capability;
	}

	@Override
	public void setCapability(final Capability capability) {
		this.capability = capability;
	}

	public BuildingMap getBuildingMap() {
		return buildingMap;
	}

	public void setBuildingMap(final BuildingMap buildingMap) {
		this.buildingMap = buildingMap;
	}

	public Short getOnMapId() {
		return onMapId;
	}

	public void setOnMapId(final Short onMapId) {
		this.onMapId = onMapId;
	}

	public ToiletType getToiletType() {
		return toiletType;
	}

	public void setToiletType(final ToiletType toiletType) {
		this.toiletType = toiletType;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(final String info) {
		this.info = info;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((buildingMap == null) ? 0 : buildingMap.hashCode());
		result = prime * result + ((capability == null) ? 0 : capability.hashCode());
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((onMapId == null) ? 0 : onMapId.hashCode());
		result = prime * result + ((toiletType == null) ? 0 : toiletType.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Toilet other = (Toilet) obj;
		if (buildingMap == null) {
			if (other.buildingMap != null) {
				return false;
			}
		} else if (!buildingMap.equals(other.buildingMap)) {
			return false;
		}
		if (capability != other.capability) {
			return false;
		}
		if (info == null) {
			if (other.info != null) {
				return false;
			}
		} else if (!info.equals(other.info)) {
			return false;
		}
		if (onMapId == null) {
			if (other.onMapId != null) {
				return false;
			}
		} else if (!onMapId.equals(other.onMapId)) {
			return false;
		}
		if (toiletType != other.toiletType) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Toilet [capability=");
		builder.append(capability);
		builder.append(", buildingMap=");
		builder.append(buildingMap);
		builder.append(", onMapId=");
		builder.append(onMapId);
		builder.append(", toiletType=");
		builder.append(toiletType);
		builder.append(", info=");
		builder.append(info);
		builder.append(", isPrivate=");
		builder.append(isPrivate);
		builder.append(", responsibleOwnUsers=");
		builder.append(responsibleOwnUsers);
		builder.append(", responsibleGroups=");
		builder.append(responsibleGroups);
		builder.append(", responsibleUsers=");
		builder.append(responsibleUsers);
		builder.append(", id=");
		builder.append(id);
		builder.append(", parentId=");
		builder.append(parentId);
		builder.append("]");
		return builder.toString();
	}

}
