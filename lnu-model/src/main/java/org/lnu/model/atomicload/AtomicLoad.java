package org.lnu.model.atomicload;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.lnu.annotation.dbtable.Essence;
import org.lnu.model.ModelWithSimpleId;
import org.lnu.model.academicgroup.AcademicGroup;
import org.lnu.model.learningplace.LearningPlace;
import org.lnu.model.learningplacegroup.LearningPlaceGroup;
import org.lnu.model.lecturer.Lecturer;
import org.lnu.model.load.Load;
import org.lnu.model.specialty.Specialty;
import org.lnu.model.student.Student;

/**
 * Entity, that describes atomic load.
 * @author OlehZanevych
 */
@Essence
@Entity
@Table(name = "t_essence_atomic_load")
public class AtomicLoad extends ModelWithSimpleId<Long> {
	
	private static final long serialVersionUID = 1L;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "load_id")
	private Load load;
	
	@NotNull
	@Column(name = "hours")
	private Short hours;
	
	@NotNull
	@Column(name = "lesson_duration")
	private Byte lessonDuration;
	
	@ManyToMany
    @JoinTable(name = "t_relationship_atomic_load_lecturers",
		joinColumns = @JoinColumn(name = "atomic_load_id"),
		inverseJoinColumns = @JoinColumn(name = "lecturer_id"))
	private List<Lecturer> lecturers;
	
	@ManyToMany
    @JoinTable(name = "t_relationship_atomic_load_specialties",
		joinColumns = @JoinColumn(name = "atomic_load_id"),
		inverseJoinColumns = @JoinColumn(name = "specialty_id"))
	private List<Specialty> specialties;
	
	@ManyToMany
    @JoinTable(name = "t_relationship_atomic_load_academic_groups",
		joinColumns = @JoinColumn(name = "atomic_load_id"),
		inverseJoinColumns = @JoinColumn(name = "academic_group_id"))
	private List<AcademicGroup> academicGroups;
	
	@ManyToMany
    @JoinTable(name = "t_relationship_atomic_load_students",
		joinColumns = @JoinColumn(name = "atomic_load_id"),
		inverseJoinColumns = @JoinColumn(name = "student_id"))
	private List<Student> students;
	
	@ManyToMany
    @JoinTable(name = "t_relationship_atomic_load_learning_place_groups",
		joinColumns = @JoinColumn(name = "atomic_load_id"),
		inverseJoinColumns = @JoinColumn(name = "learning_place_group_id"))
	private List<LearningPlaceGroup> learningPlaceGroups;
	
	@ManyToMany
    @JoinTable(name = "t_relationship_atomic_load_learning_places",
		joinColumns = @JoinColumn(name = "atomic_load_id"),
		inverseJoinColumns = @JoinColumn(name = "learning_place_id"))
	private List<LearningPlace> learningPlaces;
	
	/**
	 * Default constructor with no parameters.
	 */
	public AtomicLoad() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public AtomicLoad(final Long id) {
		super(id);
	}
	
	/**
	 * Method to initialize default values.
	 */
	@PrePersist
	private void initializeDefaultValues() {
		if (lessonDuration == null) {
			lessonDuration = 2;
		}
	}

	public Load getLoad() {
		return load;
	}

	public void setLoad(final Load load) {
		this.load = load;
	}

	public Short getHours() {
		return hours;
	}

	public void setHours(final Short hours) {
		this.hours = hours;
	}

	public Byte getLessonDuration() {
		return lessonDuration;
	}

	public void setLessonDuration(final Byte lessonDuration) {
		this.lessonDuration = lessonDuration;
	}

	public List<Lecturer> getLecturers() {
		return lecturers;
	}

	public void setLecturers(final List<Lecturer> lecturers) {
		this.lecturers = lecturers;
	}

	public List<Specialty> getSpecialties() {
		return specialties;
	}

	public void setSpecialties(final List<Specialty> specialties) {
		this.specialties = specialties;
	}

	public List<AcademicGroup> getAcademicGroups() {
		return academicGroups;
	}

	public void setAcademicGroups(final List<AcademicGroup> academicGroups) {
		this.academicGroups = academicGroups;
	}

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(final List<Student> students) {
		this.students = students;
	}

	public List<LearningPlaceGroup> getLearningPlaceGroups() {
		return learningPlaceGroups;
	}

	public void setLearningPlaceGroups(final List<LearningPlaceGroup> learningPlaceGroups) {
		this.learningPlaceGroups = learningPlaceGroups;
	}

	public List<LearningPlace> getLearningPlaces() {
		return learningPlaces;
	}

	public void setLearningPlaces(final List<LearningPlace> learningPlaces) {
		this.learningPlaces = learningPlaces;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((academicGroups == null) ? 0 : academicGroups.hashCode());
		result = prime * result + ((hours == null) ? 0 : hours.hashCode());
		result = prime * result + ((learningPlaceGroups == null) ? 0 : learningPlaceGroups.hashCode());
		result = prime * result + ((learningPlaces == null) ? 0 : learningPlaces.hashCode());
		result = prime * result + ((lecturers == null) ? 0 : lecturers.hashCode());
		result = prime * result + ((lessonDuration == null) ? 0 : lessonDuration.hashCode());
		result = prime * result + ((specialties == null) ? 0 : specialties.hashCode());
		result = prime * result + ((students == null) ? 0 : students.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AtomicLoad other = (AtomicLoad) obj;
		if (academicGroups == null) {
			if (other.academicGroups != null) {
				return false;
			}
		} else if (!academicGroups.equals(other.academicGroups)) {
			return false;
		}
		if (hours == null) {
			if (other.hours != null) {
				return false;
			}
		} else if (!hours.equals(other.hours)) {
			return false;
		}
		if (learningPlaceGroups == null) {
			if (other.learningPlaceGroups != null) {
				return false;
			}
		} else if (!learningPlaceGroups.equals(other.learningPlaceGroups)) {
			return false;
		}
		if (learningPlaces == null) {
			if (other.learningPlaces != null) {
				return false;
			}
		} else if (!learningPlaces.equals(other.learningPlaces)) {
			return false;
		}
		if (lecturers == null) {
			if (other.lecturers != null) {
				return false;
			}
		} else if (!lecturers.equals(other.lecturers)) {
			return false;
		}
		if (lessonDuration == null) {
			if (other.lessonDuration != null) {
				return false;
			}
		} else if (!lessonDuration.equals(other.lessonDuration)) {
			return false;
		}
		if (specialties == null) {
			if (other.specialties != null) {
				return false;
			}
		} else if (!specialties.equals(other.specialties)) {
			return false;
		}
		if (students == null) {
			if (other.students != null) {
				return false;
			}
		} else if (!students.equals(other.students)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AtomicLoad [hours=");
		builder.append(hours);
		builder.append(", lessonDuration=");
		builder.append(lessonDuration);
		builder.append(", lecturers=");
		builder.append(lecturers);
		builder.append(", specialties=");
		builder.append(specialties);
		builder.append(", academicGroups=");
		builder.append(academicGroups);
		builder.append(", students=");
		builder.append(students);
		builder.append(", learningPlaceGroups=");
		builder.append(learningPlaceGroups);
		builder.append(", learningPlaces=");
		builder.append(learningPlaces);
		builder.append(", id=");
		builder.append(id);
		builder.append(", parentId=");
		builder.append(parentId);
		builder.append("]");
		return builder.toString();
	}

}
