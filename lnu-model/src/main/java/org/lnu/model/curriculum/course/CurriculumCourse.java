package org.lnu.model.curriculum.course;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.lnu.annotation.dbtable.Essence;
import org.lnu.model.ModelWithSimpleId;
import org.lnu.model.course.Course;
import org.lnu.model.curriculum.Curriculum;
import org.lnu.model.department.Department;

/**
 * Entity, that describes curriculum course.
 * @author OlehZanevych
 */
@Essence
@Entity
@Table(name = "t_essence_curricula_courses")
public final class CurriculumCourse extends ModelWithSimpleId<Long> {

	private static final long serialVersionUID = 1L;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "curriculum_id")
	private Curriculum curriculum;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "course_id")
	private Course course;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "department_id")
	private Department department;
	
	/**
	 * Default constructor with no parameters.
	 */
	public CurriculumCourse() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public CurriculumCourse(final Long id) {
		super(id);
	}

	public Curriculum getCurriculum() {
		return curriculum;
	}

	public void setCurriculum(final Curriculum curriculum) {
		this.curriculum = curriculum;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(final Course course) {
		this.course = course;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(final Department department) {
		this.department = department;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((course == null) ? 0 : course.hashCode());
		result = prime * result + ((department == null) ? 0 : department.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CurriculumCourse other = (CurriculumCourse) obj;
		if (course == null) {
			if (other.course != null) {
				return false;
			}
		} else if (!course.equals(other.course)) {
			return false;
		}
		if (department == null) {
			if (other.department != null) {
				return false;
			}
		} else if (!department.equals(other.department)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CurriculumCourse [course=");
		builder.append(course);
		builder.append(", department=");
		builder.append(department);
		builder.append(", id=");
		builder.append(id);
		builder.append(", parentId=");
		builder.append(parentId);
		builder.append("]");
		return builder.toString();
	}

}
