package org.lnu.model.curriculum.semester;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Formula;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.lnu.annotation.dbtable.Essence;
import org.lnu.model.ModelWithSimpleId;
import org.lnu.model.curriculum.Curriculum;
import org.lnu.model.curriculum.semester.hours.CurriculumSemesterHours;
import org.lnu.model.enumtype.report.Report;

/**
 * Entity, that describes curriculum semester.
 * @author OlehZanevych
 */
@Essence
@Entity
@Table(name = "t_essence_curricula_semesters")
public final class CurriculumSemester extends ModelWithSimpleId<Long> {
	
	private static final long serialVersionUID = 1L;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "curriculum_id")
	private Curriculum curriculum;
	
	@NotNull
	@Column(name = "semester")
	private Byte semester;
	
	@Formula("(semester + 1) / 2")
	private Byte year;
	
	@Formula("2 - semester % 2")
	private Byte half;
	
	@Formula("("
			+ "SELECT "
					+ "semesters_credits.credits "
			+ "FROM t_essence_curricula_semesters_credits semesters_credits "
			+ "WHERE semesters_credits.id = id"
	+ ")")
	private Float credits;
	
	@Formula("("
			+ "SELECT "
					+ "semesters_all_hours.all_hours "
			+ "FROM t_essence_curricula_semesters_all_hours semesters_all_hours "
			+ "WHERE semesters_all_hours.id = id"
	+ ")")
	private Short allHours;
	
	@NotNull
	@Column(name = "independent_study")
	private Short independentStudy;
	
	@NotNull
	@Type(type = "org.lnu.model.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",
					value = "org.lnu.model.enumtype.report.Report"
			)
	)
	@Column(name = "report", columnDefinition = "e_report")
	@Enumerated(EnumType.STRING)
	private Report report;
	
	@OneToMany(mappedBy = "curriculumSemester", cascade = CascadeType.ALL)
	private List<CurriculumSemesterHours> curriculumSemesterHours;
	
	/**
	 * Default constructor with no parameters.
	 */
	public CurriculumSemester() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public CurriculumSemester(final Long id) {
		super(id);
	}

	public Curriculum getCurriculum() {
		return curriculum;
	}

	public void setCurriculum(final Curriculum curriculum) {
		this.curriculum = curriculum;
	}

	public Byte getSemester() {
		return semester;
	}

	public void setSemester(final Byte semester) {
		this.semester = semester;
	}

	public Byte getYear() {
		return year;
	}

	public void setYear(final Byte year) {
		this.year = year;
	}

	public Byte getHalf() {
		return half;
	}

	public void setHalf(final Byte half) {
		this.half = half;
	}

	public Float getCredits() {
		return credits;
	}

	public void setCredits(final Float credits) {
		this.credits = credits;
	}

	public Short getAllHours() {
		return allHours;
	}

	public void setAllHours(final Short allHours) {
		this.allHours = allHours;
	}

	public Short getIndependentStudy() {
		return independentStudy;
	}

	public void setIndependentStudy(final Short independentStudy) {
		this.independentStudy = independentStudy;
	}

	public Report getReport() {
		return report;
	}

	public void setReport(final Report report) {
		this.report = report;
	}

	public List<CurriculumSemesterHours> getCurriculumSemesterHours() {
		return curriculumSemesterHours;
	}

	public void setCurriculumSemesterHours(final List<CurriculumSemesterHours> curriculumSemesterHours) {
		this.curriculumSemesterHours = curriculumSemesterHours;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((allHours == null) ? 0 : allHours.hashCode());
		result = prime * result + ((credits == null) ? 0 : credits.hashCode());
		result = prime * result + ((curriculumSemesterHours == null) ? 0 : curriculumSemesterHours.hashCode());
		result = prime * result + ((independentStudy == null) ? 0 : independentStudy.hashCode());
		result = prime * result + ((half == null) ? 0 : half.hashCode());
		result = prime * result + ((report == null) ? 0 : report.hashCode());
		result = prime * result + ((semester == null) ? 0 : semester.hashCode());
		result = prime * result + ((year == null) ? 0 : year.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CurriculumSemester other = (CurriculumSemester) obj;
		if (allHours == null) {
			if (other.allHours != null) {
				return false;
			}
		} else if (!allHours.equals(other.allHours)) {
			return false;
		}
		if (credits == null) {
			if (other.credits != null) {
				return false;
			}
		} else if (!credits.equals(other.credits)) {
			return false;
		}
		if (curriculumSemesterHours == null) {
			if (other.curriculumSemesterHours != null) {
				return false;
			}
		} else if (!curriculumSemesterHours.equals(other.curriculumSemesterHours)) {
			return false;
		}
		if (independentStudy == null) {
			if (other.independentStudy != null) {
				return false;
			}
		} else if (!independentStudy.equals(other.independentStudy)) {
			return false;
		}
		if (half == null) {
			if (other.half != null) {
				return false;
			}
		} else if (!half.equals(other.half)) {
			return false;
		}
		if (report != other.report) {
			return false;
		}
		if (semester == null) {
			if (other.semester != null) {
				return false;
			}
		} else if (!semester.equals(other.semester)) {
			return false;
		}
		if (year == null) {
			if (other.year != null) {
				return false;
			}
		} else if (!year.equals(other.year)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CurriculumSemester [semester=");
		builder.append(semester);
		builder.append(", year=");
		builder.append(year);
		builder.append(", half=");
		builder.append(half);
		builder.append(", credits=");
		builder.append(credits);
		builder.append(", allHours=");
		builder.append(allHours);
		builder.append(", independentStudy=");
		builder.append(independentStudy);
		builder.append(", report=");
		builder.append(report);
		builder.append(", curriculumSemesterHours=");
		builder.append(curriculumSemesterHours);
		builder.append(", id=");
		builder.append(id);
		builder.append(", parentId=");
		builder.append(parentId);
		builder.append("]");
		return builder.toString();
	}
	
}
