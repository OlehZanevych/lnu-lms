package org.lnu.model.curriculum;

import java.util.List;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Formula;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.lnu.annotation.dbtable.Essence;
import org.lnu.model.APIModel;
import org.lnu.model.curriculum.course.CurriculumCourse;
import org.lnu.model.curriculum.semester.CurriculumSemester;
import org.lnu.model.enumtype.capability.Capability;
import org.lnu.model.enumtype.coursecycle.CourseCycle;
import org.lnu.model.enumtype.coursetype.CourseType;
import org.lnu.model.specialty.Specialty;

/**
 * Entity, that describes curriculum.
 * @author OlehZanevych
 */
@Essence
@Entity
@Table(name = "t_essence_curricula")
@AssociationOverrides({
	@AssociationOverride(
		name = "responsibleOwnUsers",
		joinTable = @JoinTable(name = "t_relationship_curricula_responsible_own_users",
			joinColumns = @JoinColumn(name = "curriculum_id"),
			inverseJoinColumns = @JoinColumn(name = "user_id")
		)
	),
	@AssociationOverride(
		name = "responsibleGroups",
		joinTable = @JoinTable(name = "t_relationship_curricula_responsible_groups",
			joinColumns = @JoinColumn(name = "curriculum_id"),
			inverseJoinColumns = @JoinColumn(name = "group_id")
		)
	),
	@AssociationOverride(
		name = "responsibleUsers",
		joinTable = @JoinTable(name = "v_relationship_curricula_responsible_users",
			joinColumns = @JoinColumn(name = "curriculum_id", insertable = false, updatable = false),
			inverseJoinColumns = @JoinColumn(name = "user_id", insertable = false, updatable = false)
		)
	)
})
public final class Curriculum extends APIModel<Long> {
	
	private static final long serialVersionUID = 1L;
	
	@Type(type = "org.lnu.model.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",
					value = "org.lnu.model.enumtype.capability.Capability"
			)
	)
	@Formula("get_curriculum_capability(:user.id, id, is_private)")
	@Enumerated(EnumType.STRING)
	private Capability capability;
	
	@OneToMany(mappedBy = "curriculum", cascade = CascadeType.ALL)
	private List<CurriculumCourse> curriculumCourses;
	
	@Type(type = "org.lnu.model.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",
					value = "org.lnu.model.enumtype.coursetype.CourseType"
			)
	)
	@Formula("get_course_type(id)")
	@Enumerated(EnumType.STRING)
	private CourseType courseType;
	
	@NotNull
	@Type(type = "org.lnu.model.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",
					value = "org.lnu.model.enumtype.coursecycle.CourseCycle"
			)
	)
	@Column(name = "course_cycle", columnDefinition = "e_course_cycle")
	@Enumerated(EnumType.STRING)
	private CourseCycle courseCycle;
	
	@ManyToMany
    @JoinTable(name = "t_relationship_curricula_specialties",
		joinColumns = @JoinColumn(name = "curriculum_id"),
		inverseJoinColumns = @JoinColumn(name = "specialty_id"))
    private List<Specialty> specialties;
	
	@OneToMany(mappedBy = "curriculum", cascade = CascadeType.ALL)
	private List<CurriculumSemester> curriculumSemesters;
	
	/**
	 * Default constructor with no parameters.
	 */
	public Curriculum() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public Curriculum(final Long id) {
		super(id);
	}
	
	@Override
	public Capability getCapability() {
		return capability;
	}

	@Override
	public void setCapability(final Capability capability) {
		this.capability = capability;
	}

	public List<CurriculumCourse> getCurriculumCourses() {
		return curriculumCourses;
	}

	public void setCurriculumCourses(final List<CurriculumCourse> curriculumCourses) {
		this.curriculumCourses = curriculumCourses;
	}

	public CourseType getCourseType() {
		return courseType;
	}

	public void setCourseType(final CourseType courseType) {
		this.courseType = courseType;
	}

	public CourseCycle getCourseCycle() {
		return courseCycle;
	}

	public void setCourseCycle(final CourseCycle courseCycle) {
		this.courseCycle = courseCycle;
	}

	public List<Specialty> getSpecialties() {
		return specialties;
	}

	public void setSpecialties(final List<Specialty> specialties) {
		this.specialties = specialties;
	}

	public List<CurriculumSemester> getCurriculumSemesters() {
		return curriculumSemesters;
	}

	public void setCurriculumSemesters(final List<CurriculumSemester> curriculumSemesters) {
		this.curriculumSemesters = curriculumSemesters;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((capability == null) ? 0 : capability.hashCode());
		result = prime * result + ((courseCycle == null) ? 0 : courseCycle.hashCode());
		result = prime * result + ((courseType == null) ? 0 : courseType.hashCode());
		result = prime * result + ((curriculumCourses == null) ? 0 : curriculumCourses.hashCode());
		result = prime * result + ((curriculumSemesters == null) ? 0 : curriculumSemesters.hashCode());
		result = prime * result + ((specialties == null) ? 0 : specialties.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Curriculum other = (Curriculum) obj;
		if (capability != other.capability) {
			return false;
		}
		if (courseCycle != other.courseCycle) {
			return false;
		}
		if (courseType != other.courseType) {
			return false;
		}
		if (curriculumCourses == null) {
			if (other.curriculumCourses != null) {
				return false;
			}
		} else if (!curriculumCourses.equals(other.curriculumCourses)) {
			return false;
		}
		if (curriculumSemesters == null) {
			if (other.curriculumSemesters != null) {
				return false;
			}
		} else if (!curriculumSemesters.equals(other.curriculumSemesters)) {
			return false;
		}
		if (specialties == null) {
			if (other.specialties != null) {
				return false;
			}
		} else if (!specialties.equals(other.specialties)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Curriculum [capability=");
		builder.append(capability);
		builder.append(", curriculumCourses=");
		builder.append(curriculumCourses);
		builder.append(", courseType=");
		builder.append(courseType);
		builder.append(", courseCycle=");
		builder.append(courseCycle);
		builder.append(", specialties=");
		builder.append(specialties);
		builder.append(", curriculumSemesters=");
		builder.append(curriculumSemesters);
		builder.append(", isPrivate=");
		builder.append(isPrivate);
		builder.append(", responsibleOwnUsers=");
		builder.append(responsibleOwnUsers);
		builder.append(", responsibleGroups=");
		builder.append(responsibleGroups);
		builder.append(", responsibleUsers=");
		builder.append(responsibleUsers);
		builder.append(", id=");
		builder.append(id);
		builder.append(", parentId=");
		builder.append(parentId);
		builder.append("]");
		return builder.toString();
	}

}
