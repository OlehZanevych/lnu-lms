package org.lnu.model.curriculum.semester.hours;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.lnu.annotation.dbtable.Essence;
import org.lnu.model.ModelWithSimpleId;
import org.lnu.model.curriculum.semester.CurriculumSemester;
import org.lnu.model.enumtype.classtype.ClassType;

/**
 * Entity, that describes curriculum semesters hours.
 * @author OlehZanevych
 */
@Essence
@Entity
@Table(name = "t_essence_curricula_semesters_hours")
public class CurriculumSemesterHours extends ModelWithSimpleId<Long> {
	
	private static final long serialVersionUID = 1L;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "curriculum_semester_id")
	private CurriculumSemester curriculumSemester;
	
	@NotNull
	@Type(type = "org.lnu.model.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",
					value = "org.lnu.model.enumtype.classtype.ClassType"
			)
	)
	@Column(name = "class_type", columnDefinition = "e_class_type")
	@Enumerated(EnumType.STRING)
	private ClassType classType;
	
	@NotNull
	@Column(name = "hours")
	private Short hours;
	
	@NotNull
	@Column(name = "is_academic_group_department")
	private Boolean isAcademicGroupDepartment;
	
	/**
	 * Default constructor with no parameters.
	 */
	public CurriculumSemesterHours() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public CurriculumSemesterHours(final Long id) {
		super(id);
	}

	public CurriculumSemester getCurriculumSemester() {
		return curriculumSemester;
	}

	public void setCurriculumSemester(final CurriculumSemester curriculumSemester) {
		this.curriculumSemester = curriculumSemester;
	}

	public ClassType getClassType() {
		return classType;
	}

	public void setClassType(final ClassType classType) {
		this.classType = classType;
	}

	public Short getHours() {
		return hours;
	}

	public void setHours(final Short hours) {
		this.hours = hours;
	}

	public Boolean getIsAcademicGroupDepartment() {
		return isAcademicGroupDepartment;
	}

	public void setIsAcademicGroupDepartment(final Boolean isAcademicGroupDepartment) {
		this.isAcademicGroupDepartment = isAcademicGroupDepartment;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((classType == null) ? 0 : classType.hashCode());
		result = prime * result + ((hours == null) ? 0 : hours.hashCode());
		result = prime * result + ((isAcademicGroupDepartment == null) ? 0 : isAcademicGroupDepartment.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CurriculumSemesterHours other = (CurriculumSemesterHours) obj;
		if (classType != other.classType) {
			return false;
		}
		if (hours == null) {
			if (other.hours != null) {
				return false;
			}
		} else if (!hours.equals(other.hours)) {
			return false;
		}
		if (isAcademicGroupDepartment == null) {
			if (other.isAcademicGroupDepartment != null) {
				return false;
			}
		} else if (!isAcademicGroupDepartment.equals(other.isAcademicGroupDepartment)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CurriculumSemesterHours [classType=");
		builder.append(classType);
		builder.append(", hours=");
		builder.append(hours);
		builder.append(", isAcademicGroupDepartment=");
		builder.append(isAcademicGroupDepartment);
		builder.append(", id=");
		builder.append(id);
		builder.append(", parentId=");
		builder.append(parentId);
		builder.append("]");
		return builder.toString();
	}

}
