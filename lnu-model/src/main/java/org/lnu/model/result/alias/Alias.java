package org.lnu.model.result.alias;

/**
 * Class, that is used to represent Hibernate Criteria alias.
 * @author OlehZanevych
 */
public class Alias {
	
	private String associationPath;
	
	private String alias;

	/**
	 * Constructor.
	 * @param associationPath association path
	 * @param alias alias
	 */
	public Alias(final String associationPath, final String alias) {
		this.associationPath = associationPath;
		this.alias = alias;
	}

	public String getAssociationPath() {
		return associationPath;
	}

	public void setAssociationPath(final String associationPath) {
		this.associationPath = associationPath;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(final String alias) {
		this.alias = alias;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alias == null) ? 0 : alias.hashCode());
		result = prime * result + ((associationPath == null) ? 0 : associationPath.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Alias other = (Alias) obj;
		if (alias == null) {
			if (other.alias != null) {
				return false;
			}
		} else if (!alias.equals(other.alias)) {
			return false;
		}
		if (associationPath == null) {
			if (other.associationPath != null) {
				return false;
			}
		} else if (!associationPath.equals(other.associationPath)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Alias [associationPath=");
		builder.append(associationPath);
		builder.append(", alias=");
		builder.append(alias);
		builder.append("]");
		return builder.toString();
	}

}
