package org.lnu.model.building;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Formula;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.lnu.annotation.dbtable.Essence;
import org.lnu.model.APIModel;
import org.lnu.model.enumtype.capability.Capability;

/**
 * Entity, that describes building.
 * @author OlehZanevych
 */
@Essence
@Entity
@Table(name = "t_essence_buildings")
@AssociationOverrides({
	@AssociationOverride(
		name = "responsibleOwnUsers",
		joinTable = @JoinTable(name = "t_relationship_buildings_responsible_own_users",
			joinColumns = @JoinColumn(name = "building_id"),
			inverseJoinColumns = @JoinColumn(name = "user_id")
		)
	),
	@AssociationOverride(
		name = "responsibleGroups",
		joinTable = @JoinTable(name = "t_relationship_buildings_responsible_groups",
			joinColumns = @JoinColumn(name = "building_id"),
			inverseJoinColumns = @JoinColumn(name = "group_id")
		)
	),
	@AssociationOverride(
		name = "responsibleUsers",
		joinTable = @JoinTable(name = "v_relationship_buildings_responsible_users",
			joinColumns = @JoinColumn(name = "building_id", insertable = false, updatable = false),
			inverseJoinColumns = @JoinColumn(name = "user_id", insertable = false, updatable = false)
		)
	)
})
public final class Building extends APIModel<Long> {
	
	private static final long serialVersionUID = 1L;
	
	@Type(type = "org.lnu.model.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",
					value = "org.lnu.model.enumtype.capability.Capability"
			)
	)
	@Formula("get_building_capability(:user.id, id, is_private)")
	@Enumerated(EnumType.STRING)
	private Capability capability;
	
	@NotNull
	@Column(name = "name", unique = true)
	private String name;
	
	@NotNull
	@Column(name = "abbreviation", unique = true)
	private String abbreviation;
	
	@NotNull
	@Column(name = "latitude")
	private Double latitude;
	
	@NotNull
	@Column(name = "longitude")
	private Double longitude;

	/**
	 * Default constructor with no parameters.
	 */
	public Building() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public Building(final Long id) {
		super(id);
	}
	
	@Override
	public Capability getCapability() {
		return capability;
	}

	@Override
	public void setCapability(final Capability capability) {
		this.capability = capability;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(final String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(final Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(final Double longitude) {
		this.longitude = longitude;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((abbreviation == null) ? 0 : abbreviation.hashCode());
		result = prime * result + ((capability == null) ? 0 : capability.hashCode());
		result = prime * result + ((latitude == null) ? 0 : latitude.hashCode());
		result = prime * result + ((longitude == null) ? 0 : longitude.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Building other = (Building) obj;
		if (abbreviation == null) {
			if (other.abbreviation != null) {
				return false;
			}
		} else if (!abbreviation.equals(other.abbreviation)) {
			return false;
		}
		if (capability != other.capability) {
			return false;
		}
		if (latitude == null) {
			if (other.latitude != null) {
				return false;
			}
		} else if (!latitude.equals(other.latitude)) {
			return false;
		}
		if (longitude == null) {
			if (other.longitude != null) {
				return false;
			}
		} else if (!longitude.equals(other.longitude)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Building [capability=");
		builder.append(capability);
		builder.append(", name=");
		builder.append(name);
		builder.append(", abbreviation=");
		builder.append(abbreviation);
		builder.append(", latitude=");
		builder.append(latitude);
		builder.append(", longitude=");
		builder.append(longitude);
		builder.append(", isPrivate=");
		builder.append(isPrivate);
		builder.append(", responsibleOwnUsers=");
		builder.append(responsibleOwnUsers);
		builder.append(", responsibleGroups=");
		builder.append(responsibleGroups);
		builder.append(", responsibleUsers=");
		builder.append(responsibleUsers);
		builder.append(", id=");
		builder.append(id);
		builder.append(", parentId=");
		builder.append(parentId);
		builder.append("]");
		return builder.toString();
	}
	
}
