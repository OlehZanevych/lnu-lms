package org.lnu.security;

import org.lnu.annotation.Security;
import org.lnu.dao.dao.user.UserDao;
import org.lnu.model.session.Session;
import org.lnu.model.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Default Authentication Provider.
 * @author OlehZanevych
 */
@Security("defaultAuthenticationProvider")
public class DefaultAuthenticationProvider implements AuthenticationProvider {
    
	private static final Logger LOG = LoggerFactory.getLogger(DefaultAuthenticationProvider.class);

    @Resource(name = "userDao")
    private UserDao userDao;
    
    @Resource(name = "bCryptPasswordEncoder")
    private PasswordEncoder passwordEncoder;
    
    @Override
    public boolean supports(final Class<?> clazz) {
    	return clazz.equals(UsernamePasswordAuthenticationToken.class);
    }

    @Override
    @Transactional
    public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
        
    	String login = authentication.getName();
        String password = authentication.getCredentials().toString();
        
        LOG.info(MessageFormat.format("Starting to authenticate user with login: {0}", login));

        return getAuthentication(login, password);
    }

    /**
     * Method for getting authentification.
     * @param login
     * @param password
     * @return authentification.
     */
    private Authentication getAuthentication(final String login, final String password) {
        
    	User user = getUser(login, password);
        
        Collection<GrantedAuthority> authorities = user.getRoles().stream()
        		.map(i -> new SimpleGrantedAuthority(i.getName())).collect(Collectors.toList());
        
        Session session = new Session(user.getId());

        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(login, password, authorities);
        token.setDetails(session);

        return token;
    }

	/**
     * Method for getting user.
     * @param login
     * @param password
     * @return user.
     */
    private User getUser(final String login, final String password) {
    	User user = null;
        try {
            user =  userDao.getUserByLogin(login);
            Assert.isTrue(passwordEncoder.matches(password, user.getPasswordHash()));
        } catch (Exception e) {
            LOG.error("Can't find user for login: " + login, e);
            throw new BadCredentialsException("Bad creditials");
        }
        return user;
    }

}
