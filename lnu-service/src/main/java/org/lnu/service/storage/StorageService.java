package org.lnu.service.storage;

import java.nio.file.Path;

import org.springframework.web.multipart.MultipartFile;

/**
 * Common interface for all file storage services.
 * @author OlehZanevych
 */
public interface StorageService {
	
	/**
	 * Storing file in path.
	 * @param file multipart file
	 * @param folder target upload folder
	 * @param id file description entity identifier
	 * @return stored file name
	 */
	String store(MultipartFile file, Path folder, Number id);
	
	/**
	 * Deleting file by path.
	 * @param folder target upload folder
	 * @param id file description entity identifier
	 */
	void delete(Path folder, Number id);
}
