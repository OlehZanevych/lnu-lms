package org.lnu.service.storage.exception;

/**
 * File storage exception.
 * @author OlehZanevych
 */
public class StorageException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
	 * Default constructor.
	 * @param message
	 */
	public StorageException(final String message) {
        super(message);
    }

	/**
	 * Constructor with message and cause.
	 * @param message
	 * @param cause
	 */
    public StorageException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
