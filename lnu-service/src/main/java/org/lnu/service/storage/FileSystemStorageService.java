
package org.lnu.service.storage;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.MessageFormat;

import org.apache.tika.config.TikaConfig;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.apache.tika.mime.MimeType;
import org.lnu.annotation.Service;
import org.lnu.service.storage.exception.StorageException;
import org.springframework.web.multipart.MultipartFile;

/**
 * File system storage service.
 * @author OlehZanevych
 */
@Service("fileSystemStorageService")
public class FileSystemStorageService implements StorageService {

	@Override
	public String store(final MultipartFile file, final Path folder, final Number id) {
		try {
            if (file.isEmpty()) {
                throw new StorageException("Failed to store empty file");
            }
            delete(folder, id);
            
            TikaConfig tikaConfig = TikaConfig.getDefaultConfig();
            Metadata metadata = new Metadata();
            
            InputStream inputStream = file.getInputStream();
            MediaType mediaType = tikaConfig.getMimeRepository().detect(inputStream, metadata);
            MimeType mimeType = tikaConfig.getMimeRepository().forName(mediaType.toString());
            
            String fileName = id + mimeType.getExtension();
            Files.copy(inputStream, folder.resolve(fileName));
            return fileName;
        } catch (Exception e) {
            throw new StorageException(MessageFormat.format("Failed to store file: {0}", e));
        }
	}

	@Override
	public void delete(final Path folder, final Number id) {
		try {
			DirectoryStream<Path> directoryStream = Files.newDirectoryStream(folder, id + ".*");
			for (Path path : directoryStream) {
	            Files.delete(path);
	        }
		} catch (IOException e) {
			throw new StorageException(MessageFormat.format("Failed to delete file: {0}", e));
		}
	}

}
