package org.lnu.service.dictionary;

import java.text.MessageFormat;
import java.util.LinkedHashMap;

import org.lnu.model.dictionary.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

/**
 * Default dictionary service implementation.
 * @author OlehZanevych
 *
 * @param <KEY> Enum type of dictionary key
 */
public class DefaultDictionaryService<KEY extends Enum<?>> implements DictionaryService<KEY> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultDictionaryService.class);

	protected LinkedHashMap<KEY, Value> dictionary;
	
	@Override
	public Value getValue(final KEY key) {
		Value value = dictionary.get(key);
		LOGGER.info("Getting value '{0}' from dictionary by key '{1}'", value, key);
		if (value == null) {
			throw new IllegalArgumentException(MessageFormat.format("Current dictionary doesn't contain value for key '{0}'. Please, contact dev team for fixing this issue", key));
		}
		return value;
	}
	
	@Override
	public LinkedHashMap<KEY, Value> getDictionary() {
		LOGGER.info("Getting all dictionary: '{0}'", dictionary);
		if (dictionary == null || dictionary.isEmpty()) {
			throw new IllegalArgumentException("Dictionary is empty. Please, contact dev team for fixing this issue");
		}
		return dictionary;
	}
	
	@Required
	public void setDictionary(final LinkedHashMap<KEY, Value> dictionary) {
		this.dictionary = dictionary;
	}
	
}
