package org.lnu.service.session;

import java.text.MessageFormat;

import org.lnu.annotation.Service;
import org.lnu.model.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Default implementation of session service.
 * @author OlehZanevych
 */
@Service("defaultSessionService")
public class DefaultSessionService implements SessionService {
	
	private static final Logger LOG = LoggerFactory.getLogger(DefaultSessionService.class);
	
	@Override
	public Long getUserId() {
		Session session = getSession();
		if (session == null) {
			return null;
		}
		Long userId = session.getUserId();
		LOG.info(MessageFormat.format("Getting session userId: {0}", userId));
		return userId;
	}
	
	@Override
	public Session getSession() {
		Object details = SecurityContextHolder.getContext().getAuthentication().getDetails();
		if (!(details instanceof Session)) {
			return null;
		}
		Session session = (Session) details;
		LOG.info(MessageFormat.format("Getting session: {0}", session));
		return session;
	}
}
