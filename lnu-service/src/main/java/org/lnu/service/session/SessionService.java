package org.lnu.service.session;

import org.lnu.model.session.Session;

/**
 * Interface for declaring all operations with session user.
 * @author OlehZanevych
 */
public interface SessionService {

	
	/**
	 * Method for getting session details.
	 * @return session
	 */
	Session getSession();
	
	/**
	 * Method for getting authorized User id.
	 * @return User id
	 */
	Long getUserId();
}
