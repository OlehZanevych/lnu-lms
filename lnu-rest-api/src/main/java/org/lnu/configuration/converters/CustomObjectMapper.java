package org.lnu.configuration.converters;

import org.lnu.web.rest.constant.Constants;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

/**
 * Custom Object Mapper.
 * @author OlehZanevych
 */
public class CustomObjectMapper extends ObjectMapper {
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public CustomObjectMapper() {
		super();
		setSerializationInclusion(Include.NON_NULL);

		SimpleModule module = new SimpleModule("JSONModule", new Version(2, 1, 5, null, null, null));
		registerModule(module);
		
		setDateFormat(Constants.DATE_FORMAT);
	}
}
