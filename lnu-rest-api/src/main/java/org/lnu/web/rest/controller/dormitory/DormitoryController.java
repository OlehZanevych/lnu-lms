package org.lnu.web.rest.controller.dormitory;

import javax.annotation.Resource;

import org.lnu.facade.facade.Facade;
import org.lnu.resource.dormitory.DormitoryResource;
import org.lnu.resource.message.MessageResource;
import org.lnu.resource.message.MessageType;
import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.PagedResultResource;
import org.lnu.resource.request.Request;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles all API with Dormitory entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/dormitories")
@Api(value = "dormitory")
public class DormitoryController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(DormitoryController.class);
	
	@Resource(name = "dormitoryFacade")
	private Facade<DormitoryResource, Long> facade;
	
	/**
	 * Method for creating new DormitoryResource.
	 * @param dormitoryResource DormitoryResource.
	 * @return DormitoryResource with generated identifier.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Create Dormitory")
	public DormitoryResource createResource(@RequestBody final DormitoryResource dormitoryResource) {
		LOG.info("Creating dormitory: {}", dormitoryResource);
		return facade.createResource(dormitoryResource);
	}
	
	/**
	 * Method for updating DormitoryResource.
	 * @param id DormitoryResource identifier.
	 * @param dormitoryResource updated DormitoryResource.
	 * @return notification of update DormitoryResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	@ApiOperation(value = "Update Dormitory")
	public MessageResource updateResource(@PathVariable("id") final Long id, @RequestBody final DormitoryResource dormitoryResource) {
		LOG.info("Updated dormitory with id: {}, {}", id, dormitoryResource);
		facade.updateResource(id, dormitoryResource);
		return new MessageResource(MessageType.INFO, "Dormitory Updated");
	}
	
	/**
	 * Method for getting DormitoryResource.
	 * @param id DormitoryResource identifier.
	 * @param request request
	 * @return DormitoryResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	@ApiOperation(value = "Get Dormitory by id")
	public DormitoryResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving dormitory with id: {} and request: {}", id, request);
		return facade.getResource(id, request);
	}
	
	/**
	 * Method for removing DormitoryResource.
	 * @param id DormitoryResource identifier id.
	 * @return notification of deletion DormitoryResource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete Dormitory by id")
	public MessageResource removeResource(@PathVariable("id") final Long id) {
		LOG.info("Removing dormitory with id: {}", id);
		facade.removeResource(id);
		return new MessageResource(MessageType.INFO, "Dormitory removed");
	}
	
	/**
	 * Method that returns a page of DormitoryResources.
	 * @param request request.
	 * @return page of DormitoryResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get Dormitories")
	public PagedResultResource<DormitoryResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for Dormitory Resources with request: {}", request);
		return facade.getResources(request);
	}
	
}
