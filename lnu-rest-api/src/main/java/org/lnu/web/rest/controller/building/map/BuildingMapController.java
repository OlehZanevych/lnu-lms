package org.lnu.web.rest.controller.building.map;

import javax.annotation.Resource;

import org.lnu.facade.facade.FileFacade;
import org.lnu.json.deserializer.JSONDeserializer;
import org.lnu.resource.building.map.BuildingMapResource;
import org.lnu.resource.message.MessageResource;
import org.lnu.resource.message.MessageType;
import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.PagedResultResource;
import org.lnu.resource.request.Request;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles all API with BuildingMap entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/buildingmaps")
@Api(value = "buildingmaps")
public class BuildingMapController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(BuildingMapController.class);
	
	@Resource(name = "buildingMapFacade")
	private FileFacade<BuildingMapResource> fileFacade;
	
	@Resource(name = "defaultJSONDeserializer")
	private JSONDeserializer jsonDeserializer;
	
	/**
	 * Method for updating BuildingMap file.
	 * @param id BuildingMapResource identifier
	 * @param file multipart file
	 * @return notification of update BuildingMapResource
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.POST)
	@ApiOperation(value = "Update BuildingMap file")
	public MessageResource updateFile(@PathVariable("id") final Long id,
			@RequestParam("file") final MultipartFile file) {
		
		LOG.info("Updating file for Building Map with id: {0}", id);
		
		fileFacade.updateFile(id, file);
		
		return new MessageResource(MessageType.INFO, "Building Map file updated");
	}
	
	/**
	 * Method for creating new BuildingMapResource.
	 * @param jsonContent JSON content String
	 * @param file multipart file
	 * @return BuildingMapResource with generated identifier
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Create BuildingMap")
	public BuildingMapResource createResource(@RequestParam("json") final String jsonContent,
			@RequestParam("file") final MultipartFile file) {
		
		LOG.info("Creating Building Map: {0} and file", jsonContent);
		
		BuildingMapResource buildingMapResource = jsonDeserializer.deserialize(jsonContent,
				BuildingMapResource.class);
		
		buildingMapResource = fileFacade.createResource(buildingMapResource, file);
		
		return buildingMapResource;
	}
	
	/**
	 * Method for updating BuildingMapResource.
	 * @param id BuildingMapResource identifier.
	 * @param buildingMapResource updated BuildingMapResource.
	 * @return notification of update BuildingResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	@ApiOperation(value = "Update BuildingMap")
	public MessageResource updateResource(@PathVariable("id") final Long id,
			@RequestBody final BuildingMapResource buildingMapResource) {
		
		LOG.info("Updating BuildingMap {} with id: {}", buildingMapResource, id);
		fileFacade.updateResource(id, buildingMapResource);
		return new MessageResource(MessageType.INFO, "Building Map updated");
	}
	
	/**
	 * Method for getting BuildingMapResource.
	 * @param id BuildingMapResource identifier.
	 * @param request request
	 * @return BuildingMapResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	@ApiOperation(value = "Get BuildingMap by id")
	public BuildingMapResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving Building Map with id: {} and request: {}", id, request);
		
		return fileFacade.getResource(id, request);
	}
	
	/**
	 * Method for removing BuildingMapResource.
	 * @param id BuildingMapResource identifier id.
	 * @return notification of deletion BuildingMapResource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete BuildingMap by id")
	public MessageResource removeResource(@PathVariable("id") final Long id) {
		LOG.info("Removing Building Map with id: {}", id);
		
		fileFacade.removeResource(id);
		
		return new MessageResource(MessageType.INFO, "BuildingMap removed");
	}
	
	/**
	 * Method that returns a page of BuildingMapResources.
	 * @param request request.
	 * @return page of BuildingMapResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get BuildingMaps")
	public PagedResultResource<BuildingMapResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for Building Map Resources with request: {}", request);
		
		return fileFacade.getResources(request);
	}
	
}
