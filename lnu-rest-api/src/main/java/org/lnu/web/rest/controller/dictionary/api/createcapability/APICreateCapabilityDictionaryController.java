package org.lnu.web.rest.controller.dictionary.api.createcapability;

import java.util.List;

import javax.annotation.Resource;

import org.lnu.facade.dictionary.DictionaryFacade;
import org.lnu.model.enumtype.api.createcapability.APICreateCapability;
import org.lnu.resource.dictionary.DictionaryEntry;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles getting APIs for APICreateCapability dictionary.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/dictionaries/apicreatecapabilities")
@Api(value = "apicreatecapabilities")
public class APICreateCapabilityDictionaryController extends BaseController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(APICreateCapabilityDictionaryController.class);
	
	@Resource(name = "apiCreateCapabilityDictionaryFacade")
	private DictionaryFacade<APICreateCapability> dictionaryFacade;
	
	/**
	 * Method for getting entry from APICreateCapability dictionary by key.
	 * 
	 * @param key key in dictionary
	 * @return entry with key
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.KEY, method = RequestMethod.GET)
	@ApiOperation(value = "Get api create capability entry by key")
	public DictionaryEntry<APICreateCapability> getEntry(@PathVariable("key") final APICreateCapability key) {
		LOGGER.info("Retrieving entry from APICreateCapability dictionary with key {0}", key);
		return dictionaryFacade.getEntry(key);
	}
	
	/**
	 * Method for getting all APICreateCapability dictionary.
	 * 
	 * @return APICreateCapability dictionary
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get all dictionary for api create capabilities")
	public List<DictionaryEntry<APICreateCapability>> getDictionary() {
		LOGGER.info("Retrieving all APICreateCapability dictionary");
		return dictionaryFacade.getDictionary();
	}
	
}
