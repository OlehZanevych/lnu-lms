package org.lnu.web.rest.controller.dictionary.otheruniversityplacetype;

import java.util.List;

import javax.annotation.Resource;

import org.lnu.model.enumtype.otheruniversityplacetype.OtherUniversityPlaceType;
import org.lnu.facade.dictionary.DictionaryFacade;
import org.lnu.resource.dictionary.DictionaryEntry;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles getting APIs for OtherUniversityPlaceType dictionary.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/dictionaries/otheruniversityplacetypes")
@Api(value = "otheruniversityplacetypes")
public class OtherUniversityPlaceTypeDictionaryController extends BaseController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(OtherUniversityPlaceTypeDictionaryController.class);
	
	@Resource(name = "otherUniversityPlaceTypeDictionaryFacade")
	private DictionaryFacade<OtherUniversityPlaceType> dictionaryFacade;
	
	/**
	 * Method for getting entry from OtherUniversityPlaceType dictionary by key.
	 * 
	 * @param key key in dictionary
	 * @return entry with key
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.KEY, method = RequestMethod.GET)
	@ApiOperation(value = "Get other university place type entry by key")
	public DictionaryEntry<OtherUniversityPlaceType> getEntry(@PathVariable("key") final OtherUniversityPlaceType key) {
		LOGGER.info("Retrieving entry from OtherUniversityPlaceType dictionary with key {0}", key);
		return dictionaryFacade.getEntry(key);
	}
	
	/**
	 * Method for getting all OtherUniversityPlaceType dictionary.
	 * 
	 * @return OtherUniversityPlaceType dictionary
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get all dictionary for other university place types")
	public List<DictionaryEntry<OtherUniversityPlaceType>> getDictionary() {
		LOGGER.info("Retrieving all OtherUniversityPlaceType dictionary");
		return dictionaryFacade.getDictionary();
	}
	
}
