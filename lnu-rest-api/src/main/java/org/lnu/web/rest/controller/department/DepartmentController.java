package org.lnu.web.rest.controller.department;

import javax.annotation.Resource;

import org.lnu.facade.facade.Facade;
import org.lnu.resource.department.DepartmentResource;
import org.lnu.resource.message.MessageResource;
import org.lnu.resource.message.MessageType;
import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.PagedResultResource;
import org.lnu.resource.request.Request;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles all API with Department entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/departments")
@Api(value = "department")
public class DepartmentController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(DepartmentController.class);
	
	@Resource(name = "departmentFacade")
	private Facade<DepartmentResource, Long> facade;
	
	/**
	 * Method for creating new DepartmentResource.
	 * @param departmentResource DepartmentResource.
	 * @return DepartmentResource with generated identifier.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Create Department")
	public DepartmentResource createResource(@RequestBody final DepartmentResource departmentResource) {
		LOG.info("Creating department: {}", departmentResource);
		return facade.createResource(departmentResource);
	}
	
	/**
	 * Method for updating DepartmentResource.
	 * @param id DepartmentResource identifier.
	 * @param departmentResource updated DepartmentResource.
	 * @return notification of update DepartmentResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	@ApiOperation(value = "Update Department")
	public MessageResource updateResource(@PathVariable("id") final Long id, @RequestBody final DepartmentResource departmentResource) {
		LOG.info("Updated department with id: {}, {}", id, departmentResource);
		facade.updateResource(id, departmentResource);
		return new MessageResource(MessageType.INFO, "Department Updated");
	}
	
	/**
	 * Method for getting DepartmentResource.
	 * @param id DepartmentResource identifier.
	 * @param request request
	 * @return DepartmentResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	@ApiOperation(value = "Get Department by id")
	public DepartmentResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving department with id: {} and request: {}", id, request);
		return facade.getResource(id, request);
	}
	
	/**
	 * Method for removing DepartmentResource.
	 * @param id DepartmentResource identifier id.
	 * @return notification of deletion DepartmentResource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete Department by id")
	public MessageResource removeResource(@PathVariable("id") final Long id) {
		LOG.info("Removing department with id: {}", id);
		facade.removeResource(id);
		return new MessageResource(MessageType.INFO, "Department removed");
	}
	
	/**
	 * Method that returns a page of DepartmentResources.
	 * @param request request.
	 * @return page of DepartmentResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get Departments")
	public PagedResultResource<DepartmentResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for Department Resources with request: {}", request);
		return facade.getResources(request);
	}
	
}
