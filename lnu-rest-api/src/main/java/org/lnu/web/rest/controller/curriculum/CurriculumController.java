package org.lnu.web.rest.controller.curriculum;

import javax.annotation.Resource;

import org.lnu.facade.facade.Facade;
import org.lnu.resource.curriculum.CurriculumResource;
import org.lnu.resource.message.MessageResource;
import org.lnu.resource.message.MessageType;
import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.PagedResultResource;
import org.lnu.resource.request.Request;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles all API with Curriculum entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/curricula")
@Api(value = "curriculum")
public class CurriculumController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(CurriculumController.class);
	
	@Resource(name = "curriculumFacade")
	private Facade<CurriculumResource, Long> facade;
	
	/**
	 * Method for creating new CurriculumResource.
	 * @param curriculumResource CurriculumResource.
	 * @return CurriculumResource with generated identifier.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Create Curriculum")
	public CurriculumResource createResource(@RequestBody final CurriculumResource curriculumResource) {
		LOG.info("Creating curriculum: {}", curriculumResource);
		return facade.createResource(curriculumResource);
	}
	
	/**
	 * Method for updating CurriculumResource.
	 * @param id CurriculumResource identifier.
	 * @param curriculumResource updated CurriculumResource.
	 * @return notification of update CurriculumResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	@ApiOperation(value = "Update Curriculum")
	public MessageResource updateResource(@PathVariable("id") final Long id, @RequestBody final CurriculumResource curriculumResource) {
		LOG.info("Updated curriculum with id: {}, {}", id, curriculumResource);
		facade.updateResource(id, curriculumResource);
		return new MessageResource(MessageType.INFO, "Curriculum Updated");
	}
	
	/**
	 * Method for getting CurriculumResource.
	 * @param id CurriculumResource identifier.
	 * @param request request
	 * @return CurriculumResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	@ApiOperation(value = "Get Curriculum by id")
	public CurriculumResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving curriculum with id: {} and request: {}", id, request);
		return facade.getResource(id, request);
	}
	
	/**
	 * Method for removing CurriculumResource.
	 * @param id CurriculumResource identifier id.
	 * @return notification of deletion CurriculumResource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete Curriculum by id")
	public MessageResource removeResource(@PathVariable("id") final Long id) {
		LOG.info("Removing curriculum with id: {}", id);
		facade.removeResource(id);
		return new MessageResource(MessageType.INFO, "Curriculum removed");
	}
	
	/**
	 * Method that returns a page of CurriculumResources.
	 * @param request request.
	 * @return page of CurriculumResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get Curricula")
	public PagedResultResource<CurriculumResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for Curriculum Resources with request: {}", request);
		return facade.getResources(request);
	}
	
}
