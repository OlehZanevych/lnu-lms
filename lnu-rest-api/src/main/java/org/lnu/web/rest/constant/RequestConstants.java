package org.lnu.web.rest.constant;

/**
 * Class for controller requests.
 * @author OlehZanevych
 */
public class RequestConstants {

	public static final String ID = "/{id}";
	public static final String KEY = "/{key}";
	
}
