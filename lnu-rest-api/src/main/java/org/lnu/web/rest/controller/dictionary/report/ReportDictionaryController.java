package org.lnu.web.rest.controller.dictionary.report;

import java.util.List;

import javax.annotation.Resource;

import org.lnu.model.enumtype.report.Report;
import org.lnu.facade.dictionary.DictionaryFacade;
import org.lnu.resource.dictionary.DictionaryEntry;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles getting APIs for Report dictionary.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/dictionaries/reports")
@Api(value = "reports")
public class ReportDictionaryController extends BaseController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ReportDictionaryController.class);
	
	@Resource(name = "reportDictionaryFacade")
	private DictionaryFacade<Report> dictionaryFacade;
	
	/**
	 * Method for getting entry from Report dictionary by key.
	 * 
	 * @param key key in dictionary
	 * @return entry with key
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.KEY, method = RequestMethod.GET)
	@ApiOperation(value = "Get report entry by key")
	public DictionaryEntry<Report> getEntry(@PathVariable("key") final Report key) {
		LOGGER.info("Retrieving entry from Report dictionary with key {0}", key);
		return dictionaryFacade.getEntry(key);
	}
	
	/**
	 * Method for getting all Report dictionary.
	 * 
	 * @return Report dictionary
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get all dictionary for reports")
	public List<DictionaryEntry<Report>> getDictionary() {
		LOGGER.info("Retrieving all Report dictionary");
		return dictionaryFacade.getDictionary();
	}
	
}
