package org.lnu.web.rest.controller.dictionary.learningplacetype;

import java.util.List;

import javax.annotation.Resource;

import org.lnu.model.enumtype.learningplacetype.LearningPlaceType;
import org.lnu.facade.dictionary.DictionaryFacade;
import org.lnu.resource.dictionary.DictionaryEntry;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles getting APIs for LearningPlaceType dictionary.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/dictionaries/learningplacetypes")
@Api(value = "learningplacetypes")
public class LearningPlaceTypeDictionaryController extends BaseController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LearningPlaceTypeDictionaryController.class);
	
	@Resource(name = "learningPlaceTypeDictionaryFacade")
	private DictionaryFacade<LearningPlaceType> dictionaryFacade;
	
	/**
	 * Method for getting entry from LearningPlaceType dictionary by key.
	 * 
	 * @param key key in dictionary
	 * @return entry with key
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.KEY, method = RequestMethod.GET)
	@ApiOperation(value = "Get Learning Place Type entry by key")
	public DictionaryEntry<LearningPlaceType> getEntry(@PathVariable("key") final LearningPlaceType key) {
		LOGGER.info("Retrieving entry from LearningPlaceType dictionary by key {0}", key);
		return dictionaryFacade.getEntry(key);
	}
	
	/**
	 * Method for getting all LearningPlaceType dictionary.
	 * 
	 * @return LearningPlaceType dictionary
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get all dictionary for Learning Place Types")
	public List<DictionaryEntry<LearningPlaceType>> getDictionary() {
		LOGGER.info("Retrieving all LearningPlaceType dictionary");
		return dictionaryFacade.getDictionary();
	}
	
}
