package org.lnu.web.rest.controller.dictionary.educationaldegree;

import java.util.List;

import javax.annotation.Resource;

import org.lnu.model.enumtype.educationaldegree.EducationalDegree;
import org.lnu.facade.dictionary.DictionaryFacade;
import org.lnu.resource.dictionary.DictionaryEntry;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles getting APIs for EducationalDegree dictionary.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/dictionaries/educationaldegrees")
@Api(value = "educationaldegrees")
public class EducationalDegreeDictionaryController extends BaseController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EducationalDegreeDictionaryController.class);
	
	@Resource(name = "educationalDegreeDictionaryFacade")
	private DictionaryFacade<EducationalDegree> dictionaryFacade;
	
	/**
	 * Method for getting entry from EducationalDegree dictionary by key.
	 * 
	 * @param key key in dictionary
	 * @return entry with key
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.KEY, method = RequestMethod.GET)
	@ApiOperation(value = "Get educational degree entry by key")
	public DictionaryEntry<EducationalDegree> getEntry(@PathVariable("key") final EducationalDegree key) {
		LOGGER.info("Retrieving entry from EducationalDegree dictionary with key {0}", key);
		return dictionaryFacade.getEntry(key);
	}
	
	/**
	 * Method for getting all EducationalDegree dictionary.
	 * 
	 * @return EducationalDegree dictionary
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get all dictionary for educational degrees")
	public List<DictionaryEntry<EducationalDegree>> getDictionary() {
		LOGGER.info("Retrieving all EducationalDegree dictionary");
		return dictionaryFacade.getDictionary();
	}
	
}
