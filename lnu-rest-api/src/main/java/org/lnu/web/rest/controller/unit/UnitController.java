package org.lnu.web.rest.controller.unit;

import javax.annotation.Resource;

import org.lnu.facade.facade.Facade;
import org.lnu.resource.unit.UnitResource;
import org.lnu.resource.message.MessageResource;
import org.lnu.resource.message.MessageType;
import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.PagedResultResource;
import org.lnu.resource.request.Request;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles all API with Unit entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/units")
@Api(value = "unit")
public class UnitController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(UnitController.class);
	
	@Resource(name = "unitFacade")
	private Facade<UnitResource, Long> facade;
	
	/**
	 * Method for creating new UnitResource.
	 * @param unitResource UnitResource.
	 * @return UnitResource with generated identifier.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Create Unit")
	public UnitResource createResource(@RequestBody final UnitResource unitResource) {
		LOG.info("Creating unit: {}", unitResource);
		return facade.createResource(unitResource);
	}
	
	/**
	 * Method for updating UnitResource.
	 * @param id UnitResource identifier.
	 * @param unitResource updated UnitResource.
	 * @return notification of update UnitResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	@ApiOperation(value = "Update Unit")
	public MessageResource updateResource(@PathVariable("id") final Long id, @RequestBody final UnitResource unitResource) {
		LOG.info("Updated unit with id: {}, {}", id, unitResource);
		facade.updateResource(id, unitResource);
		return new MessageResource(MessageType.INFO, "Unit Updated");
	}
	
	/**
	 * Method for getting UnitResource.
	 * @param id UnitResource identifier.
	 * @param request request
	 * @return UnitResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	@ApiOperation(value = "Get Unit by id")
	public UnitResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving unit with id: {} and request: {}", id, request);
		return facade.getResource(id, request);
	}
	
	/**
	 * Method for removing UnitResource.
	 * @param id UnitResource identifier id.
	 * @return notification of deletion UnitResource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete Unit by id")
	public MessageResource removeResource(@PathVariable("id") final Long id) {
		LOG.info("Removing unit with id: {}", id);
		facade.removeResource(id);
		return new MessageResource(MessageType.INFO, "Unit removed");
	}
	
	/**
	 * Method that returns a page of UnitResources.
	 * @param request request.
	 * @return page of UnitResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get Units")
	public PagedResultResource<UnitResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for Unit Resources with request: {}", request);
		return facade.getResources(request);
	}
	
}
