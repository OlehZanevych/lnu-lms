package org.lnu.web.rest.controller.lecturer;

import javax.annotation.Resource;

import org.lnu.facade.facade.Facade;
import org.lnu.resource.lecturer.LecturerResource;
import org.lnu.resource.message.MessageResource;
import org.lnu.resource.message.MessageType;
import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.PagedResultResource;
import org.lnu.resource.request.Request;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles all API with Lecturer entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/lecturers")
@Api(value = "lecturer")
public class LecturerController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(LecturerController.class);
	
	@Resource(name = "lecturerFacade")
	private Facade<LecturerResource, Long> facade;
	
	/**
	 * Method for creating new LecturerResource.
	 * @param lecturerResource LecturerResource.
	 * @return LecturerResource with generated identifier.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Create Lecturer")
	public LecturerResource createResource(@RequestBody final LecturerResource lecturerResource) {
		LOG.info("Creating lecturer: {}", lecturerResource);
		return facade.createResource(lecturerResource);
	}
	
	/**
	 * Method for updating LecturerResource.
	 * @param id LecturerResource identifier.
	 * @param lecturerResource updated LecturerResource.
	 * @return notification of update LecturerResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	@ApiOperation(value = "Update Lecturer")
	public MessageResource updateResource(@PathVariable("id") final Long id, @RequestBody final LecturerResource lecturerResource) {
		LOG.info("Updated lecturer with id: {}, {}", id, lecturerResource);
		facade.updateResource(id, lecturerResource);
		return new MessageResource(MessageType.INFO, "Lecturer Updated");
	}
	
	/**
	 * Method for getting LecturerResource.
	 * @param id LecturerResource identifier.
	 * @param request request
	 * @return LecturerResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	@ApiOperation(value = "Get Lecturer by id")
	public LecturerResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving lecturer with id: {} and request: {}", id, request);
		return facade.getResource(id, request);
	}
	
	/**
	 * Method for removing LecturerResource.
	 * @param id LecturerResource identifier id.
	 * @return notification of deletion LecturerResource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete Lecturer by id")
	public MessageResource removeResource(@PathVariable("id") final Long id) {
		LOG.info("Removing lecturer with id: {}", id);
		facade.removeResource(id);
		return new MessageResource(MessageType.INFO, "Lecturer removed");
	}
	
	/**
	 * Method that returns a page of LecturerResources.
	 * @param request request.
	 * @return page of LecturerResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get Lecturers")
	public PagedResultResource<LecturerResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for Lecturer Resources with request: {}", request);
		return facade.getResources(request);
	}
	
}
