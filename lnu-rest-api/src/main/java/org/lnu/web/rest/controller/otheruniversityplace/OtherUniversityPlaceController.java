package org.lnu.web.rest.controller.otheruniversityplace;

import javax.annotation.Resource;

import org.lnu.facade.facade.Facade;
import org.lnu.resource.message.MessageResource;
import org.lnu.resource.message.MessageType;
import org.lnu.resource.otheruniversityplace.OtherUniversityPlaceResource;
import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.PagedResultResource;
import org.lnu.resource.request.Request;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles all API with OtherUniversityPlace entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/otheruniversityplaces")
@Api(value = "otheruniversityplaces")
public class OtherUniversityPlaceController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(OtherUniversityPlaceController.class);
	
	@Resource(name = "otherUniversityPlaceFacade")
	private Facade<OtherUniversityPlaceResource, Long> facade;
	
	/**
	 * Method for creating new OtherUniversityPlaceResource.
	 * @param otherUniversityPlaceResource OtherUniversityPlaceResource.
	 * @return OtherUniversityPlaceResource with generated identifier.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Create OtherUniversityPlace")
	public OtherUniversityPlaceResource createResource(@RequestBody final OtherUniversityPlaceResource otherUniversityPlaceResource) {
		LOG.info("Creating OtherUniversityPlace: {}", otherUniversityPlaceResource);
		return facade.createResource(otherUniversityPlaceResource);
	}
	
	/**
	 * Method for updating OtherUniversityPlaceResource.
	 * @param id OtherUniversityPlaceResource identifier.
	 * @param otherUniversityPlaceResource updated OtherUniversityPlaceResource.
	 * @return notification of update OtherUniversityPlaceResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	@ApiOperation(value = "Update OtherUniversityPlace")
	public MessageResource updateResource(@PathVariable("id") final Long id, @RequestBody final OtherUniversityPlaceResource otherUniversityPlaceResource) {
		LOG.info("Updated OtherUniversityPlace with id: {}, {}", id, otherUniversityPlaceResource);
		facade.updateResource(id, otherUniversityPlaceResource);
		return new MessageResource(MessageType.INFO, "OtherUniversityPlace Updated");
	}
	
	/**
	 * Method for getting OtherUniversityPlaceResource.
	 * @param id OtherUniversityPlaceResource identifier.
	 * @param request request
	 * @return OtherUniversityPlaceResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	@ApiOperation(value = "Get OtherUniversityPlace by id")
	public OtherUniversityPlaceResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving OtherUniversityPlace with id: {} and request: {}", id, request);
		return facade.getResource(id, request);
	}
	
	/**
	 * Method for removing OtherUniversityPlaceResource.
	 * @param id OtherUniversityPlaceResource identifier id.
	 * @return notification of deletion OtherUniversityPlaceResource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete OtherUniversityPlace by id")
	public MessageResource removeResource(@PathVariable("id") final Long id) {
		LOG.info("Removing OtherUniversityPlace with id: {}", id);
		facade.removeResource(id);
		return new MessageResource(MessageType.INFO, "OtherUniversityPlace removed");
	}
	
	/**
	 * Method that returns a page of OtherUniversityPlaceResources.
	 * @param request request.
	 * @return page of OtherUniversityPlaceResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get OtherUniversityPlaces")
	public PagedResultResource<OtherUniversityPlaceResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for OtherUniversityPlace Resources with request: {}", request);
		return facade.getResources(request);
	}
	
}
