package org.lnu.web.rest.controller.specialty;

import javax.annotation.Resource;

import org.lnu.facade.facade.Facade;
import org.lnu.resource.specialty.SpecialtyResource;
import org.lnu.resource.message.MessageResource;
import org.lnu.resource.message.MessageType;
import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.PagedResultResource;
import org.lnu.resource.request.Request;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles all API with Specialty entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/specialties")
@Api(value = "specialty")
public class SpecialtyController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(SpecialtyController.class);
	
	@Resource(name = "specialtyFacade")
	private Facade<SpecialtyResource, Long> facade;
	
	/**
	 * Method for creating new SpecialtyResource.
	 * @param specialtyResource SpecialtyResource.
	 * @return SpecialtyResource with generated identifier.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Create Specialty")
	public SpecialtyResource createResource(@RequestBody final SpecialtyResource specialtyResource) {
		LOG.info("Creating specialty: {}", specialtyResource);
		return facade.createResource(specialtyResource);
	}
	
	/**
	 * Method for updating SpecialtyResource.
	 * @param id SpecialtyResource identifier.
	 * @param specialtyResource updated SpecialtyResource.
	 * @return notification of update SpecialtyResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	@ApiOperation(value = "Update Specialty")
	public MessageResource updateResource(@PathVariable("id") final Long id, @RequestBody final SpecialtyResource specialtyResource) {
		LOG.info("Updated specialty with id: {}, {}", id, specialtyResource);
		facade.updateResource(id, specialtyResource);
		return new MessageResource(MessageType.INFO, "Specialty Updated");
	}
	
	/**
	 * Method for getting SpecialtyResource.
	 * @param id SpecialtyResource identifier.
	 * @param request request
	 * @return SpecialtyResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	@ApiOperation(value = "Get Specialty by id")
	public SpecialtyResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving specialty with id: {} and request: {}", id, request);
		return facade.getResource(id, request);
	}
	
	/**
	 * Method for removing SpecialtyResource.
	 * @param id SpecialtyResource identifier id.
	 * @return notification of deletion SpecialtyResource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete Specialty by id")
	public MessageResource removeResource(@PathVariable("id") final Long id) {
		LOG.info("Removing specialty with id: {}", id);
		facade.removeResource(id);
		return new MessageResource(MessageType.INFO, "Specialty removed");
	}
	
	/**
	 * Method that returns a page of SpecialtyResources.
	 * @param request request.
	 * @return page of SpecialtyResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get Specialties")
	public PagedResultResource<SpecialtyResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for Specialty Resources with request: {}", request);
		return facade.getResources(request);
	}
	
}
