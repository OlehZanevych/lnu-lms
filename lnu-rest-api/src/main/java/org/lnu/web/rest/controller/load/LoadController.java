package org.lnu.web.rest.controller.load;

import javax.annotation.Resource;

import org.lnu.facade.facade.EditFacade;
import org.lnu.resource.load.LoadResource;
import org.lnu.resource.message.MessageResource;
import org.lnu.resource.message.MessageType;
import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.PagedResultResource;
import org.lnu.resource.request.Request;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles all API with Load entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/load")
@Api(value = "load")
public class LoadController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(LoadController.class);
	
	@Resource(name = "loadFacade")
	private EditFacade<LoadResource, Long> facade;
	
	/**
	 * Method for updating LoadResource.
	 * @param id LoadResource identifier.
	 * @param loadResource updated LoadResource.
	 * @return notification of update LoadResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	@ApiOperation(value = "Update Load")
	public MessageResource updateResource(@PathVariable("id") final Long id, @RequestBody final LoadResource loadResource) {
		LOG.info("Updated load with id: {}, {}", id, loadResource);
		facade.updateResource(id, loadResource);
		return new MessageResource(MessageType.INFO, "Load Updated");
	}
	
	/**
	 * Method for getting LoadResource.
	 * @param id LoadResource identifier.
	 * @param request request
	 * @return LoadResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	@ApiOperation(value = "Get Load by id")
	public LoadResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving load with id: {} and request: {}", id, request);
		return facade.getResource(id, request);
	}
	
	/**
	 * Method that returns a page of LoadResources.
	 * @param request request.
	 * @return page of LoadResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get Load")
	public PagedResultResource<LoadResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for Load Resources with request: {}", request);
		return facade.getResources(request);
	}
	
}
