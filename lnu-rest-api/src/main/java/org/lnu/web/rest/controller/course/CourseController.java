package org.lnu.web.rest.controller.course;

import javax.annotation.Resource;

import org.lnu.facade.facade.Facade;
import org.lnu.resource.course.CourseResource;
import org.lnu.resource.message.MessageResource;
import org.lnu.resource.message.MessageType;
import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.PagedResultResource;
import org.lnu.resource.request.Request;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles all API with Course entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/courses")
@Api(value = "course", description = "Courses")
public class CourseController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(CourseController.class);
	
	@Resource(name = "courseFacade")
	private Facade<CourseResource, Long> facade;
	
	/**
	 * Method for creating new CourseResource.
	 * @param courseResource CourseResource.
	 * @return CourseResource with generated identifier.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Create Course")
	public CourseResource createResource(@RequestBody final CourseResource courseResource) {
		LOG.info("Creating course: {}", courseResource);
		return facade.createResource(courseResource);
	}
	
	/**
	 * Method for updating CourseResource.
	 * @param id CourseResource identifier.
	 * @param courseResource updated CourseResource.
	 * @return notification of update CourseResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	@ApiOperation(value = "Update Course")
	public MessageResource updateResource(@PathVariable("id") final Long id, @RequestBody final CourseResource courseResource) {
		LOG.info("Updated course with id: {}, {}", id, courseResource);
		facade.updateResource(id, courseResource);
		return new MessageResource(MessageType.INFO, "Course Updated");
	}
	
	/**
	 * Method for getting CourseResource.
	 * @param id CourseResource identifier.
	 * @param request request
	 * @return CourseResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	@ApiOperation(value = "Get Course by id")
	public CourseResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving course with id: {} and request: {}", id, request);
		return facade.getResource(id, request);
	}
	
	/**
	 * Method for removing CourseResource.
	 * @param id CourseResource identifier id.
	 * @return notification of deletion CourseResource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete Course by id")
	public MessageResource removeResource(@PathVariable("id") final Long id) {
		LOG.info("Removing course with id: {}", id);
		facade.removeResource(id);
		return new MessageResource(MessageType.INFO, "Course removed");
	}
	
	/**
	 * Method that returns a page of CourseResources.
	 * @param request request.
	 * @return page of CourseResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get Courses")
	public PagedResultResource<CourseResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for Course Resources with request: {}", request);
		return facade.getResources(request);
	}
	
}
