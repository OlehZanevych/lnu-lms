package org.lnu.web.rest.controller.dictionary.api.capability.level;

import java.util.List;

import javax.annotation.Resource;

import org.lnu.facade.dictionary.DictionaryFacade;
import org.lnu.model.enumtype.api.capability.level.APICapabilityLevel;
import org.lnu.resource.dictionary.DictionaryEntry;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles getting APIs for APICapabilityLevel dictionary.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/dictionaries/apicapabilitylevels")
@Api(value = "apicapabilitylevels")
public class APICapabilityLevelDictionaryController extends BaseController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(APICapabilityLevelDictionaryController.class);
	
	@Resource(name = "apiCapabilityLevelDictionaryFacade")
	private DictionaryFacade<APICapabilityLevel> dictionaryFacade;
	
	/**
	 * Method for getting entry from APICapabilityLevel dictionary by key.
	 * 
	 * @param key key in dictionary
	 * @return entry with key
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.KEY, method = RequestMethod.GET)
	@ApiOperation(value = "Get api capability level entry by key")
	public DictionaryEntry<APICapabilityLevel> getEntry(@PathVariable("key") final APICapabilityLevel key) {
		LOGGER.info("Retrieving entry from APICapabilityLevel dictionary with key {0}", key);
		return dictionaryFacade.getEntry(key);
	}
	
	/**
	 * Method for getting all APICapabilityLevel dictionary.
	 * 
	 * @return APICapabilityLevel dictionary
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get all dictionary for api capability levels")
	public List<DictionaryEntry<APICapabilityLevel>> getDictionary() {
		LOGGER.info("Retrieving all APICapabilityLevel dictionary");
		return dictionaryFacade.getDictionary();
	}
	
}
