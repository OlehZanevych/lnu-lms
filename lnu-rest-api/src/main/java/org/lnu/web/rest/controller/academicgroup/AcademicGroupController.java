package org.lnu.web.rest.controller.academicgroup;

import javax.annotation.Resource;

import org.lnu.facade.facade.Facade;
import org.lnu.resource.academicgroup.AcademicGroupResource;
import org.lnu.resource.message.MessageResource;
import org.lnu.resource.message.MessageType;
import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.PagedResultResource;
import org.lnu.resource.request.Request;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles all API with AcademicGroup entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/academicgroups")
@Api(description = "CRUD")
public class AcademicGroupController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(AcademicGroupController.class);
	
	@Resource(name = "academicGroupFacade")
	private Facade<AcademicGroupResource, Long> facade;
	
	/**
	 * Method for creating new AcademicGroupResource.
	 * @param academicGroupResource AcademicGroupResource
	 * @return AcademicGroupResource with generated identifier
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Create AcademicGroup")
	public AcademicGroupResource createResource(@RequestBody final AcademicGroupResource academicGroupResource) {
		LOG.info("Creating AcademicGroup: {}", academicGroupResource);
		return facade.createResource(academicGroupResource);
	}
	
	/**
	 * Method for updating AcademicGroupResource.
	 * @param id AcademicGroupResource identifier
	 * @param academicGroupResource updated AcademicGroupResource
	 * @return notification of update AcademicGroupResource
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	@ApiOperation(value = "Update AcademicGroup")
	public MessageResource updateResource(@PathVariable("id") final Long id, @RequestBody final AcademicGroupResource academicGroupResource) {
		LOG.info("Updated AcademicGroup with id: {}, {}", id, academicGroupResource);
		facade.updateResource(id, academicGroupResource);
		return new MessageResource(MessageType.INFO, "AcademicGroup Updated");
	}
	
	/**
	 * Method for getting AcademicGroupResource.
	 * @param id AcademicGroupResource identifier
	 * @param request request
	 * @return AcademicGroupResource
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	@ApiOperation(value = "Get AcademicGroup by id")
	public AcademicGroupResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving AcademicGroup with id: {} and request: {}", id, request);
		return facade.getResource(id, request);
	}
	
	/**
	 * Method for removing AcademicGroupResource.
	 * @param id AcademicGroupResource identifier id
	 * @return notification of deletion AcademicGroupResource
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete AcademicGroup by id")
	public MessageResource removeResource(@PathVariable("id") final Long id) {
		LOG.info("Removing AcademicGroup with id: {}", id);
		facade.removeResource(id);
		return new MessageResource(MessageType.INFO, "AcademicGroup removed");
	}
	
	/**
	 * Method that returns a page of AcademicGroupResources.
	 * @param request request
	 * @return page of AcademicGroupResources
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get AcademicGroups")
	public PagedResultResource<AcademicGroupResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for AcademicGroup Resources with request: {}", request);
		return facade.getResources(request);
	}
	
}
