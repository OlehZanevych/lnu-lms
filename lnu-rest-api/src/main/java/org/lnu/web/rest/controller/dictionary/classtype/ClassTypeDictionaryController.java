package org.lnu.web.rest.controller.dictionary.classtype;

import java.util.List;

import javax.annotation.Resource;

import org.lnu.model.enumtype.classtype.ClassType;
import org.lnu.facade.dictionary.DictionaryFacade;
import org.lnu.resource.dictionary.DictionaryEntry;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles getting APIs for ClassType dictionary.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/dictionaries/classtypes")
@Api(value = "classtypes")
public class ClassTypeDictionaryController extends BaseController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ClassTypeDictionaryController.class);
	
	@Resource(name = "classTypeDictionaryFacade")
	private DictionaryFacade<ClassType> dictionaryFacade;
	
	/**
	 * Method for getting entry from ClassType dictionary by key.
	 * 
	 * @param key key in dictionary
	 * @return entry with key
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.KEY, method = RequestMethod.GET)
	@ApiOperation(value = "Get class type entry by key")
	public DictionaryEntry<ClassType> getEntry(@PathVariable("key") final ClassType key) {
		LOGGER.info("Retrieving entry from ClassType dictionary with key {0}", key);
		return dictionaryFacade.getEntry(key);
	}
	
	/**
	 * Method for getting all ClassType dictionary.
	 * 
	 * @return ClassType dictionary
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get all dictionary for class types")
	public List<DictionaryEntry<ClassType>> getDictionary() {
		LOGGER.info("Retrieving all ClassType dictionary");
		return dictionaryFacade.getDictionary();
	}
	
}
