package org.lnu.web.rest.controller.dictionary.weekperiodicity;

import java.util.List;

import javax.annotation.Resource;

import org.lnu.model.enumtype.weekperiodicity.WeekPeriodicity;
import org.lnu.facade.dictionary.DictionaryFacade;
import org.lnu.resource.dictionary.DictionaryEntry;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles getting APIs for WeekPeriodicity dictionary.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/dictionaries/weekperiodicities")
@Api(value = "weekperiodicities")
public class WeekPeriodicityDictionaryController extends BaseController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(WeekPeriodicityDictionaryController.class);
	
	@Resource(name = "weekPeriodicityDictionaryFacade")
	private DictionaryFacade<WeekPeriodicity> dictionaryFacade;
	
	/**
	 * Method for getting entry from WeekPeriodicity dictionary by key.
	 * 
	 * @param key key in dictionary
	 * @return entry with key
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.KEY, method = RequestMethod.GET)
	@ApiOperation(value = "Get week periodicity entry by key")
	public DictionaryEntry<WeekPeriodicity> getEntry(@PathVariable("key") final WeekPeriodicity key) {
		LOGGER.info("Retrieving entry from WeekPeriodicity dictionary with key {0}", key);
		return dictionaryFacade.getEntry(key);
	}
	
	/**
	 * Method for getting all WeekPeriodicity dictionary.
	 * 
	 * @return WeekPeriodicity dictionary
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get all dictionary for week periodicities")
	public List<DictionaryEntry<WeekPeriodicity>> getDictionary() {
		LOGGER.info("Retrieving all WeekPeriodicity dictionary");
		return dictionaryFacade.getDictionary();
	}
	
}
