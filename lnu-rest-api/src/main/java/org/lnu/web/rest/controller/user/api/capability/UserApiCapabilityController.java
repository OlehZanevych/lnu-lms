package org.lnu.web.rest.controller.user.api.capability;

import javax.annotation.Resource;

import org.lnu.facade.facade.ReadFacade;
import org.lnu.model.enumtype.api.API;
import org.lnu.model.id.user.api.UserApiId;
import org.lnu.model.user.User;
import org.lnu.resource.user.api.capability.UserApiCapabilityResource;
import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.PagedResultResource;
import org.lnu.resource.request.Request;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles all API with UserApiCapability entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/apicapabilities")
@Api(value = "apicapabilities")
public class UserApiCapabilityController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(UserApiCapabilityController.class);
	
	@Resource(name = "userApiCapabilityFacade")
	private ReadFacade<UserApiCapabilityResource, UserApiId> facade;
	
	/**
	 * Method for getting UserApiCapabilityResource.
	 * @param userId User identifier
	 * @param api API
	 * @param request request
	 * @return UserApiCapabilityResource
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/{userId}&{api}", method = RequestMethod.GET)
	@ApiOperation(value = "Get UserApiCapability by id")
	public UserApiCapabilityResource getResource(@PathVariable("userId") final Long userId,
			@PathVariable("api") final API api, final Request request) {
		LOG.info("Retrieving userApiCapability with userId: {}, api: {} and request: {}", userId, api, request);
		return facade.getResource(new UserApiId(new User(userId), api), request);
	}
	
	/**
	 * Method that returns a page of UserApiCapabilityResources.
	 * @param request request
	 * @return page of UserApiCapabilityResources
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get UserAPICapabilities")
	public PagedResultResource<UserApiCapabilityResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for UserApiCapability Resources with request: {}", request);
		return facade.getResources(request);
	}
	
}
