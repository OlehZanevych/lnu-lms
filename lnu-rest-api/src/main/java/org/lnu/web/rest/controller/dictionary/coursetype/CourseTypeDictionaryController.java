package org.lnu.web.rest.controller.dictionary.coursetype;

import java.util.List;

import javax.annotation.Resource;

import org.lnu.model.enumtype.coursetype.CourseType;
import org.lnu.facade.dictionary.DictionaryFacade;
import org.lnu.resource.dictionary.DictionaryEntry;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles getting APIs for CourseType dictionary.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/dictionaries/coursetypes")
@Api(value = "coursetypes")
public class CourseTypeDictionaryController extends BaseController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CourseTypeDictionaryController.class);
	
	@Resource(name = "courseTypeDictionaryFacade")
	private DictionaryFacade<CourseType> dictionaryFacade;
	
	/**
	 * Method for getting entry from CourseType dictionary by key.
	 * 
	 * @param key key in dictionary
	 * @return entry with key
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.KEY, method = RequestMethod.GET)
	@ApiOperation(value = "Get course type entry by key")
	public DictionaryEntry<CourseType> getEntry(@PathVariable("key") final CourseType key) {
		LOGGER.info("Retrieving entry from CourseType dictionary with key {0}", key);
		return dictionaryFacade.getEntry(key);
	}
	
	/**
	 * Method for getting all CourseType dictionary.
	 * 
	 * @return CourseType dictionary
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get all dictionary for course types")
	public List<DictionaryEntry<CourseType>> getDictionary() {
		LOGGER.info("Retrieving all CourseType dictionary");
		return dictionaryFacade.getDictionary();
	}
	
}
