package org.lnu.web.rest.controller.scientificdegree;

import javax.annotation.Resource;

import org.lnu.facade.facade.Facade;
import org.lnu.resource.scientificdegree.ScientificDegreeResource;
import org.lnu.resource.message.MessageResource;
import org.lnu.resource.message.MessageType;
import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.PagedResultResource;
import org.lnu.resource.request.Request;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles all API with ScientificDegree entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/scientificdegrees")
@Api(value = "scientificDegree")
public class ScientificDegreeController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(ScientificDegreeController.class);
	
	@Resource(name = "scientificDegreeFacade")
	private Facade<ScientificDegreeResource, Long> facade;
	
	/**
	 * Method for creating new ScientificDegreeResource.
	 * @param scientificDegreeResource ScientificDegreeResource.
	 * @return ScientificDegreeResource with generated identifier.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Create ScientificDegree")
	public ScientificDegreeResource createResource(@RequestBody final ScientificDegreeResource scientificDegreeResource) {
		LOG.info("Creating scientificDegree: {}", scientificDegreeResource);
		return facade.createResource(scientificDegreeResource);
	}
	
	/**
	 * Method for updating ScientificDegreeResource.
	 * @param id ScientificDegreeResource identifier.
	 * @param scientificDegreeResource updated ScientificDegreeResource.
	 * @return notification of update ScientificDegreeResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	@ApiOperation(value = "Update ScientificDegree")
	public MessageResource updateResource(@PathVariable("id") final Long id, @RequestBody final ScientificDegreeResource scientificDegreeResource) {
		LOG.info("Updated ScientificDegree with id: {}, {}", id, scientificDegreeResource);
		facade.updateResource(id, scientificDegreeResource);
		return new MessageResource(MessageType.INFO, "ScientificDegree Updated");
	}
	
	/**
	 * Method for getting ScientificDegreeResource.
	 * @param id ScientificDegreeResource identifier.
	 * @param request request
	 * @return ScientificDegreeResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	@ApiOperation(value = "Get ScientificDegree by id")
	public ScientificDegreeResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving ScientificDegree with id: {} and request: {}", id, request);
		return facade.getResource(id, request);
	}
	
	/**
	 * Method for removing ScientificDegreeResource.
	 * @param id ScientificDegreeResource identifier id.
	 * @return notification of deletion ScientificDegreeResource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete ScientificDegree by id")
	public MessageResource removeResource(@PathVariable("id") final Long id) {
		LOG.info("Removing ScientificDegree with id: {}", id);
		facade.removeResource(id);
		return new MessageResource(MessageType.INFO, "ScientificDegree removed");
	}
	
	/**
	 * Method that returns a page of ScientificDegreeResources.
	 * @param request request.
	 * @return page of ScientificDegreeResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get ScientificDegrees")
	public PagedResultResource<ScientificDegreeResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for ScientificDegree Resources with request: {}", request);
		return facade.getResources(request);
	}
	
}
