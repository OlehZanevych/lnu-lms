package org.lnu.web.rest.processor.resolver;

import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.Request;
import org.lnu.web.exception.InvalidOrderByException;
import org.lnu.web.rest.constant.Constants;
import org.lnu.web.rest.processor.resolver.parameters.ParametersRetriever;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.annotation.Resource;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Paged request argument resolver.
 * @author OlehZanevych
 */
@Component("requestHandlerMethodArgumentResolver")
public class RequestHandlerMethodArgumentResolver implements HandlerMethodArgumentResolver {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RequestHandlerMethodArgumentResolver.class);

	@Resource(name = "parametersRetriever")
	private ParametersRetriever parametersRetriever;
	
	@Resource(name = "orderByPattern")
	private Pattern orderByPattern;

	@Override
	public boolean supportsParameter(final MethodParameter parameter) {
		return Request.class.isAssignableFrom(parameter.getParameterType());
	}

	@Override
	public Object resolveArgument(final MethodParameter parameter,
			final ModelAndViewContainer mavContainer, final NativeWebRequest webRequest,
			final WebDataBinderFactory binderFactory) throws Exception {
		
		LOGGER.debug("Debugging PagedRequest Parsing:");
		
		Map<String, String> parameters = parametersRetriever.getParameters(webRequest);
		
		String fieldsParameter = parameters.remove(Constants.FIELDS);
		String[] fields = null;
		if (fieldsParameter != null) {
			fieldsParameter = fieldsParameter.replaceAll("\\s", "");
			fields = fieldsParameter.split(",");
		}
		
		Request request = new Request(fields);
		
		if (PagedRequest.class.isAssignableFrom(parameter.getParameterType())) {
			Integer limit = null;
			String limitStr = parameters.remove(Constants.LIMIT);
			if (limitStr != null) {
				limit = Integer.parseInt(limitStr);
			}
			
			Integer offset = null;
			String offsetStr = parameters.remove(Constants.OFFSET);
			if (offsetStr != null) {
				offset = Integer.parseInt(offsetStr);
				if (offset == 0) {
					offset = null;
				}
			}
			
			String ordersParameter = parameters.remove(Constants.ORDER_BY);
			String[] orders = null;
			if (ordersParameter != null) {
				ordersParameter = ordersParameter.replaceAll("\\s", "");
				if (!orderByPattern.matcher(ordersParameter).matches()) {
					throw new InvalidOrderByException(ordersParameter);
				}
				orders = ordersParameter.split(",");
			}
			
			PagedRequest pagedRequest = new PagedRequest(request, offset, limit, parameters, orders);
			return pagedRequest;
		} else {
			return request;
		}
	}

	public void setParametersRetriever(final ParametersRetriever parametersRetriever) {
		this.parametersRetriever = parametersRetriever;
	}

}
