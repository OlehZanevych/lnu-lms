package org.lnu.web.rest.controller.toilet;

import javax.annotation.Resource;

import org.lnu.facade.facade.Facade;
import org.lnu.resource.toilet.ToiletResource;
import org.lnu.resource.message.MessageResource;
import org.lnu.resource.message.MessageType;
import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.PagedResultResource;
import org.lnu.resource.request.Request;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles all API with Toilet entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/toilets")
@Api(value = "toilet")
public class ToiletController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(ToiletController.class);
	
	@Resource(name = "toiletFacade")
	private Facade<ToiletResource, Long> facade;
	
	/**
	 * Method for creating new ToiletResource.
	 * @param toiletResource ToiletResource.
	 * @return ToiletResource with generated identifier.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Create Toilet")
	public ToiletResource createResource(@RequestBody final ToiletResource toiletResource) {
		LOG.info("Creating toilet: {}", toiletResource);
		return facade.createResource(toiletResource);
	}
	
	/**
	 * Method for updating ToiletResource.
	 * @param id ToiletResource identifier.
	 * @param toiletResource updated ToiletResource.
	 * @return notification of update ToiletResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	@ApiOperation(value = "Update Toilet")
	public MessageResource updateResource(@PathVariable("id") final Long id, @RequestBody final ToiletResource toiletResource) {
		LOG.info("Updated toilet with id: {}, {}", id, toiletResource);
		facade.updateResource(id, toiletResource);
		return new MessageResource(MessageType.INFO, "Toilet Updated");
	}
	
	/**
	 * Method for getting ToiletResource.
	 * @param id ToiletResource identifier.
	 * @param request request
	 * @return ToiletResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	@ApiOperation(value = "Get Toilet by id")
	public ToiletResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving toilet with id: {} and request: {}", id, request);
		return facade.getResource(id, request);
	}
	
	/**
	 * Method for removing ToiletResource.
	 * @param id ToiletResource identifier id.
	 * @return notification of deletion ToiletResource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete Toilet by id")
	public MessageResource removeResource(@PathVariable("id") final Long id) {
		LOG.info("Removing toilet with id: {}", id);
		facade.removeResource(id);
		return new MessageResource(MessageType.INFO, "Toilet removed");
	}
	
	/**
	 * Method that returns a page of ToiletResources.
	 * @param request request.
	 * @return page of ToiletResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get Toilets")
	public PagedResultResource<ToiletResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for Toilet Resources with request: {}", request);
		return facade.getResources(request);
	}
	
}
