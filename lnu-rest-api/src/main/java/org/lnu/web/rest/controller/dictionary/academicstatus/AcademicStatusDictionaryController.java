package org.lnu.web.rest.controller.dictionary.academicstatus;

import java.util.List;

import javax.annotation.Resource;

import org.lnu.model.enumtype.academicstatus.AcademicStatus;
import org.lnu.facade.dictionary.DictionaryFacade;
import org.lnu.resource.dictionary.DictionaryEntry;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles getting APIs for AcademicStatus dictionary.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/dictionaries/academicstatuses")
@Api(value = "academicstatuses")
public class AcademicStatusDictionaryController extends BaseController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AcademicStatusDictionaryController.class);
	
	@Resource(name = "academicStatusDictionaryFacade")
	private DictionaryFacade<AcademicStatus> dictionaryFacade;
	
	/**
	 * Method for getting entry from AcademicStatus dictionary by key.
	 * 
	 * @param key key in dictionary
	 * @return entry with key
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.KEY, method = RequestMethod.GET)
	@ApiOperation(value = "Get academic degree entry by key")
	public DictionaryEntry<AcademicStatus> getEntry(@PathVariable("key") final AcademicStatus key) {
		LOGGER.info("Retrieving entry from AcademicStatus dictionary with key {0}", key);
		return dictionaryFacade.getEntry(key);
	}
	
	/**
	 * Method for getting all AcademicStatus dictionary.
	 * 
	 * @return AcademicStatus dictionary
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get all dictionary for academic degrees")
	public List<DictionaryEntry<AcademicStatus>> getDictionary() {
		LOGGER.info("Retrieving all AcademicStatus dictionary");
		return dictionaryFacade.getDictionary();
	}
	
}
