package org.lnu.web.rest.controller.role;

import javax.annotation.Resource;

import org.lnu.facade.facade.Facade;
import org.lnu.resource.role.RoleResource;
import org.lnu.resource.message.MessageResource;
import org.lnu.resource.message.MessageType;
import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.PagedResultResource;
import org.lnu.resource.request.Request;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles all API with Role entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/roles")
@Api(value = "role")
public class RoleController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(RoleController.class);
	
	@Resource(name = "roleFacade")
	private Facade<RoleResource, Long> facade;
	
	/**
	 * Method for creating new RoleResource.
	 * @param roleResource RoleResource.
	 * @return RoleResource with generated identifier.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Create Role")
	public RoleResource createResource(@RequestBody final RoleResource roleResource) {
		LOG.info("Creating role: {}", roleResource);
		return facade.createResource(roleResource);
	}
	
	/**
	 * Method for updating RoleResource.
	 * @param id RoleResource identifier.
	 * @param roleResource updated RoleResource.
	 * @return notification of update RoleResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	@ApiOperation(value = "Update Role")
	public MessageResource updateResource(@PathVariable("id") final Long id, @RequestBody final RoleResource roleResource) {
		LOG.info("Updated role with id: {}, {}", id, roleResource);
		facade.updateResource(id, roleResource);
		return new MessageResource(MessageType.INFO, "Role Updated");
	}
	
	/**
	 * Method for getting RoleResource.
	 * @param id RoleResource identifier.
	 * @param request request
	 * @return RoleResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	@ApiOperation(value = "Get Role by id")
	public RoleResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving role with id: {} and request: {}", id, request);
		return facade.getResource(id, request);
	}
	
	/**
	 * Method for removing RoleResource.
	 * @param id RoleResource identifier id.
	 * @return notification of deletion RoleResource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete Role by id")
	public MessageResource removeResource(@PathVariable("id") final Long id) {
		LOG.info("Removing role with id: {}", id);
		facade.removeResource(id);
		return new MessageResource(MessageType.INFO, "Role removed");
	}
	
	/**
	 * Method that returns a page of RoleResources.
	 * @param request request.
	 * @return page of RoleResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get Roles")
	public PagedResultResource<RoleResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for Role Resources with request: {}", request);
		return facade.getResources(request);
	}
	
}
