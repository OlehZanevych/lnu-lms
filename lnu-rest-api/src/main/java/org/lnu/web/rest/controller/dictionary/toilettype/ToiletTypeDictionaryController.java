package org.lnu.web.rest.controller.dictionary.toilettype;

import java.util.List;

import javax.annotation.Resource;

import org.lnu.model.enumtype.toilettype.ToiletType;
import org.lnu.facade.dictionary.DictionaryFacade;
import org.lnu.resource.dictionary.DictionaryEntry;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles getting APIs for ToiletType dictionary.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/dictionaries/toilettypes")
@Api(value = "toilettypes")
public class ToiletTypeDictionaryController extends BaseController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ToiletTypeDictionaryController.class);
	
	@Resource(name = "toiletTypeDictionaryFacade")
	private DictionaryFacade<ToiletType> dictionaryFacade;
	
	/**
	 * Method for getting entry from ToiletType dictionary by key.
	 * 
	 * @param key key in dictionary
	 * @return entry with key
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.KEY, method = RequestMethod.GET)
	@ApiOperation(value = "Get toilet type entry by key")
	public DictionaryEntry<ToiletType> getEntry(@PathVariable("key") final ToiletType key) {
		LOGGER.info("Retrieving entry from ToiletType dictionary with key {0}", key);
		return dictionaryFacade.getEntry(key);
	}
	
	/**
	 * Method for getting all ToiletType dictionary.
	 * 
	 * @return ToiletType dictionary
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get all dictionary for toilet types")
	public List<DictionaryEntry<ToiletType>> getDictionary() {
		LOGGER.info("Retrieving all ToiletType dictionary");
		return dictionaryFacade.getDictionary();
	}
	
}
