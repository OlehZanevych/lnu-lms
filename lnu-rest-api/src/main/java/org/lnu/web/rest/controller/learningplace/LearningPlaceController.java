package org.lnu.web.rest.controller.learningplace;

import javax.annotation.Resource;

import org.lnu.facade.facade.Facade;
import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.PagedResultResource;
import org.lnu.resource.request.Request;
import org.lnu.resource.learningplace.LearningPlaceResource;
import org.lnu.resource.message.MessageResource;
import org.lnu.resource.message.MessageType;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles all API with LearningPlace entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/learningplaces")
@Api(value = "learningplaces")
public class LearningPlaceController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(LearningPlaceController.class);
	
	@Resource(name = "learningPlaceFacade")
	private Facade<LearningPlaceResource, Long> facade;
	
	/**
	 * Method for creating new LearningPlaceResource.
	 * @param learningPlaceResource LearningPlaceResource.
	 * @return LearningPlaceResource with generated identifier.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Create LearningPlace")
	public LearningPlaceResource createResource(@RequestBody final LearningPlaceResource learningPlaceResource) {
		LOG.info("Creating LearningPlace: {}", learningPlaceResource);
		return facade.createResource(learningPlaceResource);
	}
	
	/**
	 * Method for updating LearningPlaceResource.
	 * @param id LearningPlaceResource identifier.
	 * @param learningPlaceResource updated LearningPlaceResource.
	 * @return notification of update LearningPlaceResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	@ApiOperation(value = "Update LearningPlace")
	public MessageResource updateResource(@PathVariable("id") final Long id, @RequestBody final LearningPlaceResource learningPlaceResource) {
		LOG.info("Updated LearningPlace with id: {}, {}", id, learningPlaceResource);
		facade.updateResource(id, learningPlaceResource);
		return new MessageResource(MessageType.INFO, "LearningPlace Updated");
	}
	
	/**
	 * Method for getting LearningPlaceResource.
	 * @param id LearningPlaceResource identifier.
	 * @param request request
	 * @return LearningPlaceResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	@ApiOperation(value = "Get LearningPlace by id")
	public LearningPlaceResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving LearningPlaceResource with id: {} and request: {}", id, request);
		return facade.getResource(id, request);
	}
	
	/**
	 * Method for removing LearningPlaceResource.
	 * @param id LearningPlaceResource identifier id.
	 * @return notification of deletion LearningPlaceResource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete LearningPlace by id")
	public MessageResource removeResource(@PathVariable("id") final Long id) {
		LOG.info("Removing LearningPlace with id: {}", id);
		facade.removeResource(id);
		return new MessageResource(MessageType.INFO, "LearningPlace removed");
	}
	
	/**
	 * Method that returns a page of LearningPlaceResources.
	 * @param request request.
	 * @return page of LearningPlaceResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get LearningPlaces")
	public PagedResultResource<LearningPlaceResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for LearningPlace Resources with request: {}", request);
		return facade.getResources(request);
	}
	
}
