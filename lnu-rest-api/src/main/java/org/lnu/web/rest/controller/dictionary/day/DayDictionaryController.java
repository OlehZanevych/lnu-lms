package org.lnu.web.rest.controller.dictionary.day;

import java.util.List;

import javax.annotation.Resource;

import org.lnu.model.enumtype.day.Day;
import org.lnu.facade.dictionary.DictionaryFacade;
import org.lnu.resource.dictionary.DictionaryEntry;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles getting APIs for Day dictionary.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/dictionaries/days")
@Api(value = "days")
public class DayDictionaryController extends BaseController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DayDictionaryController.class);
	
	@Resource(name = "dayDictionaryFacade")
	private DictionaryFacade<Day> dictionaryFacade;
	
	/**
	 * Method for getting entry from Day dictionary by key.
	 * 
	 * @param key key in dictionary
	 * @return entry with key
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.KEY, method = RequestMethod.GET)
	@ApiOperation(value = "Get day entry by key")
	public DictionaryEntry<Day> getEntry(@PathVariable("key") final Day key) {
		LOGGER.info("Retrieving entry from Day dictionary with key {0}", key);
		return dictionaryFacade.getEntry(key);
	}
	
	/**
	 * Method for getting all Day dictionary.
	 * 
	 * @return Day dictionary
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get all dictionary for days")
	public List<DictionaryEntry<Day>> getDictionary() {
		LOGGER.info("Retrieving all Day dictionary");
		return dictionaryFacade.getDictionary();
	}
	
}
