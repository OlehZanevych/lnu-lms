package org.lnu.web.rest.controller.user;

import javax.annotation.Resource;

import org.lnu.facade.facade.Facade;
import org.lnu.resource.user.UserResource;
import org.lnu.resource.message.MessageResource;
import org.lnu.resource.message.MessageType;
import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.PagedResultResource;
import org.lnu.resource.request.Request;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles all API with User entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/users")
@Api(value = "user")
public class UserController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(UserController.class);
	
	@Resource(name = "userFacade")
	private Facade<UserResource, Long> facade;
	
	/**
	 * Method for creating new UserResource.
	 * @param userResource UserResource.
	 * @return UserResource with generated identifier.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Create User")
	public UserResource createResource(@RequestBody final UserResource userResource) {
		LOG.info("Creating user: {}", userResource);
		return facade.createResource(userResource);
	}
	
	/**
	 * Method for updating UserResource.
	 * @param id UserResource identifier.
	 * @param userResource updated UserResource.
	 * @return notification of update UserResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	@ApiOperation(value = "Update User")
	public MessageResource updateResource(@PathVariable("id") final Long id, @RequestBody final UserResource userResource) {
		LOG.info("Updated user with id: {}, {}", id, userResource);
		facade.updateResource(id, userResource);
		return new MessageResource(MessageType.INFO, "User Updated");
	}
	
	/**
	 * Method for getting UserResource.
	 * @param id UserResource identifier.
	 * @param request request
	 * @return UserResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	@ApiOperation(value = "Get User by id")
	public UserResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving user with id: {} and request: {}", id, request);
		return facade.getResource(id, request);
	}
	
	/**
	 * Method for removing UserResource.
	 * @param id UserResource identifier id.
	 * @return notification of deletion UserResource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete User by id")
	public MessageResource removeResource(@PathVariable("id") final Long id) {
		LOG.info("Removing user with id: {}", id);
		facade.removeResource(id);
		return new MessageResource(MessageType.INFO, "User removed");
	}
	
	/**
	 * Method that returns a page of UserResources.
	 * @param request request.
	 * @return page of UserResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get Users")
	public PagedResultResource<UserResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for User Resources with request: {}", request);
		return facade.getResources(request);
	}
	
}
