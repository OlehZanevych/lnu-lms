package org.lnu.web.rest.controller.learningplacegroup;

import javax.annotation.Resource;

import org.lnu.facade.facade.Facade;
import org.lnu.resource.learningplacegroup.LearningPlaceGroupResource;
import org.lnu.resource.message.MessageResource;
import org.lnu.resource.message.MessageType;
import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.PagedResultResource;
import org.lnu.resource.request.Request;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles all API with LearningPlaceGroup entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/learningplacegroups")
@Api(value = "learningPlaceGroup")
public class LearningPlaceGroupController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(LearningPlaceGroupController.class);
	
	@Resource(name = "learningPlaceGroupFacade")
	private Facade<LearningPlaceGroupResource, Long> facade;
	
	/**
	 * Method for creating new LearningPlaceGroupResource.
	 * @param learningPlaceGroupResource LearningPlaceGroupResource.
	 * @return LearningPlaceGroupResource with generated identifier.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Create LearningPlaceGroup")
	public LearningPlaceGroupResource createResource(@RequestBody final LearningPlaceGroupResource learningPlaceGroupResource) {
		LOG.info("Creating learningPlaceGroup: {}", learningPlaceGroupResource);
		return facade.createResource(learningPlaceGroupResource);
	}
	
	/**
	 * Method for updating LearningPlaceGroupResource.
	 * @param id LearningPlaceGroupResource identifier.
	 * @param learningPlaceGroupResource updated LearningPlaceGroupResource.
	 * @return notification of update LearningPlaceGroupResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	@ApiOperation(value = "Update LearningPlaceGroup")
	public MessageResource updateResource(@PathVariable("id") final Long id, @RequestBody final LearningPlaceGroupResource learningPlaceGroupResource) {
		LOG.info("Updated learningPlaceGroup with id: {}, {}", id, learningPlaceGroupResource);
		facade.updateResource(id, learningPlaceGroupResource);
		return new MessageResource(MessageType.INFO, "LearningPlaceGroup Updated");
	}
	
	/**
	 * Method for getting LearningPlaceGroupResource.
	 * @param id LearningPlaceGroupResource identifier.
	 * @param request request
	 * @return LearningPlaceGroupResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	@ApiOperation(value = "Get LearningPlaceGroup by id")
	public LearningPlaceGroupResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving learningPlaceGroup with id: {} and request: {}", id, request);
		return facade.getResource(id, request);
	}
	
	/**
	 * Method for removing LearningPlaceGroupResource.
	 * @param id LearningPlaceGroupResource identifier id.
	 * @return notification of deletion LearningPlaceGroupResource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete LearningPlaceGroup by id")
	public MessageResource removeResource(@PathVariable("id") final Long id) {
		LOG.info("Removing learningPlaceGroup with id: {}", id);
		facade.removeResource(id);
		return new MessageResource(MessageType.INFO, "LearningPlaceGroup removed");
	}
	
	/**
	 * Method that returns a page of LearningPlaceGroupResources.
	 * @param request request.
	 * @return page of LearningPlaceGroupResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get LearningPlaceGroups")
	public PagedResultResource<LearningPlaceGroupResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for LearningPlaceGroup Resources with request: {}", request);
		return facade.getResources(request);
	}
	
}
