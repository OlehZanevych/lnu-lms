package org.lnu.web.rest.controller.dictionary.coursecycle;

import java.util.List;

import javax.annotation.Resource;

import org.lnu.model.enumtype.coursecycle.CourseCycle;
import org.lnu.facade.dictionary.DictionaryFacade;
import org.lnu.resource.dictionary.DictionaryEntry;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles getting APIs for CourseCycle dictionary.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/dictionaries/coursecycles")
@Api(value = "coursecycles")
public class CourseCycleDictionaryController extends BaseController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CourseCycleDictionaryController.class);
	
	@Resource(name = "courseCycleDictionaryFacade")
	private DictionaryFacade<CourseCycle> dictionaryFacade;
	
	/**
	 * Method for getting entry from CourseCycle dictionary by key.
	 * 
	 * @param key key in dictionary
	 * @return entry with key
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.KEY, method = RequestMethod.GET)
	@ApiOperation(value = "Get class type entry by key")
	public DictionaryEntry<CourseCycle> getEntry(@PathVariable("key") final CourseCycle key) {
		LOGGER.info("Retrieving entry from CourseCycle dictionary by key '{0}'", key);
		return dictionaryFacade.getEntry(key);
	}
	
	/**
	 * Method for getting all CourseCycle dictionary.
	 * 
	 * @return CourseCycle dictionary
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get all dictionary for course cycles")
	public List<DictionaryEntry<CourseCycle>> getDictionary() {
		LOGGER.info("Retrieving all CourseCycle dictionary");
		return dictionaryFacade.getDictionary();
	}
	
}
