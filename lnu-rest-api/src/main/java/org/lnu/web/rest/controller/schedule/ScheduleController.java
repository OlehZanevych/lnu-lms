package org.lnu.web.rest.controller.schedule;

import org.lnu.facade.facade.Facade;
import org.lnu.resource.schedule.ScheduleResource;
import org.lnu.resource.message.MessageResource;
import org.lnu.resource.message.MessageType;
import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.PagedResultResource;
import org.lnu.resource.request.Request;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles all API with Schedule entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/schedule")
@Api(value = "schedule")
public class ScheduleController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(ScheduleController.class);
	
	//@Resource(name = "scheduleFacade")
	private Facade<ScheduleResource, Long> facade;
	
	/**
	 * Method for creating new ScheduleResource.
	 * @param scheduleResource ScheduleResource.
	 * @return ScheduleResource with generated identifier.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Create Schedule")
	public ScheduleResource createResource(@RequestBody final ScheduleResource scheduleResource) {
		LOG.info("Creating schedule: {}", scheduleResource);
		return facade.createResource(scheduleResource);
	}
	
	/**
	 * Method for updating ScheduleResource.
	 * @param id ScheduleResource identifier.
	 * @param scheduleResource updated ScheduleResource.
	 * @return notification of update ScheduleResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	@ApiOperation(value = "Update Schedule")
	public MessageResource updateResource(@PathVariable("id") final Long id, @RequestBody final ScheduleResource scheduleResource) {
		LOG.info("Updated schedule with id: {}, {}", id, scheduleResource);
		facade.updateResource(id, scheduleResource);
		return new MessageResource(MessageType.INFO, "Schedule Updated");
	}
	
	/**
	 * Method for getting ScheduleResource.
	 * @param id ScheduleResource identifier.
	 * @param request request
	 * @return ScheduleResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	@ApiOperation(value = "Get Schedule by id")
	public ScheduleResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving schedule with id: {} and request: {}", id, request);
		return facade.getResource(id, request);
	}
	
	/**
	 * Method for removing ScheduleResource.
	 * @param id ScheduleResource identifier id.
	 * @return notification of deletion ScheduleResource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete Schedule by id")
	public MessageResource removeResource(@PathVariable("id") final Long id) {
		LOG.info("Removing schedule with id: {}", id);
		facade.removeResource(id);
		return new MessageResource(MessageType.INFO, "Schedule removed");
	}
	
	/**
	 * Method that returns a page of ScheduleResources.
	 * @param request request.
	 * @return page of ScheduleResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get Schedule")
	public PagedResultResource<ScheduleResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for Schedule Resources with request: {}", request);
		return facade.getResources(request);
	}
	
}
