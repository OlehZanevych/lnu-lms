package org.lnu.web.rest.controller.group;

import javax.annotation.Resource;

import org.lnu.facade.facade.Facade;
import org.lnu.resource.group.GroupResource;
import org.lnu.resource.message.MessageResource;
import org.lnu.resource.message.MessageType;
import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.PagedResultResource;
import org.lnu.resource.request.Request;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles all API with Group entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/groups")
@Api(value = "group")
public class GroupController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(GroupController.class);
	
	@Resource(name = "groupFacade")
	private Facade<GroupResource, Long> facade;
	
	/**
	 * Method for creating new GroupResource.
	 * @param groupResource GroupResource.
	 * @return GroupResource with generated identifier.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Create Group")
	public GroupResource createResource(@RequestBody final GroupResource groupResource) {
		LOG.info("Creating group: {}", groupResource);
		return facade.createResource(groupResource);
	}
	
	/**
	 * Method for updating GroupResource.
	 * @param id GroupResource identifier.
	 * @param groupResource updated GroupResource.
	 * @return notification of update GroupResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	@ApiOperation(value = "Update Group")
	public MessageResource updateResource(@PathVariable("id") final Long id, @RequestBody final GroupResource groupResource) {
		LOG.info("Updated group with id: {}, {}", id, groupResource);
		facade.updateResource(id, groupResource);
		return new MessageResource(MessageType.INFO, "Group Updated");
	}
	
	/**
	 * Method for getting GroupResource.
	 * @param id GroupResource identifier.
	 * @param request request
	 * @return GroupResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	@ApiOperation(value = "Get Group by id")
	public GroupResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving group with id: {} and request: {}", id, request);
		return facade.getResource(id, request);
	}
	
	/**
	 * Method for removing GroupResource.
	 * @param id GroupResource identifier id.
	 * @return notification of deletion GroupResource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete Group by id")
	public MessageResource removeResource(@PathVariable("id") final Long id) {
		LOG.info("Removing group with id: {}", id);
		facade.removeResource(id);
		return new MessageResource(MessageType.INFO, "Group removed");
	}
	
	/**
	 * Method that returns a page of GroupResources.
	 * @param request request.
	 * @return page of GroupResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get Groups")
	public PagedResultResource<GroupResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for Group Resources with request: {}", request);
		return facade.getResources(request);
	}
	
}
