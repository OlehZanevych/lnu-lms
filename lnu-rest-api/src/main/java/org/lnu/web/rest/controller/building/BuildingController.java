package org.lnu.web.rest.controller.building;

import javax.annotation.Resource;

import org.lnu.facade.facade.Facade;
import org.lnu.resource.building.BuildingResource;
import org.lnu.resource.message.MessageResource;
import org.lnu.resource.message.MessageType;
import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.PagedResultResource;
import org.lnu.resource.request.Request;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles all API with Building entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/buildings")
@Api(value = "building")
public class BuildingController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(BuildingController.class);
	
	@Resource(name = "buildingFacade")
	private Facade<BuildingResource, Long> facade;
	
	/**
	 * Method for creating new BuildingResource.
	 * @param buildingResource BuildingResource.
	 * @return BuildingResource with generated identifier.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Create Building")
	public BuildingResource createResource(@RequestBody final BuildingResource buildingResource) {
		LOG.info("Creating building: {}", buildingResource);
		return facade.createResource(buildingResource);
	}
	
	/**
	 * Method for updating BuildingResource.
	 * @param id BuildingResource identifier.
	 * @param buildingResource updated BuildingResource.
	 * @return notification of update BuildingResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	@ApiOperation(value = "Update Building")
	public MessageResource updateResource(@PathVariable("id") final Long id, @RequestBody final BuildingResource buildingResource) {
		LOG.info("Updating building with id: {}, {}", id, buildingResource);
		facade.updateResource(id, buildingResource);
		return new MessageResource(MessageType.INFO, "Building Updated");
	}
	
	/**
	 * Method for getting BuildingResource.
	 * @param id BuildingResource identifier.
	 * @param request request
	 * @return BuildingResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	@ApiOperation(value = "Get Building by id")
	public BuildingResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving building with id: {} and request: {}", id, request);
		return facade.getResource(id, request);
	}
	
	/**
	 * Method for removing BuildingResource.
	 * @param id BuildingResource identifier id.
	 * @return notification of deletion BuildingResource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete Building by id")
	public MessageResource removeResource(@PathVariable("id") final Long id) {
		LOG.info("Removing building with id: {}", id);
		facade.removeResource(id);
		return new MessageResource(MessageType.INFO, "Building removed");
	}
	
	/**
	 * Method that returns a page of BuildingResources.
	 * @param request request.
	 * @return page of BuildingResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get Buildings")
	public PagedResultResource<BuildingResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for Building Resources with request: {}", request);
		return facade.getResources(request);
	}
	
}
