package org.lnu.web.rest.controller.student;

import javax.annotation.Resource;

import org.lnu.facade.facade.Facade;
import org.lnu.resource.student.StudentResource;
import org.lnu.resource.message.MessageResource;
import org.lnu.resource.message.MessageType;
import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.PagedResultResource;
import org.lnu.resource.request.Request;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles all API with Student entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/students")
@Api(value = "student")
public class StudentController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(StudentController.class);
	
	@Resource(name = "studentFacade")
	private Facade<StudentResource, Long> facade;
	
	/**
	 * Method for creating new StudentResource.
	 * @param studentResource StudentResource.
	 * @return StudentResource with generated identifier.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Create Student")
	public StudentResource createResource(@RequestBody final StudentResource studentResource) {
		LOG.info("Creating student: {}", studentResource);
		return facade.createResource(studentResource);
	}
	
	/**
	 * Method for updating StudentResource.
	 * @param id StudentResource identifier.
	 * @param studentResource updated StudentResource.
	 * @return notification of update StudentResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.PUT)
	@ApiOperation(value = "Update Student")
	public MessageResource updateResource(@PathVariable("id") final Long id, @RequestBody final StudentResource studentResource) {
		LOG.info("Updated student with id: {}, {}", id, studentResource);
		facade.updateResource(id, studentResource);
		return new MessageResource(MessageType.INFO, "Student Updated");
	}
	
	/**
	 * Method for getting StudentResource.
	 * @param id StudentResource identifier.
	 * @param request request
	 * @return StudentResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.GET)
	@ApiOperation(value = "Get Student by id")
	public StudentResource getResource(@PathVariable("id") final Long id, final Request request) {
		LOG.info("Retrieving student with id: {} and request: {}", id, request);
		return facade.getResource(id, request);
	}
	
	/**
	 * Method for removing StudentResource.
	 * @param id StudentResource identifier id.
	 * @return notification of deletion StudentResource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = RequestConstants.ID, method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete Student by id")
	public MessageResource removeResource(@PathVariable("id") final Long id) {
		LOG.info("Removing student with id: {}", id);
		facade.removeResource(id);
		return new MessageResource(MessageType.INFO, "Student removed");
	}
	
	/**
	 * Method that returns a page of StudentResources.
	 * @param request request.
	 * @return page of StudentResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get Students")
	public PagedResultResource<StudentResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for Student Resources with request: {}", request);
		return facade.getResources(request);
	}
	
}
