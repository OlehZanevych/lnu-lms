package org.lnu.web.rest.controller.dictionary.api;

import java.util.List;

import javax.annotation.Resource;

import org.lnu.model.enumtype.api.API;
import org.lnu.facade.dictionary.DictionaryFacade;
import org.lnu.resource.dictionary.DictionaryEntry;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles getting APIs for API dictionary.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/dictionaries/apis")
@Api(value = "apiz")
public class APIDictionaryController extends BaseController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(APIDictionaryController.class);
	
	@Resource(name = "apiDictionaryFacade")
	private DictionaryFacade<API> dictionaryFacade;
	
	/**
	 * Method for getting entry from API dictionary by key.
	 * 
	 * @param key key in dictionary
	 * @return entry with key
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.KEY, method = RequestMethod.GET)
	@ApiOperation(value = "Get api entry by key")
	public DictionaryEntry<API> getEntry(@PathVariable("key") final API key) {
		LOGGER.info("Retrieving entry from API dictionary with key {0}", key);
		return dictionaryFacade.getEntry(key);
	}
	
	/**
	 * Method for getting all API dictionary.
	 * 
	 * @return API dictionary
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get all dictionary for apis")
	public List<DictionaryEntry<API>> getDictionary() {
		LOGGER.info("Retrieving all API dictionary");
		return dictionaryFacade.getDictionary();
	}
	
}
