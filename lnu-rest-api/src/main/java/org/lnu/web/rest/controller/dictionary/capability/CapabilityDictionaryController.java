package org.lnu.web.rest.controller.dictionary.capability;

import java.util.List;

import javax.annotation.Resource;

import org.lnu.model.enumtype.capability.Capability;
import org.lnu.facade.dictionary.DictionaryFacade;
import org.lnu.resource.dictionary.DictionaryEntry;
import org.lnu.web.rest.constant.RequestConstants;
import org.lnu.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller, that handles getting APIs for Capability dictionary.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/dictionaries/capabilities")
@Api(value = "capabilities")
public class CapabilityDictionaryController extends BaseController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CapabilityDictionaryController.class);
	
	@Resource(name = "capabilityDictionaryFacade")
	private DictionaryFacade<Capability> dictionaryFacade;
	
	/**
	 * Method for getting entry from Capability dictionary by key.
	 * 
	 * @param key key in dictionary
	 * @return entry with key
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = RequestConstants.KEY, method = RequestMethod.GET)
	@ApiOperation(value = "Get capability entry by key")
	public DictionaryEntry<Capability> getEntry(@PathVariable("key") final Capability key) {
		LOGGER.info("Retrieving entry from Capability dictionary with key {0}", key);
		return dictionaryFacade.getEntry(key);
	}
	
	/**
	 * Method for getting all Capability dictionary.
	 * 
	 * @return Capability dictionary
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get all dictionary for capabilities")
	public List<DictionaryEntry<Capability>> getDictionary() {
		LOGGER.info("Retrieving all Capability dictionary");
		return dictionaryFacade.getDictionary();
	}
	
}
