package org.lnu.web.exception;

/**
 * Exception, that describes invalid regex.
 * @author OlehZanevych
 *
 */
public class InvalidOrderByException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	private String value;

	/**
	 * Constructor, that takes all required parameters.
	 * @param value value.
	 */
	public InvalidOrderByException(final String value) {
		super("Invalid string for order by field");
		this.value = value;
	}

	public String getValue() {
		return value;
	}
	
}
