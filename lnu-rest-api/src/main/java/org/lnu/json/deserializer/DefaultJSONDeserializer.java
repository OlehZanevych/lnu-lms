package org.lnu.json.deserializer;

import java.io.IOException;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Default implementation of JSON deserializer.
 * @author OlehZanevych
 */
@Component("defaultJSONDeserializer")
public class DefaultJSONDeserializer implements JSONDeserializer {
	
	@Resource(name = "objectMapper")
	private ObjectMapper objectMapper;

	@Override
	public <T> T deserialize(final String content, final Class<T> valueType) {
		try {
			return objectMapper.readValue(content, valueType);
		} catch (IOException e) {
			throw new IllegalArgumentException("Invalid JSON content");
		}
	}

}
