package org.lnu.dao.numberparser;

import java.util.Map;

import com.google.common.collect.ImmutableMap;

/**
 * Class that is used for numbers parsing.
 * @author OlehZanevych
 */
public final class NumberParser {
	
	private static final Map<Class<? extends Number>, Parser> PARSERS =
			ImmutableMap.<Class<? extends Number>, Parser>builder()
			.put(Byte.class, new Parser() {
				public Number parse(final String str) {
					return Byte.parseByte(str);
				}
			})
			.put(Short.class, new Parser() {
				public Number parse(final String str) {
					return Short.parseShort(str);
				}
			})
			.put(Integer.class, new Parser() {
				public Number parse(final String str) {
					return Integer.parseInt(str);
				}
			})
			.put(Long.class, new Parser() {
				public Number parse(final String str) {
					return Long.parseLong(str);
				}
			})
			.put(Float.class, new Parser() {
				public Number parse(final String str) {
					return Float.parseFloat(str);
				}
			})
			.put(Double.class, new Parser() {
				public Number parse(final String str) {
					return Double.parseDouble(str);
				}
			})
			.build();
	
	/**
	 * Method for parsing number from string.
	 * @param str string
	 * @param numberClass number class
	 * @return parsed Number
	 */
	public static Number parse(final String str, final Class<? extends Number> numberClass) {
		return PARSERS.get(numberClass).parse(str);
	}
	
	private interface Parser {
		/**
		 * Method for parsing number from string.
		 * @param str string
		 * @return parsed Number
		 */
		Number parse(String str);
	}

}
