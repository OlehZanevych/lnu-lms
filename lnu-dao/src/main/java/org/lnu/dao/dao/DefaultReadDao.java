package org.lnu.dao.dao;

import org.lnu.dao.persistence.PersistenceManager;
import org.lnu.dao.result.CriteriaResult;
import org.lnu.model.Model;
import org.lnu.model.pagination.PagedResult;
import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default implementation, that has methods to Get entities.
 * @author OlehZanevych
 *
 * @param <ENTITY> entity type
 * @param <KEY> identifier class
 */
public class DefaultReadDao<ENTITY extends Model, KEY> implements ReadDao<ENTITY, KEY> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultReadDao.class);
	
	protected PersistenceManager<ENTITY, KEY> persistenceManager;
	
	protected Class<ENTITY> entityClass;
	
	protected CriteriaResult<ENTITY, KEY> criteriaResult;
	
	@Override
	public ENTITY getEntityById(final KEY id, final Long userId) {
		LOGGER.info("Getting {} entity with id: {}", entityClass.getSimpleName(), id);
		persistenceManager.setUserId(userId);
		return criteriaResult.getEntityById(id, persistenceManager.getSession(), entityClass);
	}
	
	@Override
	public ENTITY getEntityById(final KEY id, final Request request, final Long userId) {
		LOGGER.info("Getting {} entity with id: {} and attached request: {}",
				entityClass.getSimpleName(), id, request);
		persistenceManager.setUserId(userId);
		return criteriaResult.getEntityById(id, request, persistenceManager.getSession(), entityClass);
	}
	
	@Override
	public PagedResult<ENTITY> getEntities(final PagedRequest request, final Long userId) {
		LOGGER.info("Getting paged result for {}", entityClass.getSimpleName());
		persistenceManager.setUserId(userId);
		return criteriaResult.getEntities(request, persistenceManager.getSession(), entityClass, userId);
	}

	public PersistenceManager<ENTITY, KEY> getPersistenceManager() {
		return persistenceManager;
	}

	public void setPersistenceManager(final PersistenceManager<ENTITY, KEY> persistenceManager) {
		this.persistenceManager = persistenceManager;
	}

	public Class<ENTITY> getEntityClass() {
		return entityClass;
	}

	public void setEntityClass(final Class<ENTITY> entityClass) {
		this.entityClass = entityClass;
	}

	public CriteriaResult<ENTITY, KEY> getCriteriaResult() {
		return criteriaResult;
	}

	public void setCriteriaResult(final CriteriaResult<ENTITY, KEY> criteriaResult) {
		this.criteriaResult = criteriaResult;
	}

}
