package org.lnu.dao.dao;

import org.lnu.model.APIModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Default implementation, that has all methods to work with entities.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity type.
 * @param <KEY> Identifier class.
 */
public class DefaultDao<ENTITY extends APIModel<KEY>, KEY extends Number> extends DefaultEditDao<ENTITY, KEY>
		implements Dao<ENTITY, KEY> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultDao.class);

	@Override
	public void createEntity(final ENTITY entity, final Long userId) {
		LOGGER.info("Saving {} entity: {}", entityClass.getSimpleName(), entity);
		persistenceManager.setUserId(userId);
		persistenceManager.create(entity);
	}

	@Override
	public void removeEntityById(final KEY id, final Long userId) {
		LOGGER.info("Removing {} entity with id: {}", entityClass.getSimpleName(), id);
		persistenceManager.setUserId(userId);
		persistenceManager.remove(entityClass, id);
	}
	
}
