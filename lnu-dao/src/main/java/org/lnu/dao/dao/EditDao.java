package org.lnu.dao.dao;

import org.lnu.model.enumtype.capability.Capability;

/**
 * Interface, that has methods to Get and Update entities.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity class.
 * @param <KEY> Identifier class.
 */
public interface EditDao<ENTITY, KEY> extends ReadDao<ENTITY, KEY> {
	
	/**
	 * Getting persistent entity by id.
	 * @param id identifier
	 * @param userId current User id
	 * @return persistent entity
	 */
	ENTITY getPersistentEntityById(KEY id, Long userId);
	
	/**
	 * Updating entity.
	 * @param entity entity
	 * @param userId current User id
	 */
	void updateEntity(ENTITY entity, Long userId);
	
	/**
	 * Getting user access capability level for entity.
	 * @param id entity identifier
	 * @param userId current User id
	 * @return user access capability level
	 */
	Capability getCapability(final KEY id, final Long userId);
}
