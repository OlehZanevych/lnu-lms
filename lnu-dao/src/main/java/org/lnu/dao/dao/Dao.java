package org.lnu.dao.dao;

/**
 * Interface, that has all methods to work with entities.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity class.
 * @param <KEY> Identifier class.
 */
public interface Dao<ENTITY, KEY> extends EditDao<ENTITY, KEY> {
	
	/**
	 * Method for creating entity.
	 * @param entity entity
	 * @param userId current User id
	 */
	void createEntity(ENTITY entity, Long userId);
	
	/**
	 * Method for removing entity.
	 * @param id identifier
	 * @param userId current User id
	 */
	void removeEntityById(KEY id, Long userId);
	
}
