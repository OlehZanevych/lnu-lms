package org.lnu.dao.dao;

import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.lnu.model.APIModel;
import org.lnu.model.enumtype.capability.Capability;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default implementation, that has methods to Get and Update entities.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity type.
 * @param <KEY> Identifier class.
 */
public class DefaultEditDao<ENTITY extends APIModel<KEY>, KEY extends Number>
		extends DefaultReadDao<ENTITY, KEY> implements EditDao<ENTITY, KEY> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultEditDao.class);
	
	@Override
	public ENTITY getPersistentEntityById(final KEY id, final Long userId) {
		LOGGER.info("Getting {} persistent entity with id: {}", entityClass.getSimpleName(), id);
		persistenceManager.setUserId(userId);
		return persistenceManager.findById(entityClass, id);
	}
	
	@Override
	public void updateEntity(final ENTITY entity, final Long userId) {
		LOGGER.info("Updating {} entity with id: {}, {}", entityClass.getSimpleName(), entity.getId(), entity);
		persistenceManager.setUserId(userId);
		persistenceManager.update(entity);
	}
	
	@Override
	public Capability getCapability(final KEY id, final Long userId) {
		LOGGER.info("Getting access capability level for {} entity with id {} for user with id {}",
				entityClass.getSimpleName(), id, userId);
		persistenceManager.setUserId(userId);
		return (Capability) persistenceManager.getSession().createCriteria(entityClass)
			.setProjection(Projections.property("capability"))
			.add(Restrictions.eq("id", id))
			.uniqueResult();
	}
	
}
