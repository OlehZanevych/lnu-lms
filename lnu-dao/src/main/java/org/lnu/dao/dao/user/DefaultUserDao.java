package org.lnu.dao.dao.user;

import org.hibernate.criterion.Restrictions;
import org.lnu.dao.dao.DefaultDao;
import org.lnu.model.user.User;

/**
 * Default User Dao.
 * @author OlehZanevych
 */
public class DefaultUserDao extends DefaultDao<User, Long> implements UserDao {
	
	@Override
	public User getUserByLogin(final String login) {
		persistenceManager.setUserId(1L);
		return (User) getPersistenceManager().getSession().createCriteria(User.class)
				.add(Restrictions.eq("login", login)).uniqueResult();
	}

}
