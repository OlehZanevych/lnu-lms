package org.lnu.dao.dao.user;

import org.lnu.dao.dao.Dao;
import org.lnu.model.user.User;

/**
 * User dao interface.
 * @author OlehZanevych
 */
public interface UserDao extends Dao<User, Long> {

	/**
	 * Method for getting user by login.
	 * @param login login
	 * @return User.
	 */
	User getUserByLogin(String login);
	
}
