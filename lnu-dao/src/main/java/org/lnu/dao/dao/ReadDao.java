package org.lnu.dao.dao;

import org.lnu.model.pagination.PagedResult;
import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.Request;

/**
 * Interface, that has methods to Get entities.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity class.
 * @param <KEY> Identifier class.
 */
public interface ReadDao<ENTITY, KEY> {
	
	/**
	 * Method for getting entity by id.
	 * @param id identifier
	 * @param userId current User id
	 * @return entity
	 */
	ENTITY getEntityById(KEY id, Long userId);
	
	/**
	 * Method for finding entity by id with attached request.
	 * @param id identifier
	 * @param request attached request
	 * @param userId current User id
	 * @return Entity
	 */
	ENTITY getEntityById(KEY id, Request request, Long userId);
	
	/**
	 * Method for getting paged Result.
	 * @param request paged request
	 * @param userId current User id
	 * @return paged result.
	 */
	PagedResult<ENTITY> getEntities(PagedRequest request, Long userId);

}
