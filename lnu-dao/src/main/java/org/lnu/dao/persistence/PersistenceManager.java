package org.lnu.dao.persistence;

import org.hibernate.Session;
import org.lnu.model.Model;
/**
 * Interface, that is used to encapsulate all work
 * with entity manager.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity Class
 * @param <KEY> Identifier Class
 */
public interface PersistenceManager<ENTITY extends Model, KEY> {

	/**
	 * Method for creating entity.
	 * @param entity Instance of entity
	 * @return Entity with Id
	 */
    ENTITY create(ENTITY entity);

    /**
     * Finds entity from database by identifier.
     * @param clazz entity class
     * @param id entity identifier
     * @return Entity instance
     */
    ENTITY findById(Class<ENTITY> clazz, KEY id);

    /**
     * Updates entity.
     * @param entity entity instance
     * @return Updated instance
     */
    ENTITY update(ENTITY entity);

    /**
     * Removes entity.
     * @param clazz entity class
     * @param id entity identifier
     */
    void remove(Class<ENTITY> clazz, KEY id);
	
    /**
     * Get Hibernate Session.
     * @return Hibernate Session instance
     */
	Session getSession();
	
	/**
	 * Setting current User id.
	 * @param userId current User id
	 */
	void setUserId(Long userId);
}
