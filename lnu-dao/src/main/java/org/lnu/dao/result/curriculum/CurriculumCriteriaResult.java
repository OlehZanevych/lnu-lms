package org.lnu.dao.result.curriculum;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.transform.ResultTransformer;
import org.lnu.dao.annotation.CriteriaResult;
import org.lnu.dao.constants.Constants;
import org.lnu.dao.result.DefaultCriteriaResult;
import org.lnu.dao.result.childcollection.factory.ChildCollectionFactory;
import org.lnu.dao.result.transformer.ResultTransformers;
import org.lnu.model.curriculum.Curriculum;
import org.lnu.model.curriculum.course.CurriculumCourse;
import org.lnu.model.curriculum.semester.CurriculumSemester;
import org.lnu.model.curriculum.semester.hours.CurriculumSemesterHours;
import org.lnu.model.enumtype.classtype.ClassType;
import org.lnu.model.enumtype.coursecycle.CourseCycle;
import org.lnu.model.enumtype.coursetype.CourseType;
import org.lnu.model.enumtype.report.Report;
import org.lnu.model.specialty.Specialty;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

/**
 * Class, that contains methods for receiving
 * Curriculum from Hibernate Criteria.
 * @author OlehZanevych
 */
@CriteriaResult("curriculumCriteriaResult")
public class CurriculumCriteriaResult extends DefaultCriteriaResult<Curriculum, Long> {
	
	private static final ImmutableList<String> DEFAULT_FIELDS =
			ImmutableList.<String>builder()
			.add("courseId")
			.add("departmentId")
			.add("courseType")
			.add("courseCycle")
			.add("specialtyIds")
			.add("semester")
			.add("year")
			.add("half")
			.add("credits")
			.add("allHours")
			.add("independentStudy")
			.add("report")
			.add("classType")
			.add("hours")
			.add("isAcademicGroupDepartment")
			.build();
	
	protected static final ImmutableMap<String, ProjectionProcessor> PROJECTION_PROCESSORS =
			ImmutableMap.<String, ProjectionProcessor>builder()
			.putAll(DEFAULT_PROJECTION_PROCESSORS)
		    .put("curriculumCourses", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("curriculumCourses", "curriculumCoursesAlias",
		    				CurriculumCourse.class, ResultTransformers.CURRICULUM_COURSE_RESULT_TRANSFORMER);
		    	}
		    })
		    .put("courseId", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("curriculumCourses", "curriculumCoursesAlias",
		    				CurriculumCourse.class, ResultTransformers.CURRICULUM_COURSE_RESULT_TRANSFORMER)
		    			.addProjection("curriculumCoursesAlias.course.id", "course.id");
		    	}
		    })
		    .put("departmentId", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("curriculumCourses", "curriculumCoursesAlias",
		    				CurriculumCourse.class, ResultTransformers.CURRICULUM_COURSE_RESULT_TRANSFORMER)
		    			.addProjection("curriculumCoursesAlias.department.id", "department.id");
		    	}
		    })
		    .put("courseType", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("courseType"), "courseType");
		    	}
		    })
		    .put("courseCycle", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("courseCycle"), "courseCycle");
		    	}
		    })
		    .put("specialtyIds", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("specialties", "specialtiesAlias",
		    				Specialty.class, ResultTransformers.SPECIALTY_RESULT_TRANSFORMER);
		    	}
		    })
		    .put("curriculumSemesters", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("curriculumSemesters", "curriculumSemestersAlias",
		    				CurriculumSemester.class, ResultTransformers.CURRICULUM_SEMESTER_RESULT_TRANSFORMER);
		    	}
		    })
		    .put("semester", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("curriculumSemesters", "curriculumSemestersAlias",
		    				CurriculumSemester.class, ResultTransformers.CURRICULUM_SEMESTER_RESULT_TRANSFORMER)
		    			.addProjection("curriculumSemestersAlias.semester", "semester");
		    	}
		    })
		    .put("year", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("curriculumSemesters", "curriculumSemestersAlias",
		    				CurriculumSemester.class, ResultTransformers.CURRICULUM_SEMESTER_RESULT_TRANSFORMER)
		    			.addProjection("curriculumSemestersAlias.year", "year");
		    	}
		    })
		    .put("half", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("curriculumSemesters", "curriculumSemestersAlias",
		    				CurriculumSemester.class, ResultTransformers.CURRICULUM_SEMESTER_RESULT_TRANSFORMER)
		    			.addProjection("curriculumSemestersAlias.half", "half");
		    	}
		    })
		    .put("credits", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("curriculumSemesters", "curriculumSemestersAlias",
		    				CurriculumSemester.class, ResultTransformers.CURRICULUM_SEMESTER_RESULT_TRANSFORMER)
		    			.addProjection("curriculumSemestersAlias.credits", "credits");
		    	}
		    })
		    .put("allHours", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("curriculumSemesters", "curriculumSemestersAlias",
		    				CurriculumSemester.class, ResultTransformers.CURRICULUM_SEMESTER_RESULT_TRANSFORMER)
		    			.addProjection("curriculumSemestersAlias.allHours", "allHours");
		    	}
		    })
		    .put("independentStudy", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("curriculumSemesters", "curriculumSemestersAlias",
		    				CurriculumSemester.class, ResultTransformers.CURRICULUM_SEMESTER_RESULT_TRANSFORMER)
		    			.addProjection("curriculumSemestersAlias.independentStudy", "independentStudy");
		    	}
		    })
		    .put("report", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("curriculumSemesters", "curriculumSemestersAlias",
		    				CurriculumSemester.class, ResultTransformers.CURRICULUM_SEMESTER_RESULT_TRANSFORMER)
		    			.addProjection("curriculumSemestersAlias.report", "report");
		    	}
		    })
		    .put("curriculumSemesterHours", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("curriculumSemesters", "curriculumSemestersAlias",
		    				CurriculumSemester.class, ResultTransformers.CURRICULUM_SEMESTER_RESULT_TRANSFORMER)
		    			.addCollectionProjection("curriculumSemesterHours", "curriculumSemestersHoursAlias",
		    					CurriculumSemesterHours.class, ResultTransformers.CURRICULUM_SEMESTER_HOURS_RESULT_TRANSFORMER);
		    	}
		    })
		    .put("classType", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("curriculumSemesters", "curriculumSemestersAlias",
		    				CurriculumSemester.class, ResultTransformers.CURRICULUM_SEMESTER_RESULT_TRANSFORMER)
		    			.addCollectionProjection("curriculumSemesterHours", "curriculumSemestersHoursAlias",
		    					CurriculumSemesterHours.class, ResultTransformers.CURRICULUM_SEMESTER_HOURS_RESULT_TRANSFORMER)
		    			.addProjection("curriculumSemestersHoursAlias.classType", "classType");
		    	}
		    })
		    .put("hours", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("curriculumSemesters", "curriculumSemestersAlias",
		    				CurriculumSemester.class, ResultTransformers.CURRICULUM_SEMESTER_RESULT_TRANSFORMER)
	    			.addCollectionProjection("curriculumSemesterHours", "curriculumSemestersHoursAlias",
	    					CurriculumSemesterHours.class, ResultTransformers.CURRICULUM_SEMESTER_HOURS_RESULT_TRANSFORMER)
	    			.addProjection("curriculumSemestersHoursAlias.hours", "hours");
		    	}
		    })
		    .put("isAcademicGroupDepartment", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("curriculumSemesters", "curriculumSemestersAlias",
		    				CurriculumSemester.class, ResultTransformers.CURRICULUM_SEMESTER_RESULT_TRANSFORMER)
	    			.addCollectionProjection("curriculumSemesterHours", "curriculumSemestersHoursAlias",
	    					CurriculumSemesterHours.class, ResultTransformers.CURRICULUM_SEMESTER_HOURS_RESULT_TRANSFORMER)
	    			.addProjection("curriculumSemestersHoursAlias.isAcademicGroupDepartment", "isAcademicGroupDepartment");
		    	}
		    })
		    .build();
	
	private static final ImmutableMap<String, ParameterProcessor> PARAMETER_PROCESSORS =
			ImmutableMap.<String, ParameterProcessor>builder()
			.putAll(COMMON_PARAMETER_PROCESSORS)
			.putAll(getDefaultParameterProcessors(Curriculum.class))
			.put("curriculumCourses", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Curriculum.class, "curriculumCriteria")
				    		.add(Restrictions.eqProperty("curriculumCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("curriculumCriteria.curriculumCourses", "curriculumCoursesAlias")
				    		.add(createNumberRestrictions("curriculumCoursesAlias.id", value, Long.class))
				    		.setProjection(Projections.property("curriculumCriteria.id"))
		    		));
		    	}
		    })
			.put("curriculumCoursesSize", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createSizeRestrictions("curriculumCourses", value));
		    	}
		    })
			.put("courseId", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Curriculum.class, "curriculumCriteria")
				    		.add(Restrictions.eqProperty("curriculumCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("curriculumCriteria.curriculumCourses", "curriculumCoursesAlias")
				    		.add(createNumberRestrictions("curriculumCoursesAlias.course.id", value, Long.class))
				    		.setProjection(Projections.property("curriculumCriteria.id"))
		    		));
		    	}
		    })
			.put("departmentId", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Curriculum.class, "curriculumCriteria")
				    		.add(Restrictions.eqProperty("curriculumCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("curriculumCriteria.curriculumCourses", "curriculumCoursesAlias")
				    		.add(createNumberRestrictions("curriculumCoursesAlias.department.id", value, Long.class))
				    		.setProjection(Projections.property("curriculumCriteria.id"))
		    		));
		    	}
		    })
			.put("courseType", new ParameterProcessor() {
				public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
					criteria.add(createEnumRestriction("courseType", value, CourseType.class));
		    	}
		    })
			.put("courseCycle", new ParameterProcessor() {
				public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
					criteria.add(createEnumRestriction("courseCycle", value, CourseCycle.class));
		    	}
		    })
			.put("specialtyIds", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Curriculum.class, "curriculumCriteria")
				    		.add(Restrictions.eqProperty("curriculumCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("curriculumCriteria.specialties", "specialtiesAlias")
				    		.add(createNumberRestrictions("specialtiesAlias.id", value, Long.class))
				    		.setProjection(Projections.property("curriculumCriteria.id"))
		    		));
		    	}
		    })
			.put("specialtyIdsSize", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createSizeRestrictions("specialties", value));
		    	}
		    })
			.put("curriculumSemesters", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Curriculum.class, "curriculumCriteria")
				    		.add(Restrictions.eqProperty("curriculumCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("curriculumCriteria.curriculumSemesters", "curriculumSemestersAlias")
				    		.add(createNumberRestrictions("curriculumSemestersAlias.id", value, Long.class))
				    		.setProjection(Projections.property("curriculumCriteria.id"))
		    		));
		    	}
		    })
			.put("curriculumSemestersSize", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createSizeRestrictions("curriculumSemesters", value));
		    	}
		    })
			.put("semester", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Curriculum.class, "curriculumCriteria")
				    		.add(Restrictions.eqProperty("curriculumCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("curriculumCriteria.curriculumSemesters", "curriculumSemestersAlias")
				    		.add(createNumberRestrictions("curriculumSemestersAlias.semester", value, Byte.class))
				    		.setProjection(Projections.property("curriculumCriteria.id"))
		    		));
		    	}
		    })
			.put("year", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Curriculum.class, "curriculumCriteria")
				    		.add(Restrictions.eqProperty("curriculumCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("curriculumCriteria.curriculumSemesters", "curriculumSemestersAlias")
				    		.add(createNumberRestrictions("curriculumSemestersAlias.year", value, Byte.class))
				    		.setProjection(Projections.property("curriculumCriteria.id"))
		    		));
		    	}
		    })
			.put("half", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Curriculum.class, "curriculumCriteria")
				    		.add(Restrictions.eqProperty("curriculumCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("curriculumCriteria.curriculumSemesters", "curriculumSemestersAlias")
				    		.add(createNumberRestrictions("curriculumSemestersAlias.half", value, Byte.class))
				    		.setProjection(Projections.property("curriculumCriteria.id"))
		    		));
		    	}
		    })
			.put("credits", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Curriculum.class, "curriculumCriteria")
				    		.add(Restrictions.eqProperty("curriculumCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("curriculumCriteria.curriculumSemesters", "curriculumSemestersAlias")
				    		.add(createNumberRestrictions("curriculumSemestersAlias.credits", value, Float.class))
				    		.setProjection(Projections.property("curriculumCriteria.id"))
		    		));
		    	}
		    })
			.put("allHours", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Curriculum.class, "curriculumCriteria")
				    		.add(Restrictions.eqProperty("curriculumCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("curriculumCriteria.curriculumSemesters", "curriculumSemestersAlias")
				    		.add(createNumberRestrictions("curriculumSemestersAlias.allHours", value, Short.class))
				    		.setProjection(Projections.property("curriculumCriteria.id"))
		    		));
		    	}
		    })
			.put("independentStudy", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Curriculum.class, "curriculumCriteria")
				    		.add(Restrictions.eqProperty("curriculumCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("curriculumCriteria.curriculumSemesters", "curriculumSemestersAlias")
				    		.add(createNumberRestrictions("curriculumSemestersAlias.independentStudy", value,
				    				Short.class))
				    		.setProjection(Projections.property("curriculumCriteria.id"))
		    		));
		    	}
		    })
			.put("report", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Curriculum.class, "curriculumCriteria")
				    		.add(Restrictions.eqProperty("curriculumCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("curriculumCriteria.curriculumSemesters", "curriculumSemestersAlias")
				    		.add(createEnumRestriction("curriculumSemestersAlias.report", value, Report.class))
				    		.setProjection(Projections.property("curriculumCriteria.id"))
		    		));
		    	}
		    })
			.put("curriculumSemesterHours", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Curriculum.class, "curriculumCriteria")
		    				.add(Restrictions.eqProperty("curriculumCriteria.id", Constants.ROOT_CRITERIA + ".id"))
		    				.createAlias("curriculumCriteria.curriculumSemesters", "curriculumSemestersAlias")
		    				.createAlias("curriculumSemestersAlias.curriculumSemesterHours",
		    						"curriculumSemesterHoursAlias")
		    				.add(createNumberRestrictions("curriculumSemesterHoursAlias.id", value, Long.class))
		    				.setProjection(Projections.property("curriculumCriteria.id"))
		    		));
		    	}
		    })
			.put("curriculumSemesterHoursSize", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Curriculum.class, "curriculumCriteria")
		    				.add(Restrictions.eqProperty("curriculumCriteria.id", Constants.ROOT_CRITERIA + ".id"))
		    				.createAlias("curriculumCriteria.curriculumSemesters", "curriculumSemestersAlias")
		    				.add(createSizeRestrictions("curriculumSemestersAlias.curriculumSemesterHours", value))
		    				.setProjection(Projections.property("curriculumCriteria.id"))
		    		));
		    	}
		    })
			.put("classType", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Curriculum.class, "curriculumCriteria")
		    				.add(Restrictions.eqProperty("curriculumCriteria.id", Constants.ROOT_CRITERIA + ".id"))
		    				.createAlias("curriculumCriteria.curriculumSemesters", "curriculumSemestersAlias")
		    				.createAlias("curriculumSemestersAlias.curriculumSemesterHours",
		    						"curriculumSemesterHoursAlias")
		    				.add(createEnumRestriction("curriculumSemesterHoursAlias.classType", value,
		    						ClassType.class))
		    				.setProjection(Projections.property("curriculumCriteria.id"))
		    		));
		    	}
		    })
			.put("hours", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Curriculum.class, "curriculumCriteria")
		    				.add(Restrictions.eqProperty("curriculumCriteria.id", Constants.ROOT_CRITERIA + ".id"))
		    				.createAlias("curriculumCriteria.curriculumSemesters", "curriculumSemestersAlias")
		    				.createAlias("curriculumSemestersAlias.curriculumSemesterHours",
		    						"curriculumSemesterHoursAlias")
		    				.add(createNumberRestrictions("curriculumSemesterHoursAlias.hours", value,
		    						Short.class))
		    				.setProjection(Projections.property("curriculumCriteria.id"))
		    		));
		    	}
		    })
			.put("isAcademicGroupDepartment", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Curriculum.class, "curriculumCriteria")
		    				.add(Restrictions.eqProperty("curriculumCriteria.id", Constants.ROOT_CRITERIA + ".id"))
		    				.createAlias("curriculumCriteria.curriculumSemesters", "curriculumSemestersAlias")
		    				.createAlias("curriculumSemestersAlias.curriculumSemesterHours",
		    						"curriculumSemesterHoursAlias")
		    				.add(createBooleanRestriction("curriculumSemesterHoursAlias.isAcademicGroupDepartment",
		    						value))
		    				.setProjection(Projections.property("curriculumCriteria.id"))
		    		));
		    	}
		    })
		    .build();
	
	protected static final ImmutableMap<String, OrderProcessor> ORDER_PROCESSORS =
			ImmutableMap.<String, OrderProcessor>builder()
			.putAll(DEFAULT_ORDER_PROCESSORS)
			.put("courseType", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "courseType";
		    	}
		    })
			.put("courseCycle", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "courseCycle";
		    	}
		    })
		    .build();
	
	@Override
	protected ResultTransformer getResultTransformer() {
		return ResultTransformers.CURRICULUM_RESULT_TRANSFORMER;
	}
	
	@Override
	protected List<String> getDefaultFields() {
		return DEFAULT_FIELDS;
	}
	
	@Override
	protected Map<String, ProjectionProcessor> getProjectionProcessors() {
		return PROJECTION_PROCESSORS;
	}
	
	@Override
	protected ImmutableMap<String, ParameterProcessor> getParameterProcessors() {
		return PARAMETER_PROCESSORS;
	}
	
	@Override
	protected Map<String, OrderProcessor> getOrderProcessors() {
		return ORDER_PROCESSORS;
	}
	
}
