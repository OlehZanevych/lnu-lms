package org.lnu.dao.result.unit;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.transform.ResultTransformer;
import org.lnu.dao.annotation.CriteriaResult;
import org.lnu.dao.result.DefaultCriteriaResult;
import org.lnu.dao.result.childcollection.factory.ChildCollectionFactory;
import org.lnu.dao.result.transformer.ResultTransformers;
import org.lnu.model.enumtype.unit.type.UnitType;
import org.lnu.model.unit.Unit;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

/**
 * Class, that contains methods for receiving
 * Units from Hibernate Criteria.
 * @author OlehZanevych
 */
@CriteriaResult("unitCriteriaResult")
public class UnitCriteriaResult extends DefaultCriteriaResult<Unit, Long> {
	
	private static final ImmutableList<String> DEFAULT_FIELDS =
			ImmutableList.<String>builder()
			.add("name")
			.add("abbreviation")
			.add("unitType")
			.add("website")
			.add("email")
			.add("phone")
			.add("deanId")
			.add("buildingMapId")
			.add("onMapId")
			.add("buildingId")
			.add("room")
			.build();
	
	protected static final ImmutableMap<String, ProjectionProcessor> PROJECTION_PROCESSORS =
			ImmutableMap.<String, ProjectionProcessor>builder()
			.putAll(DEFAULT_PROJECTION_PROCESSORS)
		    .put("name", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("name"), "name");
		    	}
		    })
		    .put("abbreviation", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("abbreviation"), "abbreviation");
		    	}
		    })
		    .put("unitType", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("unitType"), "unitType");
		    	}
		    })
		    .put("website", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("website"), "website");
		    	}
		    })
		    .put("email", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("email"), "email");
		    	}
		    })
		    .put("phone", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("phone"), "phone");
		    	}
		    })
		    .put("deanId", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("dean.id"), "dean.id");
		    	}
		    })
		    .put("buildingMapId", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("buildingMap.id"), "buildingMap.id");
		    	}
		    })
		    .put("onMapId", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("onMapId"), "onMapId");
		    	}
		    })
		    .put("buildingId", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		createAlias("buildingMap", "buildingMapAlias", criteria, aliases);
		    		projections.add(Projections.property("buildingMapAlias.building.id"),
		    				"buildingMap.building.id");
		    	}
		    })
		    .put("room", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("room"), "room");
		    	}
		    })
		    .put("info", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("info"), "info");
		    	}
		    })
		    .build();
	
	private static final ImmutableMap<String, ParameterProcessor> PARAMETER_PROCESSORS =
			ImmutableMap.<String, ParameterProcessor>builder()
			.putAll(COMMON_PARAMETER_PROCESSORS)
			.putAll(getDefaultParameterProcessors(Unit.class))
			.put("name", new ParameterProcessor() {
				public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
					criteria.add(createLikeRestrictions("name", value));
		    	}
		    })
			.put("abbreviation", new ParameterProcessor() {
				public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
					criteria.add(createLikeRestrictions("abbreviation", value));
		    	}
		    })
			.put("unitType", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createEnumRestriction("unitType", value, UnitType.class));
		    	}
		    })
		    .put("website", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createLikeRestrictions("website", value));
		    	}
		    })
		    .put("email", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createLikeRestrictions("email", value));
		    	}
		    })
		    .put("phone", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createLikeRestrictions("phone", value));
		    	}
		    })
		    .put("deanId", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createUserIdRestrictions("dean.id", value, userId));
		    	}
		    })
		    .put("buildingMapId", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createNumberRestrictions("buildingMap.id", value, Long.class));
		    	}
		    })
		    .put("onMapId", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createNumberRestrictions("onMapId", value, Short.class));
		    	}
		    })
		    .put("buildingId", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		createAlias("buildingMap", "buildingMapAlias", criteria, aliases);
		    		criteria.add(createNumberRestrictions("buildingMapAlias.building.id", value, Long.class));
		    	}
		    })
		    .put("room", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createLikeRestrictions("room", value));
		    	}
		    })
		    .put("info", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createLikeRestrictions("info", value));
		    	}
		    })
		    .build();
	
	protected static final ImmutableMap<String, OrderProcessor> ORDER_PROCESSORS =
			ImmutableMap.<String, OrderProcessor>builder()
			.putAll(DEFAULT_ORDER_PROCESSORS)
			.put("name", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "name";
		    	}
		    })
			.put("abbreviation", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "abbreviation";
		    	}
		    })
			.put("unitType", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "unitType";
		    	}
		    })
			.put("website", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "website";
		    	}
		    })
			.put("email", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "email";
		    	}
		    })
			.put("phone", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "phone";
		    	}
		    })
			.put("deanId", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "dean.id";
		    	}
		    })
			.put("buildingMapId", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "buildingMap.id";
		    	}
		    })
			.put("onMapId", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "onMapId";
		    	}
		    })
			.put("buildingId", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		createAlias("buildingMap", "buildingMapAlias", criteria, aliases);
		    		return "buildingMapAlias.building.id";
		    	}
		    })
			.put("room", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "room";
		    	}
		    })
			.put("info", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "info";
		    	}
		    })
		    .build();
	
	@Override
	protected ResultTransformer getResultTransformer() {
		return ResultTransformers.FACULTY_RESULT_TRANSFORMER;
	}
	
	@Override
	protected List<String> getDefaultFields() {
		return DEFAULT_FIELDS;
	}
	
	@Override
	protected Map<String, ProjectionProcessor> getProjectionProcessors() {
		return PROJECTION_PROCESSORS;
	}
	
	@Override
	protected ImmutableMap<String, ParameterProcessor> getParameterProcessors() {
		return PARAMETER_PROCESSORS;
	}
	
	@Override
	protected Map<String, OrderProcessor> getOrderProcessors() {
		return ORDER_PROCESSORS;
	}
	
}
