package org.lnu.dao.result;

import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.lnu.dao.result.childcollection.factory.ChildCollectionFactory;
import org.lnu.model.ModelWithSimpleId;

import com.google.common.collect.ImmutableMap;

/**
 * Class, that contains common methods and processors
 * for receiving data from Hibernate Criteria
 * for entity with simple id.
 * @author OlehZanevych
 * 
 * @param <ENTITY> entity class
 * @param <KEY> identifier class
 */
public abstract class AbstractCriteriaResultWithSimpleId<ENTITY extends ModelWithSimpleId<KEY>,
		KEY extends Number> extends AbstractCriteriaResult<ENTITY, KEY> {
	
	protected static final ImmutableMap<String, ProjectionProcessor> IGNORED_PROJECTION_PROCESSORS =
			ImmutableMap.<String, ProjectionProcessor>builder()
		    .put("id", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    	}
		    })
		    .build();
	
	protected static final ImmutableMap<String, ParameterProcessor> COMMON_PARAMETER_PROCESSORS =
			ImmutableMap.<String, ParameterProcessor>builder()
		    .put("id", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createNumberRestrictions("id", value, Long.class));
		    	}
		    })
		    .build();
	
	protected static final ImmutableMap<String, OrderProcessor> COMMON_ORDER_PROCESSORS =
			ImmutableMap.<String, OrderProcessor>builder()
		    .put("id", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "id";
		    	}
		    })
		    .build();
	
	@Override
	protected Criterion createIdRestriction(final KEY id) {
		return Restrictions.eq("id", id);
	}
	
	@Override
	protected void addIdProjection(final ProjectionList projections) {
		projections.add(Projections.property("id"), "id");
	}
	
	@Override
	protected Map<String, ProjectionProcessor> getProjectionProcessors() {
		return IGNORED_PROJECTION_PROCESSORS;
	}
	
	@Override
	protected Map<String, ParameterProcessor> getParameterProcessors() {
		return COMMON_PARAMETER_PROCESSORS;
	}
	
	@Override
	protected Map<String, OrderProcessor> getOrderProcessors() {
		return COMMON_ORDER_PROCESSORS;
	}

}
