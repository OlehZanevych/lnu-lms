package org.lnu.dao.result.role;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.transform.ResultTransformer;
import org.lnu.dao.annotation.CriteriaResult;
import org.lnu.dao.constants.Constants;
import org.lnu.dao.result.DefaultCriteriaResult;
import org.lnu.dao.result.childcollection.factory.ChildCollectionFactory;
import org.lnu.dao.result.transformer.ResultTransformers;
import org.lnu.model.api.capability.APICapability;
import org.lnu.model.enumtype.api.API;
import org.lnu.model.enumtype.api.capability.level.APICapabilityLevel;
import org.lnu.model.role.Role;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

/**
 * Class, that contains methods for receiving
 * Roles from Hibernate Criteria.
 * @author OlehZanevych
 */
@CriteriaResult("roleCriteriaResult")
public class RoleCriteriaResult extends DefaultCriteriaResult<Role, Long> {
	
	private static final ImmutableList<String> DEFAULT_FIELDS =
			ImmutableList.<String>builder()
			.add("name")
			.add("abbreviation")
			.add("description")
			.build();
	
	protected static final ImmutableMap<String, ProjectionProcessor> PROJECTION_PROCESSORS =
			ImmutableMap.<String, ProjectionProcessor>builder()
			.putAll(DEFAULT_PROJECTION_PROCESSORS)
		    .put("name", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("name"), "name");
		    	}
		    })
		    .put("abbreviation", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("abbreviation"), "abbreviation");
		    	}
		    })
		    .put("description", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("description"), "description");
		    	}
		    })
		    .put("apiCapabilities", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("apiCapabilities", "apiCapabilitiesAlias",
		    				APICapability.class, ResultTransformers.API_CAPABILITY_RESULT_TRANSFORMER);
		    	}
		    })
		    .put("api", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("apiCapabilities", "apiCapabilitiesAlias",
		    				APICapability.class, ResultTransformers.API_CAPABILITY_RESULT_TRANSFORMER)
		    			.addProjection("apiCapabilitiesAlias.api", "api");
		    	}
		    })
		    .put("apiCapabilityLevel", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("apiCapabilities", "apiCapabilitiesAlias",
		    				APICapability.class, ResultTransformers.API_CAPABILITY_RESULT_TRANSFORMER)
		    			.addProjection("apiCapabilitiesAlias.apiCapabilityLevel", "apiCapabilityLevel");
		    	}
		    })
		    .build();
	
	private static final ImmutableMap<String, ParameterProcessor> PARAMETER_PROCESSORS =
			ImmutableMap.<String, ParameterProcessor>builder()
			.putAll(COMMON_PARAMETER_PROCESSORS)
			.putAll(getDefaultParameterProcessors(Role.class))
			.put("name", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long roleId) {
		    		
		    		criteria.add(createLikeRestrictions("name", value));
		    	}
		    })
			.put("abbreviation", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long roleId) {
		    		
		    		criteria.add(createLikeRestrictions("abbreviation", value));
		    	}
		    })
			.put("description", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long roleId) {
		    		
		    		criteria.add(createLikeRestrictions("description", value));
		    	}
		    })
			.put("apiCapabilities", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long roleId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Role.class, "roleCriteria")
				    		.add(Restrictions.eqProperty("roleCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("roleCriteria.apiCapabilities", "apiCapabilitiesAlias")
				    		.add(createNumberRestrictions("apiCapabilitiesAlias.id", value, Long.class))
				    		.setProjection(Projections.property("roleCriteria.id"))
		    		));
		    	}
		    })
			.put("apiCapabilitiesSize", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long roleId) {
		    		
		    		criteria.add(createSizeRestrictions("apiCapabilities", value));
		    	}
		    })
			.put("api", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long roleId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Role.class, "roleCriteria")
				    		.add(Restrictions.eqProperty("roleCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("roleCriteria.apiCapabilities", "apiCapabilitiesAlias")
				    		.add(createEnumRestriction("apiCapabilitiesAlias.api", value, API.class))
				    		.setProjection(Projections.property("roleCriteria.id"))
		    		));
		    	}
		    })
			.put("apiCapabilityLevel", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long roleId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Role.class, "roleCriteria")
				    		.add(Restrictions.eqProperty("roleCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("roleCriteria.apiCapabilities", "apiCapabilitiesAlias")
				    		.add(createEnumRestriction("apiCapabilitiesAlias.apiCapabilityLevel", value,
				    				APICapabilityLevel.class))
				    		.setProjection(Projections.property("roleCriteria.id"))
		    		));
		    	}
		    })
		    .build();
	
	private static final ImmutableMap<String, OrderProcessor> ORDER_PROCESSORS =
			ImmutableMap.<String, OrderProcessor>builder()
			.putAll(DEFAULT_ORDER_PROCESSORS)
			.put("name", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "name";
		    	}
		    })
			.put("abbreviation", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "abbreviation";
		    	}
		    })
			.put("description", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "description";
		    	}
		    })
		    .build();
	
	@Override
	protected ResultTransformer getResultTransformer() {
		return ResultTransformers.ROLE_RESULT_TRANSFORMER;
	}
	
	@Override
	protected List<String> getDefaultFields() {
		return DEFAULT_FIELDS;
	}
	
	@Override
	protected Map<String, ProjectionProcessor> getProjectionProcessors() {
		return PROJECTION_PROCESSORS;
	}
	
	@Override
	protected ImmutableMap<String, ParameterProcessor> getParameterProcessors() {
		return PARAMETER_PROCESSORS;
	}
	
	@Override
	protected Map<String, OrderProcessor> getOrderProcessors() {
		return ORDER_PROCESSORS;
	}
	
}
