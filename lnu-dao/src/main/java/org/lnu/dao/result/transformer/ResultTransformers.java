package org.lnu.dao.result.transformer;

import org.hibernate.transform.ResultTransformer;
import org.lnu.model.academicgroup.AcademicGroup;
import org.lnu.model.api.capability.APICapability;
import org.lnu.model.atomicload.AtomicLoad;
import org.lnu.model.building.Building;
import org.lnu.model.building.map.BuildingMap;
import org.lnu.model.course.Course;
import org.lnu.model.curriculum.Curriculum;
import org.lnu.model.curriculum.course.CurriculumCourse;
import org.lnu.model.curriculum.semester.CurriculumSemester;
import org.lnu.model.curriculum.semester.hours.CurriculumSemesterHours;
import org.lnu.model.department.Department;
import org.lnu.model.dormitory.Dormitory;
import org.lnu.model.unit.Unit;
import org.lnu.model.group.Group;
import org.lnu.model.learningplace.LearningPlace;
import org.lnu.model.learningplacegroup.LearningPlaceGroup;
import org.lnu.model.lecturer.Lecturer;
import org.lnu.model.load.Load;
import org.lnu.model.otheruniversityplace.OtherUniversityPlace;
import org.lnu.model.role.Role;
import org.lnu.model.scientificdegree.ScientificDegree;
import org.lnu.model.specialty.Specialty;
import org.lnu.model.student.Student;
import org.lnu.model.toilet.Toilet;
import org.lnu.model.user.User;
import org.lnu.model.user.api.capability.UserApiCapability;

/**
 * Class that contains all Hibernate Criteria result transformers.
 * @author OlehZanevych
 */
public class ResultTransformers {
	
	public static final ResultTransformer ACADEMIC_GROUP_RESULT_TRANSFORMER =
			new DefaultResultTransformer(AcademicGroup.class);
	
	public static final ResultTransformer API_CAPABILITY_RESULT_TRANSFORMER =
			new DefaultResultTransformer(APICapability.class);
	
	public static final ResultTransformer ATOMIC_LOAD_RESULT_TRANSFORMER =
			new DefaultResultTransformer(AtomicLoad.class);
	
	public static final ResultTransformer BUILDING_RESULT_TRANSFORMER =
			new DefaultResultTransformer(Building.class);
	
	public static final ResultTransformer BUILDING_MAP_RESULT_TRANSFORMER =
			new DefaultResultTransformer(BuildingMap.class);
	
	public static final ResultTransformer COURSE_RESULT_TRANSFORMER =
			new DefaultResultTransformer(Course.class);
	
	public static final ResultTransformer CURRICULUM_RESULT_TRANSFORMER =
			new DefaultResultTransformer(Curriculum.class);
	
	public static final ResultTransformer CURRICULUM_COURSE_RESULT_TRANSFORMER =
			new DefaultResultTransformer(CurriculumCourse.class);
	
	public static final ResultTransformer CURRICULUM_SEMESTER_RESULT_TRANSFORMER =
			new DefaultResultTransformer(CurriculumSemester.class);
	
	public static final ResultTransformer CURRICULUM_SEMESTER_HOURS_RESULT_TRANSFORMER =
			new DefaultResultTransformer(CurriculumSemesterHours.class);
	
	public static final ResultTransformer DEPARTMENT_RESULT_TRANSFORMER =
			new DefaultResultTransformer(Department.class);
	
	public static final ResultTransformer DORMITORY_RESULT_TRANSFORMER =
			new DefaultResultTransformer(Dormitory.class);
	
	public static final ResultTransformer FACULTY_RESULT_TRANSFORMER =
			new DefaultResultTransformer(Unit.class);
	
	public static final ResultTransformer GROUP_RESULT_TRANSFORMER =
			new DefaultResultTransformer(Group.class);
	
	public static final ResultTransformer LEARNING_PLACE_RESULT_TRANSFORMER =
			new DefaultResultTransformer(LearningPlace.class);
	
	public static final ResultTransformer LEARNING_PLACE_GROUP_RESULT_TRANSFORMER =
			new DefaultResultTransformer(LearningPlaceGroup.class);
	
	public static final ResultTransformer LECTURER_RESULT_TRANSFORMER =
			new DefaultResultTransformer(Lecturer.class);
	
	public static final ResultTransformer LOAD_RESULT_TRANSFORMER =
			new DefaultResultTransformer(Load.class);
	
	public static final ResultTransformer OTHER_UNIVERSITY_PLACE_RESULT_TRANSFORMER =
			new DefaultResultTransformer(OtherUniversityPlace.class);
	
	public static final ResultTransformer ROLE_RESULT_TRANSFORMER =
			new DefaultResultTransformer(Role.class);
	
	public static final ResultTransformer SCIENTIFIC_DEGREE_RESULT_TRANSFORMER =
			new DefaultResultTransformer(ScientificDegree.class);
	
	public static final ResultTransformer SPECIALTY_RESULT_TRANSFORMER =
			new DefaultResultTransformer(Specialty.class);
	
	public static final ResultTransformer STUDENT_RESULT_TRANSFORMER =
			new DefaultResultTransformer(Student.class);
	
	public static final ResultTransformer TOILET_RESULT_TRANSFORMER =
			new DefaultResultTransformer(Toilet.class);
	
	public static final ResultTransformer USER_RESULT_TRANSFORMER =
			new DefaultResultTransformer(User.class);
	
	public static final ResultTransformer USER_API_CAPABILITY_RESULT_TRANSFORMER =
			new DefaultResultTransformer(UserApiCapability.class);

}
