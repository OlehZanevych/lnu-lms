package org.lnu.dao.result.childcollection;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.HashSet;

import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.transform.ResultTransformer;
import org.lnu.model.Model;
import org.lnu.model.result.alias.Alias;

/**
 * Class that is used for deep representation of child collection.
 * @author OlehZanevych
 */
public class ChildCollection {
	
	private String collectionField;
	
	private Class<? extends Model> entityClass;
	
	private ResultTransformer resultTransformer;
	
	private HashSet<Alias> aliases = new HashSet<Alias>();
	
	private ProjectionList projections = Projections.projectionList();
	
	private HashMap<String, ChildCollection> children = new HashMap<String, ChildCollection>();

	/**
	 * Constructor.
	 * @param collectionField collection field name
	 * @param alias alias name
	 * @param entityClass entity class
	 * @param resultTransformer Hibernate Criteria result transformer
	 */
	public ChildCollection(final String collectionField, final String alias,
			final Class<? extends Model> entityClass, final ResultTransformer resultTransformer) {
		
		this.collectionField = collectionField;
		this.entityClass = entityClass;
		this.resultTransformer = resultTransformer;
		
		createAlias(collectionField, alias);
		
		addProjection("id", "parentId");
		addProjection(MessageFormat.format("{0}.id", alias), "id");
	}
	
	/**
	 * Method for creating alias for current child collection joined field.
	 * @param associationPath association path
	 * @param alias alias
	 * @return ChildCollection
	 */
	public ChildCollection createAlias(final String associationPath, final String alias) {
		aliases.add(new Alias(associationPath, alias));
		return this;
	}
	
	/**
	 * Method for adding collection projection.
	 * @param collectionField collection field
	 * @param alias collection alias
	 * @param entityClass entity class
	 * @param resultTransformer Hibernate Criteria result transformer
	 * @return ChildCollection
	 */
	public ChildCollection addCollectionProjection(final String collectionField, final String alias,
			final Class<? extends Model> entityClass, final ResultTransformer resultTransformer) {
		
		ChildCollection childCollection = children.get(alias);
		if (childCollection == null) {
			childCollection = new ChildCollection(collectionField, alias, entityClass, resultTransformer);
			children.put(alias, childCollection);
		}
		return childCollection;
	}
	
	/**
	 * Method for adding projection for current child collection field.
	 * @param fromField field from which store value
	 * @param inField field in which store value
	 * @return ChildCollection
	 */
	public ChildCollection addProjection(final String fromField, final String inField) {
		projections.add(Projections.property(fromField), inField);
		return this;
	}

	public String getCollectionField() {
		return collectionField;
	}

	public Class<? extends Model> getEntityClass() {
		return entityClass;
	}

	public ResultTransformer getResultTransformer() {
		return resultTransformer;
	}

	public HashSet<Alias> getAliases() {
		return aliases;
	}

	public ProjectionList getProjections() {
		return projections;
	}

	public HashMap<String, ChildCollection> getChildren() {
		return children;
	}
	
}
