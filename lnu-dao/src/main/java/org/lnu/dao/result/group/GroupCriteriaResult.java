package org.lnu.dao.result.group;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.transform.ResultTransformer;
import org.lnu.dao.annotation.CriteriaResult;
import org.lnu.dao.constants.Constants;
import org.lnu.dao.result.DefaultCriteriaResult;
import org.lnu.dao.result.childcollection.factory.ChildCollectionFactory;
import org.lnu.dao.result.transformer.ResultTransformers;
import org.lnu.model.group.Group;
import org.lnu.model.user.User;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

/**
 * Class, that contains methods for receiving
 * Groups from Hibernate Criteria.
 * @author OlehZanevych
 */
@CriteriaResult("groupCriteriaResult")
public class GroupCriteriaResult extends DefaultCriteriaResult<Group, Long> {
	
	private static final ImmutableList<String> DEFAULT_FIELDS =
			ImmutableList.<String>builder()
			.add("name")
			.add("abbreviation")
			.add("description")
			.build();
	
	protected static final ImmutableMap<String, ProjectionProcessor> PROJECTION_PROCESSORS =
			ImmutableMap.<String, ProjectionProcessor>builder()
			.putAll(DEFAULT_PROJECTION_PROCESSORS)
		    .put("name", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("name"), "name");
		    	}
		    })
		    .put("abbreviation", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("abbreviation"), "abbreviation");
		    	}
		    })
		    .put("description", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("description"), "description");
		    	}
		    })
		    .put("userIds", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("users", "usersAlias",
		    				User.class, ResultTransformers.USER_RESULT_TRANSFORMER);
		    	}
		    })
		    .build();
	
	private static final ImmutableMap<String, ParameterProcessor> PARAMETER_PROCESSORS =
			ImmutableMap.<String, ParameterProcessor>builder()
			.putAll(COMMON_PARAMETER_PROCESSORS)
			.putAll(getDefaultParameterProcessors(Group.class))
			.put("name", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long groupId) {
		    		
		    		criteria.add(createLikeRestrictions("name", value));
		    	}
		    })
			.put("abbreviation", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long groupId) {
		    		
		    		criteria.add(createLikeRestrictions("abbreviation", value));
		    	}
		    })
			.put("description", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long groupId) {
		    		
		    		criteria.add(createLikeRestrictions("description", value));
		    	}
		    })
			.put("userIds", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Group.class, "groupCriteria")
				    		.add(Restrictions.eqProperty("groupCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("groupCriteria.users", "usersAlias")
				    		.add(createUserIdRestrictions("usersAlias.id", value, userId))
				    		.setProjection(Projections.property("groupCriteria.id"))
		    		));
		    	}
		    })
			.put("userIdsSize", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createSizeRestrictions("users", value));
		    	}
		    })
		    .build();
	
	private static final ImmutableMap<String, OrderProcessor> ORDER_PROCESSORS =
			ImmutableMap.<String, OrderProcessor>builder()
			.putAll(DEFAULT_ORDER_PROCESSORS)
			.put("name", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "name";
		    	}
		    })
			.put("abbreviation", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "abbreviation";
		    	}
		    })
			.put("description", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "description";
		    	}
		    })
		    .build();
	
	@Override
	protected ResultTransformer getResultTransformer() {
		return ResultTransformers.GROUP_RESULT_TRANSFORMER;
	}
	
	@Override
	protected List<String> getDefaultFields() {
		return DEFAULT_FIELDS;
	}
	
	@Override
	protected Map<String, ProjectionProcessor> getProjectionProcessors() {
		return PROJECTION_PROCESSORS;
	}
	
	@Override
	protected ImmutableMap<String, ParameterProcessor> getParameterProcessors() {
		return PARAMETER_PROCESSORS;
	}
	
	@Override
	protected Map<String, OrderProcessor> getOrderProcessors() {
		return ORDER_PROCESSORS;
	}
	
}
