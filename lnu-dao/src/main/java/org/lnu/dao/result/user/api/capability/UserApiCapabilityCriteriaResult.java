package org.lnu.dao.result.user.api.capability;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.hibernate.transform.ResultTransformer;
import org.lnu.dao.annotation.CriteriaResult;
import org.lnu.dao.result.AbstractCriteriaResult;
import org.lnu.dao.result.childcollection.factory.ChildCollectionFactory;
import org.lnu.dao.result.transformer.ResultTransformers;
import org.lnu.model.enumtype.api.API;
import org.lnu.model.enumtype.api.capability.level.APICapabilityLevel;
import org.lnu.model.enumtype.api.createcapability.APICreateCapability;
import org.lnu.model.id.user.api.UserApiId;
import org.lnu.model.user.api.capability.UserApiCapability;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

/**
 * Class, that contains methods for receiving
 * UserAPICapabilitys from Hibernate Criteria.
 * @author OlehZanevych
 */
@CriteriaResult("userApiCapabilityCriteriaResult")
public class UserApiCapabilityCriteriaResult extends AbstractCriteriaResult<UserApiCapability, UserApiId> {
	
	private static final ImmutableList<String> DEFAULT_FIELDS =
			ImmutableList.<String>builder()
			.add("apiCapabilityLevel")
			.add("apiCreateCapability")
			.build();
	
	protected static final ImmutableMap<String, ProjectionProcessor> PROJECTION_PROCESSORS =
			ImmutableMap.<String, ProjectionProcessor>builder()
		    .put("userId", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    	}
		    })
		    .put("api", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    	}
		    })
		    .put("apiCapabilityLevel", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("apiCapabilityLevel"), "apiCapabilityLevel");
		    	}
		    })
		    .put("apiCreateCapability", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("apiCreateCapability"), "apiCreateCapability");
		    	}
		    })
		    .build();
	
	private static final ImmutableMap<String, ParameterProcessor> PARAMETER_PROCESSORS =
			ImmutableMap.<String, ParameterProcessor>builder()
			.put("userId", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createUserIdRestrictions("user.id", value, userId));
		    	}
		    })
			.put("api", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createEnumRestriction("api", value, API.class));
		    	}
		    })
			.put("apiCapabilityLevel", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createEnumRestriction("apiCapabilityLevel", value, APICapabilityLevel.class));
		    	}
		    })
			.put("apiCreateCapability", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createEnumRestriction("apiCreateCapability", value, APICreateCapability.class));
		    	}
		    })
		    .build();
	
	private static final ImmutableMap<String, OrderProcessor> ORDER_PROCESSORS =
			ImmutableMap.<String, OrderProcessor>builder()
			.put("userId", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "user.id";
		    	}
		    })
			.put("api", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "api";
		    	}
		    })
			.put("apiCapabilityLevel", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "apiCapabilityLevel";
		    	}
		    })
			.put("apiCreateCapability", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "apiCreateCapability";
		    	}
		    })
		    .build();
	
	@Override
	protected Criterion createIdRestriction(final UserApiId userApiId) {
		SimpleExpression userIdRestriction = Restrictions.eq("user.id", userApiId.getUser().getId());
		SimpleExpression apiRestriction = Restrictions.eq("api", userApiId.getApi());
		return Restrictions.and(userIdRestriction, apiRestriction);
	}

	@Override
	protected void addIdProjection(final ProjectionList projections) {
		projections.add(Projections.property("user.id"), "user.id")
			.add(Projections.property("api"), "api");
	}
	
	@Override
	protected ResultTransformer getResultTransformer() {
		return ResultTransformers.USER_API_CAPABILITY_RESULT_TRANSFORMER;
	}
	
	@Override
	protected List<String> getDefaultFields() {
		return DEFAULT_FIELDS;
	}
	
	@Override
	protected Map<String, ProjectionProcessor> getProjectionProcessors() {
		return PROJECTION_PROCESSORS;
	}
	
	@Override
	protected ImmutableMap<String, ParameterProcessor> getParameterProcessors() {
		return PARAMETER_PROCESSORS;
	}
	
	@Override
	protected Map<String, OrderProcessor> getOrderProcessors() {
		return ORDER_PROCESSORS;
	}
	
}
