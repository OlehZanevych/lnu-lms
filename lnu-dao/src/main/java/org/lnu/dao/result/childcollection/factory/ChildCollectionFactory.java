package org.lnu.dao.result.childcollection.factory;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.ResultTransformer;
import org.lnu.dao.constants.Constants;
import org.lnu.dao.result.childcollection.ChildCollection;
import org.lnu.model.Model;
import org.lnu.model.result.alias.Alias;
import org.springframework.beans.BeanUtils;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * Class that is used for projecting fields of child collections.
 * @author OlehZanevych
 */
@SuppressFBWarnings("DM_NEW_FOR_GETCLASS")
public final class ChildCollectionFactory {
	
	private static final Class<? extends Collection<Model>> DEFAULT_LIST_CLASS =
			(Class<? extends Collection<Model>>) (new LinkedList<Model>()).getClass();
	
	private static final Class<? extends Collection<Model>> DEFAULT_SET_CLASS =
			(Class<? extends Collection<Model>>) (new HashSet<Model>()).getClass();
	
	private Session session;
	
	private Class<? extends Model> entityClass;
	
	private HashMap<String, ChildCollection> children = new HashMap<String, ChildCollection>();
	
	/**
	 * Constructor.
	 * @param session Hibernate Session
	 * @param entityClass entity class
	 */
	public ChildCollectionFactory(final Session session,
			final Class<? extends Model> entityClass) {
		
		this.session = session;
		this.entityClass = entityClass;
	}
	
	/**
	 * Method for adding collection projection.
	 * @param collectionField collection field
	 * @param alias collection alias
	 * @param entityClass entity class
	 * @param resultTransformer Hibernate Criteria result transformer
	 * @return ChildCollection
	 */
	public ChildCollection addCollectionProjection(final String collectionField, final String alias,
			final Class<? extends Model> entityClass, final ResultTransformer resultTransformer) {
		
		ChildCollection childCollection = children.get(alias);
		if (childCollection == null) {
			childCollection = new ChildCollection(collectionField, alias, entityClass, resultTransformer);
			children.put(alias, childCollection);
		}
		return childCollection;
	}
	
	/**
	 * Method for loading child collections.
	 * @param entity entity, which need to load child collection
	 */
	public void loadChildCollections(final Model entity) {
		if (entity != null) {
			Collection<ChildCollection> childCollections = children.values();
			if (!childCollections.isEmpty()) {
				for (ChildCollection childCollection : childCollections) {
					loadChildCollection(entity, childCollection, entityClass);
				}
			}
		}
	}
	
	/**
	 * Method for loading child collections.
	 * @param entities list of entities, which need to load child collections
	 */
	public void loadChildCollections(final List<? extends Model> entities) {
		if (!entities.isEmpty()) {
			Collection<ChildCollection> childCollections = children.values();
			if (!childCollections.isEmpty()) {
				Map<Object, Model> entitiesMap = entities.stream()
						.collect(Collectors.toMap(i -> i.getId(), i -> i));
				for (ChildCollection childCollection : childCollections) {
					loadChildCollection(entitiesMap, childCollection, entityClass);
				}
			}
		}
	}
	
	/**
	 * Method for loading child collection.
	 * @param entity entity, which need to load child collection
	 */
	private void loadChildCollection(final Model entity, final ChildCollection childCollection,
			final Class<? extends Model> entityClass) {
		
		Object id = entity.getId();
		
		Criteria criteria = session.createCriteria(entityClass, Constants.ROOT_CRITERIA)
				.add(Restrictions.eq("id", id));
		
		Collection<Model> childEntities = loadChildEntities(criteria, childCollection);
		
		if (!childEntities.isEmpty()) {
			String collectionField = childCollection.getCollectionField();
			Class<?> collectionType = FieldUtils.getField(entityClass, collectionField, true).getType();
			if (collectionType.equals(Set.class)) {
				childEntities = new HashSet<Model>(childEntities);
			}
			writeCollectionField(entity, collectionField, childEntities);
			
			loadChildCollections(childEntities, childCollection);
		}
	}
	
	/**
	 * Method for loading child collection.
	 * @param entitiesMap map of entities, which need to load child collection
	 * @param childCollection child collection
	 * @param entityClass entity class
	 */
	private void loadChildCollection(final Map<Object, Model> entitiesMap,
			final ChildCollection childCollection, final Class<? extends Model> entityClass) {
		
		Object[] ids = entitiesMap.keySet().stream().toArray(Object[]::new);
		
		Criteria criteria = session.createCriteria(entityClass, Constants.ROOT_CRITERIA)
				.add(Restrictions.in("id", ids));
		
		List<Model> childEntities = loadChildEntities(criteria, childCollection);
		
		String collectionField = childCollection.getCollectionField();
		Class<?> collectionType = FieldUtils.getField(entityClass, collectionField, true).getType();
		Class<? extends Collection<Model>> collectionClass = null;
		if (collectionType.equals(List.class)) {
			collectionClass = DEFAULT_LIST_CLASS;
		} else {
			if (collectionType.equals(Set.class)) {
				collectionClass = DEFAULT_SET_CLASS;
			} else {
				throw new IllegalArgumentException(MessageFormat.format(
						"Field {0} is not collection of appropriate type. Please, contact dev team for fixing this issue",
						collectionField));
			}
		}
		Map<Number, Collection<Model>> collections =
				new HashMap<Number, Collection<Model>>();
		for (Model childEntity : childEntities) {
			Number parentId = childEntity.getParentId();
			Collection<Model> collection = collections.get(parentId);
			if (collection == null) {
				collection = BeanUtils.instantiateClass(collectionClass);
				collections.put(parentId, collection);
			}
			collection.add(childEntity);
		}
		for (Entry<Number, Collection<Model>> collectionEntry : collections.entrySet()) {
			writeCollectionField(entitiesMap.get(collectionEntry.getKey()), collectionField,
					collectionEntry.getValue());
		}
		if (!childEntities.isEmpty()) {
			loadChildCollections(childEntities, childCollection);
		}
	}
	
	/**
	 * Method for loading child entities.
	 * @param criteria Hibernate Criteria
	 * @param childCollection child collection
	 * @return loaded child entities
	 */
	private List<Model> loadChildEntities(final Criteria criteria,
			final ChildCollection childCollection) {
		
		for (Alias childAlias : childCollection.getAliases()) {
			criteria.createAlias(childAlias.getAssociationPath(), childAlias.getAlias());
		}
		criteria.setProjection(childCollection.getProjections())
			.setResultTransformer(childCollection.getResultTransformer());
		return criteria.list();
	}
	
	/**
	 * Method for loading child collections.
	 * @param childEntities child entities
	 * @param childCollection child collection
	 */
	private void loadChildCollections(final Collection<Model> childEntities,
			final ChildCollection childCollection) {
		
		Collection<ChildCollection> childCollectionChildren = childCollection.getChildren().values();
		if (!childCollectionChildren.isEmpty()) {
			Map<Object, Model> childEntitiesMap = childEntities.stream()
					.collect(Collectors.toMap(i -> i.getId(), i -> i));
			
			Class<? extends Model> childEntityClass = childCollection.getEntityClass();
			for (ChildCollection child : childCollectionChildren) {
				loadChildCollection(childEntitiesMap, child, childEntityClass);
			}
		}
	}
	
	/**
	 * Method for setting entity field.
	 * @param entity entity
	 * @param collectionField collection field
	 * @param collection collection
	 */
	private void writeCollectionField(final Model entity, final String collectionField,
			final Collection<Model> collection) {
		
		try {
			FieldUtils.writeField(entity,  collectionField, collection, true);
		} catch (IllegalAccessException e) {
			throw new IllegalArgumentException(MessageFormat.format(
					"Can't save field {0} for class {1}. Please, contact dev team for fixing this issue",
					collectionField, entity.getClass()));
		}
	}
	
}
