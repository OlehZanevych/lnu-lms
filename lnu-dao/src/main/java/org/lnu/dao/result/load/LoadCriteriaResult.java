package org.lnu.dao.result.load;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.transform.ResultTransformer;
import org.lnu.dao.annotation.CriteriaResult;
import org.lnu.dao.constants.Constants;
import org.lnu.dao.result.DefaultCriteriaResult;
import org.lnu.dao.result.childcollection.factory.ChildCollectionFactory;
import org.lnu.dao.result.transformer.ResultTransformers;
import org.lnu.model.academicgroup.AcademicGroup;
import org.lnu.model.atomicload.AtomicLoad;
import org.lnu.model.enumtype.classtype.ClassType;
import org.lnu.model.learningplace.LearningPlace;
import org.lnu.model.learningplacegroup.LearningPlaceGroup;
import org.lnu.model.lecturer.Lecturer;
import org.lnu.model.load.Load;
import org.lnu.model.specialty.Specialty;
import org.lnu.model.student.Student;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

/**
 * Class, that contains methods for receiving
 * Load from Hibernate Criteria.
 * @author OlehZanevych
 */
@CriteriaResult("loadCriteriaResult")
public class LoadCriteriaResult extends DefaultCriteriaResult<Load, Long> {
	
	private static final ImmutableList<String> DEFAULT_FIELDS =
			ImmutableList.<String>builder()
			.add("curriculumSemesterHoursId")
			.add("classType")
			.add("allHours")
			.add("semester")
			.add("year")
			.add("half")
			.add("courseId")
			.add("departmentId")
			.add("hours")
			.add("lessonDuration")
			.add("lecturerIds")
			.add("specialtyIds")
			.add("academicGroupIds")
			.add("studentIds")
			.add("learningPlaceGroupIds")
			.add("learningPlaceIds")
			.build();
	
	protected static final ImmutableMap<String, ProjectionProcessor> PROJECTION_PROCESSORS =
			ImmutableMap.<String, ProjectionProcessor>builder()
			.putAll(DEFAULT_PROJECTION_PROCESSORS)
		    .put("curriculumSemesterHoursId", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("curriculumSemesterHours.id"), "curriculumSemesterHours.id");
		    	}
		    })
		    .put("classType", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		createAlias("curriculumSemesterHours", "curriculumSemesterHoursAlias", criteria, aliases);
		    		projections.add(Projections.property("curriculumSemesterHoursAlias.classType"),
		    				"curriculumSemesterHours.classType");
		    	}
		    })
		    .put("allHours", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		createAlias("curriculumSemesterHours", "curriculumSemesterHoursAlias", criteria, aliases);
		    		projections.add(Projections.property("curriculumSemesterHoursAlias.hours"),
		    				"curriculumSemesterHours.hours");
		    	}
		    })
		    .put("semester", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		createAlias("curriculumSemesterHours", "curriculumSemesterHoursAlias", criteria, aliases);
		    		createAlias("curriculumSemesterHoursAlias.curriculumSemester", "curriculumSemesterAlias",
		    				criteria, aliases);
		    		projections.add(Projections.property("curriculumSemesterAlias.semester"),
		    				"curriculumSemesterHours.curriculumSemester.semester");
		    	}
		    })
		    .put("year", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		createAlias("curriculumSemesterHours", "curriculumSemesterHoursAlias", criteria, aliases);
		    		createAlias("curriculumSemesterHoursAlias.curriculumSemester", "curriculumSemesterAlias",
		    				criteria, aliases);
		    		projections.add(Projections.property("curriculumSemesterAlias.year"),
		    				"curriculumSemesterHours.curriculumSemester.year");
		    	}
		    })
		    .put("half", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		createAlias("curriculumSemesterHours", "curriculumSemesterHoursAlias", criteria, aliases);
		    		createAlias("curriculumSemesterHoursAlias.curriculumSemester", "curriculumSemesterAlias",
		    				criteria, aliases);
		    		projections.add(Projections.property("curriculumSemesterAlias.half"),
		    				"curriculumSemesterHours.curriculumSemester.half");
		    	}
		    })
		    .put("courseId", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("course.id"), "course.id");
		    	}
		    })
		    .put("departmentId", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("department.id"), "department.id");
		    	}
		    })
		    .put("atomicLoads", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("atomicLoads", "atomicLoadsAlias",
		    				AtomicLoad.class, ResultTransformers.ATOMIC_LOAD_RESULT_TRANSFORMER);
		    	}
		    })
		    .put("hours", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("atomicLoads", "atomicLoadsAlias",
		    				AtomicLoad.class, ResultTransformers.ATOMIC_LOAD_RESULT_TRANSFORMER)
		    			.addProjection("atomicLoadsAlias.hours", "hours");
		    	}
		    })
		    .put("lessonDuration", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("atomicLoads", "atomicLoadsAlias",
		    				AtomicLoad.class, ResultTransformers.ATOMIC_LOAD_RESULT_TRANSFORMER)
		    			.addProjection("atomicLoadsAlias.lessonDuration", "lessonDuration");
		    	}
		    })
		    .put("lecturerIds", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("atomicLoads", "atomicLoadsAlias",
		    				AtomicLoad.class, ResultTransformers.ATOMIC_LOAD_RESULT_TRANSFORMER)
		    			.addCollectionProjection("atomicLoadsAlias.lecturers", "lecturersAlias",
		    					Lecturer.class, ResultTransformers.LECTURER_RESULT_TRANSFORMER);
		    	}
		    })
		    .put("specialtyIds", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("atomicLoads", "atomicLoadsAlias",
		    				AtomicLoad.class, ResultTransformers.ATOMIC_LOAD_RESULT_TRANSFORMER)
		    			.addCollectionProjection("atomicLoadsAlias.specialties", "specialtiesAlias",
		    					Specialty.class, ResultTransformers.SPECIALTY_RESULT_TRANSFORMER);
		    	}
		    })
		    .put("academicGroupIds", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("atomicLoads", "atomicLoadsAlias",
		    				AtomicLoad.class, ResultTransformers.ATOMIC_LOAD_RESULT_TRANSFORMER)
		    			.addCollectionProjection("atomicLoadsAlias.academicGroups", "academicGroupsAlias",
		    					AcademicGroup.class, ResultTransformers.ACADEMIC_GROUP_RESULT_TRANSFORMER);
		    	}
		    })
		    .put("studentIds", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("atomicLoads", "atomicLoadsAlias",
		    				AtomicLoad.class, ResultTransformers.ATOMIC_LOAD_RESULT_TRANSFORMER)
		    			.addCollectionProjection("atomicLoadsAlias.students", "studentsAlias",
		    					Student.class, ResultTransformers.STUDENT_RESULT_TRANSFORMER);
		    	}
		    })
		    .put("learningPlaceGroupIds", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("atomicLoads", "atomicLoadsAlias",
		    				AtomicLoad.class, ResultTransformers.ATOMIC_LOAD_RESULT_TRANSFORMER)
		    			.addCollectionProjection("atomicLoadsAlias.learningPlaceGroups", "learningPlaceGroupsAlias",
		    					LearningPlaceGroup.class, ResultTransformers.LEARNING_PLACE_GROUP_RESULT_TRANSFORMER);
		    	}
		    })
		    .put("learningPlaceIds", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("atomicLoads", "atomicLoadsAlias",
		    				AtomicLoad.class, ResultTransformers.ATOMIC_LOAD_RESULT_TRANSFORMER)
		    			.addCollectionProjection("atomicLoadsAlias.learningPlaces", "learningPlacesAlias",
		    					LearningPlace.class, ResultTransformers.LEARNING_PLACE_RESULT_TRANSFORMER);
		    	}
		    })
		    .build();
	
	private static final ImmutableMap<String, ParameterProcessor> PARAMETER_PROCESSORS =
			ImmutableMap.<String, ParameterProcessor>builder()
			.putAll(COMMON_PARAMETER_PROCESSORS)
			.putAll(getDefaultParameterProcessors(Load.class))
		    .put("curriculumSemesterHoursId", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createNumberRestrictions("curriculumSemesterHours.id", value, Long.class));
		    	}
		    })
		    .put("classType", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		createAlias("curriculumSemesterHours", "curriculumSemesterHoursAlias", criteria, aliases);
		    		criteria.add(createEnumRestriction("curriculumSemesterHoursAlias.classType", value,
		    				ClassType.class));
		    	}
		    })
		    .put("allHours", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		createAlias("curriculumSemesterHours", "curriculumSemesterHoursAlias", criteria, aliases);
		    		criteria.add(createNumberRestrictions("curriculumSemesterHoursAlias.hours", value,
		    				Short.class));
		    	}
		    })
		    .put("semester", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		createAlias("curriculumSemesterHours", "curriculumSemesterHoursAlias", criteria, aliases);
		    		createAlias("curriculumSemesterHoursAlias.curriculumSemester", "curriculumSemesterAlias",
		    				criteria, aliases);
		    		criteria.add(createNumberRestrictions("curriculumSemesterAlias.semester", value, Byte.class));
		    	}
		    })
		    .put("year", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		createAlias("curriculumSemesterHours", "curriculumSemesterHoursAlias", criteria, aliases);
		    		createAlias("curriculumSemesterHoursAlias.curriculumSemester", "curriculumSemesterAlias",
		    				criteria, aliases);
		    		criteria.add(createNumberRestrictions("curriculumSemesterAlias.year", value, Byte.class));
		    	}
		    })
		    .put("half", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		createAlias("curriculumSemesterHours", "curriculumSemesterHoursAlias", criteria, aliases);
		    		createAlias("curriculumSemesterHoursAlias.curriculumSemester", "curriculumSemesterAlias",
		    				criteria, aliases);
		    		criteria.add(createNumberRestrictions("curriculumSemesterAlias.half", value, Byte.class));
		    	}
		    })
		    .put("courseId", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createNumberRestrictions("course.id", value, Long.class));
		    	}
		    })
		    .put("departmentId", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createNumberRestrictions("department.id", value, Long.class));
		    	}
		    })
		    .put("atomicLoads", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Load.class, "loadCriteria")
				    		.add(Restrictions.eqProperty("loadCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("loadCriteria.atomicLoads", "atomicLoadsAlias")
				    		.add(createNumberRestrictions("atomicLoadsAlias.id", value, Long.class))
				    		.setProjection(Projections.property("loadCriteria.id"))
		    		));
		    	}
		    })
			.put("atomicLoadsSize", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createSizeRestrictions("atomicLoads", value));
		    	}
		    })
			.put("hours", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Load.class, "loadCriteria")
				    		.add(Restrictions.eqProperty("loadCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("loadCriteria.atomicLoads", "atomicLoadsAlias")
				    		.add(createNumberRestrictions("atomicLoadsAlias.hours", value, Short.class))
				    		.setProjection(Projections.property("loadCriteria.id"))
		    		));
		    	}
		    })
			.put("lessonDuration", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Load.class, "loadCriteria")
				    		.add(Restrictions.eqProperty("loadCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("loadCriteria.atomicLoads", "atomicLoadsAlias")
				    		.add(createNumberRestrictions("atomicLoadsAlias.lessonDuration", value, Byte.class))
				    		.setProjection(Projections.property("loadCriteria.id"))
		    		));
		    	}
		    })
			.put("lecturerIds", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Load.class, "loadCriteria")
				    		.add(Restrictions.eqProperty("loadCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("loadCriteria.atomicLoads", "atomicLoadsAlias")
				    		.createAlias("atomicLoadsAlias.lecturers", "lecturersAlias")
				    		.add(createNumberRestrictions("lecturersAlias.id", value, Long.class))
				    		.setProjection(Projections.property("loadCriteria.id"))
		    		));
		    	}
		    })
			.put("lecturerIdsSize", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Load.class, "loadCriteria")
				    		.add(Restrictions.eqProperty("loadCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("loadCriteria.atomicLoads", "atomicLoadsAlias")
				    		.add(createSizeRestrictions("atomicLoadsAlias.lecturers", value))
				    		.setProjection(Projections.property("loadCriteria.id"))
		    		));
		    	}
		    })
			.put("specialtyIds", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Load.class, "loadCriteria")
				    		.add(Restrictions.eqProperty("loadCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("loadCriteria.atomicLoads", "atomicLoadsAlias")
				    		.createAlias("atomicLoadsAlias.specialties", "specialtiesAlias")
				    		.add(createNumberRestrictions("specialtiesAlias.id", value, Long.class))
				    		.setProjection(Projections.property("loadCriteria.id"))
		    		));
		    	}
		    })
			.put("specialtyIdsSize", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Load.class, "loadCriteria")
				    		.add(Restrictions.eqProperty("loadCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("loadCriteria.atomicLoads", "atomicLoadsAlias")
				    		.add(createSizeRestrictions("atomicLoadsAlias.specialties", value))
				    		.setProjection(Projections.property("loadCriteria.id"))
		    		));
		    	}
		    })
			.put("academicGroupIds", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Load.class, "loadCriteria")
				    		.add(Restrictions.eqProperty("loadCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("loadCriteria.atomicLoads", "atomicLoadsAlias")
				    		.createAlias("atomicLoadsAlias.academicGroups", "academicGroupsAlias")
				    		.add(createNumberRestrictions("academicGroupsAlias.id", value, Long.class))
				    		.setProjection(Projections.property("loadCriteria.id"))
		    		));
		    	}
		    })
			.put("academicGroupIdsSize", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Load.class, "loadCriteria")
				    		.add(Restrictions.eqProperty("loadCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("loadCriteria.atomicLoads", "atomicLoadsAlias")
				    		.add(createSizeRestrictions("atomicLoadsAlias.academicGroups", value))
				    		.setProjection(Projections.property("loadCriteria.id"))
		    		));
		    	}
		    })
			.put("studentIds", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Load.class, "loadCriteria")
				    		.add(Restrictions.eqProperty("loadCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("loadCriteria.atomicLoads", "atomicLoadsAlias")
				    		.createAlias("atomicLoadsAlias.students", "studentsAlias")
				    		.add(createNumberRestrictions("studentsAlias.id", value, Long.class))
				    		.setProjection(Projections.property("loadCriteria.id"))
		    		));
		    	}
		    })
			.put("studentIdsSize", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Load.class, "loadCriteria")
				    		.add(Restrictions.eqProperty("loadCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("loadCriteria.atomicLoads", "atomicLoadsAlias")
				    		.add(createSizeRestrictions("atomicLoadsAlias.students", value))
				    		.setProjection(Projections.property("loadCriteria.id"))
		    		));
		    	}
		    })
			.put("learningPlaceGroupIds", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Load.class, "loadCriteria")
				    		.add(Restrictions.eqProperty("loadCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("loadCriteria.atomicLoads", "atomicLoadsAlias")
				    		.createAlias("atomicLoadsAlias.learningPlaceGroups", "learningPlaceGroupsAlias")
				    		.add(createNumberRestrictions("learningPlaceGroupsAlias.id", value, Long.class))
				    		.setProjection(Projections.property("loadCriteria.id"))
		    		));
		    	}
		    })
			.put("learningPlaceGroupIdsSize", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Load.class, "loadCriteria")
				    		.add(Restrictions.eqProperty("loadCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("loadCriteria.atomicLoads", "atomicLoadsAlias")
				    		.add(createSizeRestrictions("atomicLoadsAlias.learningPlaceGroups", value))
				    		.setProjection(Projections.property("loadCriteria.id"))
		    		));
		    	}
		    })
			.put("learningPlaceIds", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Load.class, "loadCriteria")
				    		.add(Restrictions.eqProperty("loadCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("loadCriteria.atomicLoads", "atomicLoadsAlias")
				    		.createAlias("atomicLoadsAlias.learningPlaces", "learningPlacesAlias")
				    		.add(createNumberRestrictions("learningPlacesAlias.id", value, Long.class))
				    		.setProjection(Projections.property("loadCriteria.id"))
		    		));
		    	}
		    })
			.put("learningPlaceIdsSize", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(Load.class, "loadCriteria")
				    		.add(Restrictions.eqProperty("loadCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("loadCriteria.atomicLoads", "atomicLoadsAlias")
				    		.add(createSizeRestrictions("atomicLoadsAlias.learningPlaces", value))
				    		.setProjection(Projections.property("loadCriteria.id"))
		    		));
		    	}
		    })
		    .build();
	
	protected static final ImmutableMap<String, OrderProcessor> ORDER_PROCESSORS =
			ImmutableMap.<String, OrderProcessor>builder()
			.putAll(DEFAULT_ORDER_PROCESSORS)
			.put("curriculumSemesterHoursId", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "curriculumSemesterHours.id";
		    	}
		    })
			.put("classType", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		createAlias("curriculumSemesterHours", "curriculumSemesterHoursAlias", criteria, aliases);
		    		return "curriculumSemesterHoursAlias.classType";
		    	}
		    })
			.put("allHours", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		createAlias("curriculumSemesterHours", "curriculumSemesterHoursAlias", criteria, aliases);
		    		return "curriculumSemesterHoursAlias.hours";
		    	}
		    })
			.put("semester", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		createAlias("curriculumSemesterHours", "curriculumSemesterHoursAlias", criteria, aliases);
		    		createAlias("curriculumSemesterHoursAlias.curriculumSemester", "curriculumSemesterAlias",
		    				criteria, aliases);
		    		return "curriculumSemesterAlias.semester";
		    	}
		    })
			.put("year", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		createAlias("curriculumSemesterHours", "curriculumSemesterHoursAlias", criteria, aliases);
		    		createAlias("curriculumSemesterHoursAlias.curriculumSemester", "curriculumSemesterAlias",
		    				criteria, aliases);
		    		return "curriculumSemesterAlias.year";
		    	}
		    })
			.put("half", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "curriculumSemesterAlias.half";
		    	}
		    })
			.put("courseId", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "course.id";
		    	}
		    })
			.put("departmentId", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "department.id";
		    	}
		    })
		    .build();
	
	@Override
	protected ResultTransformer getResultTransformer() {
		return ResultTransformers.LOAD_RESULT_TRANSFORMER;
	}
	
	@Override
	protected List<String> getDefaultFields() {
		return DEFAULT_FIELDS;
	}
	
	@Override
	protected Map<String, ProjectionProcessor> getProjectionProcessors() {
		return PROJECTION_PROCESSORS;
	}
	
	@Override
	protected ImmutableMap<String, ParameterProcessor> getParameterProcessors() {
		return PARAMETER_PROCESSORS;
	}
	
	@Override
	protected Map<String, OrderProcessor> getOrderProcessors() {
		return ORDER_PROCESSORS;
	}
	
}
