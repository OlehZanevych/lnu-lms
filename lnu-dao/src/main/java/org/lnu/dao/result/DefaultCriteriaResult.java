package org.lnu.dao.result;

import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.lnu.dao.constants.Constants;
import org.lnu.dao.result.childcollection.factory.ChildCollectionFactory;
import org.lnu.dao.result.transformer.ResultTransformers;
import org.lnu.model.APIModel;
import org.lnu.model.enumtype.capability.Capability;
import org.lnu.model.group.Group;
import org.lnu.model.user.User;

import com.google.common.collect.ImmutableMap;

/**
 * Class, that contains default methods and processors
 * for receiving data from Hibernate Criteria
 * for security API.
 * @author OlehZanevych
 * 
 * @param <ENTITY> entity class
 * @param <KEY> identifier class
 */
public abstract class DefaultCriteriaResult<ENTITY extends APIModel<KEY>, KEY extends Number>
		extends AbstractCriteriaResultWithSimpleId<ENTITY, KEY> {
	
	protected static final ImmutableMap<String, ProjectionProcessor> DEFAULT_PROJECTION_PROCESSORS =
			ImmutableMap.<String, ProjectionProcessor>builder()
			.putAll(IGNORED_PROJECTION_PROCESSORS)
		    .put("isPrivate", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("isPrivate"), "isPrivate");
		    	}
		    })
		    .put("capability", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("capability"), "capability");
		    	}
		    })
		    .put("responsibleOwnUserIds", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("responsibleOwnUsers",
		    				"responsibleOwnUsersAlias", User.class,
		    				ResultTransformers.USER_RESULT_TRANSFORMER);
		    	}
		    })
		    .put("responsibleGroupIds", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("responsibleGroups", "responsibleGroupsAlias",
		    				Group.class, ResultTransformers.GROUP_RESULT_TRANSFORMER);
		    	}
		    })
		    .put("responsibleUserIds", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		childCollectionFactory.addCollectionProjection("responsibleUsers", "responsibleUsersAlias",
		    				User.class, ResultTransformers.USER_RESULT_TRANSFORMER);
		    	}
		    })
		    .build();
	
	/**
	 * Getting default filter parameter processors for security API.
	 * @param entityClass entity class
	 * @return default filter parameter processors
	 */
	protected static ImmutableMap<String, ParameterProcessor> getDefaultParameterProcessors(
			final Class<? extends APIModel<?>> entityClass) {
		return ImmutableMap.<String, ParameterProcessor>builder()
			.put("isPrivate", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createBooleanRestriction("isPrivate", value));
		    	}
		    })
			.put("capability", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createEnumRestriction("capability", value, Capability.class));
		    	}
		    })
			.put("responsibleOwnUserIds", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(entityClass, "currentCriteria")
				    		.add(Restrictions.eqProperty("currentCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("currentCriteria.responsibleOwnUsers", "responsibleOwnUsersAlias")
				    		.add(createUserIdRestrictions("responsibleOwnUsersAlias.id", value, userId))
				    		.setProjection(Projections.property("currentCriteria.id"))
		    		));
		    	}
		    })
			.put("responsibleOwnUserIdsSize", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createSizeRestrictions("responsibleOwnUsers", value));
		    	}
		    })
			.put("responsibleGroupIds", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(entityClass, "currentCriteria")
				    		.add(Restrictions.eqProperty("currentCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("currentCriteria.responsibleGroups", "responsibleGroupsAlias")
				    		.add(createNumberRestrictions("responsibleGroupsAlias.id", value, Long.class))
				    		.setProjection(Projections.property("currentCriteria.id"))
		    		));
		    	}
		    })
			.put("responsibleGroupIdsSize", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createSizeRestrictions("responsibleGroups", value));
		    	}
		    })
			.put("responsibleUserIds", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(Subqueries.exists(
		    				DetachedCriteria.forClass(entityClass, "currentCriteria")
				    		.add(Restrictions.eqProperty("currentCriteria.id", Constants.ROOT_CRITERIA + ".id"))
				    		.createAlias("currentCriteria.responsibleUsers", "responsibleUsersAlias")
				    		.add(createUserIdRestrictions("responsibleUsersAlias.id", value, userId))
				    		.setProjection(Projections.property("currentCriteria.id"))
		    		));
		    	}
		    })
			.put("responsibleUserIdsSize", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createSizeRestrictions("responsibleUsers", value));
		    	}
		    })
		    .build();
	}
	
	protected static final ImmutableMap<String, OrderProcessor> DEFAULT_ORDER_PROCESSORS =
			ImmutableMap.<String, OrderProcessor>builder()
			.putAll(COMMON_ORDER_PROCESSORS)
			.put("isPrivate", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "isPrivate";
		    	}
		    })
			.put("capability", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "capability";
		    	}
		    })
		    .build();

}
