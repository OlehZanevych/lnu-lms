package org.lnu.dao.result.file;

import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.lnu.dao.result.DefaultCriteriaResult;
import org.lnu.dao.result.childcollection.factory.ChildCollectionFactory;
import org.lnu.model.file.FileModel;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

/**
 * Class, that contains default methods and processors
 * for receiving data from Hibernate Criteria
 * for security file API.
 * @author OlehZanevych
 * 
 * @param <ENTITY> entity class
 */
public abstract class FileCriteriaResult<ENTITY extends FileModel> extends DefaultCriteriaResult<ENTITY, Long> {
	
	protected static final ImmutableList<String> COMMON_FILE_DEFAULT_FIELDS =
			ImmutableList.<String>builder()
			.add("fileURI")
			.build();
	
	protected static final ImmutableMap<String, ProjectionProcessor> COMMON_FILE_PROJECTION_PROCESSORS =
			ImmutableMap.<String, ProjectionProcessor>builder()
			.putAll(DEFAULT_PROJECTION_PROCESSORS)
		    .put("fileURI", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("fileURI"), "fileURI");
		    	}
		    })
		    .build();
	
	/**
	 * Getting common filter parameter processors for security file API.
	 * @param entityClass entity class
	 * @return common file parameter processors
	 */
	protected static ImmutableMap<String, ParameterProcessor> getCommonFileParameterProcessors(
			final Class<? extends FileModel> entityClass) {
		return ImmutableMap.<String, ParameterProcessor>builder()
			.putAll(COMMON_PARAMETER_PROCESSORS)
			.putAll(getDefaultParameterProcessors(entityClass))
			.put("fileURI", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createLikeRestrictions("fileURI", value));
		    	}
		    })
		    .build();
	}
	
	protected static final ImmutableMap<String, OrderProcessor> COMMON_FILE_ORDER_PROCESSORS =
			ImmutableMap.<String, OrderProcessor>builder()
			.putAll(DEFAULT_ORDER_PROCESSORS)
			.put("fileURI", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "fileURI";
		    	}
		    })
		    .build();

}
