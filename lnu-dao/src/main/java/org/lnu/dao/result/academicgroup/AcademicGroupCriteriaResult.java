package org.lnu.dao.result.academicgroup;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.transform.ResultTransformer;
import org.lnu.dao.annotation.CriteriaResult;
import org.lnu.dao.result.DefaultCriteriaResult;
import org.lnu.dao.result.childcollection.factory.ChildCollectionFactory;
import org.lnu.dao.result.transformer.ResultTransformers;
import org.lnu.model.academicgroup.AcademicGroup;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

/**
 * Class, that contains methods for receiving
 * AcademicGroups from Hibernate Criteria.
 * @author OlehZanevych
 */
@CriteriaResult("academicGroupCriteriaResult")
public class AcademicGroupCriteriaResult extends DefaultCriteriaResult<AcademicGroup, Long> {
	
	private static final ImmutableList<String> DEFAULT_FIELDS =
			ImmutableList.<String>builder()
			.add("name")
			.add("departmentId")
			.add("specialtyId")
			.add("year")
			.add("mentorId")
			.add("leaderId")
			.build();
	
	protected static final ImmutableMap<String, ProjectionProcessor> PROJECTION_PROCESSORS =
			ImmutableMap.<String, ProjectionProcessor>builder()
			.putAll(DEFAULT_PROJECTION_PROCESSORS)
		    .put("name", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("name"), "name");
		    	}
		    })
		    .put("departmentId", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("department.id"), "department.id");
		    	}
		    })
		    .put("specialtyId", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("specialty.id"), "specialty.id");
		    	}
		    })
		    .put("year", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("year"), "year");
		    	}
		    })
		    .put("mentorId", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("mentor.id"), "mentor.id");
		    	}
		    })
		    .put("leaderId", new ProjectionProcessor() {
		    	public void addProjection(final Criteria criteria, final ProjectionList projections,
		    			final Set<String> aliases, final ChildCollectionFactory childCollectionFactory) {
		    		
		    		projections.add(Projections.property("leader.id"), "leader.id");
		    	}
		    })
		    .build();
	
	private static final ImmutableMap<String, ParameterProcessor> PARAMETER_PROCESSORS =
			ImmutableMap.<String, ParameterProcessor>builder()
			.putAll(COMMON_PARAMETER_PROCESSORS)
			.putAll(getDefaultParameterProcessors(AcademicGroup.class))
		    .put("name", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createLikeRestrictions("name", value));
		    	}
		    })
		    .put("departmentId", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createNumberRestrictions("department.id", value, Long.class));
		    	}
		    })
		    .put("specialtyId", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createNumberRestrictions("specialty.id", value, Long.class));
		    	}
		    })
		    .put("year", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createNumberRestrictions("year", value, Byte.class));
		    	}
		    })
		    .put("mentorId", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createNumberRestrictions("mentor.id", value, Long.class));
		    	}
		    })
		    .put("leaderId", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final Set<String> aliases,
		    			final Long userId) {
		    		
		    		criteria.add(createNumberRestrictions("leader.id", value, Long.class));
		    	}
		    })
		    .build();
	
	protected static final ImmutableMap<String, OrderProcessor> ORDER_PROCESSORS =
			ImmutableMap.<String, OrderProcessor>builder()
			.putAll(DEFAULT_ORDER_PROCESSORS)
			.put("name", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "name";
		    	}
		    })
			.put("departmentId", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "department.id";
		    	}
		    })
			.put("specialtyId", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "specialty.id";
		    	}
		    })
			.put("year", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "year";
		    	}
		    })
			.put("mentorId", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "mentor.id";
		    	}
		    })
			.put("leaderId", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final Set<String> aliases) {
		    		return "leader.id";
		    	}
		    })
		    .build();
	
	@Override
	protected ResultTransformer getResultTransformer() {
		return ResultTransformers.ACADEMIC_GROUP_RESULT_TRANSFORMER;
	}
	
	@Override
	protected List<String> getDefaultFields() {
		return DEFAULT_FIELDS;
	}
	
	@Override
	protected Map<String, ProjectionProcessor> getProjectionProcessors() {
		return PROJECTION_PROCESSORS;
	}
	
	@Override
	protected ImmutableMap<String, ParameterProcessor> getParameterProcessors() {
		return PARAMETER_PROCESSORS;
	}
	
	@Override
	protected Map<String, OrderProcessor> getOrderProcessors() {
		return ORDER_PROCESSORS;
	}
	
}
