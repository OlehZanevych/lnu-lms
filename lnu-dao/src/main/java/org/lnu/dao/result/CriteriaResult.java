package org.lnu.dao.result;

import org.hibernate.Session;
import org.lnu.model.Model;
import org.lnu.model.pagination.PagedResult;
import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.Request;

/**
 * Interface, that is used for receiving
 * data using Hibernate Session.
 * @author OlehZanevych
 * 
 * @param <ENTITY> entity class
 * @param <KEY> identifier class
 */
public interface CriteriaResult<ENTITY extends Model, KEY> {
	
	/**
	 * Method for getting entity by id.
	 * @param id identifier
	 * @param session Hibernate session
	 * @param entityClass entity class
	 * @return entity
	 */
	ENTITY getEntityById(KEY id, Session session, Class<ENTITY> entityClass);
	
	/**
	 * Method for getting entity by id and attached request.
	 * @param id identifier
	 * @param request request
	 * @param session Hibernate session
	 * @param entityClass entity class
	 * @return entity
	 */
	ENTITY getEntityById(KEY id, Request request, Session session, Class<ENTITY> entityClass);
	
	/**
	 * Method for getting paged entities for request.
	 * @param request PagedRequest
	 * @param session Hibernate Session
	 * @param entityClass entity class
	 * @param userId current userId
	 * @return PagedResult
	 */
	PagedResult<ENTITY> getEntities(PagedRequest request, Session session, Class<ENTITY> entityClass, Long userId);
	
}
