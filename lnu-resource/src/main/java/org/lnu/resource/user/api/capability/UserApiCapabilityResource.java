package org.lnu.resource.user.api.capability;

import java.text.MessageFormat;

import org.lnu.model.enumtype.api.API;
import org.lnu.model.enumtype.api.capability.level.APICapabilityLevel;
import org.lnu.model.enumtype.api.createcapability.APICreateCapability;
import org.lnu.resource.ApiResource;
import org.lnu.resource.composite.id.CompositeId;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Resource for web layer, that describes UserAPICapability.
 * @author OlehZanevych
 */
public class UserApiCapabilityResource implements ApiResource<Long> {
	
	private Long userId;
	
	private API api;
	
	private APICapabilityLevel apiCapabilityLevel;
	
	private APICreateCapability apiCreateCapability;
	
	/**
	 * Default constructor with no parameters.
	 */
	public UserApiCapabilityResource() {
		
	}

	@Override
	public String getRootUri() {
		return MessageFormat.format("/{0}", API.apicapabilities);
	}
	
	@Override
	public String getUri() {
		if (userId == null || api == null) {
			return null;
		}
		return MessageFormat.format("{0}/{1,number,#}&{2}", getRootUri(), userId, api);
	}
	
	@Override
	@JsonIgnore
	public CompositeId getId() {
		return new CompositeId(UserApiCapabilityResource.class, userId, api);
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(final Long userId) {
		this.userId = userId;
	}

	public API getApi() {
		return api;
	}

	public void setApi(final API api) {
		this.api = api;
	}

	public APICapabilityLevel getApiCapabilityLevel() {
		return apiCapabilityLevel;
	}

	public void setApiCapabilityLevel(final APICapabilityLevel apiCapabilityLevel) {
		this.apiCapabilityLevel = apiCapabilityLevel;
	}

	public APICreateCapability getApiCreateCapability() {
		return apiCreateCapability;
	}

	public void setApiCreateCapability(final APICreateCapability apiCreateCapability) {
		this.apiCreateCapability = apiCreateCapability;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((api == null) ? 0 : api.hashCode());
		result = prime * result + ((apiCapabilityLevel == null) ? 0 : apiCapabilityLevel.hashCode());
		result = prime * result + ((apiCreateCapability == null) ? 0 : apiCreateCapability.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		UserApiCapabilityResource other = (UserApiCapabilityResource) obj;
		if (api != other.api) {
			return false;
		}
		if (apiCapabilityLevel != other.apiCapabilityLevel) {
			return false;
		}
		if (apiCreateCapability != other.apiCreateCapability) {
			return false;
		}
		if (userId == null) {
			if (other.userId != null) {
				return false;
			}
		} else if (!userId.equals(other.userId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserApiCapabilityResource [userId=");
		builder.append(userId);
		builder.append(", api=");
		builder.append(api);
		builder.append(", apiCapabilityLevel=");
		builder.append(apiCapabilityLevel);
		builder.append(", apiCreateCapability=");
		builder.append(apiCreateCapability);
		builder.append("]");
		return builder.toString();
	}

}
