package org.lnu.resource;

/**
 * Base Resource with id.
 * @author OlehZanevych
 * 
 * @param <KEY> Identifier of Resource
 */
public abstract class ResourceWithSimpleId<KEY extends Number> implements Resource {

	protected KEY id;
	
	/**
	 * Default constructor with no parameters.
	 */
	protected ResourceWithSimpleId() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	protected ResourceWithSimpleId(final KEY id) {
		this.id = id;
	}

	public KEY getId() {
		return id;
	}

	public void setId(final KEY id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ResourceWithSimpleId)) {
			return false;
		}
		ResourceWithSimpleId<?> other = (ResourceWithSimpleId<?>) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ResourceWithSimpleId [id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}
	
}
