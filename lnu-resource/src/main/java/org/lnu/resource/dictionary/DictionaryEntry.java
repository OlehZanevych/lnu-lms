package org.lnu.resource.dictionary;

import org.lnu.model.dictionary.Value;

/**
 * Class, that is used to represent dictionary entry.
 * @author OlehZanevych
 * 
 * @param <KEY> Enum type of dictionary key
 */
public class DictionaryEntry<KEY> extends Value {
	
	private KEY key;
	
	/**
	 * Constructor.
	 * @param key enum key
	 * @param value Value
	 */
	public DictionaryEntry(final KEY key, final Value value) {
		super(value);
		this.key = key;
	}

	public KEY getKey() {
		return key;
	}

	public void setKey(final KEY key) {
		this.key = key;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		DictionaryEntry<?> other = (DictionaryEntry<?>) obj;
		if (key == null) {
			if (other.key != null) {
				return false;
			}
		} else if (!key.equals(other.key)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DictionaryEntry [key=");
		builder.append(key);
		builder.append(", name=");
		builder.append(name);
		builder.append(", abbreviation=");
		builder.append(abbreviation);
		builder.append("]");
		return builder.toString();
	}

}
