package org.lnu.resource.load;

import java.text.MessageFormat;
import java.util.List;

import org.lnu.model.enumtype.api.API;
import org.lnu.model.enumtype.classtype.ClassType;
import org.lnu.resource.SecurityAPIResource;
import org.lnu.resource.atomicload.AtomicLoadResource;

/**
 * Resource for web layer, that describes Load.
 * @author OlehZanevych
 */
public class LoadResource extends SecurityAPIResource<Long> {
	
	private Long curriculumSemesterHoursId;
	
	private Long courseId;
	
	private Long departmentId;
	
	private ClassType classType;
	
	private Short allHours;
	
	private Byte semester;
	
	private Byte year;
	
	private Byte half;
	
	private List<AtomicLoadResource> atomicLoads;

	@Override
	public String getRootUri() {
		return MessageFormat.format("/{0}", API.load);
	}

	/**
	 * Default constructor with no parameters.
	 */
	public LoadResource() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public LoadResource(final Long id) {
		super(id);
	}

	public Long getCurriculumSemesterHoursId() {
		return curriculumSemesterHoursId;
	}

	public void setCurriculumSemesterHoursId(final Long curriculumSemesterHoursId) {
		this.curriculumSemesterHoursId = curriculumSemesterHoursId;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(final Long courseId) {
		this.courseId = courseId;
	}

	public Long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(final Long departmentId) {
		this.departmentId = departmentId;
	}

	public ClassType getClassType() {
		return classType;
	}

	public void setClassType(final ClassType classType) {
		this.classType = classType;
	}

	public Short getAllHours() {
		return allHours;
	}

	public void setAllHours(final Short allHours) {
		this.allHours = allHours;
	}

	public Byte getSemester() {
		return semester;
	}

	public void setSemester(final Byte semester) {
		this.semester = semester;
	}

	public Byte getYear() {
		return year;
	}

	public void setYear(final Byte year) {
		this.year = year;
	}

	public Byte getHalf() {
		return half;
	}

	public void setHalf(final Byte half) {
		this.half = half;
	}

	public List<AtomicLoadResource> getAtomicLoads() {
		return atomicLoads;
	}

	public void setAtomicLoads(final List<AtomicLoadResource> atomicLoads) {
		this.atomicLoads = atomicLoads;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((allHours == null) ? 0 : allHours.hashCode());
		result = prime * result + ((atomicLoads == null) ? 0 : atomicLoads.hashCode());
		result = prime * result + ((classType == null) ? 0 : classType.hashCode());
		result = prime * result + ((courseId == null) ? 0 : courseId.hashCode());
		result = prime * result + ((curriculumSemesterHoursId == null) ? 0 : curriculumSemesterHoursId.hashCode());
		result = prime * result + ((departmentId == null) ? 0 : departmentId.hashCode());
		result = prime * result + ((half == null) ? 0 : half.hashCode());
		result = prime * result + ((semester == null) ? 0 : semester.hashCode());
		result = prime * result + ((year == null) ? 0 : year.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		LoadResource other = (LoadResource) obj;
		if (allHours == null) {
			if (other.allHours != null) {
				return false;
			}
		} else if (!allHours.equals(other.allHours)) {
			return false;
		}
		if (atomicLoads == null) {
			if (other.atomicLoads != null) {
				return false;
			}
		} else if (!atomicLoads.equals(other.atomicLoads)) {
			return false;
		}
		if (classType != other.classType) {
			return false;
		}
		if (courseId == null) {
			if (other.courseId != null) {
				return false;
			}
		} else if (!courseId.equals(other.courseId)) {
			return false;
		}
		if (curriculumSemesterHoursId == null) {
			if (other.curriculumSemesterHoursId != null) {
				return false;
			}
		} else if (!curriculumSemesterHoursId.equals(other.curriculumSemesterHoursId)) {
			return false;
		}
		if (departmentId == null) {
			if (other.departmentId != null) {
				return false;
			}
		} else if (!departmentId.equals(other.departmentId)) {
			return false;
		}
		if (half == null) {
			if (other.half != null) {
				return false;
			}
		} else if (!half.equals(other.half)) {
			return false;
		}
		if (semester == null) {
			if (other.semester != null) {
				return false;
			}
		} else if (!semester.equals(other.semester)) {
			return false;
		}
		if (year == null) {
			if (other.year != null) {
				return false;
			}
		} else if (!year.equals(other.year)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LoadResource [curriculumSemesterHoursId=");
		builder.append(curriculumSemesterHoursId);
		builder.append(", courseId=");
		builder.append(courseId);
		builder.append(", departmentId=");
		builder.append(departmentId);
		builder.append(", classType=");
		builder.append(classType);
		builder.append(", allHours=");
		builder.append(allHours);
		builder.append(", semester=");
		builder.append(semester);
		builder.append(", year=");
		builder.append(year);
		builder.append(", half=");
		builder.append(half);
		builder.append(", atomicLoads=");
		builder.append(atomicLoads);
		builder.append(", isPrivate=");
		builder.append(isPrivate);
		builder.append(", capability=");
		builder.append(capability);
		builder.append(", responsibleOwnUserIds=");
		builder.append(responsibleOwnUserIds);
		builder.append(", responsibleGroupIds=");
		builder.append(responsibleGroupIds);
		builder.append(", responsibleUserIds=");
		builder.append(responsibleUserIds);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}
	
}
