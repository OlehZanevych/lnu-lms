package org.lnu.resource.specialty;

import java.text.MessageFormat;
import java.util.List;

import org.lnu.annotation.CrudableResource;
import org.lnu.model.enumtype.api.API;
import org.lnu.model.enumtype.educationaldegree.EducationalDegree;
import org.lnu.resource.SecurityAPIResource;

/**
 * Resource for web layer, that describes Specialty.
 * @author OlehZanevych
 */
@CrudableResource
public class SpecialtyResource extends SecurityAPIResource<Long> {
	
	private String name;
	
	private String code;
	
	private Long unitId;
	
	private List<Long> departmentIds;
	
	private EducationalDegree educationalDegree;

	@Override
	public String getRootUri() {
		return MessageFormat.format("/{0}", API.specialties);
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(final String code) {
		this.code = code;
	}

	public Long getUnitId() {
		return unitId;
	}

	public void setUnitId(final Long unitId) {
		this.unitId = unitId;
	}

	public List<Long> getDepartmentIds() {
		return departmentIds;
	}

	public void setDepartmentIds(final List<Long> departmentIds) {
		this.departmentIds = departmentIds;
	}

	public EducationalDegree getEducationalDegree() {
		return educationalDegree;
	}

	public void setEducationalDegree(final EducationalDegree educationalDegree) {
		this.educationalDegree = educationalDegree;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((departmentIds == null) ? 0 : departmentIds.hashCode());
		result = prime * result + ((educationalDegree == null) ? 0 : educationalDegree.hashCode());
		result = prime * result + ((unitId == null) ? 0 : unitId.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SpecialtyResource other = (SpecialtyResource) obj;
		if (code == null) {
			if (other.code != null) {
				return false;
			}
		} else if (!code.equals(other.code)) {
			return false;
		}
		if (departmentIds == null) {
			if (other.departmentIds != null) {
				return false;
			}
		} else if (!departmentIds.equals(other.departmentIds)) {
			return false;
		}
		if (educationalDegree != other.educationalDegree) {
			return false;
		}
		if (unitId == null) {
			if (other.unitId != null) {
				return false;
			}
		} else if (!unitId.equals(other.unitId)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SpecialtyResource [name=");
		builder.append(name);
		builder.append(", code=");
		builder.append(code);
		builder.append(", unitId=");
		builder.append(unitId);
		builder.append(", departmentIds=");
		builder.append(departmentIds);
		builder.append(", educationalDegree=");
		builder.append(educationalDegree);
		builder.append(", isPrivate=");
		builder.append(isPrivate);
		builder.append(", capability=");
		builder.append(capability);
		builder.append(", responsibleOwnUserIds=");
		builder.append(responsibleOwnUserIds);
		builder.append(", responsibleGroupIds=");
		builder.append(responsibleGroupIds);
		builder.append(", responsibleUserIds=");
		builder.append(responsibleUserIds);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
