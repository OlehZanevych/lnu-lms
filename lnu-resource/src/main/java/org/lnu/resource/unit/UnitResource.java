package org.lnu.resource.unit;

import java.text.MessageFormat;

import org.lnu.annotation.CrudableResource;
import org.lnu.model.enumtype.api.API;
import org.lnu.model.enumtype.unit.type.UnitType;
import org.lnu.resource.SecurityAPIResource;

/**
 * Resource for web layer, that describes Unit.
 * @author OlehZanevych
 */
@CrudableResource
public class UnitResource extends SecurityAPIResource<Long> {
	
	private String name;
	
	private String abbreviation;
	
	private UnitType unitType;
	
	private String website;
	
	private String email;
	
	private String phone;
	
	private Long deanId;
	
	private Long buildingMapId;
	
	private Short onMapId;
	
	private Long buildingId;
	
	private String room;
	
	private String info;
	
	/**
	 * Default constructor with no parameters.
	 */
	public UnitResource() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public UnitResource(final Long id) {
		super(id);
	}

	@Override
	public String getRootUri() {
		return MessageFormat.format("/{0}", API.units);
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(final String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public UnitType getUnitType() {
		return unitType;
	}

	public void setUnitType(final UnitType unitType) {
		this.unitType = unitType;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(final String website) {
		this.website = website;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(final String phone) {
		this.phone = phone;
	}

	public Long getDeanId() {
		return deanId;
	}

	public void setDeanId(final Long deanId) {
		this.deanId = deanId;
	}

	public Long getBuildingMapId() {
		return buildingMapId;
	}

	public void setBuildingMapId(final Long buildingMapId) {
		this.buildingMapId = buildingMapId;
	}

	public Short getOnMapId() {
		return onMapId;
	}

	public void setOnMapId(final Short onMapId) {
		this.onMapId = onMapId;
	}

	public Long getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(final Long buildingId) {
		this.buildingId = buildingId;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(final String room) {
		this.room = room;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(final String info) {
		this.info = info;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((abbreviation == null) ? 0 : abbreviation.hashCode());
		result = prime * result + ((buildingId == null) ? 0 : buildingId.hashCode());
		result = prime * result + ((buildingMapId == null) ? 0 : buildingMapId.hashCode());
		result = prime * result + ((deanId == null) ? 0 : deanId.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((onMapId == null) ? 0 : onMapId.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((room == null) ? 0 : room.hashCode());
		result = prime * result + ((unitType == null) ? 0 : unitType.hashCode());
		result = prime * result + ((website == null) ? 0 : website.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		UnitResource other = (UnitResource) obj;
		if (abbreviation == null) {
			if (other.abbreviation != null) {
				return false;
			}
		} else if (!abbreviation.equals(other.abbreviation)) {
			return false;
		}
		if (buildingId == null) {
			if (other.buildingId != null) {
				return false;
			}
		} else if (!buildingId.equals(other.buildingId)) {
			return false;
		}
		if (buildingMapId == null) {
			if (other.buildingMapId != null) {
				return false;
			}
		} else if (!buildingMapId.equals(other.buildingMapId)) {
			return false;
		}
		if (deanId == null) {
			if (other.deanId != null) {
				return false;
			}
		} else if (!deanId.equals(other.deanId)) {
			return false;
		}
		if (email == null) {
			if (other.email != null) {
				return false;
			}
		} else if (!email.equals(other.email)) {
			return false;
		}
		if (info == null) {
			if (other.info != null) {
				return false;
			}
		} else if (!info.equals(other.info)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (onMapId == null) {
			if (other.onMapId != null) {
				return false;
			}
		} else if (!onMapId.equals(other.onMapId)) {
			return false;
		}
		if (phone == null) {
			if (other.phone != null) {
				return false;
			}
		} else if (!phone.equals(other.phone)) {
			return false;
		}
		if (room == null) {
			if (other.room != null) {
				return false;
			}
		} else if (!room.equals(other.room)) {
			return false;
		}
		if (unitType != other.unitType) {
			return false;
		}
		if (website == null) {
			if (other.website != null) {
				return false;
			}
		} else if (!website.equals(other.website)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UnitResource [name=");
		builder.append(name);
		builder.append(", abbreviation=");
		builder.append(abbreviation);
		builder.append(", unitType=");
		builder.append(unitType);
		builder.append(", website=");
		builder.append(website);
		builder.append(", email=");
		builder.append(email);
		builder.append(", phone=");
		builder.append(phone);
		builder.append(", deanId=");
		builder.append(deanId);
		builder.append(", buildingMapId=");
		builder.append(buildingMapId);
		builder.append(", onMapId=");
		builder.append(onMapId);
		builder.append(", buildingId=");
		builder.append(buildingId);
		builder.append(", room=");
		builder.append(room);
		builder.append(", info=");
		builder.append(info);
		builder.append(", isPrivate=");
		builder.append(isPrivate);
		builder.append(", capability=");
		builder.append(capability);
		builder.append(", responsibleOwnUserIds=");
		builder.append(responsibleOwnUserIds);
		builder.append(", responsibleGroupIds=");
		builder.append(responsibleGroupIds);
		builder.append(", responsibleUserIds=");
		builder.append(responsibleUserIds);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
