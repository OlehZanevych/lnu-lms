package org.lnu.resource.lesson;

import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.lnu.annotation.CrudableResource;
import org.lnu.model.enumtype.day.Day;
import org.lnu.model.enumtype.weekperiodicity.WeekPeriodicity;
import org.lnu.resource.ResourceWithSimpleId;

/**
 * Resource for web layer, that describes Lesson.
 * @author OlehZanevych
 */
@CrudableResource
public class LessonResource extends ResourceWithSimpleId<Long> {
	
	@NotNull(message = "Field required")
	private Day day;
	
	@NotNull(message = "Field required")
	@Min(value = 8, message = "Minimal value is 8")
	@Max(value = 20, message = "Maximal value is 20")
	private Byte hour;
	
	@NotNull(message = "Field required")
	@Min(value = 0, message = "Minimal value is 0")
	@Max(value = 55, message = "Maximal value is 55")
	private Byte minute;
	
	@NotNull(message = "Field required")
	@Min(value = 20, message = "Minimal value is 20")
	@Max(value = 120, message = "Maximal value is 120")
	private Byte duration;
	
	@NotNull(message = "Field required")
	private WeekPeriodicity weekPeriodicity;
	
	@NotNull(message = "Field required")
	private List<Long> placeIds;

	public Day getDay() {
		return day;
	}

	public void setDay(final Day day) {
		this.day = day;
	}

	public Byte getHour() {
		return hour;
	}

	public void setHour(final Byte hour) {
		this.hour = hour;
	}

	public Byte getMinute() {
		return minute;
	}

	public void setMinute(final Byte minute) {
		this.minute = minute;
	}

	public Byte getDuration() {
		return duration;
	}

	public void setDuration(final Byte duration) {
		this.duration = duration;
	}

	public WeekPeriodicity getWeekPeriodicity() {
		return weekPeriodicity;
	}

	public void setWeekPeriodicity(final WeekPeriodicity weekPeriodicity) {
		this.weekPeriodicity = weekPeriodicity;
	}

	public List<Long> getPlaceIds() {
		return placeIds;
	}

	public void setPlaceIds(final List<Long> placeIds) {
		this.placeIds = placeIds;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((day == null) ? 0 : day.hashCode());
		result = prime * result + ((duration == null) ? 0 : duration.hashCode());
		result = prime * result + ((hour == null) ? 0 : hour.hashCode());
		result = prime * result + ((minute == null) ? 0 : minute.hashCode());
		result = prime * result + ((placeIds == null) ? 0 : placeIds.hashCode());
		result = prime * result + ((weekPeriodicity == null) ? 0 : weekPeriodicity.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		LessonResource other = (LessonResource) obj;
		if (day != other.day) {
			return false;
		}
		if (duration == null) {
			if (other.duration != null) {
				return false;
			}
		} else if (!duration.equals(other.duration)) {
			return false;
		}
		if (hour == null) {
			if (other.hour != null) {
				return false;
			}
		} else if (!hour.equals(other.hour)) {
			return false;
		}
		if (minute == null) {
			if (other.minute != null) {
				return false;
			}
		} else if (!minute.equals(other.minute)) {
			return false;
		}
		if (placeIds == null) {
			if (other.placeIds != null) {
				return false;
			}
		} else if (!placeIds.equals(other.placeIds)) {
			return false;
		}
		if (weekPeriodicity != other.weekPeriodicity) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "LessonResource [day=" + day + ", hour=" + hour + ", minute=" + minute + ", duration=" + duration
				+ ", weekPeriodicity=" + weekPeriodicity + ", placeIds=" + placeIds + ", id=" + id + "]";
	}
	
}
