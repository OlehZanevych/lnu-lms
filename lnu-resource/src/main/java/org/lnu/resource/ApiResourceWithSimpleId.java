package org.lnu.resource;

import java.text.MessageFormat;

/**
 * Abstract class, that all API resources with simple id
 * need to extend.
 * @author OlehZanevych
 * 
 * @param <KEY> identifier class
 */
public abstract class ApiResourceWithSimpleId<KEY extends Number>
		extends ResourceWithSimpleId<KEY> implements ApiResource<KEY> {
	
	/**
	 * Default constructor with no parameters.
	 */
	protected ApiResourceWithSimpleId() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	protected ApiResourceWithSimpleId(final KEY id) {
		super(id);
	}
	
	@Override
	public String getUri() {
		if (id == null) {
			return null;
		}
		return MessageFormat.format("{0}/{1,number,#}", getRootUri(), id);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ApiResourceWithSimpleId [id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
