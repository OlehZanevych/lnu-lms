package org.lnu.resource.toilet;

import java.text.MessageFormat;

import org.lnu.annotation.CrudableResource;
import org.lnu.model.enumtype.api.API;
import org.lnu.model.enumtype.toilettype.ToiletType;
import org.lnu.resource.SecurityAPIResource;

/**
 * Resource for web layer, that describes Toilet.
 * @author OlehZanevych
 */
@CrudableResource
public class ToiletResource extends SecurityAPIResource<Long> {
	
	private Long buildingMapId;
	
	private Short onMapId;
	
	private Long buildingId;
	
	private ToiletType toiletType;
	
	private String info;
	
	/**
	 * Default constructor with no parameters.
	 */
	public ToiletResource() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public ToiletResource(final Long id) {
		super(id);
	}

	@Override
	public String getRootUri() {
		return MessageFormat.format("/{0}", API.toilets);
	}

	public Long getBuildingMapId() {
		return buildingMapId;
	}

	public void setBuildingMapId(final Long buildingMapId) {
		this.buildingMapId = buildingMapId;
	}

	public Short getOnMapId() {
		return onMapId;
	}

	public void setOnMapId(final Short onMapId) {
		this.onMapId = onMapId;
	}

	public Long getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(final Long buildingId) {
		this.buildingId = buildingId;
	}

	public ToiletType getToiletType() {
		return toiletType;
	}

	public void setToiletType(final ToiletType toiletType) {
		this.toiletType = toiletType;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(final String info) {
		this.info = info;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((buildingId == null) ? 0 : buildingId.hashCode());
		result = prime * result + ((buildingMapId == null) ? 0 : buildingMapId.hashCode());
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((onMapId == null) ? 0 : onMapId.hashCode());
		result = prime * result + ((toiletType == null) ? 0 : toiletType.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ToiletResource other = (ToiletResource) obj;
		if (buildingId == null) {
			if (other.buildingId != null) {
				return false;
			}
		} else if (!buildingId.equals(other.buildingId)) {
			return false;
		}
		if (buildingMapId == null) {
			if (other.buildingMapId != null) {
				return false;
			}
		} else if (!buildingMapId.equals(other.buildingMapId)) {
			return false;
		}
		if (info == null) {
			if (other.info != null) {
				return false;
			}
		} else if (!info.equals(other.info)) {
			return false;
		}
		if (onMapId == null) {
			if (other.onMapId != null) {
				return false;
			}
		} else if (!onMapId.equals(other.onMapId)) {
			return false;
		}
		if (toiletType != other.toiletType) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ToiletResource [buildingMapId=");
		builder.append(buildingMapId);
		builder.append(", onMapId=");
		builder.append(onMapId);
		builder.append(", buildingId=");
		builder.append(buildingId);
		builder.append(", toiletType=");
		builder.append(toiletType);
		builder.append(", info=");
		builder.append(info);
		builder.append(", isPrivate=");
		builder.append(isPrivate);
		builder.append(", capability=");
		builder.append(capability);
		builder.append(", responsibleOwnUserIds=");
		builder.append(responsibleOwnUserIds);
		builder.append(", responsibleGroupIds=");
		builder.append(responsibleGroupIds);
		builder.append(", responsibleUserIds=");
		builder.append(responsibleUserIds);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
