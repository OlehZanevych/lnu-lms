package org.lnu.resource.lecturer;

import java.text.MessageFormat;

import org.lnu.annotation.CrudableResource;
import org.lnu.model.enumtype.academicstatus.AcademicStatus;
import org.lnu.model.enumtype.api.API;
import org.lnu.resource.SecurityAPIResource;

/**
 * Resource for web layer, that describes Lecturer.
 * @author OlehZanevych
 */
@CrudableResource
public class LecturerResource extends SecurityAPIResource<Long> {
	
	private String name;
	
	private String surname;
	
	private String firstName;
	
	private String middleName;
	
	private String email;
	
	private String phone;
	
	private String info;
	
	private Long departmentId;
	
	private Long unitId;
	
	private Long scientificDegreeId;
	
	private AcademicStatus academicStatus;
	
	/**
	 * Default constructor with no parameters.
	 */
	public LecturerResource() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public LecturerResource(final Long id) {
		super(id);
	}

	@Override
	public String getRootUri() {
		return MessageFormat.format("/{0}", API.lecturers);
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(final String surname) {
		this.surname = surname;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(final String middleName) {
		this.middleName = middleName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(final String phone) {
		this.phone = phone;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(final String info) {
		this.info = info;
	}

	public Long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(final Long departmentId) {
		this.departmentId = departmentId;
	}

	public Long getUnitId() {
		return unitId;
	}

	public void setUnitId(final Long unitId) {
		this.unitId = unitId;
	}

	public Long getScientificDegreeId() {
		return scientificDegreeId;
	}

	public void setScientificDegreeId(final Long scientificDegreeId) {
		this.scientificDegreeId = scientificDegreeId;
	}

	public AcademicStatus getAcademicStatus() {
		return academicStatus;
	}

	public void setAcademicStatus(final AcademicStatus academicStatus) {
		this.academicStatus = academicStatus;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((academicStatus == null) ? 0 : academicStatus.hashCode());
		result = prime * result + ((departmentId == null) ? 0 : departmentId.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((unitId == null) ? 0 : unitId.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((middleName == null) ? 0 : middleName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((scientificDegreeId == null) ? 0 : scientificDegreeId.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		LecturerResource other = (LecturerResource) obj;
		if (academicStatus != other.academicStatus) {
			return false;
		}
		if (departmentId == null) {
			if (other.departmentId != null) {
				return false;
			}
		} else if (!departmentId.equals(other.departmentId)) {
			return false;
		}
		if (email == null) {
			if (other.email != null) {
				return false;
			}
		} else if (!email.equals(other.email)) {
			return false;
		}
		if (unitId == null) {
			if (other.unitId != null) {
				return false;
			}
		} else if (!unitId.equals(other.unitId)) {
			return false;
		}
		if (firstName == null) {
			if (other.firstName != null) {
				return false;
			}
		} else if (!firstName.equals(other.firstName)) {
			return false;
		}
		if (info == null) {
			if (other.info != null) {
				return false;
			}
		} else if (!info.equals(other.info)) {
			return false;
		}
		if (middleName == null) {
			if (other.middleName != null) {
				return false;
			}
		} else if (!middleName.equals(other.middleName)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (phone == null) {
			if (other.phone != null) {
				return false;
			}
		} else if (!phone.equals(other.phone)) {
			return false;
		}
		if (scientificDegreeId == null) {
			if (other.scientificDegreeId != null) {
				return false;
			}
		} else if (!scientificDegreeId.equals(other.scientificDegreeId)) {
			return false;
		}
		if (surname == null) {
			if (other.surname != null) {
				return false;
			}
		} else if (!surname.equals(other.surname)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LecturerResource [name=");
		builder.append(name);
		builder.append(", surname=");
		builder.append(surname);
		builder.append(", firstName=");
		builder.append(firstName);
		builder.append(", middleName=");
		builder.append(middleName);
		builder.append(", email=");
		builder.append(email);
		builder.append(", phone=");
		builder.append(phone);
		builder.append(", info=");
		builder.append(info);
		builder.append(", departmentId=");
		builder.append(departmentId);
		builder.append(", unitId=");
		builder.append(unitId);
		builder.append(", scientificDegreeId=");
		builder.append(scientificDegreeId);
		builder.append(", academicStatus=");
		builder.append(academicStatus);
		builder.append(", isPrivate=");
		builder.append(isPrivate);
		builder.append(", capability=");
		builder.append(capability);
		builder.append(", responsibleOwnUserIds=");
		builder.append(responsibleOwnUserIds);
		builder.append(", responsibleGroupIds=");
		builder.append(responsibleGroupIds);
		builder.append(", responsibleUserIds=");
		builder.append(responsibleUserIds);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
