package org.lnu.resource.building.map;

import java.text.MessageFormat;

import org.lnu.annotation.CrudableResource;
import org.lnu.model.enumtype.api.API;
import org.lnu.resource.FileAPIResource;

/**
 * Resource for web layer, that describes BuildingMap.
 * @author OlehZanevych
 */
@CrudableResource
public class BuildingMapResource extends FileAPIResource {
	
	private String name;
	
	private Long buildingId;
	
	private String description;
	
	/**
	 * Default constructor with no parameters.
	 */
	public BuildingMapResource() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public BuildingMapResource(final Long id) {
		super(id);
	}
	
	@Override
	public String getRootUri() {
		return MessageFormat.format("/{0}", API.buildingmaps);
	}
	
	public String getFileURI() {
		return "upload/maps/" + id + ".dwg";
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Long getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(final Long buildingId) {
		this.buildingId = buildingId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((buildingId == null) ? 0 : buildingId.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BuildingMapResource other = (BuildingMapResource) obj;
		if (buildingId == null) {
			if (other.buildingId != null) {
				return false;
			}
		} else if (!buildingId.equals(other.buildingId)) {
			return false;
		}
		if (description == null) {
			if (other.description != null) {
				return false;
			}
		} else if (!description.equals(other.description)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BuildingMapResource [name=");
		builder.append(name);
		builder.append(", buildingId=");
		builder.append(buildingId);
		builder.append(", description=");
		builder.append(description);
		builder.append(", file=");
		builder.append(file);
		builder.append(", isPrivate=");
		builder.append(isPrivate);
		builder.append(", capability=");
		builder.append(capability);
		builder.append(", responsibleOwnUserIds=");
		builder.append(responsibleOwnUserIds);
		builder.append(", responsibleGroupIds=");
		builder.append(responsibleGroupIds);
		builder.append(", responsibleUserIds=");
		builder.append(responsibleUserIds);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
