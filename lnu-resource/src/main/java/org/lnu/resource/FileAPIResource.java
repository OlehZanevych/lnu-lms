package org.lnu.resource;

import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Abstract class, that all file API resources need to implement.
 * @author OlehZanevych
 */
public abstract class FileAPIResource extends SecurityAPIResource<Long> {
	
	@JsonIgnore
	protected MultipartFile file;
	
	protected String fileURI;
	
	/**
	 * Default constructor with no parameters.
	 */
	public FileAPIResource() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public FileAPIResource(final Long id) {
		super(id);
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(final MultipartFile file) {
		this.file = file;
	}

	public String getFileURI() {
		return fileURI;
	}

	public void setFileURI(final String fileURI) {
		this.fileURI = fileURI;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((file == null) ? 0 : file.hashCode());
		result = prime * result + ((fileURI == null) ? 0 : fileURI.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		FileAPIResource other = (FileAPIResource) obj;
		if (file == null) {
			if (other.file != null) {
				return false;
			}
		} else if (!file.equals(other.file)) {
			return false;
		}
		if (fileURI == null) {
			if (other.fileURI != null) {
				return false;
			}
		} else if (!fileURI.equals(other.fileURI)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FileAPIResource [file=");
		builder.append(file);
		builder.append(", fileURI=");
		builder.append(fileURI);
		builder.append(", isPrivate=");
		builder.append(isPrivate);
		builder.append(", capability=");
		builder.append(capability);
		builder.append(", responsibleOwnUserIds=");
		builder.append(responsibleOwnUserIds);
		builder.append(", responsibleGroupIds=");
		builder.append(responsibleGroupIds);
		builder.append(", responsibleUserIds=");
		builder.append(responsibleUserIds);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
