package org.lnu.resource.atomicload;

import java.util.List;

import org.lnu.resource.ResourceWithSimpleId;

/**
 * Resource for web layer, that describes AtomicLoad.
 * @author OlehZanevych
 */
public class AtomicLoadResource extends ResourceWithSimpleId<Long> {
	
	private Short hours;
	
	private Byte lessonDuration;
	
	private List<Long> lecturerIds;
	
	private List<Long> specialtyIds;
	
	private List<Long> academicGroupIds;

	private List<Long> studentIds;
	
	private List<Long> learningPlaceGroupIds;
	
	private List<Long> learningPlaceIds;
	
	/**
	 * Default constructor with no parameters.
	 */
	public AtomicLoadResource() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public AtomicLoadResource(final Long id) {
		super(id);
	}

	public Short getHours() {
		return hours;
	}

	public void setHours(final Short hours) {
		this.hours = hours;
	}

	public Byte getLessonDuration() {
		return lessonDuration;
	}

	public void setLessonDuration(final Byte lessonDuration) {
		this.lessonDuration = lessonDuration;
	}

	public List<Long> getLecturerIds() {
		return lecturerIds;
	}

	public void setLecturerIds(final List<Long> lecturerIds) {
		this.lecturerIds = lecturerIds;
	}

	public List<Long> getSpecialtyIds() {
		return specialtyIds;
	}

	public void setSpecialtyIds(final List<Long> specialtyIds) {
		this.specialtyIds = specialtyIds;
	}

	public List<Long> getAcademicGroupIds() {
		return academicGroupIds;
	}

	public void setAcademicGroupIds(final List<Long> academicGroupIds) {
		this.academicGroupIds = academicGroupIds;
	}

	public List<Long> getStudentIds() {
		return studentIds;
	}

	public void setStudentIds(final List<Long> studentIds) {
		this.studentIds = studentIds;
	}

	public List<Long> getLearningPlaceGroupIds() {
		return learningPlaceGroupIds;
	}

	public void setLearningPlaceGroupIds(final List<Long> learningPlaceGroupIds) {
		this.learningPlaceGroupIds = learningPlaceGroupIds;
	}

	public List<Long> getLearningPlaceIds() {
		return learningPlaceIds;
	}

	public void setLearningPlaceIds(final List<Long> learningPlaceIds) {
		this.learningPlaceIds = learningPlaceIds;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((academicGroupIds == null) ? 0 : academicGroupIds.hashCode());
		result = prime * result + ((hours == null) ? 0 : hours.hashCode());
		result = prime * result + ((learningPlaceGroupIds == null) ? 0 : learningPlaceGroupIds.hashCode());
		result = prime * result + ((learningPlaceIds == null) ? 0 : learningPlaceIds.hashCode());
		result = prime * result + ((lecturerIds == null) ? 0 : lecturerIds.hashCode());
		result = prime * result + ((lessonDuration == null) ? 0 : lessonDuration.hashCode());
		result = prime * result + ((specialtyIds == null) ? 0 : specialtyIds.hashCode());
		result = prime * result + ((studentIds == null) ? 0 : studentIds.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AtomicLoadResource other = (AtomicLoadResource) obj;
		if (academicGroupIds == null) {
			if (other.academicGroupIds != null) {
				return false;
			}
		} else if (!academicGroupIds.equals(other.academicGroupIds)) {
			return false;
		}
		if (hours == null) {
			if (other.hours != null) {
				return false;
			}
		} else if (!hours.equals(other.hours)) {
			return false;
		}
		if (learningPlaceGroupIds == null) {
			if (other.learningPlaceGroupIds != null) {
				return false;
			}
		} else if (!learningPlaceGroupIds.equals(other.learningPlaceGroupIds)) {
			return false;
		}
		if (learningPlaceIds == null) {
			if (other.learningPlaceIds != null) {
				return false;
			}
		} else if (!learningPlaceIds.equals(other.learningPlaceIds)) {
			return false;
		}
		if (lecturerIds == null) {
			if (other.lecturerIds != null) {
				return false;
			}
		} else if (!lecturerIds.equals(other.lecturerIds)) {
			return false;
		}
		if (lessonDuration == null) {
			if (other.lessonDuration != null) {
				return false;
			}
		} else if (!lessonDuration.equals(other.lessonDuration)) {
			return false;
		}
		if (specialtyIds == null) {
			if (other.specialtyIds != null) {
				return false;
			}
		} else if (!specialtyIds.equals(other.specialtyIds)) {
			return false;
		}
		if (studentIds == null) {
			if (other.studentIds != null) {
				return false;
			}
		} else if (!studentIds.equals(other.studentIds)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AtomicLoadResource [hours=");
		builder.append(hours);
		builder.append(", lessonDuration=");
		builder.append(lessonDuration);
		builder.append(", lecturerIds=");
		builder.append(lecturerIds);
		builder.append(", specialtyIds=");
		builder.append(specialtyIds);
		builder.append(", academicGroupIds=");
		builder.append(academicGroupIds);
		builder.append(", studentIds=");
		builder.append(studentIds);
		builder.append(", learningPlaceGroupIds=");
		builder.append(learningPlaceGroupIds);
		builder.append(", learningPlaceIds=");
		builder.append(learningPlaceIds);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
