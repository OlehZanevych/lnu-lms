package org.lnu.resource.apicapability;

import org.lnu.annotation.CrudableResource;
import org.lnu.model.enumtype.api.API;
import org.lnu.model.enumtype.api.capability.level.APICapabilityLevel;
import org.lnu.resource.ResourceWithSimpleId;

/**
 * Resource for web layer, that describes ApiCapability.
 * @author OlehZanevych
 */
@CrudableResource
public class ApiCapabilityResource extends ResourceWithSimpleId<Long> {
	
	private API api;
	
	private APICapabilityLevel apiCapabilityLevel;

	/**
	 * Default constructor with no parameters.
	 */
	public ApiCapabilityResource() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public ApiCapabilityResource(final Long id) {
		super(id);
	}
	
	public API getApi() {
		return api;
	}

	public void setApi(final API api) {
		this.api = api;
	}

	public APICapabilityLevel getApiCapabilityLevel() {
		return apiCapabilityLevel;
	}

	public void setApiCapabilityLevel(final APICapabilityLevel apiCapabilityLevel) {
		this.apiCapabilityLevel = apiCapabilityLevel;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((api == null) ? 0 : api.hashCode());
		result = prime * result + ((apiCapabilityLevel == null) ? 0 : apiCapabilityLevel.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ApiCapabilityResource other = (ApiCapabilityResource) obj;
		if (api != other.api) {
			return false;
		}
		if (apiCapabilityLevel != other.apiCapabilityLevel) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ApiCapabilityResource [api=");
		builder.append(api);
		builder.append(", apiCapabilityLevel=");
		builder.append(apiCapabilityLevel);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
