package org.lnu.resource.role;

import java.text.MessageFormat;
import java.util.List;

import org.lnu.annotation.CrudableResource;
import org.lnu.model.enumtype.api.API;
import org.lnu.resource.SecurityAPIResource;
import org.lnu.resource.apicapability.ApiCapabilityResource;

/**
 * Resource for web layer, that describes Role.
 * @author OlehZanevych
 */
@CrudableResource
public class RoleResource extends SecurityAPIResource<Long> {
	
	private String name;
	
	private String abbreviation;
	
	private String description;
	
	private List<ApiCapabilityResource> apiCapabilities;

	/**
	 * Default constructor with no parameters.
	 */
	public RoleResource() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public RoleResource(final Long id) {
		super(id);
	}
	
	@Override
	public String getRootUri() {
		return MessageFormat.format("/{0}", API.roles);
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(final String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public List<ApiCapabilityResource> getApiCapabilities() {
		return apiCapabilities;
	}

	public void setApiCapabilities(final List<ApiCapabilityResource> apiCapabilities) {
		this.apiCapabilities = apiCapabilities;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((abbreviation == null) ? 0 : abbreviation.hashCode());
		result = prime * result + ((apiCapabilities == null) ? 0 : apiCapabilities.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RoleResource other = (RoleResource) obj;
		if (abbreviation == null) {
			if (other.abbreviation != null) {
				return false;
			}
		} else if (!abbreviation.equals(other.abbreviation)) {
			return false;
		}
		if (apiCapabilities == null) {
			if (other.apiCapabilities != null) {
				return false;
			}
		} else if (!apiCapabilities.equals(other.apiCapabilities)) {
			return false;
		}
		if (description == null) {
			if (other.description != null) {
				return false;
			}
		} else if (!description.equals(other.description)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RoleResource [name=");
		builder.append(name);
		builder.append(", abbreviation=");
		builder.append(abbreviation);
		builder.append(", description=");
		builder.append(description);
		builder.append(", apiCapabilities=");
		builder.append(apiCapabilities);
		builder.append(", isPrivate=");
		builder.append(isPrivate);
		builder.append(", capability=");
		builder.append(capability);
		builder.append(", responsibleOwnUserIds=");
		builder.append(responsibleOwnUserIds);
		builder.append(", responsibleGroupIds=");
		builder.append(responsibleGroupIds);
		builder.append(", responsibleUserIds=");
		builder.append(responsibleUserIds);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
