package org.lnu.resource.dormitory;

import java.text.MessageFormat;

import org.lnu.annotation.CrudableResource;
import org.lnu.model.enumtype.api.API;
import org.lnu.resource.SecurityAPIResource;

/**
 * Resource for web layer, that describes Dormitory.
 * @author OlehZanevych
 */
@CrudableResource
public class DormitoryResource extends SecurityAPIResource<Long> {
	
	private Short number;
	
	private Long buildingMapId;
	
	private Short onMapId;
	
	private Long buildingId;
	
	private Byte maxStudents;
	
	private Float square;
	
	private String info;
	
	/**
	 * Default constructor with no parameters.
	 */
	public DormitoryResource() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public DormitoryResource(final Long id) {
		super(id);
	}

	@Override
	public String getRootUri() {
		return MessageFormat.format("/{0}", API.dormitories);
	}

	public Short getNumber() {
		return number;
	}

	public void setNumber(final Short number) {
		this.number = number;
	}

	public Long getBuildingMapId() {
		return buildingMapId;
	}

	public void setBuildingMapId(final Long buildingMapId) {
		this.buildingMapId = buildingMapId;
	}

	public Short getOnMapId() {
		return onMapId;
	}

	public void setOnMapId(final Short onMapId) {
		this.onMapId = onMapId;
	}

	public Long getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(final Long buildingId) {
		this.buildingId = buildingId;
	}

	public Byte getMaxStudents() {
		return maxStudents;
	}

	public void setMaxStudents(final Byte maxStudents) {
		this.maxStudents = maxStudents;
	}

	public Float getSquare() {
		return square;
	}

	public void setSquare(final Float square) {
		this.square = square;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(final String info) {
		this.info = info;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((buildingId == null) ? 0 : buildingId.hashCode());
		result = prime * result + ((buildingMapId == null) ? 0 : buildingMapId.hashCode());
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((maxStudents == null) ? 0 : maxStudents.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((onMapId == null) ? 0 : onMapId.hashCode());
		result = prime * result + ((square == null) ? 0 : square.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		DormitoryResource other = (DormitoryResource) obj;
		if (buildingId == null) {
			if (other.buildingId != null) {
				return false;
			}
		} else if (!buildingId.equals(other.buildingId)) {
			return false;
		}
		if (buildingMapId == null) {
			if (other.buildingMapId != null) {
				return false;
			}
		} else if (!buildingMapId.equals(other.buildingMapId)) {
			return false;
		}
		if (info == null) {
			if (other.info != null) {
				return false;
			}
		} else if (!info.equals(other.info)) {
			return false;
		}
		if (maxStudents == null) {
			if (other.maxStudents != null) {
				return false;
			}
		} else if (!maxStudents.equals(other.maxStudents)) {
			return false;
		}
		if (number == null) {
			if (other.number != null) {
				return false;
			}
		} else if (!number.equals(other.number)) {
			return false;
		}
		if (onMapId == null) {
			if (other.onMapId != null) {
				return false;
			}
		} else if (!onMapId.equals(other.onMapId)) {
			return false;
		}
		if (square == null) {
			if (other.square != null) {
				return false;
			}
		} else if (!square.equals(other.square)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DormitoryResource [number=");
		builder.append(number);
		builder.append(", buildingMapId=");
		builder.append(buildingMapId);
		builder.append(", onMapId=");
		builder.append(onMapId);
		builder.append(", buildingId=");
		builder.append(buildingId);
		builder.append(", maxStudents=");
		builder.append(maxStudents);
		builder.append(", square=");
		builder.append(square);
		builder.append(", info=");
		builder.append(info);
		builder.append(", isPrivate=");
		builder.append(isPrivate);
		builder.append(", capability=");
		builder.append(capability);
		builder.append(", responsibleOwnUserIds=");
		builder.append(responsibleOwnUserIds);
		builder.append(", responsibleGroupIds=");
		builder.append(responsibleGroupIds);
		builder.append(", responsibleUserIds=");
		builder.append(responsibleUserIds);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
