package org.lnu.resource.learningplacegroup;

import java.text.MessageFormat;
import java.util.List;

import org.lnu.annotation.CrudableResource;
import org.lnu.model.enumtype.api.API;
import org.lnu.resource.SecurityAPIResource;

/**
 * Resource for web layer, that describes LearningPlaceGroup.
 * @author OlehZanevych
 */
@CrudableResource
public class LearningPlaceGroupResource extends SecurityAPIResource<Long> {
	
	private String name;
	
	private String abbreviation;
	
	private Long unitId;
	
	private List<Long> learningPlaceIds;
	
	/**
	 * Default constructor with no parameters.
	 */
	public LearningPlaceGroupResource() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public LearningPlaceGroupResource(final Long id) {
		super(id);
	}

	@Override
	public String getRootUri() {
		return MessageFormat.format("/{0}", API.learningplacegroups);
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(final String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public Long getUnitId() {
		return unitId;
	}

	public void setUnitId(final Long unitId) {
		this.unitId = unitId;
	}

	public List<Long> getLearningPlaceIds() {
		return learningPlaceIds;
	}

	public void setLearningPlaceIds(final List<Long> learningPlaceIds) {
		this.learningPlaceIds = learningPlaceIds;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((abbreviation == null) ? 0 : abbreviation.hashCode());
		result = prime * result + ((learningPlaceIds == null) ? 0 : learningPlaceIds.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((unitId == null) ? 0 : unitId.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		LearningPlaceGroupResource other = (LearningPlaceGroupResource) obj;
		if (abbreviation == null) {
			if (other.abbreviation != null) {
				return false;
			}
		} else if (!abbreviation.equals(other.abbreviation)) {
			return false;
		}
		if (learningPlaceIds == null) {
			if (other.learningPlaceIds != null) {
				return false;
			}
		} else if (!learningPlaceIds.equals(other.learningPlaceIds)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (unitId == null) {
			if (other.unitId != null) {
				return false;
			}
		} else if (!unitId.equals(other.unitId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LearningPlaceGroupResource [name=");
		builder.append(name);
		builder.append(", abbreviation=");
		builder.append(abbreviation);
		builder.append(", unitId=");
		builder.append(unitId);
		builder.append(", learningPlaceIds=");
		builder.append(learningPlaceIds);
		builder.append(", isPrivate=");
		builder.append(isPrivate);
		builder.append(", capability=");
		builder.append(capability);
		builder.append(", responsibleOwnUserIds=");
		builder.append(responsibleOwnUserIds);
		builder.append(", responsibleGroupIds=");
		builder.append(responsibleGroupIds);
		builder.append(", responsibleUserIds=");
		builder.append(responsibleUserIds);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
