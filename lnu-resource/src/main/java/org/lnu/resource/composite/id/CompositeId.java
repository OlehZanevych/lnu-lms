package org.lnu.resource.composite.id;

import java.util.Arrays;

/**
 * Composite ID of Resource.
 * @author OlehZanevych 
 */
public class CompositeId {
	
	private Class<?> clazz;
	
	private Object[] values;

	/**
	 * Default constructor with no parameters.
	 */
	public CompositeId() {
		
	}

	/**
	 * Constructor with class of Resource and values of composite ID.
	 * @param clazz class of Resource
	 * @param values values of composite ID
	 */
	public CompositeId(final Class<?> clazz, final Object... values) {
		this.clazz = clazz;
		this.values = values;
	}

	public Class<?> getClazz() {
		return clazz;
	}

	public void setClazz(final Class<?> clazz) {
		this.clazz = clazz;
	}

	public Object[] getValues() {
		return values;
	}

	public void setValues(final Object[] values) {
		this.values = values;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clazz == null) ? 0 : clazz.hashCode());
		result = prime * result + Arrays.hashCode(values);
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CompositeId other = (CompositeId) obj;
		if (clazz == null) {
			if (other.clazz != null) {
				return false;
			}
		} else if (!clazz.equals(other.clazz)) {
			return false;
		}
		if (!Arrays.equals(values, other.values)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CompositeId [clazz=");
		builder.append(clazz);
		builder.append(", values=");
		builder.append(Arrays.toString(values));
		builder.append("]");
		return builder.toString();
	}

}
