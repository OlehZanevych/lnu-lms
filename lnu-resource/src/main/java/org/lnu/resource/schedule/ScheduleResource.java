package org.lnu.resource.schedule;

import java.text.MessageFormat;
import java.util.List;

import org.lnu.model.enumtype.api.API;
import org.lnu.model.enumtype.classtype.ClassType;
import org.lnu.resource.SecurityAPIResource;
import org.lnu.resource.lecturer.LecturerResource;
import org.lnu.resource.lesson.LessonResource;

/**
 * Resource for web layer, that describes Schedule.
 * @author OlehZanevych
 */
public class ScheduleResource extends SecurityAPIResource<Long> {
	
	private String courseName;
	
	private ClassType classType;
	
	private Short hours;
	
	private Long academicGroupId;
	
	private Byte semester;
	
	private List<LecturerResource> lecturers;
	
	private List<LessonResource> lessons;

	@Override
	public String getRootUri() {
		return MessageFormat.format("/{0}", API.schedule);
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(final String courseName) {
		this.courseName = courseName;
	}

	public ClassType getClassType() {
		return classType;
	}

	public void setClassType(final ClassType classType) {
		this.classType = classType;
	}

	public Short getHours() {
		return hours;
	}

	public void setHours(final Short hours) {
		this.hours = hours;
	}

	public Long getAcademicGroupId() {
		return academicGroupId;
	}

	public void setAcademicGroupId(final Long academicGroupId) {
		this.academicGroupId = academicGroupId;
	}

	public Byte getSemester() {
		return semester;
	}

	public void setSemester(final Byte semester) {
		this.semester = semester;
	}

	public List<LecturerResource> getLecturers() {
		return lecturers;
	}

	public void setLecturers(final List<LecturerResource> lecturers) {
		this.lecturers = lecturers;
	}

	public List<LessonResource> getLessons() {
		return lessons;
	}

	public void setLessons(final List<LessonResource> lessons) {
		this.lessons = lessons;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((academicGroupId == null) ? 0 : academicGroupId.hashCode());
		result = prime * result + ((classType == null) ? 0 : classType.hashCode());
		result = prime * result + ((courseName == null) ? 0 : courseName.hashCode());
		result = prime * result + ((hours == null) ? 0 : hours.hashCode());
		result = prime * result + ((lecturers == null) ? 0 : lecturers.hashCode());
		result = prime * result + ((lessons == null) ? 0 : lessons.hashCode());
		result = prime * result + ((semester == null) ? 0 : semester.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ScheduleResource other = (ScheduleResource) obj;
		if (academicGroupId == null) {
			if (other.academicGroupId != null) {
				return false;
			}
		} else if (!academicGroupId.equals(other.academicGroupId)) {
			return false;
		}
		if (classType != other.classType) {
			return false;
		}
		if (courseName == null) {
			if (other.courseName != null) {
				return false;
			}
		} else if (!courseName.equals(other.courseName)) {
			return false;
		}
		if (hours == null) {
			if (other.hours != null) {
				return false;
			}
		} else if (!hours.equals(other.hours)) {
			return false;
		}
		if (lecturers == null) {
			if (other.lecturers != null) {
				return false;
			}
		} else if (!lecturers.equals(other.lecturers)) {
			return false;
		}
		if (lessons == null) {
			if (other.lessons != null) {
				return false;
			}
		} else if (!lessons.equals(other.lessons)) {
			return false;
		}
		if (semester == null) {
			if (other.semester != null) {
				return false;
			}
		} else if (!semester.equals(other.semester)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ScheduleResource [courseName=" + courseName + ", classType=" + classType + ", hours=" + hours
				+ ", academicGroupId=" + academicGroupId + ", semester=" + semester + ", lecturers=" + lecturers
				+ ", lessons=" + lessons + ", id=" + id + "]";
	}

}
