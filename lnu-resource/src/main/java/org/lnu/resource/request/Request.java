package org.lnu.resource.request;

import java.util.Arrays;

/**
 * Request, that comes from controller.
 * @author OlehZanevych
 */
public class Request {

	protected String[] fields;

	/**
	 * Default constructor.
	 */
	public Request() {
		
	}

	/**
	 * Constructor.
	 * @param fields fields
	 */
	public Request(final String[] fields) {
		this.fields = fields;
	}
	
	/**
	 * Constructor.
	 * @param request Request
	 */
	public Request(final Request request) {
		fields = request.fields;
	}

	public String[] getFields() {
		return fields;
	}

	public void setFields(final String[] fields) {
		this.fields = fields;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(fields);
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Request other = (Request) obj;
		if (!Arrays.equals(fields, other.fields)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Request [fields=");
		builder.append(Arrays.toString(fields));
		builder.append("]");
		return builder.toString();
	}
	
}
