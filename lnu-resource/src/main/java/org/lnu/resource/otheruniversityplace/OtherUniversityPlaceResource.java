package org.lnu.resource.otheruniversityplace;

import java.text.MessageFormat;

import org.lnu.annotation.CrudableResource;
import org.lnu.model.enumtype.api.API;
import org.lnu.model.enumtype.otheruniversityplacetype.OtherUniversityPlaceType;
import org.lnu.resource.SecurityAPIResource;

/**
 * Resource for web layer, that describes OtherUniversityPlace.
 * @author OlehZanevych
 */
@CrudableResource
public class OtherUniversityPlaceResource extends SecurityAPIResource<Long> {
	
	private String name;
	
	private Long buildingMapId;
	
	private Short onMapId;
	
	private Long buildingId;
	
	private OtherUniversityPlaceType otherUniversityPlaceType;
	
	private Float square;
	
	private Byte roomsCount;
	
	private String info;
	
	/**
	 * Default constructor with no parameters.
	 */
	public OtherUniversityPlaceResource() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public OtherUniversityPlaceResource(final Long id) {
		super(id);
	}

	@Override
	public String getRootUri() {
		return MessageFormat.format("/{0}", API.otheruniversityplaces);
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Long getBuildingMapId() {
		return buildingMapId;
	}

	public void setBuildingMapId(final Long buildingMapId) {
		this.buildingMapId = buildingMapId;
	}

	public Short getOnMapId() {
		return onMapId;
	}

	public void setOnMapId(final Short onMapId) {
		this.onMapId = onMapId;
	}

	public Long getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(final Long buildingId) {
		this.buildingId = buildingId;
	}

	public OtherUniversityPlaceType getOtherUniversityPlaceType() {
		return otherUniversityPlaceType;
	}

	public void setOtherUniversityPlaceType(final OtherUniversityPlaceType otherUniversityPlaceType) {
		this.otherUniversityPlaceType = otherUniversityPlaceType;
	}

	public Float getSquare() {
		return square;
	}

	public void setSquare(final Float square) {
		this.square = square;
	}

	public Byte getRoomsCount() {
		return roomsCount;
	}

	public void setRoomsCount(final Byte roomsCount) {
		this.roomsCount = roomsCount;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(final String info) {
		this.info = info;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((buildingId == null) ? 0 : buildingId.hashCode());
		result = prime * result + ((buildingMapId == null) ? 0 : buildingMapId.hashCode());
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((onMapId == null) ? 0 : onMapId.hashCode());
		result = prime * result + ((otherUniversityPlaceType == null) ? 0 : otherUniversityPlaceType.hashCode());
		result = prime * result + ((roomsCount == null) ? 0 : roomsCount.hashCode());
		result = prime * result + ((square == null) ? 0 : square.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		OtherUniversityPlaceResource other = (OtherUniversityPlaceResource) obj;
		if (buildingId == null) {
			if (other.buildingId != null) {
				return false;
			}
		} else if (!buildingId.equals(other.buildingId)) {
			return false;
		}
		if (buildingMapId == null) {
			if (other.buildingMapId != null) {
				return false;
			}
		} else if (!buildingMapId.equals(other.buildingMapId)) {
			return false;
		}
		if (info == null) {
			if (other.info != null) {
				return false;
			}
		} else if (!info.equals(other.info)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (onMapId == null) {
			if (other.onMapId != null) {
				return false;
			}
		} else if (!onMapId.equals(other.onMapId)) {
			return false;
		}
		if (otherUniversityPlaceType != other.otherUniversityPlaceType) {
			return false;
		}
		if (roomsCount == null) {
			if (other.roomsCount != null) {
				return false;
			}
		} else if (!roomsCount.equals(other.roomsCount)) {
			return false;
		}
		if (square == null) {
			if (other.square != null) {
				return false;
			}
		} else if (!square.equals(other.square)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OtherUniversityPlaceResource [name=");
		builder.append(name);
		builder.append(", buildingMapId=");
		builder.append(buildingMapId);
		builder.append(", onMapId=");
		builder.append(onMapId);
		builder.append(", buildingId=");
		builder.append(buildingId);
		builder.append(", otherUniversityPlaceType=");
		builder.append(otherUniversityPlaceType);
		builder.append(", square=");
		builder.append(square);
		builder.append(", roomsCount=");
		builder.append(roomsCount);
		builder.append(", info=");
		builder.append(info);
		builder.append(", isPrivate=");
		builder.append(isPrivate);
		builder.append(", capability=");
		builder.append(capability);
		builder.append(", responsibleOwnUserIds=");
		builder.append(responsibleOwnUserIds);
		builder.append(", responsibleGroupIds=");
		builder.append(responsibleGroupIds);
		builder.append(", responsibleUserIds=");
		builder.append(responsibleUserIds);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
