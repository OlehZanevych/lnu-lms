package org.lnu.resource;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Abstract class, that all API resources need to extend.
 * @author OlehZanevych
 * 
 * @param <KEY> Identifier of Resource
 */
public interface ApiResource<KEY> extends Resource {
	
	/**
	 * Getting root URI.
	 * @return root URI
	 */
	@JsonIgnore
	String getRootUri();
	
	/**
	 * Method for getting unique URI for all resources.
	 * @return URI of string representation.
	 */
	String getUri();
	
}
