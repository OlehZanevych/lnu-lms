package org.lnu.resource;

/**
 * Base interface for all resources.
 * @author OlehZanevych
 */
public interface Resource {
	
	/**
	 * Getting resource identifier.
	 * @return resource
	 */
	Object getId(); 

}
