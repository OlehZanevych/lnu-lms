package org.lnu.resource;

import java.util.List;

import org.lnu.model.enumtype.capability.Capability;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Abstract class, that all security API resources need to implement.
 * @author OlehZanevych
 * 
 * @param <KEY> Identifier class, that extends Number.
 */
public abstract class SecurityAPIResource<KEY extends Number> extends ApiResourceWithSimpleId<KEY> {
	
	protected Boolean isPrivate;
	
	protected Capability capability;
	
	protected List<Long> responsibleOwnUserIds;
	
	protected List<Long> responsibleGroupIds;
	
	protected List<Long> responsibleUserIds;
	
	/**
	 * Default constructor with no parameters.
	 */
	protected SecurityAPIResource() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	protected SecurityAPIResource(final KEY id) {
		super(id);
	}
	
	/**
	 * Getting required capability level for modification.
	 * @return capability level
	 */
	@JsonIgnore
	public Capability getRequiredCapability() {
		if (isPrivate != null || responsibleOwnUserIds != null || responsibleGroupIds != null) {
			return Capability.MODIFY_ALL;
		}
		return Capability.MODIFY_CONTENT;
	}
	
	public Boolean getIsPrivate() {
		return isPrivate;
	}

	public void setIsPrivate(final Boolean isPrivate) {
		this.isPrivate = isPrivate;
	}

	public Capability getCapability() {
		return capability;
	}

	public void setCapability(final Capability capability) {
		this.capability = capability;
	}

	public List<Long> getResponsibleOwnUserIds() {
		return responsibleOwnUserIds;
	}

	public void setResponsibleOwnUserIds(final List<Long> responsibleOwnUserIds) {
		this.responsibleOwnUserIds = responsibleOwnUserIds;
	}

	public List<Long> getResponsibleGroupIds() {
		return responsibleGroupIds;
	}

	public void setResponsibleGroupIds(final List<Long> responsibleGroupIds) {
		this.responsibleGroupIds = responsibleGroupIds;
	}

	public List<Long> getResponsibleUserIds() {
		return responsibleUserIds;
	}

	public void setResponsibleUserIds(final List<Long> responsibleUserIds) {
		this.responsibleUserIds = responsibleUserIds;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((capability == null) ? 0 : capability.hashCode());
		result = prime * result + ((isPrivate == null) ? 0 : isPrivate.hashCode());
		result = prime * result + ((responsibleGroupIds == null) ? 0 : responsibleGroupIds.hashCode());
		result = prime * result + ((responsibleOwnUserIds == null) ? 0 : responsibleOwnUserIds.hashCode());
		result = prime * result + ((responsibleUserIds == null) ? 0 : responsibleUserIds.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SecurityAPIResource<?> other = (SecurityAPIResource<?>) obj;
		if (capability != other.capability) {
			return false;
		}
		if (isPrivate == null) {
			if (other.isPrivate != null) {
				return false;
			}
		} else if (!isPrivate.equals(other.isPrivate)) {
			return false;
		}
		if (responsibleGroupIds == null) {
			if (other.responsibleGroupIds != null) {
				return false;
			}
		} else if (!responsibleGroupIds.equals(other.responsibleGroupIds)) {
			return false;
		}
		if (responsibleOwnUserIds == null) {
			if (other.responsibleOwnUserIds != null) {
				return false;
			}
		} else if (!responsibleOwnUserIds.equals(other.responsibleOwnUserIds)) {
			return false;
		}
		if (responsibleUserIds == null) {
			if (other.responsibleUserIds != null) {
				return false;
			}
		} else if (!responsibleUserIds.equals(other.responsibleUserIds)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SecurityAPIResource [isPrivate=");
		builder.append(isPrivate);
		builder.append(", capability=");
		builder.append(capability);
		builder.append(", responsibleOwnUserIds=");
		builder.append(responsibleOwnUserIds);
		builder.append(", responsibleGroupIds=");
		builder.append(responsibleGroupIds);
		builder.append(", responsibleUserIds=");
		builder.append(responsibleUserIds);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
