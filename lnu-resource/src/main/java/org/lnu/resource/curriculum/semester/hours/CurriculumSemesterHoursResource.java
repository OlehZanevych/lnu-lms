package org.lnu.resource.curriculum.semester.hours;

import org.lnu.annotation.CrudableResource;
import org.lnu.model.enumtype.classtype.ClassType;
import org.lnu.resource.ResourceWithSimpleId;

/**
 * Resource for web layer, that describes CurriculumSemesterHours.
 * @author OlehZanevych
 */
@CrudableResource
public class CurriculumSemesterHoursResource extends ResourceWithSimpleId<Long> {
	
	private ClassType classType;
	
	private Short hours;
	
	private Boolean isAcademicGroupDepartment;
	
	/**
	 * Default constructor with no parameters.
	 */
	public CurriculumSemesterHoursResource() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public CurriculumSemesterHoursResource(final Long id) {
		super(id);
	}

	public ClassType getClassType() {
		return classType;
	}

	public void setClassType(final ClassType classType) {
		this.classType = classType;
	}

	public Short getHours() {
		return hours;
	}

	public void setHours(final Short hours) {
		this.hours = hours;
	}

	public Boolean getIsAcademicGroupDepartment() {
		return isAcademicGroupDepartment;
	}

	public void setIsAcademicGroupDepartment(final Boolean isAcademicGroupDepartment) {
		this.isAcademicGroupDepartment = isAcademicGroupDepartment;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((classType == null) ? 0 : classType.hashCode());
		result = prime * result + ((hours == null) ? 0 : hours.hashCode());
		result = prime * result + ((isAcademicGroupDepartment == null) ? 0 : isAcademicGroupDepartment.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CurriculumSemesterHoursResource other = (CurriculumSemesterHoursResource) obj;
		if (classType != other.classType) {
			return false;
		}
		if (hours == null) {
			if (other.hours != null) {
				return false;
			}
		} else if (!hours.equals(other.hours)) {
			return false;
		}
		if (isAcademicGroupDepartment == null) {
			if (other.isAcademicGroupDepartment != null) {
				return false;
			}
		} else if (!isAcademicGroupDepartment.equals(other.isAcademicGroupDepartment)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CurriculumSemesterHoursResource [classType=");
		builder.append(classType);
		builder.append(", hours=");
		builder.append(hours);
		builder.append(", isAcademicGroupDepartment=");
		builder.append(isAcademicGroupDepartment);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
