package org.lnu.resource.curriculum.semester;

import java.util.List;

import org.lnu.annotation.CrudableResource;
import org.lnu.model.enumtype.report.Report;
import org.lnu.resource.ResourceWithSimpleId;
import org.lnu.resource.curriculum.semester.hours.CurriculumSemesterHoursResource;

/**
 * Resource for web layer, that describes CurriculumSemester.
 * @author OlehZanevych
 */
@CrudableResource
public class CurriculumSemesterResource extends ResourceWithSimpleId<Long> {
	
	private Byte semester;
	
	private Byte year;
	
	private Byte half;
	
	private Float credits;
	
	private Short allHours;
	
	private Short independentStudy;
	
	private Report report;
	
	private List<CurriculumSemesterHoursResource> curriculumSemesterHours;

	/**
	 * Default constructor with no parameters.
	 */
	public CurriculumSemesterResource() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public CurriculumSemesterResource(final Long id) {
		super(id);
	}

	public Byte getSemester() {
		return semester;
	}

	public void setSemester(final Byte semester) {
		this.semester = semester;
	}

	public Byte getYear() {
		return year;
	}

	public void setYear(final Byte year) {
		this.year = year;
	}

	public Byte getHalf() {
		return half;
	}

	public void setHalf(final Byte half) {
		this.half = half;
	}

	public Float getCredits() {
		return credits;
	}

	public void setCredits(final Float credits) {
		this.credits = credits;
	}

	public Short getAllHours() {
		return allHours;
	}

	public void setAllHours(final Short allHours) {
		this.allHours = allHours;
	}

	public Short getIndependentStudy() {
		return independentStudy;
	}

	public void setIndependentStudy(final Short independentStudy) {
		this.independentStudy = independentStudy;
	}

	public Report getReport() {
		return report;
	}

	public void setReport(final Report report) {
		this.report = report;
	}

	public List<CurriculumSemesterHoursResource> getCurriculumSemesterHours() {
		return curriculumSemesterHours;
	}

	public void setCurriculumSemesterHours(final List<CurriculumSemesterHoursResource> curriculumSemesterHours) {
		this.curriculumSemesterHours = curriculumSemesterHours;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((allHours == null) ? 0 : allHours.hashCode());
		result = prime * result + ((credits == null) ? 0 : credits.hashCode());
		result = prime * result + ((curriculumSemesterHours == null) ? 0 : curriculumSemesterHours.hashCode());
		result = prime * result + ((half == null) ? 0 : half.hashCode());
		result = prime * result + ((independentStudy == null) ? 0 : independentStudy.hashCode());
		result = prime * result + ((report == null) ? 0 : report.hashCode());
		result = prime * result + ((semester == null) ? 0 : semester.hashCode());
		result = prime * result + ((year == null) ? 0 : year.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CurriculumSemesterResource other = (CurriculumSemesterResource) obj;
		if (allHours == null) {
			if (other.allHours != null) {
				return false;
			}
		} else if (!allHours.equals(other.allHours)) {
			return false;
		}
		if (credits == null) {
			if (other.credits != null) {
				return false;
			}
		} else if (!credits.equals(other.credits)) {
			return false;
		}
		if (curriculumSemesterHours == null) {
			if (other.curriculumSemesterHours != null) {
				return false;
			}
		} else if (!curriculumSemesterHours.equals(other.curriculumSemesterHours)) {
			return false;
		}
		if (half == null) {
			if (other.half != null) {
				return false;
			}
		} else if (!half.equals(other.half)) {
			return false;
		}
		if (independentStudy == null) {
			if (other.independentStudy != null) {
				return false;
			}
		} else if (!independentStudy.equals(other.independentStudy)) {
			return false;
		}
		if (report != other.report) {
			return false;
		}
		if (semester == null) {
			if (other.semester != null) {
				return false;
			}
		} else if (!semester.equals(other.semester)) {
			return false;
		}
		if (year == null) {
			if (other.year != null) {
				return false;
			}
		} else if (!year.equals(other.year)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CurriculumSemesterResource [semester=");
		builder.append(semester);
		builder.append(", year=");
		builder.append(year);
		builder.append(", half=");
		builder.append(half);
		builder.append(", credits=");
		builder.append(credits);
		builder.append(", allHours=");
		builder.append(allHours);
		builder.append(", independentStudy=");
		builder.append(independentStudy);
		builder.append(", report=");
		builder.append(report);
		builder.append(", curriculumSemesterHours=");
		builder.append(curriculumSemesterHours);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
