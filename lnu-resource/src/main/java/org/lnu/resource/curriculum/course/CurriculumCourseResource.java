package org.lnu.resource.curriculum.course;

import org.lnu.annotation.CrudableResource;
import org.lnu.resource.ResourceWithSimpleId;

/**
 * Resource for web layer, that describes CurriculumCourser.
 * @author OlehZanevych
 */
@CrudableResource
public class CurriculumCourseResource extends ResourceWithSimpleId<Long> {
	
	private Long courseId;
	
	private Long departmentId;

	/**
	 * Default constructor with no parameters.
	 */
	public CurriculumCourseResource() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public CurriculumCourseResource(final Long id) {
		super(id);
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(final Long courseId) {
		this.courseId = courseId;
	}

	public Long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(final Long departmentId) {
		this.departmentId = departmentId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((courseId == null) ? 0 : courseId.hashCode());
		result = prime * result + ((departmentId == null) ? 0 : departmentId.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CurriculumCourseResource other = (CurriculumCourseResource) obj;
		if (courseId == null) {
			if (other.courseId != null) {
				return false;
			}
		} else if (!courseId.equals(other.courseId)) {
			return false;
		}
		if (departmentId == null) {
			if (other.departmentId != null) {
				return false;
			}
		} else if (!departmentId.equals(other.departmentId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CurriculumCourseResource [courseId=");
		builder.append(courseId);
		builder.append(", departmentId=");
		builder.append(departmentId);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
