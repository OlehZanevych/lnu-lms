package org.lnu.resource.curriculum;

import java.text.MessageFormat;
import java.util.List;

import org.lnu.annotation.CrudableResource;
import org.lnu.model.enumtype.api.API;
import org.lnu.model.enumtype.coursecycle.CourseCycle;
import org.lnu.model.enumtype.coursetype.CourseType;
import org.lnu.resource.SecurityAPIResource;
import org.lnu.resource.curriculum.course.CurriculumCourseResource;
import org.lnu.resource.curriculum.semester.CurriculumSemesterResource;

/**
 * Resource for web layer, that describes Curriculum.
 * @author OlehZanevych
 */
@CrudableResource
public class CurriculumResource extends SecurityAPIResource<Long> {
	
	private List<CurriculumCourseResource> curriculumCourses;
	
	private CourseType courseType;
	
	private CourseCycle courseCycle;
	
	private List<Long> specialtyIds;
	
	private List<CurriculumSemesterResource> curriculumSemesters;
	
	/**
	 * Default constructor with no parameters.
	 */
	public CurriculumResource() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public CurriculumResource(final Long id) {
		super(id);
	}

	@Override
	public String getRootUri() {
		return MessageFormat.format("/{0}", API.curricula);
	}

	public List<CurriculumCourseResource> getCurriculumCourses() {
		return curriculumCourses;
	}

	public void setCurriculumCourses(final List<CurriculumCourseResource> curriculumCourses) {
		this.curriculumCourses = curriculumCourses;
	}

	public CourseType getCourseType() {
		return courseType;
	}

	public void setCourseType(final CourseType courseType) {
		this.courseType = courseType;
	}

	public CourseCycle getCourseCycle() {
		return courseCycle;
	}

	public void setCourseCycle(final CourseCycle courseCycle) {
		this.courseCycle = courseCycle;
	}

	public List<Long> getSpecialtyIds() {
		return specialtyIds;
	}

	public void setSpecialtyIds(final List<Long> specialtyIds) {
		this.specialtyIds = specialtyIds;
	}

	public List<CurriculumSemesterResource> getCurriculumSemesters() {
		return curriculumSemesters;
	}

	public void setCurriculumSemesters(final List<CurriculumSemesterResource> curriculumSemesters) {
		this.curriculumSemesters = curriculumSemesters;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((courseCycle == null) ? 0 : courseCycle.hashCode());
		result = prime * result + ((courseType == null) ? 0 : courseType.hashCode());
		result = prime * result + ((curriculumCourses == null) ? 0 : curriculumCourses.hashCode());
		result = prime * result + ((curriculumSemesters == null) ? 0 : curriculumSemesters.hashCode());
		result = prime * result + ((specialtyIds == null) ? 0 : specialtyIds.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CurriculumResource other = (CurriculumResource) obj;
		if (courseCycle != other.courseCycle) {
			return false;
		}
		if (courseType != other.courseType) {
			return false;
		}
		if (curriculumCourses == null) {
			if (other.curriculumCourses != null) {
				return false;
			}
		} else if (!curriculumCourses.equals(other.curriculumCourses)) {
			return false;
		}
		if (curriculumSemesters == null) {
			if (other.curriculumSemesters != null) {
				return false;
			}
		} else if (!curriculumSemesters.equals(other.curriculumSemesters)) {
			return false;
		}
		if (specialtyIds == null) {
			if (other.specialtyIds != null) {
				return false;
			}
		} else if (!specialtyIds.equals(other.specialtyIds)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CurriculumResource [curriculumCourses=");
		builder.append(curriculumCourses);
		builder.append(", courseType=");
		builder.append(courseType);
		builder.append(", courseCycle=");
		builder.append(courseCycle);
		builder.append(", specialtyIds=");
		builder.append(specialtyIds);
		builder.append(", curriculumSemesters=");
		builder.append(curriculumSemesters);
		builder.append(", isPrivate=");
		builder.append(isPrivate);
		builder.append(", capability=");
		builder.append(capability);
		builder.append(", responsibleOwnUserIds=");
		builder.append(responsibleOwnUserIds);
		builder.append(", responsibleGroupIds=");
		builder.append(responsibleGroupIds);
		builder.append(", responsibleUserIds=");
		builder.append(responsibleUserIds);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
