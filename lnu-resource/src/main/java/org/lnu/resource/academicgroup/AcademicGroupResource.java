package org.lnu.resource.academicgroup;

import java.text.MessageFormat;

import org.lnu.annotation.CrudableResource;
import org.lnu.model.enumtype.api.API;
import org.lnu.resource.SecurityAPIResource;

/**
 * Resource for web layer, that describes AcademicGroup.
 * @author OlehZanevych
 */
@CrudableResource
public class AcademicGroupResource extends SecurityAPIResource<Long> {
	
	private String name;
	
	private Long departmentId;
	
	private Long specialtyId;
	
	private Byte year;
	
	private Long mentorId;
	
	private Long leaderId;
	
	/**
	 * Default constructor with no parameters.
	 */
	public AcademicGroupResource() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public AcademicGroupResource(final Long id) {
		super(id);
	}

	@Override
	public String getRootUri() {
		return MessageFormat.format("/{0}", API.academicgroups);
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(final Long departmentId) {
		this.departmentId = departmentId;
	}

	public Long getSpecialtyId() {
		return specialtyId;
	}

	public void setSpecialtyId(final Long specialtyId) {
		this.specialtyId = specialtyId;
	}

	public Byte getYear() {
		return year;
	}

	public void setYear(final Byte year) {
		this.year = year;
	}

	public Long getMentorId() {
		return mentorId;
	}

	public void setMentorId(final Long mentorId) {
		this.mentorId = mentorId;
	}

	public Long getLeaderId() {
		return leaderId;
	}

	public void setLeaderId(final Long leaderId) {
		this.leaderId = leaderId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((departmentId == null) ? 0 : departmentId.hashCode());
		result = prime * result + ((leaderId == null) ? 0 : leaderId.hashCode());
		result = prime * result + ((mentorId == null) ? 0 : mentorId.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((specialtyId == null) ? 0 : specialtyId.hashCode());
		result = prime * result + ((year == null) ? 0 : year.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AcademicGroupResource other = (AcademicGroupResource) obj;
		if (departmentId == null) {
			if (other.departmentId != null) {
				return false;
			}
		} else if (!departmentId.equals(other.departmentId)) {
			return false;
		}
		if (leaderId == null) {
			if (other.leaderId != null) {
				return false;
			}
		} else if (!leaderId.equals(other.leaderId)) {
			return false;
		}
		if (mentorId == null) {
			if (other.mentorId != null) {
				return false;
			}
		} else if (!mentorId.equals(other.mentorId)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (specialtyId == null) {
			if (other.specialtyId != null) {
				return false;
			}
		} else if (!specialtyId.equals(other.specialtyId)) {
			return false;
		}
		if (year == null) {
			if (other.year != null) {
				return false;
			}
		} else if (!year.equals(other.year)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AcademicGroupResource [name=");
		builder.append(name);
		builder.append(", departmentId=");
		builder.append(departmentId);
		builder.append(", specialtyId=");
		builder.append(specialtyId);
		builder.append(", year=");
		builder.append(year);
		builder.append(", mentorId=");
		builder.append(mentorId);
		builder.append(", leaderId=");
		builder.append(leaderId);
		builder.append(", isPrivate=");
		builder.append(isPrivate);
		builder.append(", capability=");
		builder.append(capability);
		builder.append(", responsibleOwnUserIds=");
		builder.append(responsibleOwnUserIds);
		builder.append(", responsibleGroupIds=");
		builder.append(responsibleGroupIds);
		builder.append(", responsibleUserIds=");
		builder.append(responsibleUserIds);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
