package org.lnu.facade.facade;

/**
 * Common interface for all facade methods.
 * @author OlehZanevych
 *
 * @param <RESOURCE> Resource class.
 * @param <KEY> Identifier class.
 */
public interface Facade<RESOURCE, KEY> extends EditFacade<RESOURCE, KEY> {
	
	/**
	 * Method for creating entity.
	 * @param resource
	 * @return Entity resource.
	 */
	RESOURCE createResource(RESOURCE resource);

	/**
	 * Method for removing entity.
	 * @param id id
	 */
	void removeResource(KEY id);
	
}
