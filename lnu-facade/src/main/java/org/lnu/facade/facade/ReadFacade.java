package org.lnu.facade.facade;

import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.PagedResultResource;
import org.lnu.resource.request.Request;

/**
 * Common interface for Get facade methods.
 * @author OlehZanevych
 *
 * @param <RESOURCE> Resource class.
 * @param <KEY> Identifier class.
 */
public interface ReadFacade<RESOURCE, KEY> {
	
	/**
	 * Method for getting entity.
	 * @param id id
	 * @param request Request
	 * @return Entity resource.
	 */
	RESOURCE getResource(final KEY id, Request request);
	
	/**
	 * Method for getting paged result for entities.
	 * @param request PagedRequest
	 * @return Paged Result.
	 */
	PagedResultResource<RESOURCE> getResources(PagedRequest request);

}
