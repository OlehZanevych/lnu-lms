package org.lnu.facade.facade;

import java.util.List;

import org.lnu.converter.AbstractConverter;
import org.lnu.dao.dao.ReadDao;
import org.lnu.model.Model;
import org.lnu.model.pagination.PagedResult;
import org.lnu.resource.Resource;
import org.lnu.resource.request.PagedRequest;
import org.lnu.resource.request.PagedResultResource;
import org.lnu.resource.request.Request;
import org.lnu.service.session.SessionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

/**
 * Default implementation of Get facade methods.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity class.
 * @param <RESOURCE> Resource class.
 * @param <DAO> DAO.
 * @param <ENTITYCONVERTER> Entity converter class.
 * @param <KEY> Identifier class.
 */
@Transactional
public class DefaultReadFacade<ENTITY extends Model, RESOURCE extends Resource,
		DAO extends ReadDao<ENTITY, KEY>, ENTITYCONVERTER extends AbstractConverter<ENTITY, RESOURCE>, KEY>
		implements ReadFacade<RESOURCE, KEY> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultReadFacade.class);
	
	protected DAO dao;
	
	protected ENTITYCONVERTER entityConverter;
	
	@javax.annotation.Resource(name = "defaultSessionService")
	protected SessionService sessionService;
	
	@Override
	public RESOURCE getResource(final KEY id, final Request request) {
		
		LOGGER.info("Getting resource with id: {}", id);
		
		return entityConverter.convert(dao.getEntityById(id, request, sessionService.getUserId()));
	}
	
	@Override
	public PagedResultResource<RESOURCE> getResources(final PagedRequest request) {
		
		LOGGER.info("Getting paged result resource");
		
		PagedResult<ENTITY> pagedResult = dao.getEntities(request, sessionService.getUserId());

		List<RESOURCE> resources = entityConverter.convertAll(pagedResult.getEntities());

		return new PagedResultResource<>(pagedResult.getOffset(), pagedResult.getLimit(), pagedResult.getCount(), resources);
	}

	public DAO getDao() {
		return dao;
	}

	@Required
	public void setDao(final DAO dao) {
		this.dao = dao;
	}

	public ENTITYCONVERTER getEntityConverter() {
		return entityConverter;
	}

	@Required
	public void setEntityConverter(final ENTITYCONVERTER entityConverter) {
		this.entityConverter = entityConverter;
	}

}
