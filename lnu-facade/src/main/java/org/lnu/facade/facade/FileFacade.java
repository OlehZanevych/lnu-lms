package org.lnu.facade.facade;

import org.springframework.web.multipart.MultipartFile;

/**
 * Common interface for all file facade methods.
 * @author OlehZanevych
 *
 * @param <RESOURCE> Resource class.
 */
public interface FileFacade<RESOURCE> extends Facade<RESOURCE, Long> {
	
	/**
	 * Creating entity and file.
	 * @param resource entity resource
	 * @param file multipart file
	 * @return created entity resource.
	 */
	RESOURCE createResource(RESOURCE resource, MultipartFile file);
	
	/**
	 * Updating file.
	 * @param id identifier
	 * @param file multipart file
	 */
	void updateFile(Long id, MultipartFile file);
	
	/**
	 * Removing file.
	 * @param id identifier
	 */
	void removeFile(Long id);
}
