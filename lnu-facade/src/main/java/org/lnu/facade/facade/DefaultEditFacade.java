package org.lnu.facade.facade;

import org.lnu.converter.SecurityConverter;
import org.lnu.converter.SecurityResourceConverter;
import org.lnu.dao.dao.EditDao;
import org.lnu.model.APIModel;
import org.lnu.model.enumtype.api.API;
import org.lnu.resource.SecurityAPIResource;
import org.lnu.security.exception.AccessDeniedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

/**
 * Default implementation of Get and Update facade methods.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity class.
 * @param <RESOURCE> Resource class.
 * @param <DAO> DAO.
 * @param <ENTITYCONVERTER> Entity converter class.
 * @param <RESOURCECONVERTER> Resource converter class.
 * @param <KEY> Identifier class.
 */
@Transactional
public class DefaultEditFacade<ENTITY extends APIModel<KEY>, RESOURCE extends SecurityAPIResource<KEY>,
		DAO extends EditDao<ENTITY, KEY>, ENTITYCONVERTER extends SecurityConverter<ENTITY, RESOURCE, KEY>,
		RESOURCECONVERTER extends SecurityResourceConverter<RESOURCE, ENTITY, KEY>, KEY extends Number>
		extends DefaultReadFacade<ENTITY, RESOURCE, DAO, ENTITYCONVERTER, KEY>
		implements EditFacade<RESOURCE, KEY> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultEditFacade.class);
	
	protected API api;
	
	protected RESOURCECONVERTER resourceConverter;
	
	@Override
	public void updateResource(final KEY id, final RESOURCE resource) {
		LOGGER.info("Updating resource: {0} with id: {1}", resource, id);
		
		Long userId = sessionService.getUserId();
		
		if (dao.getCapability(id, userId).compareTo(resource.getRequiredCapability()) < 0) {
			throw new AccessDeniedException("Access is denied to current user");
		}
		
		ENTITY entity = dao.getPersistentEntityById(id, userId);
		
		resourceConverter.convert(resource, entity);
		
		dao.updateEntity(entity, userId);
	}

	public API getApi() {
		return api;
	}

	public void setApi(final API api) {
		this.api = api;
	}

	public RESOURCECONVERTER getResourceConverter() {
		return resourceConverter;
	}

	@Required
	public void setResourceConverter(final RESOURCECONVERTER resourceConverter) {
		this.resourceConverter = resourceConverter;
	}

}
