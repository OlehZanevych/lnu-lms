package org.lnu.facade.facade;

import java.nio.file.Path;
import java.nio.file.Paths;

import javax.annotation.Resource;

import org.lnu.converter.SecurityConverter;
import org.lnu.converter.SecurityResourceConverter;
import org.lnu.dao.dao.Dao;
import org.lnu.model.file.FileModel;
import org.lnu.resource.FileAPIResource;
import org.lnu.service.storage.StorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 * Default implementation of all file facade methods.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity class.
 * @param <RESOURCE> Resource class.
 * @param <DAO> DAO.
 * @param <ENTITYCONVERTER> Entity converter class.
 * @param <RESOURCECONVERTER> Resource converter class.
 */
@Transactional
public class DefaultFileFacade<ENTITY extends FileModel, RESOURCE extends FileAPIResource,
		DAO extends Dao<ENTITY, Long>, ENTITYCONVERTER extends SecurityConverter<ENTITY, RESOURCE, Long>,
		RESOURCECONVERTER extends SecurityResourceConverter<RESOURCE, ENTITY, Long>>
		extends DefaultFacade<ENTITY, RESOURCE, DAO, ENTITYCONVERTER, RESOURCECONVERTER, Long>
		implements FileFacade<RESOURCE> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultFileFacade.class);
	
	private String uploadFolder;
	
	@Value("${root.folder}")
    private String rootFolder;
	
	@Resource(name = "fileSystemStorageService")
	private StorageService storageService;

	@Override
	public RESOURCE createResource(final RESOURCE resource, final MultipartFile file) {
		LOGGER.info("Creating resource: {} and file", resource);
		
		ENTITY entity = resourceConverter.convert(resource);

		dao.createEntity(entity, sessionService.getUserId());
		
		updateFile(entity.getId(), file);

		return entityConverter.convert(entity);
	}
	
	
	@Override
	public void removeResource(final Long id) {
		LOGGER.info("Removing resource with id: {}", id);
		
		super.removeResource(id);
		
		removeFile(id);
	}
	
	/**
	 * Updating file.
	 * @param entity entity
	 * @param file multipart file
	 */
	protected void updateFile(final ENTITY entity, final MultipartFile file) {
		LOGGER.info("Updating file for entity: {}", entity);
		
		Long userId = sessionService.getUserId();
		
		Path folder = Paths.get(rootFolder + uploadFolder);
		
		String storedFileName = storageService.store(file, folder, entity.getId());
		
		entity.setFileURI(uploadFolder + storedFileName);
		
		dao.updateEntity(entity, userId);
	}
	
	@Override
	public void updateFile(final Long id, final MultipartFile file) {
		LOGGER.info("Updating file for resource with id: {}", id);
		
		Long userId = sessionService.getUserId();
		
		ENTITY entity = dao.getPersistentEntityById(id, userId);
		
		updateFile(entity, file);
	}
	
	@Override
	public void removeFile(final Long id) {
		LOGGER.info("Removing file for resource with id: {}", id);
		
		Path folder = Paths.get(rootFolder + uploadFolder);
		
		storageService.delete(folder, id);
	}

	public String getUploadFolder() {
		return uploadFolder;
	}

	@Required
	public void setUploadFolder(final String uploadFolder) {
		this.uploadFolder = uploadFolder;
	}
	
}
