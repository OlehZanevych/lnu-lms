package org.lnu.facade.facade;

import org.lnu.converter.SecurityConverter;
import org.lnu.converter.SecurityResourceConverter;
import org.lnu.dao.dao.Dao;
import org.lnu.model.APIModel;
import org.lnu.model.enumtype.capability.Capability;
import org.lnu.resource.SecurityAPIResource;
import org.lnu.security.exception.AccessDeniedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 * Default implementation of all facade methods.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity class.
 * @param <RESOURCE> Resource class.
 * @param <DAO> DAO.
 * @param <ENTITYCONVERTER> Entity converter class.
 * @param <RESOURCECONVERTER> Resource converter class.
 * @param <KEY> Identifier class.
 */
@Transactional
public class DefaultFacade<ENTITY extends APIModel<KEY>, RESOURCE extends SecurityAPIResource<KEY>,
		DAO extends Dao<ENTITY, KEY>, ENTITYCONVERTER extends SecurityConverter<ENTITY, RESOURCE, KEY>,
		RESOURCECONVERTER extends SecurityResourceConverter<RESOURCE, ENTITY, KEY>, KEY extends Number>
		extends DefaultEditFacade<ENTITY, RESOURCE, DAO, ENTITYCONVERTER, RESOURCECONVERTER, KEY>
		implements Facade<RESOURCE, KEY> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultFacade.class);

	@Override
	public RESOURCE createResource(final RESOURCE resource) {
		LOGGER.info("Creating resource: {}", resource);
		
		ENTITY entity = resourceConverter.convert(resource);

		dao.createEntity(entity, sessionService.getUserId());

		return entityConverter.convert(entity);
	}

	@Override
	public void removeResource(final KEY id) {
		LOGGER.info("Removing resource with id: {}", id);
		
		Long userId = sessionService.getUserId();
		
		if (dao.getCapability(id, userId).compareTo(Capability.MODIFY_CONTENT) < 0) {
			throw new AccessDeniedException("Access is denied to current user");
		}
		
		dao.removeEntityById(id, userId);
	}
	
}
