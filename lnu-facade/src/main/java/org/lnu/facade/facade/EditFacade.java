package org.lnu.facade.facade;

/**
 * Common interface for Get and Update facade methods.
 * @author OlehZanevych
 *
 * @param <RESOURCE> Resource class.
 * @param <KEY> Identifier class.
 */
public interface EditFacade<RESOURCE, KEY> extends ReadFacade<RESOURCE, KEY> {
	
	/**
	 * Method for updating entity.
	 * @param id id
	 * @param resource resource
	 */
	void updateResource(final KEY id, RESOURCE resource);

}
