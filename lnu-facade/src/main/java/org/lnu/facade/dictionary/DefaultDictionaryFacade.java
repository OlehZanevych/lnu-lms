package org.lnu.facade.dictionary;

import java.util.List;
import java.util.stream.Collectors;

import org.lnu.resource.dictionary.DictionaryEntry;
import org.lnu.service.dictionary.DictionaryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

/**
 * Default dictionary facade implementation.
 * @author OlehZanevych
 *
 * @param <KEY> Enum type of dictionary key
 */
public class DefaultDictionaryFacade<KEY extends Enum<?>> implements DictionaryFacade<KEY> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultDictionaryFacade.class);

	private DictionaryService<KEY> dictionaryService;
	
	@Override
	public DictionaryEntry<KEY> getEntry(final KEY key) {
		LOGGER.info("Getting entry from dictionary by key '{0}'", key);
		return new DictionaryEntry<KEY>(key, dictionaryService.getValue(key));
	}

	@Override
	public List<DictionaryEntry<KEY>> getDictionary() {
		LOGGER.info("Getting all dictionary");
		return dictionaryService.getDictionary().entrySet().stream()
				.map(e -> new DictionaryEntry<KEY>(e.getKey(), e.getValue())).collect(Collectors.toList());
	}
	
	@Required
	public void setDictionaryService(final DictionaryService<KEY> dictionaryService) {
		this.dictionaryService = dictionaryService;
	}

	public DictionaryService<KEY> getDictionaryService() {
		return dictionaryService;
	}

}
