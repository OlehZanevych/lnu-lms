package org.lnu.facade.dictionary;

import java.util.List;

import org.lnu.resource.dictionary.DictionaryEntry;

/**
 * Interface for dictionary facades.
 * @author OlehZanevych
 *
 * @param <KEY> Enum type of dictionary key
 */
public interface DictionaryFacade<KEY extends Enum<?>> {

	/**
	 * Method for getting entry from dictionary by key.
	 * 
	 * @param key key in dictionary
	 * @return entry for key
	 */
	DictionaryEntry<KEY> getEntry(KEY key);
	
	/**
	 * Method for getting all dictionary.
	 * 
	 * @return dictionary
	 */
	List<DictionaryEntry<KEY>> getDictionary();

}
