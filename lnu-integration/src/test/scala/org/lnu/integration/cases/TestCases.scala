package org.lnu.integration.cases

import io.gatling.core.Predef.scenario

import org.lnu.integration.cases.get.GETIntegrationTest
import org.lnu.integration.cases.get.dictionary.DictionaryIntegrationTest

import org.lnu.integration.cases.academicgroup.AcademicGroupIntegrationTest
import org.lnu.integration.cases.building.BuildingIntegrationTest
import org.lnu.integration.cases.building.map.BuildingMapIntegrationTest
import org.lnu.integration.cases.course.CourseIntegrationTest
import org.lnu.integration.cases.curriculum.CurriculumIntegrationTest
import org.lnu.integration.cases.department.DepartmentIntegrationTest
import org.lnu.integration.cases.dormitory.DormitoryIntegrationTest
import org.lnu.integration.cases.group.GroupIntegrationTest
import org.lnu.integration.cases.lecturer.LecturerIntegrationTest
import org.lnu.integration.cases.learningplace.LearningPlaceIntegrationTest
import org.lnu.integration.cases.otheruniversityplace.OtherUniversityPlaceIntegrationTest
import org.lnu.integration.cases.role.RoleIntegrationTest
import org.lnu.integration.cases.scientificdegree.ScientificDegreeIntegrationTest
import org.lnu.integration.cases.specialty.SpecialtyIntegrationTest
import org.lnu.integration.cases.student.StudentIntegrationTest
import org.lnu.integration.cases.toilet.ToiletIntegrationTest
import org.lnu.integration.cases.unit.UnitIntegrationTest
import org.lnu.integration.cases.user.UserIntegrationTest

/**
 * All test cases, that are in system.
 * @author OlehZanevych
 */
object TestCases {

    val scn = scenario("Integration test scenario")
              .exec(
                  GETIntegrationTest.testCase,
                  DictionaryIntegrationTest.testCase,
                  
                  AcademicGroupIntegrationTest.testCase,
                  BuildingIntegrationTest.testCase,
                  BuildingMapIntegrationTest.testCase,
                  CourseIntegrationTest.testCase,
                  CurriculumIntegrationTest.testCase,
                  DepartmentIntegrationTest.testCase,
                  DormitoryIntegrationTest.testCase,
                  GroupIntegrationTest.testCase,
                  LearningPlaceIntegrationTest.testCase,
                  LecturerIntegrationTest.testCase,
                  OtherUniversityPlaceIntegrationTest.testCase,
                  RoleIntegrationTest.testCase,
                  ScientificDegreeIntegrationTest.testCase,
                  SpecialtyIntegrationTest.testCase,
                  StudentIntegrationTest.testCase,
                  ToiletIntegrationTest.testCase,
                  UnitIntegrationTest.testCase,
                  UserIntegrationTest.testCase
              )
  
}
