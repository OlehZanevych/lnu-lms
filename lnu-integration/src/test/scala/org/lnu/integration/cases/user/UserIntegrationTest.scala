package org.lnu.integration.cases.user

import org.lnu.integration.BaseCRUDIntegrationTest
import io.gatling.core.Predef.exec

/**
 * Integration test of user API.
 * @author OlehZanevych
 */
object UserIntegrationTest extends BaseCRUDIntegrationTest {

  val testCase =
    exec(post(
      "users", "user1",
      "user/data3.json"))
    .exec(post(
      "users", "user2",
      "user/data4.json"))
    .exec(post(
      "users", "user3",
      "user/data5.json"))
    .exec(post(
      "groups", "group1",
      "group/data3.json"))
    .exec(post(
      "groups", "group2",
      "group/data4.json"))
    .exec(post(
      "groups", "group3",
      "group/data5.json"))
    .exec(post(
      "roles", "role1",
      "role/data3.json"))
    .exec(post(
      "roles", "role2",
      "role/data4.json"))
    .exec(post(
      "roles", "role3",
      "role/data5.json"))
    .exec(crud(
      "users", "user",
      "user/data1.json", "user/data2.json"))
    .exec(delete("role1"))
    .exec(delete("role2"))
    .exec(delete("role3"))
    .exec(delete("group1"))
    .exec(delete("group2"))
    .exec(delete("group3"))
    .exec(delete("user1"))
    .exec(delete("user2"))
    .exec(delete("user3"))

}