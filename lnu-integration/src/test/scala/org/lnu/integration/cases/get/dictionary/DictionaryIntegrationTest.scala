package org.lnu.integration.cases.get.dictionary

import io.gatling.core.Predef.checkBuilder2Check
import io.gatling.core.Predef.exec
import io.gatling.core.Predef.findCheckBuilder2ValidatorCheckBuilder
import io.gatling.core.Predef.foreach
import io.gatling.core.Predef.stringToExpression
import io.gatling.core.Predef.validatorCheckBuilder2CheckBuilder
import io.gatling.core.Predef.value2Expression
import io.gatling.core.Predef.value2Success

import io.gatling.core.structure.ChainBuilder

import io.gatling.http.Predef.http
import io.gatling.http.Predef.jsonPath
import io.gatling.http.Predef.status

import org.lnu.integration.cases.Constants

/**
 * Dictionary integration tests
 * @author OlehZanevych
 */
object DictionaryIntegrationTest {
  
  val apis = Array(
      "academicstatuses",
      "apicapabilitylevels",
      "apicreatecapabilities",
      "apis",
      "capabilities",
      "classtypes",
      "coursecycles",
      "coursetypes",
      "days",
      "educationaldegrees",
      "learningplacetypes",
      "otheruniversityplacetypes",
      "reports",
      "toilettypes",
      "unittypes",
      "weekperiodicities"
  )
  
  val testCase = test(apis)
        
  def test(apis: Array[String]): ChainBuilder = {
    var result = exec()
    apis.foreach{api => {
      val dictionaryUri = Constants.ROOT_DICTIONARY_URI + api
      result = result.exec(
        checkUri("GET " + dictionaryUri, dictionaryUri)
        .check(jsonPath("$[*]").ofType[Map[String, Any]].findAll.saveAs("dictionary"))
      )
      .foreach("${dictionary}", "dictionaryEntry") {
        exec(session => {
          val dictionaryEntry = session("dictionaryEntry").as[Map[String, Any]]
          val dictionaryEntryUri = dictionaryUri + "/" + dictionaryEntry("key")
          session.set("dictionaryEntryUri", dictionaryEntryUri)
        })
        .exec(checkUri("GET ${dictionaryEntryUri}", "${dictionaryEntryUri}"))
      }
    }}
    result
  }
  
  def checkUri(title: String, uri: String) =
    http(title)
      .get(uri)
      .check(status.is(200))
  
}