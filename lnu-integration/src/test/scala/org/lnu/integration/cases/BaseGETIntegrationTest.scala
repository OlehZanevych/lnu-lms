package org.lnu.integration.cases

import scala.util.Random

import io.gatling.core.Predef.checkBuilder2Check
import io.gatling.core.Predef.exec
import io.gatling.core.Predef.findCheckBuilder2ValidatorCheckBuilder
import io.gatling.core.Predef.stringToExpression
import io.gatling.core.Predef.validatorCheckBuilder2CheckBuilder
import io.gatling.core.Predef.value2Expression
import io.gatling.core.Predef.value2Success

import io.gatling.core.structure.ChainBuilder

import io.gatling.http.Predef.http
import io.gatling.http.Predef.jsonPath
import io.gatling.http.Predef.status

/**
 * Implementation of methods for testing GET method.
 * @author OlehZanevych
 */
abstract class BaseGETIntegrationTest {
  
  val longIdField = Array(
      "id" -> "Long"
  )
  
  val userIdField = Array(
      "id" -> "userId"
  )
  
  val securityRootFields = Array(
      "isPrivate" -> "Boolean",
      "capability" -> "capabilities"
  )
  
  val securityCollectionIdFields = Array(
      "responsibleOwnUserIds" -> "userId",
      "responsibleGroupIds" -> "Long",
      "responsibleUserIds" -> "userId"
  )
  
  val defaultParameters = Array(
      "offset" -> "1",
      "limit" -> "2147483647"
  )
  
  def test(apis: Array[(String, String, Boolean, Array[(String, String)], Array[(String, String)],
      Array[(String, String)])]): ChainBuilder = {
    var result = exec()
    apis.foreach{api => {
      val rootUri = api._1
      val rootTitle = "GET " + rootUri
      
      result = result.exec(checkUri(rootTitle, rootUri))
      
      val idType = api._2
      
      val isSecurityAPI = api._3
      
      var rootFields: Array[(String, String)] = idType match {
        case "Long" => longIdField
        case "User" => userIdField
        case other => Array()
      }
      if (isSecurityAPI) {
        rootFields ++= securityRootFields
      }
      if (api._4 != null) {
        rootFields ++= api._4
      }
      
      var collectionIdFields = api._5
      if (isSecurityAPI) {
        collectionIdFields = if (collectionIdFields != null) {
          securityCollectionIdFields ++ collectionIdFields
        } else {
          securityCollectionIdFields
        }
      }
      
      val collectionInfoFields = api._6
      
      var fields = rootFields
      if (collectionIdFields != null) {
        fields ++= collectionIdFields
      }
      if (collectionInfoFields != null) {
        fields ++= collectionInfoFields
      }
      
      var parameters = defaultParameters.map{defaultParameter =>
        (defaultParameter._1, defaultParameter._1 + "=" + defaultParameter._2)}
      parameters ++= fields.map{field => {
        val (value, newResult) = createFilterValue(field._2, result)
        result = newResult
        (field._1, field._1 + "=" + value)
      }}
      if (collectionIdFields != null) {
        parameters ++= collectionIdFields.map{collectionIdField => collectionIdField._1 + "Size"}
          .map{parameterName => (parameterName, parameterName + "=" + collectionSizeFilter)}
      }
      
      val orders = rootFields.map{field => (field._1, field._1 + orderType)}
      
      val fieldNames = fields.map{field => field._1}
      
      parameters.foreach{parameter => {
        val title = rootTitle + " with " + parameter._1 + " parameter"
        val uri = rootUri + "?" + parameter._2
        result = result.exec(checkUri(title, uri))
      }}
      
      orders.foreach{order => {
        val title = rootTitle + " ordered by " + order._1
        val uri = rootUri + "?orderBy=" + order._2
        result = result.exec(checkUri(title, uri))
      }}
      
      fieldNames.foreach{fieldName => {
        val title = rootTitle + " " + fieldName
        val uri = rootUri + "?fields=" + fieldName
        result = result.exec(checkUri(title, uri))
      }}
      
      val title = rootTitle + " with all parameters"
      val uri = rootUri + "?" + parameters.map{parameter => parameter._2}.mkString("&") +
        "&orderBy=" + orders.map{order => order._2}.mkString(",") +
        "&fields=" + fieldNames.mkString(",")
      result = result.exec(checkUri(title, uri))
    }}
    result
  }
  
  def checkUri(title: String, uri: String) =
    http(title)
      .get(uri)
      .check(status.is(200))
  
  def createFilterValue(valueType: String, result: ChainBuilder): (String, ChainBuilder) = {
    var newResult = result
    val filterValue = valueType match {
      case "Boolean" => "null,notnull,false,true"
        
      case "Byte" => "null,notnull,!=0,1,>1,>=2;<=126,<127,127,null,notnull"
      case "Short" => "null,notnull,!=0,1,>1,>=2;<=32766,<32767,32767,null,notnull"
      case "Integer" => "null,notnull,!=0,1,>1,>=2;<=2147483646,<2147483647,2147483647,null,notnull"
      case "Long" => "null,notnull,!=0,1,>1,>=2;<=9223372036854775806,<9223372036854775807,9223372036854775807,null,notnull"
      
      case "Float" => "null,notnull,!=0,0.5,>0.5,>=0.51;<=3.40282346638528859e%2B38,<3.40282346638528860e%2B38,3.40282346638528860e%2B38,null,notnull"
      case "Double" => "null,notnull,!=0,0.5,>0.5,>=0.51;<=1.79769313486231569e%2B308d,<1.79769313486231570e%2B308d,1.79769313486231570e%2B308d,null,notnull"
      
      case "String" => "null,notnull,а,и;о,е,null,notnull"
        
      case "Polygon" => "null,notnull"
        
      case "userId" => "null,notnull,userId,1,>1,>=2;<=9223372036854775806,<9223372036854775807,9223372036854775807,null,notnull"
        
      case dictionaryUri => {
        val (enumFilterValue, result) = createEnumFilterValue(dictionaryUri, newResult)
        newResult = result
        enumFilterValue
      }
    }
    (filterValue, newResult)
  }
  
  val collectionSizeFilter = "empty,notempty,!=0,1,>1,>=2;<=2147483646,<2147483647,2147483647,empty,notempty"
  
  def createEnumFilterValue(dictionary: String, result: ChainBuilder): (String, ChainBuilder) = {
    val dictionaryUri = Constants.ROOT_DICTIONARY_URI + dictionary
    val newResult = result.exec(
      checkUri("GET " + dictionaryUri, dictionaryUri)
      .check(jsonPath("$[*].key").ofType[String].findAll.saveAs("dictionaryKeys"))
    )
    .exec(session => {
      val dictionaryKeys = session("dictionaryKeys").as[Vector[String]]
      val enumFilterValue = "null,notnull," + dictionaryKeys.mkString(",")
      session.set(dictionary + "Keys", enumFilterValue)
    })
    ("${" + dictionary + "Keys}", newResult)
  }
  
  def orderType: String = {
    val random = new Random
    random.nextInt(2) match {
      case 0 => "-asc"
      case 1 => "-desc"
    }
  }
        
}
