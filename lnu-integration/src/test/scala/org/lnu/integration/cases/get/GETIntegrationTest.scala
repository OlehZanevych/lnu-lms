package org.lnu.integration.cases.get

import org.lnu.integration.cases.BaseGETIntegrationTest

/**
 * GET integration tests
 * @author OlehZanevych
 */
object GETIntegrationTest extends BaseGETIntegrationTest {

  val apis = Array(
      (
          "academicgroups", "Long", true,
          Array(
              "name" -> "String",
              "departmentId" -> "Long",
              "specialtyId" -> "Long",
              "year" -> "Byte",
              "mentorId" -> "Long",
              "leaderId" -> "Long"
          ),
          null,
          null
      ),
      
      (
          "apicapabilities", "Other", false,
          Array(
              "userId" -> "userId",
              "api" -> "apis",
              "apiCapabilityLevel" -> "apicapabilitylevels",
              "apiCreateCapability" -> "apicreatecapabilities"
          ),
          null,
          null
      ),
      
      (
          "buildings", "Long", true,
          Array(
              "name" -> "String",
              "abbreviation" -> "String",
              "latitude" -> "Double",
              "longitude" -> "Double"
          ),
          null,
          null
      ),
      
      (
          "buildingmaps", "Long", true,
          Array(
              "fileURI" -> "String",
              "name" -> "String",
              "buildingId" -> "Long",
              "description" -> "String"
          ),
          null,
          null
      ),
      
      (
          "courses", "Long", true,
          Array(
              "name" -> "String",
              "abbreviation" -> "String",
              "info" -> "String"
          ),
          null,
          null
      ),
      
      (
          "curricula", "Long", true,
          Array(
              "courseType" -> "coursetypes",
              "courseCycle" -> "coursecycles"
          ),
          Array(
              "curriculumCourses" -> "Long",
              "specialtyIds" -> "Long",
              "curriculumSemesters" -> "Long",
              "curriculumSemesterHours" -> "Long"
          ),
          Array(
              "courseId" -> "Long",
              "departmentId" -> "Long",
              "semester" -> "Byte",
              "year" -> "Byte",
              "half" -> "Byte",
              "credits" -> "Float",
              "allHours" -> "Short",
              "independentStudy" -> "Short",
              "report" -> "reports",
              "classType" -> "classtypes",
              "hours" -> "Short",
              "isAcademicGroupDepartment" -> "Boolean"
          )
      ),
      
      (
          "departments", "Long", true,
          Array(
              "name" -> "String",
              "abbreviation" -> "String",
              "unitId" -> "Long",
              "email" -> "String",
              "phone" -> "String",
              "headId" -> "userId",
              "buildingMapId" -> "Long",
              "onMapId" -> "Short",
              "buildingId" -> "Long",
              "room" -> "String",
              "info" -> "String"
          ),
          null,
          null
      ),
      
      (
          "dormitories", "Long", true,
          Array(
              "number" -> "Short",
              "buildingMapId" -> "Long",
              "onMapId" -> "Short",
              "buildingId" -> "Long",
              "maxStudents" -> "Byte",
              "square" -> "Float",
              "info" -> "String"
          ),
          null,
          null
      ),
      
      (
          "groups", "Long", true,
          Array(
              "name" -> "String",
              "abbreviation" -> "String",
              "description" -> "String"
          ),
          Array(
              "userIds" -> "userId"
          ),
          null
      ),
      
      (
          "learningplaces", "Long", true,
          Array(
              "name" -> "String",
              "buildingMapId" -> "Long",
              "onMapId" -> "Short",
              "buildingId" -> "Long",
              "learningPlaceType" -> "learningplacetypes",
              "square" -> "Float",
              "roomsCount" -> "Byte",
              "seats" -> "Short",
              "seatsOnTest" -> "Short",
              "info" -> "String"
          ),
          null,
          null
      ),
      
      (
          "learningplacegroups", "Long", true,
          Array(
              "name" -> "String",
              "abbreviation" -> "String",
              "unitId" -> "Long"
          ),
          Array(
              "learningPlaceIds" -> "Long"
          ),
          null
      ),
      
      (
          "lecturers", "User", true,
          Array(
              "name" -> "String",
              "surname" -> "String",
              "firstName" -> "String",
              "middleName" -> "String",
              "email" -> "String",
              "phone" -> "String",
              "info" -> "String",
              "departmentId" -> "Long",
              "unitId" -> "Long",
              "scientificDegreeId" -> "Long",
              "academicStatus" -> "academicstatuses"
          ),
          null,
          null
      ),
      
      (
          "load", "Long", true,
          Array(
              "curriculumSemesterHoursId" -> "Long",
              "courseId" -> "Long",
              "departmentId" -> "Long",
              "classType" -> "classtypes",
              "allHours" -> "Short",
              "semester" -> "Byte",
              "year" -> "Byte",
              "half" -> "Byte"
          ),
          Array(
              "atomicLoads" -> "Long",
              "lecturerIds" -> "Long",
              "specialtyIds" -> "Long",
              "academicGroupIds" -> "Long",
              "studentIds" -> "Long",
              "learningPlaceGroupIds" -> "Long",
              "learningPlaceIds" -> "Long"
          ),
          Array(
              "hours" -> "Short",
              "lessonDuration" -> "Byte"
          )
      ),
      
      (
          "otheruniversityplaces", "Long", true,
          Array(
              "name" -> "String",
              "buildingMapId" -> "Long",
              "onMapId" -> "Short",
              "buildingId" -> "Long",
              "otherUniversityPlaceType" -> "otheruniversityplacetypes",
              "square" -> "Float",
              "roomsCount" -> "Byte",
              "info" -> "String"
          ),
          null,
          null
      ),
      
      (
          "roles", "Long", true,
          Array(
              "name" -> "String",
              "abbreviation" -> "String",
              "description" -> "String"
          ),
          Array(
              "apiCapabilities" -> "Long"
          ),
          Array(
              "api" -> "apis",
              "apiCapabilityLevel" -> "apicapabilitylevels"
          )
      ),
      
      (
          "scientificdegrees", "Long", true,
          Array(
              "name" -> "String",
              "abbreviation" -> "String"
          ),
          null,
          null
      ),
      
      (
          "specialties", "Long", true,
          Array(
              "name" -> "String",
              "code" -> "String",
              "unitId" -> "Long",
              "educationalDegree" -> "educationaldegrees"
          ),
          Array(
              "departmentIds" -> "Long"
          ),
          null
      ),
      
      (
          "students", "User", true,
          Array(
              "name" -> "String",
              "surname" -> "String",
              "firstName" -> "String",
              "middleName" -> "String",
              "email" -> "String",
              "phone" -> "String",
              "info" -> "String",
              "academicGroupId" -> "Long"
          ),
          null,
          null
      ),
      
      (
          "toilets", "Long", true,
          Array(
              "buildingMapId" -> "Long",
              "onMapId" -> "Short",
              "buildingId" -> "Long",
              "toiletType" -> "toilettypes",
              "info" -> "String"
          ),
          null,
          null
      ),
      
      (
          "units", "Long", true,
          Array(
              "name" -> "String",
              "abbreviation" -> "String",
              "unitType" -> "unittypes",
              "website" -> "String",
              "email" -> "String",
              "phone" -> "String",
              "deanId" -> "userId",
              "buildingMapId" -> "Long",
              "onMapId" -> "Short",
              "buildingId" -> "Long",
              "room" -> "String",
              "info" -> "String"
          ),
          null,
          null
      ),
      
      (
          "users", "User", true,
          Array(
              "login" -> "String",
              "name" -> "String",
              "surname" -> "String",
              "firstName" -> "String",
              "middleName" -> "String",
              "email" -> "String",
              "phone" -> "String",
              "info" -> "String"
          ),
          Array(
              "roleIds" -> "Long"
          ),
          null
      )
  )

  val testCase = test(apis)
  
}
