package org.lnu.integration.cases

/**
 * Object that contains constants which are used in integration tests.
 * @author OlehZanevych
 */
object Constants {
  
  val LOGIN = "admin"
  
  val PASSWORD = "simple-password"
  
  val ROOT_DICTIONARY_URI = "dictionaries/"
  
}