package org.lnu.integration

import io.gatling.core.Predef.checkBuilder2Check
import io.gatling.core.Predef.findCheckBuilder2ValidatorCheckBuilder
import io.gatling.core.Predef.exec
import io.gatling.core.Predef.stringToExpression
import io.gatling.core.Predef.validatorCheckBuilder2CheckBuilder
import io.gatling.core.Predef.value2Expression
import io.gatling.core.Predef.value2Success

import io.gatling.core.structure.ChainBuilder

import io.gatling.http.Predef.ELFileBody
import io.gatling.http.Predef.http
import io.gatling.http.Predef.jsonPath
import io.gatling.http.Predef.status

import io.gatling.http.request.builder.HttpRequestBuilder

import org.lnu.integration.cases.Constants

/**
 * Implementation of methods for testing CRUD methods.
 * @author OlehZanevych
 */
abstract class BaseCRUDIntegrationTest {
  
  def crud(uri: String, key: String, postDataFile: String, putDataFile: String,
      fields: Array[(String, String)] = null): ChainBuilder = {
    
    exec(post(uri, key, postDataFile, false, fields))
    .exec(update(key, putDataFile))
    .exec(delete(key))
  }
  
  def crudFile(uri: String, key: String, postDataFile: String, postSourceFile: String,
      putDataFile: String, putSourceFile: String,
      fields: Array[(String, String)] = null) : ChainBuilder = {
    
    exec(postFile(uri, key, postDataFile, postSourceFile, false, fields))
    .exec(update(key, putDataFile))
    .exec(updateFile(key, putSourceFile))
    .exec(delete(key))
  }

  def post(uri: String, key: String, postDataFile: String, saveId: Boolean = true,
      fields: Array[(String, String)] = null): ChainBuilder = {
    
    var scenario = http("Post " + uri)
      .post(uri)
      .basicAuth(Constants.LOGIN, Constants.PASSWORD)
      .header("Content-Type", "application/json")
      .body(ELFileBody("data/" + postDataFile))
      .asJSON
      .check(status.is(201))
      .check(jsonPath("$.uri").find.saveAs(key + "Uri"))
    postOptions(uri, key, scenario, saveId, fields)
  }
  
  def postFile(uri: String, key: String, postDataFile: String, postSourceFile: String,
      saveId: Boolean = true, fields: Array[(String, String)] = null): ChainBuilder = {
    
    var scenario = http("Post " + uri)
      .post(uri)
      .basicAuth(Constants.LOGIN, Constants.PASSWORD)
      .formParam("json", ELFileBody("data/" + postDataFile))
      .formUpload("file", "request-bodies/" + postSourceFile)
      .check(status.is(201))
      .check(jsonPath("$.uri").find.saveAs(key + "Uri"))
    postOptions(uri, key, scenario, saveId, fields)
  }
  
  def update(key: String, putDataFile: String): ChainBuilder = {
    exec(http("Update ${" + key + "RootUri}")
      .put("${" + key + "Uri}")
      .basicAuth(Constants.LOGIN, Constants.PASSWORD)
      .header("Content-Type", "application/json")
      .body(ELFileBody("data/" + putDataFile))
      .asJSON
      .check(status.is(200)))
    .exec(checkUrl(key, 200))
  }
  
  def updateFile(key: String, putSourceFile: String): ChainBuilder = {
    exec(http("Update File ${" + key + "RootUri}")
      .post("${" + key + "Uri}")
      .basicAuth(Constants.LOGIN, Constants.PASSWORD)
      .formUpload("file", "request-bodies/" + putSourceFile)
      .check(status.is(200)))
    .exec(checkUrl(key, 200))
  }
  
  def delete(key: String): ChainBuilder = {
    exec(http("Delete ${" + key + "RootUri}")
      .delete("${" + key + "Uri}")
      .basicAuth(Constants.LOGIN, Constants.PASSWORD)
      .check(status.is(204)))
    .exec(checkUrl(key, 404))
  }
  
  def checkUrl(key: String, requiredStatus: Int): ChainBuilder = {
    exec(http("Single Get ${" + key + "RootUri}")
      .get("${" + key + "Uri}")
      .check(status.is(requiredStatus)))
  }
  
  def postOptions(uri: String, key: String, scenario: HttpRequestBuilder, saveId: Boolean,
      fields: Array[(String, String)]): ChainBuilder = {
    
    var additionalScenario = scenario
    if (saveId) {
      additionalScenario = additionalScenario.check(jsonPath("$.id").find.saveAs(key + "Id"))
    }
    if (fields != null) {
      fields.foreach{field => {
        additionalScenario = additionalScenario.check(jsonPath("$." + field._1).find.saveAs(field._2))
      }}
    }
    exec(additionalScenario)
    .exec(session => {
        session.set(key + "RootUri", uri)
    })
    .exec(checkUrl(key, 200))
  }

}