package org.lnu.integration.cases.learningplacegroup

import org.lnu.integration.BaseCRUDIntegrationTest
import io.gatling.core.Predef.exec

/**
 * Integration test of LearningPlaceGroup API.
 * @author OlehZanevych
 */
object LearningPlaceGroupIntegrationTest extends BaseCRUDIntegrationTest {

  val testCase =
    exec(post(
      "users", "user1",
      "user/data3.json"))
    .exec(post(
      "users", "user2",
      "user/data4.json"))
    .exec(post(
      "users", "user3",
      "user/data5.json"))
    .exec(post(
      "groups", "group1",
      "group/data3.json"))
    .exec(post(
      "groups", "group2",
      "group/data4.json"))
    .exec(post(
      "groups", "group3",
      "group/data5.json"))
    .exec(post(
      "buildings", "building1",
      "building/data1.json"))
    .exec(post(
      "buildings", "building2",
      "building/data2.json"))
    .exec(postFile(
      "buildingmaps", "buildingMap1",
      "building/map/data1.json", "building/map/map1.dwg"))
    .exec(postFile(
      "buildingmaps", "buildingMap2",
      "building/map/data2.json", "building/map/map2.dwg"))
    .exec(post(
      "units", "unit1",
      "unit/data3.json"))
    .exec(post(
      "units", "unit2",
      "unit/data4.json"))
    .exec(post(
      "learningplaces", "learningPlace1",
      "learningplace/data1.json"))
    .exec(post(
      "learningplaces", "learningPlace2",
      "learningplace/data2.json"))
    .exec(post(
      "learningplaces", "learningPlace3",
      "learningplace/data3.json"))
    .exec(post(
      "learningplaces", "learningPlace4",
      "learningplace/data4.json"))
    .exec(crud(
      "learningplacegroups", "learningPlaceGroup",
      "learningplacegroup/data1.json", "learningplacegroup/data2.json"))
    .exec(delete("learningPlace1"))
    .exec(delete("learningPlace2"))
    .exec(delete("learningPlace3"))
    .exec(delete("learningPlace4"))
    .exec(delete("unit1"))
    .exec(delete("unit2"))
    .exec(delete("buildingMap1"))
    .exec(delete("buildingMap2"))
    .exec(delete("building1"))
    .exec(delete("building2"))
    .exec(delete("group1"))
    .exec(delete("group2"))
    .exec(delete("group3"))
    .exec(delete("user1"))
    .exec(delete("user2"))
    .exec(delete("user3"))

}
