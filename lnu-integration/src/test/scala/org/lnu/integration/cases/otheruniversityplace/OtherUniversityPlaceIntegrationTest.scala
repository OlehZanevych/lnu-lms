package org.lnu.integration.cases.otheruniversityplace

import org.lnu.integration.BaseCRUDIntegrationTest
import io.gatling.core.Predef.exec

/**
 * Integration test of Other University Place API.
 * @author OlehZanevych
 */
object OtherUniversityPlaceIntegrationTest extends BaseCRUDIntegrationTest {

  val testCase =
    exec(post(
      "users", "user1",
      "user/data3.json"))
    .exec(post(
      "users", "user2",
      "user/data4.json"))
    .exec(post(
      "users", "user3",
      "user/data5.json"))
    .exec(post(
      "groups", "group1",
      "group/data3.json"))
    .exec(post(
      "groups", "group2",
      "group/data4.json"))
    .exec(post(
      "groups", "group3",
      "group/data5.json"))
    .exec(post(
      "buildings", "building1",
      "building/data1.json"))
    .exec(post(
      "buildings", "building2",
      "building/data2.json"))
    .exec(postFile(
      "buildingmaps", "buildingMap1",
      "building/map/data1.json", "building/map/map1.dwg"))
    .exec(postFile(
      "buildingmaps", "buildingMap2",
      "building/map/data2.json", "building/map/map2.dwg"))
    .exec(crud(
      "otheruniversityplaces", "otherUniversityPlace",
      "otheruniversityplace/data1.json", "otheruniversityplace/data2.json"))
    .exec(delete("buildingMap1"))
    .exec(delete("buildingMap2"))
    .exec(delete("building1"))
    .exec(delete("building2"))
    .exec(delete("group1"))
    .exec(delete("group2"))
    .exec(delete("group3"))
    .exec(delete("user1"))
    .exec(delete("user2"))
    .exec(delete("user3"))
      
}