package org.lnu.integration.cases.student

import org.lnu.integration.BaseCRUDIntegrationTest
import io.gatling.core.Predef.exec

/**
 * Integration test of Student API.
 * @author OlehZanevych
 */
object StudentIntegrationTest extends BaseCRUDIntegrationTest {

  val testCase =
    exec(post(
      "users", "user1",
      "user/data3.json"))
    .exec(post(
      "users", "user2",
      "user/data4.json"))
    .exec(post(
      "users", "user3",
      "user/data5.json"))
    .exec(post(
      "groups", "group1",
      "group/data3.json"))
    .exec(post(
      "groups", "group2",
      "group/data4.json"))
    .exec(post(
      "groups", "group3",
      "group/data5.json"))
    .exec(post(
      "buildings", "building1",
      "building/data1.json"))
    .exec(post(
      "buildings", "building2",
      "building/data2.json"))
    .exec(postFile(
      "buildingmaps", "buildingMap1",
      "building/map/data1.json", "building/map/map1.dwg"))
    .exec(postFile(
      "buildingmaps", "buildingMap2",
      "building/map/data2.json", "building/map/map2.dwg"))
    .exec(post(
      "units", "unit1",
      "unit/data3.json"))
    .exec(post(
      "units", "unit2",
      "unit/data4.json"))
    .exec(post(
      "scientificdegrees", "scientificDegree1",
      "scientificdegree/data1.json"))
    .exec(post(
      "scientificdegrees", "scientificDegree2",
      "scientificdegree/data2.json"))
    .exec(post(
      "lecturers", "head1",
      "lecturer/data3.json"))
    .exec(post(
      "lecturers", "head2",
      "lecturer/data4.json"))
    .exec(post(
      "departments", "department1",
      "department/data1.json"))
    .exec(post(
      "departments", "department2",
      "department/data2.json"))
    .exec(post(
      "departments", "department3",
      "department/data3.json"))
    .exec(post(
      "departments", "department4",
      "department/data4.json"))
    .exec(post(
      "specialties", "specialty1",
      "specialty/data1.json"))
    .exec(post(
      "specialties", "specialty2",
      "specialty/data2.json"))
    .exec(post(
      "lecturers", "mentor1",
      "lecturer/data1.json"))
    .exec(post(
      "lecturers", "mentor2",
      "lecturer/data5.json"))
    .exec(post(
      "academicgroups", "academicGroup1",
      "academicgroup/data3.json"))
    .exec(post(
      "academicgroups", "academicGroup2",
      "academicgroup/data4.json"))
    .exec(crud(
      "students", "student",
      "student/data1.json", "student/data2.json"))
    .exec(delete("academicGroup1"))
    .exec(delete("academicGroup2"))
    .exec(delete("mentor1"))
    .exec(delete("mentor2"))
    .exec(delete("specialty1"))
    .exec(delete("specialty2"))
    .exec(delete("department1"))
    .exec(delete("department2"))
    .exec(delete("department3"))
    .exec(delete("department4"))
    .exec(delete("head1"))
    .exec(delete("head2"))
    .exec(delete("scientificDegree1"))
    .exec(delete("scientificDegree2"))
    .exec(delete("unit1"))
    .exec(delete("unit2"))
    .exec(delete("buildingMap1"))
    .exec(delete("buildingMap2"))
    .exec(delete("building1"))
    .exec(delete("building2"))
    .exec(delete("group1"))
    .exec(delete("group2"))
    .exec(delete("group3"))
    .exec(delete("user1"))
    .exec(delete("user2"))
    .exec(delete("user3"))
      
}