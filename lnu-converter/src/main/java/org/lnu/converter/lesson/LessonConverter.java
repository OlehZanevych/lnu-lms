package org.lnu.converter.lesson;

import java.util.List;
import java.util.stream.Collectors;

import org.lnu.annotation.Converter;
import org.lnu.converter.AbstractConverter;
import org.lnu.model.enumtype.day.Day;
import org.lnu.model.enumtype.weekperiodicity.WeekPeriodicity;
import org.lnu.model.learningplace.LearningPlace;
import org.lnu.model.lesson.Lesson;
import org.lnu.resource.lesson.LessonResource;

/**
 * Converter, that converts from Lesson to LessonResource.
 * @author OlehZanevych
 */
@Converter("lessonConverter")
public class LessonConverter extends AbstractConverter<Lesson, LessonResource> {

	@Override
	public LessonResource convert(final Lesson source, final LessonResource target) {
		
		Long id = source.getId();
		if (id != null) {
			target.setId(id);
		}
		
		Day day = source.getDay();
		if (day != null) {
			target.setDay(day);
		}
		
		Byte hour = source.getHour();
		if (hour != null) {
			target.setHour(hour);
		}
		
		Byte minute = source.getMinute();
		if (minute != null) {
			target.setMinute(minute);
		}
		
		Byte duration = source.getDuration();
		if (duration != null) {
			target.setDuration(duration);
		}
		
		WeekPeriodicity weekPeriodicity = source.getWeekPeriodicity();
		if (weekPeriodicity != null) {
			target.setWeekPeriodicity(weekPeriodicity);
		}
		
		List<LearningPlace> places = source.getPlaces();
		if (places != null) {
			target.setPlaceIds(places.stream().map(i -> i.getId()).collect(Collectors.toList()));
		}
		
		return target;
	}

	@Override
	public LessonResource convert(final Lesson source) {
		return convert(source, new LessonResource());
	}

}
