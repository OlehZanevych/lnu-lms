package org.lnu.converter.lesson;

import java.util.List;
import java.util.stream.Collectors;

import org.lnu.annotation.Converter;
import org.lnu.converter.AbstractResourceConverter;
import org.lnu.model.lesson.Lesson;
import org.lnu.model.enumtype.day.Day;
import org.lnu.model.enumtype.weekperiodicity.WeekPeriodicity;
import org.lnu.model.learningplace.LearningPlace;
import org.lnu.resource.lesson.LessonResource;

/**
 * Converter, that converts from LessonResource to Lesson.
 * @author OlehZanevych
 */
@Converter("lessonResourceConverter")
public class LessonResourceConverter extends AbstractResourceConverter<LessonResource, Lesson> {

	@Override
	public Lesson convert(final LessonResource source, final Lesson target) {

		Day day = source.getDay();
		if (day != null) {
			target.setDay(day);
		}
		
		Byte hour = source.getHour();
		if (hour != null) {
			target.setHour(hour);
		}
		
		Byte minute = source.getMinute();
		if (minute != null) {
			target.setMinute(minute);
		}
		
		Byte duration = source.getDuration();
		if (duration != null) {
			target.setDuration(duration);
		}
		
		WeekPeriodicity weekPeriodicity = source.getWeekPeriodicity();
		if (weekPeriodicity != null) {
			target.setWeekPeriodicity(weekPeriodicity);
		}
		
		List<Long> placeIds = source.getPlaceIds();
		if (placeIds != null) {
			target.setPlaces(placeIds.stream().map(i -> {
				LearningPlace learningPlace = new LearningPlace();
				learningPlace.setId(i);
				return learningPlace;
			}).collect(Collectors.toList()));
		}
		
		return target;
	}

	@Override
	public Lesson convert(final LessonResource source) {
		return convert(source, new Lesson());
	}

}
