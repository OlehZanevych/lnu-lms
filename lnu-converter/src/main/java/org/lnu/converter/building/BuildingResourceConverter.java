package org.lnu.converter.building;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityResourceConverter;
import org.lnu.model.building.Building;
import org.lnu.resource.building.BuildingResource;

/**
 * Converter, that converts from BuildingResource to Building.
 * @author OlehZanevych
 */
@Converter("buildingResourceConverter")
public class BuildingResourceConverter extends SecurityResourceConverter<BuildingResource, Building, Long> {

	@Override
	public Building convert(final BuildingResource source, final Building target) {
		
		super.convert(source, target);

		String name = source.getName();
		if (name != null) {
			if (name.isEmpty()) {
				target.setName(null);
			} else {
				target.setName(name);
			}
		}
		
		String abbreviation = source.getAbbreviation();
		if (abbreviation != null) {
			if (abbreviation.isEmpty()) {
				target.setAbbreviation(null);
			} else {
				target.setAbbreviation(abbreviation);
			}
		}
		
		Double latitude = source.getLatitude();
		if (latitude != null) {
			if (latitude == 0) {
				target.setLatitude(null);
			} else {
				target.setLatitude(latitude);
			}
		}
		
		Double longitude = source.getLongitude();
		if (longitude != null) {
			if (longitude == 0) {
				target.setLongitude(null);
			} else {
				target.setLongitude(longitude);
			}
		}
		
		return target;
	}

	@Override
	public Building convert(final BuildingResource source) {
		return convert(source, new Building());
	}

}
