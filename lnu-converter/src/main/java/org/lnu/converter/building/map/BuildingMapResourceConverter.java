package org.lnu.converter.building.map;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityResourceConverter;
import org.lnu.model.building.Building;
import org.lnu.model.building.map.BuildingMap;
import org.lnu.resource.building.map.BuildingMapResource;

/**
 * Converter, that converts from BuildingMapResource to BuildingMap.
 * @author OlehZanevych
 */
@Converter("buildingMapResourceConverter")
public class BuildingMapResourceConverter extends SecurityResourceConverter<BuildingMapResource, BuildingMap, Long> {

	@Override
	public BuildingMap convert(final BuildingMapResource source, final BuildingMap target) {
		
		super.convert(source, target);

		String name = source.getName();
		if (name != null) {
			if (name.isEmpty()) {
				target.setName(null);
			} else {
				target.setName(name);
			}
		}
		
		Long buildingId = source.getBuildingId();
		if (buildingId != null) {
			if (buildingId == 0) {
				target.setBuilding(null);
			} else {
				target.setBuilding(new Building(buildingId));
			}
			
		}
		
		String description = source.getDescription();
		if (description != null) {
			if (description.isEmpty()) {
				target.setDescription(null);
			} else {
				target.setDescription(description);
			}
		}
		
		return target;
	}

	@Override
	public BuildingMap convert(final BuildingMapResource source) {
		return convert(source, new BuildingMap());
	}

}
