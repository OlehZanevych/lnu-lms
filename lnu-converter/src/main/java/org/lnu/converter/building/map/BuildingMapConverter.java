package org.lnu.converter.building.map;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityConverter;
import org.lnu.model.building.Building;
import org.lnu.model.building.map.BuildingMap;
import org.lnu.resource.building.map.BuildingMapResource;

/**
 * Converter, that converts from BuildingMap to BuildingMapResource.
 * @author OlehZanevych
 */
@Converter("buildingMapConverter")
public class BuildingMapConverter extends SecurityConverter<BuildingMap, BuildingMapResource, Long> {

	@Override
	public BuildingMapResource convert(final BuildingMap source, final BuildingMapResource target) {
		super.convert(source, target);
		
		String name = source.getName();
		if (name != null) {
			target.setName(name);
		}
		
		Building building = source.getBuilding();
		if (building != null) {
			target.setBuildingId(building.getId());
		}
		
		String description = source.getDescription();
		if (description != null) {
			target.setDescription(description);
		}
		
		return target;
	}

	@Override
	public BuildingMapResource convert(final BuildingMap source) {
		return convert(source, new BuildingMapResource());
	}

}
