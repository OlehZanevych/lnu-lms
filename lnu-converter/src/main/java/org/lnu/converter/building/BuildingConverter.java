package org.lnu.converter.building;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityConverter;
import org.lnu.model.building.Building;
import org.lnu.resource.building.BuildingResource;

/**
 * Converter, that converts from Building to BuildingResource.
 * @author OlehZanevych
 */
@Converter("buildingConverter")
public class BuildingConverter extends SecurityConverter<Building, BuildingResource, Long> {

	@Override
	public BuildingResource convert(final Building source, final BuildingResource target) {
		super.convert(source, target);
		
		String name = source.getName();
		if (name != null) {
			target.setName(name);
		}
		
		String abbreviation = source.getAbbreviation();
		if (abbreviation != null) {
			target.setAbbreviation(abbreviation);
		}
		
		Double latitude = source.getLatitude();
		if (latitude != null) {
			target.setLatitude(latitude);
		}
		
		Double longitude = source.getLongitude();
		if (longitude != null) {
			target.setLongitude(longitude);
		}
		
		return target;
	}

	@Override
	public BuildingResource convert(final Building source) {
		return convert(source, new BuildingResource());
	}

}
