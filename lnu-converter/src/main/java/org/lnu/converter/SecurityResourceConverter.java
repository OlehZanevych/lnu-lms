package org.lnu.converter;

import java.util.List;
import java.util.stream.Collectors;

import org.lnu.model.APIModel;
import org.lnu.model.group.Group;
import org.lnu.model.user.User;
import org.lnu.resource.SecurityAPIResource;

/**
 * Resource converter for base and security properties.
 * @author OlehZanevych
 *
 * @param <SOURCE> Resource
 * @param <TARGET> Entity
 * @param <KEY> Identifier of Resource and Entity
 */
public abstract class SecurityResourceConverter<SOURCE extends SecurityAPIResource<KEY>,
		TARGET extends APIModel<KEY>, KEY extends Number> extends AbstractResourceConverter<SOURCE, TARGET> {
	
	@Override
	public TARGET convert(final SOURCE source, final TARGET target) {

		Boolean isPrivate = source.getIsPrivate();
		if (isPrivate != null) {
			target.setIsPrivate(isPrivate);
		}
		
		List<Long> responsibleOwnUserIds = source.getResponsibleOwnUserIds();
		if (responsibleOwnUserIds != null) {
			target.setResponsibleOwnUsers(responsibleOwnUserIds.stream().map(i -> new User(i))
					.collect(Collectors.toList()));
		}
		
		List<Long> responsibleGroupIds = source.getResponsibleGroupIds();
		if (responsibleGroupIds != null) {
			target.setResponsibleGroups(responsibleGroupIds.stream().map(i -> new Group(i))
					.collect(Collectors.toList()));
		}
		
		return target;
	}

}
