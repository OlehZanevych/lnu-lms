package org.lnu.converter.academicgroup;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityResourceConverter;
import org.lnu.model.academicgroup.AcademicGroup;
import org.lnu.model.department.Department;
import org.lnu.model.specialty.Specialty;
import org.lnu.model.student.Student;
import org.lnu.model.lecturer.Lecturer;
import org.lnu.resource.academicgroup.AcademicGroupResource;

/**
 * Converter, that converts from AcademicGroupResource to AcademicGroup.
 * @author OlehZanevych
 */
@Converter("academicGroupResourceConverter")
public class AcademicGroupResourceConverter extends SecurityResourceConverter<AcademicGroupResource, AcademicGroup, Long> {

	@Override
	public AcademicGroup convert(final AcademicGroupResource source, final AcademicGroup target) {
		super.convert(source, target);
		
		String name = source.getName();
		if (name != null) {
			if (name.isEmpty()) {
				target.setName(null);
			} else {
				target.setName(name);
			}
		}
		
		Long departmentId = source.getDepartmentId();
		if (departmentId != null) {
			if (departmentId == 0) {
				target.setDepartment(null);
			} else {
				Department department = new Department(departmentId);
				target.setDepartment(department);
			}
		}
		
		Long specialtyId = source.getSpecialtyId();
		if (specialtyId != null) {
			if (specialtyId == 0) {
				target.setSpecialty(null);
			} else {
				Specialty specialty = new Specialty(specialtyId);
				target.setSpecialty(specialty);
			}
		}
		
		Byte year = source.getYear();
		if (year != null) {
			if (year == 0) {
				target.setYear(null);
			} else {
				target.setYear(year);
			}
		}
		
		Long mentorId = source.getMentorId();
		if (mentorId != null) {
			if (mentorId == 0) {
				target.setMentor(null);
			} else {
				Lecturer mentor = new Lecturer(mentorId);
				target.setMentor(mentor);
			}
		}
		
		Long leaderId = source.getLeaderId();
		if (leaderId != null) {
			if (leaderId == 0) {
				target.setLeader(null);
			} else {
				target.setLeader(new Student(leaderId));
			}
		}
		
		return target;
	}

	@Override
	public AcademicGroup convert(final AcademicGroupResource source) {
		return convert(source, new AcademicGroup());
	}

}
