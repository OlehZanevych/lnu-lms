package org.lnu.converter.academicgroup;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityConverter;
import org.lnu.model.department.Department;
import org.lnu.model.specialty.Specialty;
import org.lnu.model.student.Student;
import org.lnu.model.lecturer.Lecturer;
import org.lnu.model.academicgroup.AcademicGroup;
import org.lnu.resource.academicgroup.AcademicGroupResource;

/**
 * Converter, that converts from AcademicGroup to AcademicGroupResource.
 * @author OlehZanevych
 */
@Converter("academicGroupConverter")
public class AcademicGroupConverter extends SecurityConverter<AcademicGroup, AcademicGroupResource, Long> {

	@Override
	public AcademicGroupResource convert(final AcademicGroup source, final AcademicGroupResource target) {
		super.convert(source, target);
		
		String name = source.getName();
		if (name != null) {
			target.setName(name);
		}
		
		Department department = source.getDepartment();
		if (department != null) {
			target.setDepartmentId(department.getId());
		}
		
		Specialty specialty = source.getSpecialty();
		if (specialty != null) {
			target.setSpecialtyId(specialty.getId());
		}
		
		Byte year = source.getYear();
		if (year != null) {
			target.setYear(year);
		}
		
		Lecturer mentor = source.getMentor();
		if (mentor != null) {
			target.setMentorId(mentor.getId());
		}
		
		Student leader = source.getLeader();
		if (leader != null) {
			target.setLeaderId(leader.getId());
		}
		
		return target;
	}

	@Override
	public AcademicGroupResource convert(final AcademicGroup source) {
		return convert(source, new AcademicGroupResource());
	}

}
