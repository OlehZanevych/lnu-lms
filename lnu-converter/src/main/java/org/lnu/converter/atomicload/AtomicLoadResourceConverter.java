package org.lnu.converter.atomicload;

import java.util.List;
import java.util.stream.Collectors;

import org.lnu.annotation.Converter;
import org.lnu.converter.AbstractResourceConverter;
import org.lnu.model.academicgroup.AcademicGroup;
import org.lnu.model.atomicload.AtomicLoad;
import org.lnu.model.learningplace.LearningPlace;
import org.lnu.model.learningplacegroup.LearningPlaceGroup;
import org.lnu.model.lecturer.Lecturer;
import org.lnu.model.specialty.Specialty;
import org.lnu.model.student.Student;
import org.lnu.resource.atomicload.AtomicLoadResource;

/**
 * Converter, that converts from AtomicLoadResource to AtomicLoad.
 * @author OlehZanevych
 */
@Converter("atomicLoadResourceConverter")
public class AtomicLoadResourceConverter extends AbstractResourceConverter<AtomicLoadResource, AtomicLoad> {

	@Override
	public AtomicLoad convert(final AtomicLoadResource source, final AtomicLoad target) {
		Short hours = source.getHours();
		if (hours != null) {
			if (hours == 0) {
				target.setHours(null);
			} else {
				target.setHours(hours);
			}
		}
		
		Byte lessonDuration = source.getLessonDuration();
		if (lessonDuration != null) {
			if (lessonDuration == 0) {
				target.setLessonDuration(null);
			} else {
				target.setLessonDuration(lessonDuration);
			}
		}
		
		List<Long> lecturerIds = source.getLecturerIds();
		if (lecturerIds != null) {
			target.setLecturers(lecturerIds.stream().map(i -> new Lecturer(i))
					.collect(Collectors.toList()));
		}
		
		List<Long> specialtyIds = source.getSpecialtyIds();
		if (specialtyIds != null) {
			target.setSpecialties(specialtyIds.stream().map(i -> new Specialty(i))
					.collect(Collectors.toList()));
		}
		
		List<Long> academicGroupIds = source.getAcademicGroupIds();
		if (academicGroupIds != null) {
			target.setAcademicGroups(academicGroupIds.stream()
					.map(i -> new AcademicGroup(i)).collect(Collectors.toList()));
		}
		
		List<Long> studentIds = source.getStudentIds();
		if (studentIds != null) {
			target.setStudents(studentIds.stream().map(i -> new Student(i))
					.collect(Collectors.toList()));
		}
		
		List<Long> learningPlaceGroupIds = source.getLearningPlaceGroupIds();
		if (learningPlaceGroupIds != null) {
			target.setLearningPlaceGroups(learningPlaceGroupIds.stream()
					.map(i -> new LearningPlaceGroup(i)).collect(Collectors.toList()));
		}
		
		List<Long> learningPlaceIds = source.getLearningPlaceIds();
		if (learningPlaceIds != null) {
			target.setLearningPlaces(learningPlaceIds.stream()
					.map(i -> new LearningPlace(i)).collect(Collectors.toList()));
		}
		
		return target;
	}

	@Override
	public AtomicLoad convert(final AtomicLoadResource source) {
		return convert(source, new AtomicLoad());
	}

}
