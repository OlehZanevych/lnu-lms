package org.lnu.converter.atomicload;

import java.util.List;
import java.util.stream.Collectors;

import org.lnu.annotation.Converter;
import org.lnu.converter.BaseConverter;
import org.lnu.model.academicgroup.AcademicGroup;
import org.lnu.model.atomicload.AtomicLoad;
import org.lnu.model.learningplace.LearningPlace;
import org.lnu.model.learningplacegroup.LearningPlaceGroup;
import org.lnu.model.lecturer.Lecturer;
import org.lnu.model.specialty.Specialty;
import org.lnu.model.student.Student;
import org.lnu.resource.atomicload.AtomicLoadResource;

/**
 * Converter, that converts from AtomicLoad to AtomicLoadResource.
 * @author OlehZanevych
 */
@Converter("atomicLoadConverter")
public class AtomicLoadConverter extends BaseConverter<AtomicLoad, AtomicLoadResource, Long> {

	@Override
	public AtomicLoadResource convert(final AtomicLoad source, final AtomicLoadResource target) {
		super.convert(source, target);
		
		Short hours = source.getHours();
		if (hours != null) {
			target.setHours(hours);
		}
		
		Byte lessonDuration = source.getLessonDuration();
		if (lessonDuration != null) {
			target.setLessonDuration(lessonDuration);
		}
		
		List<Lecturer> lecturers = source.getLecturers();
		if (lecturers != null) {
			target.setLecturerIds(lecturers.stream().map(i -> i.getId())
					.collect(Collectors.toList()));
		}
		
		List<Specialty> specialties = source.getSpecialties();
		if (specialties != null) {
			target.setSpecialtyIds(specialties.stream().map(i -> i.getId())
					.collect(Collectors.toList()));
		}
		
		List<AcademicGroup> academicGroups = source.getAcademicGroups();
		if (academicGroups != null) {
			target.setAcademicGroupIds(academicGroups.stream().map(i -> i.getId())
					.collect(Collectors.toList()));
		}
		
		List<Student> students = source.getStudents();
		if (students != null) {
			target.setStudentIds(students.stream().map(i -> i.getId())
					.collect(Collectors.toList()));
		}
		
		List<LearningPlaceGroup> learningPlaceGroups = source.getLearningPlaceGroups();
		if (learningPlaceGroups != null) {
			target.setLearningPlaceGroupIds(learningPlaceGroups.stream().map(i -> i.getId())
					.collect(Collectors.toList()));
		}
		
		List<LearningPlace> learningPlaces = source.getLearningPlaces();
		if (learningPlaces != null) {
			target.setLearningPlaceIds(learningPlaces.stream().map(i -> i.getId())
					.collect(Collectors.toList()));
		}
		
		return target;
	}

	@Override
	public AtomicLoadResource convert(final AtomicLoad source) {
		return convert(source, new AtomicLoadResource());
	}

}
