package org.lnu.converter.dormitory;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityConverter;
import org.lnu.model.building.Building;
import org.lnu.model.building.map.BuildingMap;
import org.lnu.model.dormitory.Dormitory;
import org.lnu.resource.dormitory.DormitoryResource;

/**
 * Converter, that converts from Dormitory to DormitoryResource.
 * @author OlehZanevych
 */
@Converter("dormitoryConverter")
public class DormitoryConverter extends SecurityConverter<Dormitory, DormitoryResource, Long> {

	@Override
	public DormitoryResource convert(final Dormitory source, final DormitoryResource target) {
		super.convert(source, target);
		
		Short number = source.getNumber();
		if (number != null) {
			target.setNumber(number);
		}
		
		BuildingMap buildingMap = source.getBuildingMap();
		if (buildingMap != null) {
			target.setBuildingMapId(buildingMap.getId());
			Building building = buildingMap.getBuilding();
			if (building != null) {
				target.setBuildingId(building.getId());
			}
		}
		
		Short onMapId = source.getOnMapId();
		if (onMapId != null) {
			target.setOnMapId(onMapId);
		}
		
		Byte maxStudents = source.getMaxStudents();
		if (maxStudents != null) {
			target.setMaxStudents(maxStudents);
		}
		
		Float square = source.getSquare();
		if (square != null) {
			target.setSquare(square);
		}
		
		String info = source.getInfo();
		if (info != null) {
			target.setInfo(info);
		}
		
		return target;
	}

	@Override
	public DormitoryResource convert(final Dormitory source) {
		return convert(source, new DormitoryResource());
	}

}
