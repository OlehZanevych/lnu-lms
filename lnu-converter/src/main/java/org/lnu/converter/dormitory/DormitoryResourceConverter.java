package org.lnu.converter.dormitory;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityResourceConverter;
import org.lnu.model.building.map.BuildingMap;
import org.lnu.model.dormitory.Dormitory;
import org.lnu.resource.dormitory.DormitoryResource;

/**
 * Converter, that converts from DormitoryResource to Dormitory.
 * @author OlehZanevych
 */
@Converter("dormitoryResourceConverter")
public class DormitoryResourceConverter extends SecurityResourceConverter<DormitoryResource, Dormitory, Long> {

	@Override
	public Dormitory convert(final DormitoryResource source, final Dormitory target) {
		super.convert(source, target);

		Short number = source.getNumber();
		if (number != null) {
			if (number == 0) {
				target.setNumber(null);
			} else {
				target.setNumber(number);
			}
		}
		
		Long buildingMapId = source.getBuildingMapId();
		if (buildingMapId != null) {
			if (buildingMapId == 0) {
				target.setBuildingMap(null);
			} else {
				target.setBuildingMap(new BuildingMap(buildingMapId));
			}
		}
		
		Short onMapId = source.getOnMapId();
		if (onMapId != null) {
			if (onMapId == 0) {
				target.setOnMapId(null);
			} else {
				target.setOnMapId(onMapId);
			}
		}
		
		Byte maxStudents = source.getMaxStudents();
		if (maxStudents != null) {
			if (maxStudents == 0) {
				target.setMaxStudents(null);
			} else {
				target.setMaxStudents(maxStudents);
			}
		}
		
		Float square = source.getSquare();
		if (square != null) {
			if (square == 0) {
				target.setSquare(null);
			} else {
				target.setSquare(square);
			}
		}
		
		String info = source.getInfo();
		if (info != null) {
			if (info.isEmpty()) {
				target.setInfo(null);
			} else {
				target.setInfo(info);
			}
		}
		
		return target;
	}

	@Override
	public Dormitory convert(final DormitoryResource source) {
		return convert(source, new Dormitory());
	}

}
