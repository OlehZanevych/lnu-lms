package org.lnu.converter.unit;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityResourceConverter;
import org.lnu.model.building.map.BuildingMap;
import org.lnu.model.enumtype.unit.type.UnitType;
import org.lnu.model.unit.Unit;
import org.lnu.model.lecturer.Lecturer;
import org.lnu.resource.unit.UnitResource;

/**
 * Converter, that converts from UnitResource to Unit.
 * @author OlehZanevych
 */
@Converter("unitResourceConverter")
public class UnitResourceConverter extends SecurityResourceConverter<UnitResource, Unit, Long> {

	@Override
	public Unit convert(final UnitResource source, final Unit target) {
		
		super.convert(source, target);

		String name = source.getName();
		if (name != null) {
			if (name.isEmpty()) {
				target.setName(null);
			} else {
				target.setName(name);
			}
		}
		
		String abbreviation = source.getAbbreviation();
		if (abbreviation != null) {
			if (abbreviation.isEmpty()) {
				target.setAbbreviation(null);
			} else {
				target.setAbbreviation(abbreviation);
			}
		}
		
		UnitType unitType = source.getUnitType();
		if (unitType != null) {
			if (unitType == UnitType.NULL) {
				target.setUnitType(null);
			} else {
				target.setUnitType(unitType);
			}
		}
		
		String website = source.getWebsite();
		if (website != null) {
			if (website.isEmpty()) {
				target.setWebsite(null);
			} else {
				target.setWebsite(website);
			}
		}
		
		String email = source.getEmail();
		if (email != null) {
			if (email.isEmpty()) {
				target.setEmail(null);
			} else {
				target.setEmail(email);
			}
		}
		
		String phone = source.getPhone();
		if (phone != null) {
			if (phone.isEmpty()) {
				target.setPhone(null);
			} else {
				target.setPhone(phone);
			}
		}
		
		Long deanId = source.getDeanId();
		if (deanId != null) {
			if (deanId == 0) {
				target.setDean(null);
			} else {
				Lecturer dean = new Lecturer(deanId);
				target.setDean(dean);
			}
		}
		
		Long buildingMapId = source.getBuildingMapId();
		if (buildingMapId != null) {
			if (buildingMapId == 0) {
				target.setBuildingMap(null);
			} else {
				target.setBuildingMap(new BuildingMap(buildingMapId));
			}
		}
		
		Short onMapId = source.getOnMapId();
		if (onMapId != null) {
			if (onMapId == 0) {
				target.setOnMapId(null);
			} else {
				target.setOnMapId(onMapId);
			}
		}
		
		String room = source.getRoom();
		if (room != null) {
			if (room.isEmpty()) {
				target.setRoom(null);
			} else {
				target.setRoom(room);
			}
		}
		
		String info = source.getInfo();
		if (info != null) {
			if (info.isEmpty()) {
				target.setInfo(null);
			} else {
				target.setInfo(info);
			}
		}
		
		return target;
	}

	@Override
	public Unit convert(final UnitResource source) {
		return convert(source, new Unit());
	}

}
