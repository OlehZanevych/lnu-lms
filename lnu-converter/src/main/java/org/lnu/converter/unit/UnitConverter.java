package org.lnu.converter.unit;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityConverter;
import org.lnu.model.building.Building;
import org.lnu.model.building.map.BuildingMap;
import org.lnu.model.enumtype.unit.type.UnitType;
import org.lnu.model.unit.Unit;
import org.lnu.model.lecturer.Lecturer;
import org.lnu.resource.unit.UnitResource;

/**
 * Converter, that converts from Unit to UnitResource.
 * @author OlehZanevych
 */
@Converter("unitConverter")
public class UnitConverter extends SecurityConverter<Unit, UnitResource, Long> {

	@Override
	public UnitResource convert(final Unit source, final UnitResource target) {
		
		super.convert(source, target);
		
		String name = source.getName();
		if (name != null) {
			target.setName(name);
		}
		
		String abbreviation = source.getAbbreviation();
		if (abbreviation != null) {
			target.setAbbreviation(abbreviation);
		}
		
		UnitType unitType = source.getUnitType();
		if (unitType != null) {
			target.setUnitType(unitType);
		}
		
		String website = source.getWebsite();
		if (website != null) {
			target.setWebsite(website);
		}
		
		String email = source.getEmail();
		if (email != null) {
			target.setEmail(email);
		}
		
		String phone = source.getPhone();
		if (phone != null) {
			target.setPhone(phone);
		}
		
		Lecturer dean = source.getDean();
		if (dean != null) {
			target.setDeanId(dean.getId());
		}
		
		BuildingMap buildingMap = source.getBuildingMap();
		if (buildingMap != null) {
			target.setBuildingMapId(buildingMap.getId());
			Building building = buildingMap.getBuilding();
			if (building != null) {
				target.setBuildingId(building.getId());
			}
		}
		
		Short onMapId = source.getOnMapId();
		if (onMapId != null) {
			target.setOnMapId(onMapId);
		}
		
		String room = source.getRoom();
		if (room != null) {
			target.setRoom(room);
		}
		
		String info = source.getInfo();
		if (info != null) {
			target.setInfo(info);
		}
		
		return target;
	}

	@Override
	public UnitResource convert(final Unit source) {
		return convert(source, new UnitResource());
	}

}
