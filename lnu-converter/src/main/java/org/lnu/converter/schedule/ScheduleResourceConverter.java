package org.lnu.converter.schedule;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;

import org.lnu.annotation.Converter;
import org.lnu.converter.AbstractResourceConverter;
import org.lnu.converter.lesson.LessonResourceConverter;
import org.lnu.model.schedule.Schedule;
import org.lnu.model.academicgroup.AcademicGroup;
import org.lnu.model.atomicload.AtomicLoad;
import org.lnu.model.enumtype.classtype.ClassType;
import org.lnu.model.lesson.Lesson;
import org.lnu.resource.lesson.LessonResource;
import org.lnu.resource.schedule.ScheduleResource;

/**
 * Converter, that converts from ScheduleResource to Schedule.
 * @author OlehZanevych
 */
@Converter("scheduleResourceConverter")
public class ScheduleResourceConverter extends AbstractResourceConverter<ScheduleResource, Schedule> {

	@Resource(name = "lessonResourceConverter")
	private LessonResourceConverter lessonResourceConverter;
	
	@Override
	public Schedule convert(final ScheduleResource source, final Schedule target) {

		ClassType classType = source.getClassType();
		if (classType != null) {
			target.setClassType(classType);
		}
		
		Short hours = source.getHours();
		if (hours != null) {
			target.setHours(hours);
		}
		
		Long academicGroupId = source.getAcademicGroupId();
		if (academicGroupId != null) {
			AcademicGroup academicGroup = new AcademicGroup();
			academicGroup.setId(academicGroupId);
			target.setAcademicGroup(academicGroup);
		}
		
		Byte semester = source.getSemester();
		if (semester != null) {
			target.setSemester(semester);
		}
		
		List<LessonResource> lessonResources = source.getLessons();
		if (lessonResources != null) {
			List<Lesson> lessons = target.getLessons();
			if (lessons == null) {
				lessons = new LinkedList<Lesson>();
				target.setLessons(lessons);
			}
			AtomicLoad parent = new AtomicLoad();
			parent.setId(target.getId());
			lessonResourceConverter.updateCollection(lessonResources, lessons, "setAtomicLoad", parent);
		}
		
		return target;
	}

	@Override
	public Schedule convert(final ScheduleResource source) {
		return convert(source, new Schedule());
	}

}
