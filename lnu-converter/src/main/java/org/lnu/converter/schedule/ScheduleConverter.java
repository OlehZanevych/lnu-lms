package org.lnu.converter.schedule;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.lnu.annotation.Converter;
import org.lnu.converter.AbstractConverter;
import org.lnu.converter.lecturer.LecturerConverter;
import org.lnu.converter.lesson.LessonConverter;
import org.lnu.model.academicgroup.AcademicGroup;
import org.lnu.model.enumtype.classtype.ClassType;
import org.lnu.model.lecturer.Lecturer;
import org.lnu.model.lesson.Lesson;
import org.lnu.model.schedule.Schedule;
import org.lnu.resource.schedule.ScheduleResource;

/**
 * Converter, that converts from Schedule to ScheduleResource.
 * @author OlehZanevych
 */
@Converter("scheduleConverter")
public class ScheduleConverter extends AbstractConverter<Schedule, ScheduleResource> {
	
	@Resource(name = "lecturerConverter")
	private LecturerConverter lecturerConverter;
	
	@Resource(name = "lessonConverter")
	private LessonConverter lessonConverter;
	
	@Override
	public ScheduleResource convert(final Schedule source, final ScheduleResource target) {
		
		Long id = source.getId();
		if (id != null) {
			target.setId(id);
		}
		
		ClassType classType = source.getClassType();
		if (classType != null) {
			target.setClassType(classType);
		}
		
		Short hours = source.getHours();
		if (hours != null) {
			target.setHours(hours);
		}
		
		AcademicGroup academicGroup = source.getAcademicGroup();
		if (academicGroup != null) {
			target.setAcademicGroupId(academicGroup.getId());
		}
		
		Byte semester = source.getSemester();
		if (semester != null) {
			target.setSemester(semester);
		}
		
		List<Lecturer> lecturers = source.getLecturers();
		if (lecturers != null) {
			target.setLecturers(lecturers.stream()
					.map(i -> lecturerConverter.convert(i)).collect(Collectors.toList()));
		}
		
		List<Lesson> lessons = source.getLessons();
		if (lessons != null) {
			target.setLessons(lessons.stream()
					.map(i -> lessonConverter.convert(i)).collect(Collectors.toList()));
		}
		
		return target;
	}

	@Override
	public ScheduleResource convert(final Schedule source) {
		return convert(source, new ScheduleResource());
	}

}
