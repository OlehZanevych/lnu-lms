package org.lnu.converter;

import org.lnu.model.ModelWithSimpleId;
import org.lnu.resource.ResourceWithSimpleId;

/**
 * Base entity converter.
 * @author OlehZanevych
 *
 * @param <SOURCE> Entity
 * @param <TARGET> Resource
 * @param <KEY> Identifier of Resource and Entity
 */
public abstract class BaseConverter<SOURCE extends ModelWithSimpleId<KEY>,
		TARGET extends ResourceWithSimpleId<KEY>, KEY extends Number>
		extends AbstractConverter<SOURCE, TARGET> {
	
	@Override
	public TARGET convert(final SOURCE source, final TARGET target) {
		
		KEY id = source.getId();
		if (id != null) {
			target.setId(id);
		}
		
		return target;
	}

}
