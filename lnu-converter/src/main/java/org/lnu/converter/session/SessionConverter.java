package org.lnu.converter.session;

import org.lnu.annotation.Converter;
import org.lnu.converter.AbstractConverter;
import org.lnu.model.session.Session;
import org.lnu.resource.session.SessionResource;

/**
 * Session converter.
 * @author OlehZanevych
 *
 */
@Converter("sessionConverter")
public class SessionConverter extends AbstractConverter<Session, SessionResource> {
	
	@Override
	public SessionResource convert(final Session source, final SessionResource target) {
		
		Long userId = source.getUserId();
		if (userId != null) {
			target.setUserId(userId);
		}
		
		return target;
	}

	@Override
	public SessionResource convert(final Session source) {
		return convert(source, new SessionResource());
	}

}
