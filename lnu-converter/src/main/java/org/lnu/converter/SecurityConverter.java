package org.lnu.converter;

import java.util.List;
import java.util.stream.Collectors;

import org.lnu.model.APIModel;
import org.lnu.model.enumtype.capability.Capability;
import org.lnu.model.group.Group;
import org.lnu.model.user.User;
import org.lnu.resource.SecurityAPIResource;

/**
 * Entity converter for base and security properties.
 * @author OlehZanevych
 *
 * @param <SOURCE> Entity
 * @param <TARGET> Resource
 * @param <KEY> Identifier of Resource and Entity
 */
public abstract class SecurityConverter<SOURCE extends APIModel<KEY>, TARGET extends SecurityAPIResource<KEY>,
		KEY extends Number> extends BaseConverter<SOURCE, TARGET, KEY> {
	
	@Override
	public TARGET convert(final SOURCE source, final TARGET target) {
		super.convert(source, target);
		
		Boolean isPrivate = source.getIsPrivate();
		if (isPrivate != null) {
			target.setIsPrivate(isPrivate);
		}
		
		Capability capability = source.getCapability();
		if (capability != null) {
			target.setCapability(capability);
		}
		
		List<User> responsibleOwnUsers = source.getResponsibleOwnUsers();
		if (responsibleOwnUsers != null) {
			target.setResponsibleOwnUserIds(responsibleOwnUsers.stream().map(i -> i.getId())
					.collect(Collectors.toList()));
		}
		
		List<Group> responsibleGroups = source.getResponsibleGroups();
		if (responsibleGroups != null) {
			target.setResponsibleGroupIds(responsibleGroups.stream().map(i -> i.getId())
					.collect(Collectors.toList()));
		}
		
		List<User> responsibleUsers = source.getResponsibleUsers();
		if (responsibleUsers != null) {
			target.setResponsibleUserIds(responsibleUsers.stream().map(i -> i.getId())
					.collect(Collectors.toList()));
		}
		
		return target;
	}

}
