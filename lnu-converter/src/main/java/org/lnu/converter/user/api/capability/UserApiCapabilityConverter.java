package org.lnu.converter.user.api.capability;

import org.lnu.annotation.Converter;
import org.lnu.converter.AbstractConverter;
import org.lnu.model.enumtype.api.API;
import org.lnu.model.enumtype.api.capability.level.APICapabilityLevel;
import org.lnu.model.enumtype.api.createcapability.APICreateCapability;
import org.lnu.model.user.User;
import org.lnu.model.user.api.capability.UserApiCapability;
import org.lnu.resource.user.api.capability.UserApiCapabilityResource;

/**
 * Converter, that converts from UserAPICapability to UserAPICapabilityResource.
 * @author OlehZanevych
 */
@Converter("userApiCapabilityConverter")
public class UserApiCapabilityConverter extends AbstractConverter<UserApiCapability, UserApiCapabilityResource> {

	@Override
	public UserApiCapabilityResource convert(final UserApiCapability source, final UserApiCapabilityResource target) {
		User user = source.getUser();
		if (user != null) {
			target.setUserId(user.getId());
		}
		
		API api = source.getApi();
		if (api != null) {
			target.setApi(api);
		}
		
		APICapabilityLevel apiCapabilityLevel = source.getApiCapabilityLevel();
		if (apiCapabilityLevel != null) {
			target.setApiCapabilityLevel(apiCapabilityLevel);
		}
		
		APICreateCapability apiCreateCapability = source.getApiCreateCapability();
		if (apiCreateCapability != null) {
			target.setApiCreateCapability(apiCreateCapability);
		}
		
		return target;
	}

	@Override
	public UserApiCapabilityResource convert(final UserApiCapability source) {
		return convert(source, new UserApiCapabilityResource());
	}

}
