package org.lnu.converter.user;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityResourceConverter;
import org.lnu.model.role.Role;
import org.lnu.model.user.User;
import org.lnu.resource.user.UserResource;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Converter, that converts from UserResource to User.
 * @author OlehZanevych
 */
@Converter("userResourceConverter")
public class UserResourceConverter extends SecurityResourceConverter<UserResource, User, Long> {
	
	@Resource(name = "bCryptPasswordEncoder")
    private PasswordEncoder passwordEncoder;

	@Override
	public User convert(final UserResource source, final User target) {
		
		super.convert(source, target);
		
		String login = source.getLogin();
		if (login != null) {
			if (login.isEmpty()) {
				target.setLogin(null);
			} else {
				target.setLogin(login);
			}
		}
		
		String password = source.getPassword();
		if (password != null) {
			if (password.isEmpty()) {
				target.setPasswordHash(null);
			} else {
				target.setPasswordHash(passwordEncoder.encode(password));
			}
		}
		
		String surname = source.getSurname();
		if (surname != null) {
			if (surname.isEmpty()) {
				target.setSurname(null);
			} else {
				target.setSurname(surname);
			}
		}
		
		String firstName = source.getFirstName();
		if (firstName != null) {
			if (firstName.isEmpty()) {
				target.setFirstName(null);
			} else {
				target.setFirstName(firstName);
			}
		}
		
		String middleName = source.getMiddleName();
		if (middleName != null) {
			if (middleName.isEmpty()) {
				target.setMiddleName(null);
			} else {
				target.setMiddleName(middleName);
			}
		}
		
		String email = source.getEmail();
		if (email != null) {
			if (email.isEmpty()) {
				target.setEmail(null);
			} else {
				target.setEmail(email);
			}
		}
		
		String phone = source.getPhone();
		if (phone != null) {
			if (phone.isEmpty()) {
				target.setPhone(null);
			} else {
				target.setPhone(phone);
			}
		}
		
		String info = source.getInfo();
		if (info != null) {
			if (info.isEmpty()) {
				target.setInfo(null);
			} else {
				target.setInfo(info);
			}
		}
		
		List<Long> roleIds = source.getRoleIds();
		if (roleIds != null) {
			target.setRoles(roleIds.stream().map(i -> new Role(i))
					.collect(Collectors.toList()));
		}
		
		return target;
	}

	@Override
	public User convert(final UserResource source) {
		return convert(source, new User());
	}

}
