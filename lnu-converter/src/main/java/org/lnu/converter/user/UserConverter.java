package org.lnu.converter.user;

import java.util.List;
import java.util.stream.Collectors;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityConverter;
import org.lnu.model.role.Role;
import org.lnu.model.user.User;
import org.lnu.resource.user.UserResource;

/**
 * Converter, that converts from User to UserResource.
 * @author OlehZanevych
 */
@Converter("userConverter")
public class UserConverter extends SecurityConverter<User, UserResource, Long> {

	@Override
	public UserResource convert(final User source, final UserResource target) {
		
		super.convert(source, target);
		
		String login = source.getLogin();
		if (login != null) {
			target.setLogin(login);
		}
		
		String name = source.getName();
		if (name != null) {
			target.setName(name);
		}
		
		String surname = source.getSurname();
		if (surname != null) {
			target.setSurname(surname);
		}
		
		String firstName = source.getFirstName();
		if (firstName != null) {
			target.setFirstName(firstName);
		}
		
		String middleName = source.getMiddleName();
		if (middleName != null) {
			target.setMiddleName(middleName);
		}
		
		String email = source.getEmail();
		if (email != null) {
			target.setEmail(email);
		}
		
		String phone = source.getPhone();
		if (phone != null) {
			target.setPhone(phone);
		}
		
		String info = source.getInfo();
		if (info != null) {
			target.setInfo(info);
		}
		
		List<Role> roles = source.getRoles();
		if (roles != null) {
			target.setRoleIds(roles.stream().map(i -> i.getId())
					.collect(Collectors.toList()));
		}
		
		return target;
	}

	@Override
	public UserResource convert(final User source) {
		return convert(source, new UserResource());
	}

}
