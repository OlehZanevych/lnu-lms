package org.lnu.converter.apicapability;

import org.lnu.annotation.Converter;
import org.lnu.converter.AbstractResourceConverter;
import org.lnu.model.api.capability.APICapability;
import org.lnu.model.enumtype.api.API;
import org.lnu.model.enumtype.api.capability.level.APICapabilityLevel;
import org.lnu.resource.apicapability.ApiCapabilityResource;
/**
 * Converter, that converts from ApiCapabilityResource to ApiCapability.
 * @author OlehZanevych
 */
@Converter("apiCapabilityResourceConverter")
public class ApiCapabilityResourceConverter extends AbstractResourceConverter<ApiCapabilityResource, APICapability> {
	
	@Override
	public APICapability convert(final ApiCapabilityResource source, final APICapability target) {

		API api = source.getApi();
		if (api != null) {
			if (api == API.NULL) {
				target.setApi(null);
			} else {
				target.setApi(api);
			}
		}
		
		APICapabilityLevel apiCapabilityLevel = source.getApiCapabilityLevel();
		if (apiCapabilityLevel != null) {
			if (apiCapabilityLevel == APICapabilityLevel.NULL) {
				target.setApiCapabilityLevel(null);
			} else {
				target.setApiCapabilityLevel(apiCapabilityLevel);
			}
		}
		
		return target;
	}

	@Override
	public APICapability convert(final ApiCapabilityResource source) {
		return convert(source, new APICapability());
	}

}
