package org.lnu.converter.apicapability;

import org.lnu.annotation.Converter;
import org.lnu.converter.BaseConverter;
import org.lnu.model.api.capability.APICapability;
import org.lnu.model.enumtype.api.API;
import org.lnu.model.enumtype.api.capability.level.APICapabilityLevel;
import org.lnu.resource.apicapability.ApiCapabilityResource;

/**
 * Converter, that converts from APICapability to APICapabilityResource.
 * @author OlehZanevych
 */
@Converter("apiCapabilityConverter")
public class ApiCapabilityConverter extends BaseConverter<APICapability, ApiCapabilityResource, Long> {

	@Override
	public ApiCapabilityResource convert(final APICapability source, final ApiCapabilityResource target) {
		
		super.convert(source, target);
		
		API api = source.getApi();
		if (api != null) {
			target.setApi(api);
		}
		
		APICapabilityLevel apiCapabilityLevel = source.getApiCapabilityLevel();
		if (apiCapabilityLevel != null) {
			target.setApiCapabilityLevel(apiCapabilityLevel);
		}
		
		return target;
	}

	@Override
	public ApiCapabilityResource convert(final APICapability source) {
		return convert(source, new ApiCapabilityResource());
	}

}
