package org.lnu.converter.load;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityConverter;
import org.lnu.converter.atomicload.AtomicLoadConverter;
import org.lnu.model.atomicload.AtomicLoad;
import org.lnu.model.course.Course;
import org.lnu.model.curriculum.semester.CurriculumSemester;
import org.lnu.model.curriculum.semester.hours.CurriculumSemesterHours;
import org.lnu.model.department.Department;
import org.lnu.model.enumtype.classtype.ClassType;
import org.lnu.model.load.Load;
import org.lnu.resource.load.LoadResource;

/**
 * Converter, that converts from Load to LoadResource.
 * @author OlehZanevych
 */
@Converter("loadConverter")
public class LoadConverter extends SecurityConverter<Load, LoadResource, Long> {
	
	@Resource(name = "atomicLoadConverter")
	private AtomicLoadConverter atomicLoadConverter;
	
	@Override
	public LoadResource convert(final Load source, final LoadResource target) {
		super.convert(source, target);
		
		CurriculumSemesterHours curriculumSemesterHours = source.getCurriculumSemesterHours();
		if (curriculumSemesterHours != null) {
			target.setCurriculumSemesterHoursId(curriculumSemesterHours.getId());
			
			ClassType classType = curriculumSemesterHours.getClassType();
			if (classType != null) {
				target.setClassType(classType);
			}
			
			Short allHours = curriculumSemesterHours.getHours();
			if (allHours != null) {
				target.setAllHours(allHours);
			}
			
			CurriculumSemester curriculumSemester = curriculumSemesterHours.getCurriculumSemester();
			if (curriculumSemester != null) {
				Byte semester = curriculumSemester.getSemester();
				if (semester != null) {
					target.setSemester(semester);
				}
				
				Byte year = curriculumSemester.getYear();
				if (year != null) {
					target.setYear(year);
				}
				
				Byte half = curriculumSemester.getHalf();
				if (half != null) {
					target.setHalf(half);
				}
			}
		}
		
		Course course = source.getCourse();
		if (course != null) {
			target.setCourseId(course.getId());
		}
		
		Department department = source.getDepartment();
		if (department != null) {
			target.setDepartmentId(department.getId());
		}
		
		List<AtomicLoad> atomicLoads = source.getAtomicLoads();
		if (atomicLoads != null) {
			target.setAtomicLoads(atomicLoads.stream()
					.map(i -> atomicLoadConverter.convert(i)).collect(Collectors.toList()));
		}
		
		return target;
	}

	@Override
	public LoadResource convert(final Load source) {
		return convert(source, new LoadResource());
	}

}
