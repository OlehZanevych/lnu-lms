package org.lnu.converter.load;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityResourceConverter;
import org.lnu.converter.atomicload.AtomicLoadResourceConverter;
import org.lnu.model.load.Load;
import org.lnu.model.atomicload.AtomicLoad;
import org.lnu.resource.atomicload.AtomicLoadResource;
import org.lnu.resource.load.LoadResource;

/**
 * Converter, that converts from LoadResource to Load.
 * @author OlehZanevych
 */
@Converter("loadResourceConverter")
public class LoadResourceConverter extends SecurityResourceConverter<LoadResource, Load, Long> {

	@Resource(name = "atomicLoadResourceConverter")
	private AtomicLoadResourceConverter atomicLoadResourceConverter;
	
	@Override
	public Load convert(final LoadResource source, final Load target) {
		super.convert(source, target);
		
		List<AtomicLoadResource> atomicLoadResources = source.getAtomicLoads();
		if (atomicLoadResources != null) {
			List<AtomicLoad> atomicLoads = target.getAtomicLoads();
			if (atomicLoads == null) {
				atomicLoads = new LinkedList<AtomicLoad>();
				target.setAtomicLoads(atomicLoads);
			}
			atomicLoadResourceConverter.updateCollection(atomicLoadResources,
					atomicLoads, "load", target);
		}
		
		return target;
	}

	@Override
	public Load convert(final LoadResource source) {
		return convert(source, new Load());
	}

}
