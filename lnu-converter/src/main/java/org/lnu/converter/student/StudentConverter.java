package org.lnu.converter.student;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityConverter;
import org.lnu.model.academicgroup.AcademicGroup;
import org.lnu.model.student.Student;
import org.lnu.resource.student.StudentResource;

/**
 * Converter, that converts from Student to StudentResource.
 * @author OlehZanevych
 */
@Converter("studentConverter")
public class StudentConverter extends SecurityConverter<Student, StudentResource, Long> {

	@Override
	public StudentResource convert(final Student source, final StudentResource target) {
		
		super.convert(source, target);
		
		String name = source.getName();
		if (name != null) {
			target.setName(name);
		}
		
		String surname = source.getSurname();
		if (surname != null) {
			target.setSurname(surname);
		}
		
		String firstName = source.getFirstName();
		if (firstName != null) {
			target.setFirstName(firstName);
		}
		
		String middleName = source.getMiddleName();
		if (middleName != null) {
			target.setMiddleName(middleName);
		}
		
		String email = source.getEmail();
		if (email != null) {
			target.setEmail(email);
		}
		
		String phone = source.getPhone();
		if (phone != null) {
			target.setPhone(phone);
		}
		
		String info = source.getInfo();
		if (info != null) {
			target.setInfo(info);
		}
		
		AcademicGroup academicGroup = source.getAcademicGroup();
		if (academicGroup != null) {
			target.setAcademicGroupId(academicGroup.getId());
		}
		
		return target;
	}

	@Override
	public StudentResource convert(final Student source) {
		return convert(source, new StudentResource());
	}

}
