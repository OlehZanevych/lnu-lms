package org.lnu.converter.learningplacegroup;

import java.util.List;
import java.util.stream.Collectors;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityResourceConverter;
import org.lnu.model.learningplace.LearningPlace;
import org.lnu.model.learningplacegroup.LearningPlaceGroup;
import org.lnu.model.unit.Unit;
import org.lnu.resource.learningplacegroup.LearningPlaceGroupResource;

/**
 * Converter, that converts from LearningPlaceGroupResource to LearningPlaceGroup.
 * @author OlehZanevych
 */
@Converter("learningPlaceGroupResourceConverter")
public class LearningPlaceGroupResourceConverter extends SecurityResourceConverter<LearningPlaceGroupResource, LearningPlaceGroup, Long> {

	@Override
	public LearningPlaceGroup convert(final LearningPlaceGroupResource source, final LearningPlaceGroup target) {
		super.convert(source, target);

		String name = source.getName();
		if (name != null) {
			if (name.isEmpty()) {
				target.setName(null);
			} else {
				target.setName(name);
			}
		}
		
		String abbreviation = source.getAbbreviation();
		if (abbreviation != null) {
			if (abbreviation.isEmpty()) {
				target.setAbbreviation(null);
			} else {
				target.setAbbreviation(abbreviation);
			}
		}
		
		Long unitId = source.getUnitId();
		if (unitId != null) {
			if (unitId == 0) {
				target.setUnit(null);
			} else {
				Unit unit = new Unit(unitId);
				target.setUnit(unit);
			}
		}
		
		List<Long> learningPlaceIds = source.getLearningPlaceIds();
		if (learningPlaceIds != null) {
			target.setLearningPlaces(learningPlaceIds.stream().map(i -> new LearningPlace(i))
					.collect(Collectors.toList()));
		}
		
		return target;
	}

	@Override
	public LearningPlaceGroup convert(final LearningPlaceGroupResource source) {
		return convert(source, new LearningPlaceGroup());
	}

}
