package org.lnu.converter.learningplacegroup;

import java.util.List;
import java.util.stream.Collectors;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityConverter;
import org.lnu.model.learningplace.LearningPlace;
import org.lnu.model.learningplacegroup.LearningPlaceGroup;
import org.lnu.model.unit.Unit;
import org.lnu.resource.learningplacegroup.LearningPlaceGroupResource;

/**
 * Converter, that converts from LearningPlaceGroup to LearningPlaceGroupResource.
 * @author OlehZanevych
 */
@Converter("learningPlaceGroupConverter")
public class LearningPlaceGroupConverter extends SecurityConverter<LearningPlaceGroup, LearningPlaceGroupResource, Long> {

	@Override
	public LearningPlaceGroupResource convert(final LearningPlaceGroup source, final LearningPlaceGroupResource target) {
		super.convert(source, target);
		
		String name = source.getName();
		if (name != null) {
			target.setName(name);
		}
		
		String abbreviation = source.getAbbreviation();
		if (abbreviation != null) {
			target.setAbbreviation(abbreviation);
		}
		
		Unit unit = source.getUnit();
		if (unit != null) {
			target.setUnitId(unit.getId());
		}
		
		List<LearningPlace> learningPlaces = source.getLearningPlaces();
		if (learningPlaces != null) {
			target.setLearningPlaceIds(learningPlaces.stream().map(i -> i.getId()).collect(Collectors.toList()));
		}
		
		return target;
	}

	@Override
	public LearningPlaceGroupResource convert(final LearningPlaceGroup source) {
		return convert(source, new LearningPlaceGroupResource());
	}

}
