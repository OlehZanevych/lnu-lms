package org.lnu.converter.specialty;

import java.util.List;
import java.util.stream.Collectors;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityConverter;
import org.lnu.model.department.Department;
import org.lnu.model.enumtype.educationaldegree.EducationalDegree;
import org.lnu.model.unit.Unit;
import org.lnu.model.specialty.Specialty;
import org.lnu.resource.specialty.SpecialtyResource;

/**
 * Converter, that converts from Specialty to SpecialtyResource.
 * @author OlehZanevych
 */
@Converter("specialtyConverter")
public class SpecialtyConverter extends SecurityConverter<Specialty, SpecialtyResource, Long> {

	@Override
	public SpecialtyResource convert(final Specialty source, final SpecialtyResource target) {
		
		super.convert(source, target);
		
		String name = source.getName();
		if (name != null) {
			target.setName(name);
		}
		
		String code = source.getCode();
		if (code != null) {
			target.setCode(code);
		}
		
		Unit unit = source.getUnit();
		if (unit != null) {
			target.setUnitId(unit.getId());
		}
		
		List<Department> departments = source.getDepartments();
		if (departments != null) {
			target.setDepartmentIds(departments.stream().map(i -> i.getId()).collect(Collectors.toList()));
		}
		
		EducationalDegree educationalDegree = source.getEducationalDegree();
		if (educationalDegree != null) {
			target.setEducationalDegree(educationalDegree);
		}
		
		return target;
	}

	@Override
	public SpecialtyResource convert(final Specialty source) {
		return convert(source, new SpecialtyResource());
	}

}
