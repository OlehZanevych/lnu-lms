package org.lnu.converter.specialty;

import java.util.List;
import java.util.stream.Collectors;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityResourceConverter;
import org.lnu.model.department.Department;
import org.lnu.model.enumtype.educationaldegree.EducationalDegree;
import org.lnu.model.specialty.Specialty;
import org.lnu.model.unit.Unit;
import org.lnu.resource.specialty.SpecialtyResource;

/**
 * Converter, that converts from SpecialtyResource to Specialty.
 * @author OlehZanevych
 */
@Converter("specialtyResourceConverter")
public class SpecialtyResourceConverter extends SecurityResourceConverter<SpecialtyResource, Specialty, Long> {

	@Override
	public Specialty convert(final SpecialtyResource source, final Specialty target) {
		
		super.convert(source, target);

		String name = source.getName();
		if (name != null) {
			if (name.isEmpty()) {
				target.setName(null);
			} else {
				target.setName(name);
			}
		}
		
		String code = source.getCode();
		if (code != null) {
			if (code.isEmpty()) {
				target.setCode(null);
			} else {
				target.setCode(code);
			}
		}
		
		Long unitId = source.getUnitId();
		if (unitId != null) {
			if (unitId == 0) {
				target.setUnit(null);
			} else {
				Unit unit = new Unit(unitId);
				target.setUnit(unit);
			}
		}
		
		List<Long> departmentIds = source.getDepartmentIds();
		if (departmentIds != null) {
			target.setDepartments(departmentIds.stream().map(i -> new Department(i))
					.collect(Collectors.toList()));
		}
		
		EducationalDegree educationalDegree = source.getEducationalDegree();
		if (educationalDegree != null) {
			if (educationalDegree == EducationalDegree.NULL) {
				target.setEducationalDegree(null);
			} else {
				target.setEducationalDegree(educationalDegree);
			}
		}
		
		return target;
	}

	@Override
	public Specialty convert(final SpecialtyResource source) {
		return convert(source, new Specialty());
	}

}
