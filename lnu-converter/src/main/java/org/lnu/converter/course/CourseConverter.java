package org.lnu.converter.course;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityConverter;
import org.lnu.model.course.Course;
import org.lnu.resource.course.CourseResource;

/**
 * Converter, that converts from Course to CourseResource.
 * @author OlehZanevych
 */
@Converter("courseConverter")
public class CourseConverter extends SecurityConverter<Course, CourseResource, Long> {

	@Override
	public CourseResource convert(final Course source, final CourseResource target) {
		super.convert(source, target);
		
		String name = source.getName();
		if (name != null) {
			target.setName(name);
		}
		
		String abbreviation = source.getAbbreviation();
		if (abbreviation != null) {
			target.setAbbreviation(abbreviation);
		}
		
		String info = source.getInfo();
		if (info != null) {
			target.setInfo(info);
		}
		
		return target;
	}

	@Override
	public CourseResource convert(final Course source) {
		return convert(source, new CourseResource());
	}

}
