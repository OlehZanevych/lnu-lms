package org.lnu.converter.course;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityResourceConverter;
import org.lnu.model.course.Course;
import org.lnu.resource.course.CourseResource;

/**
 * Converter, that converts from CourseResource to Course.
 * @author OlehZanevych
 */
@Converter("courseResourceConverter")
public class CourseResourceConverter extends SecurityResourceConverter<CourseResource, Course, Long> {

	@Override
	public Course convert(final CourseResource source, final Course target) {
		super.convert(source, target);

		String name = source.getName();
		if (name != null) {
			if (name.isEmpty()) {
				target.setName(null);
			} else {
				target.setName(name);
			}
		}
		
		String abbreviation = source.getAbbreviation();
		if (abbreviation != null) {
			if (abbreviation.isEmpty()) {
				target.setAbbreviation(null);
			} else {
				target.setAbbreviation(abbreviation);
			}
		}
		
		String info = source.getInfo();
		if (info != null) {
			if (info.isEmpty()) {
				target.setInfo(null);
			} else {
				target.setInfo(info);
			}
		}
		
		return target;
	}

	@Override
	public Course convert(final CourseResource source) {
		return convert(source, new Course());
	}

}
