package org.lnu.converter.otheruniversityplace;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityResourceConverter;
import org.lnu.model.building.map.BuildingMap;
import org.lnu.model.enumtype.otheruniversityplacetype.OtherUniversityPlaceType;
import org.lnu.model.otheruniversityplace.OtherUniversityPlace;
import org.lnu.resource.otheruniversityplace.OtherUniversityPlaceResource;

/**
 * Converter, that converts from OtherUniversityPlaceResource to OtherUniversityPlace.
 * @author OlehZanevych
 */
@Converter("otherUniversityPlaceResourceConverter")
public class OtherUniversityPlaceResourceConverter
	extends SecurityResourceConverter<OtherUniversityPlaceResource, OtherUniversityPlace, Long> {

	@Override
	public OtherUniversityPlace convert(final OtherUniversityPlaceResource source, final OtherUniversityPlace target) {
		super.convert(source, target);

		String name = source.getName();
		if (name != null) {
			if (name.isEmpty()) {
				target.setName(null);
			} else {
				target.setName(name);
			}
		}
		
		Long buildingMapId = source.getBuildingMapId();
		if (buildingMapId != null) {
			if (buildingMapId == 0) {
				target.setBuildingMap(null);
			} else {
				target.setBuildingMap(new BuildingMap(buildingMapId));
			}
		}
		
		Short onMapId = source.getOnMapId();
		if (onMapId != null) {
			if (onMapId == 0) {
				target.setOnMapId(null);
			} else {
				target.setOnMapId(onMapId);
			}
		}
		
		OtherUniversityPlaceType otherUniversityPlaceType = source.getOtherUniversityPlaceType();
		if (otherUniversityPlaceType != null) {
			if (otherUniversityPlaceType == OtherUniversityPlaceType.NULL) {
				target.setOtherUniversityPlaceType(null);
			} else {
				target.setOtherUniversityPlaceType(otherUniversityPlaceType);
			}
		}
		
		Float square = source.getSquare();
		if (square != null) {
			if (square == 0) {
				target.setSquare(null);
			} else {
				target.setSquare(square);
			}
		}
		
		Byte roomsCount = source.getRoomsCount();
		if (roomsCount != null) {
			if (roomsCount == 0) {
				target.setRoomsCount(null);
			} else {
				target.setRoomsCount(roomsCount);
			}
		}
		
		String info = source.getInfo();
		if (info != null) {
			if (info.isEmpty()) {
				target.setInfo(null);
			} else {
				target.setInfo(info);
			}
		}
		
		return target;
	}

	@Override
	public OtherUniversityPlace convert(final OtherUniversityPlaceResource source) {
		return convert(source, new OtherUniversityPlace());
	}

}
