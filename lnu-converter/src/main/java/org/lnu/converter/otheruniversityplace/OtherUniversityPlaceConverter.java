package org.lnu.converter.otheruniversityplace;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityConverter;
import org.lnu.model.building.Building;
import org.lnu.model.building.map.BuildingMap;
import org.lnu.model.enumtype.otheruniversityplacetype.OtherUniversityPlaceType;
import org.lnu.model.otheruniversityplace.OtherUniversityPlace;
import org.lnu.resource.otheruniversityplace.OtherUniversityPlaceResource;

/**
 * Converter, that converts from OtherUniversityPlace to OtherUniversityPlaceResource.
 * @author OlehZanevych
 */
@Converter("otherUniversityPlaceConverter")
public class OtherUniversityPlaceConverter extends SecurityConverter<OtherUniversityPlace, OtherUniversityPlaceResource, Long> {

	@Override
	public OtherUniversityPlaceResource convert(final OtherUniversityPlace source, final OtherUniversityPlaceResource target) {
		super.convert(source, target);
		
		String name = source.getName();
		if (name != null) {
			target.setName(name);
		}
		
		BuildingMap buildingMap = source.getBuildingMap();
		if (buildingMap != null) {
			target.setBuildingMapId(buildingMap.getId());
			Building building = buildingMap.getBuilding();
			if (building != null) {
				target.setBuildingId(building.getId());
			}
		}
		
		Short onMapId = source.getOnMapId();
		if (onMapId != null) {
			target.setOnMapId(onMapId);
		}
		
		OtherUniversityPlaceType otherUniversityPlaceType = source.getOtherUniversityPlaceType();
		if (otherUniversityPlaceType != null) {
			target.setOtherUniversityPlaceType(otherUniversityPlaceType);
		}
		
		Float square = source.getSquare();
		if (square != null) {
			target.setSquare(square);
		}
		
		Byte roomsCount = source.getRoomsCount();
		if (roomsCount != null) {
			target.setRoomsCount(roomsCount);
		}
		
		String info = source.getInfo();
		if (info != null) {
			target.setInfo(info);
		}
		
		return target;
	}

	@Override
	public OtherUniversityPlaceResource convert(final OtherUniversityPlace source) {
		return convert(source, new OtherUniversityPlaceResource());
	}

}
