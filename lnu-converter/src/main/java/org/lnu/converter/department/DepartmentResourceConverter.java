package org.lnu.converter.department;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityResourceConverter;
import org.lnu.model.building.map.BuildingMap;
import org.lnu.model.department.Department;
import org.lnu.model.unit.Unit;
import org.lnu.model.lecturer.Lecturer;
import org.lnu.resource.department.DepartmentResource;

/**
 * Converter, that converts from DepartmentResource to Department.
 * @author OlehZanevych
 */
@Converter("departmentResourceConverter")
public class DepartmentResourceConverter extends SecurityResourceConverter<DepartmentResource, Department, Long> {

	@Override
	public Department convert(final DepartmentResource source, final Department target) {

		super.convert(source, target);
		
		String name = source.getName();
		if (name != null) {
			if (name.isEmpty()) {
				target.setName(null);
			} else {
				target.setName(name);
			}
		}
		
		String abbreviation = source.getAbbreviation();
		if (abbreviation != null) {
			if (abbreviation.isEmpty()) {
				target.setAbbreviation(null);
			} else {
				target.setAbbreviation(abbreviation);
			}
		}
		
		Long unitId = source.getUnitId();
		if (unitId != null) {
			if (unitId == 0) {
				target.setUnit(null);
			} else {
				Unit unit = new Unit(unitId);
				target.setUnit(unit);
			}
		}
		
		String email = source.getEmail();
		if (email != null) {
			if (email.isEmpty()) {
				target.setEmail(null);
			} else {
				target.setEmail(email);
			}
		}
		
		String phone = source.getPhone();
		if (phone != null) {
			if (phone.isEmpty()) {
				target.setPhone(null);
			} else {
				target.setPhone(phone);
			}
		}
		
		Long headId = source.getHeadId();
		if (headId != null) {
			if (headId == 0) {
				target.setHead(null);
			} else {
				Lecturer head = new Lecturer(headId);
				target.setHead(head);
			}
		}
		
		Long buildingMapId = source.getBuildingMapId();
		if (buildingMapId != null) {
			if (buildingMapId == 0) {
				target.setBuildingMap(null);
			} else {
				target.setBuildingMap(new BuildingMap(buildingMapId));
			}
		}
		
		Short onMapId = source.getOnMapId();
		if (onMapId != null) {
			if (onMapId == 0) {
				target.setOnMapId(null);
			} else {
				target.setOnMapId(onMapId);
			}
		}
		
		String room = source.getRoom();
		if (room != null) {
			if (room.isEmpty()) {
				target.setRoom(null);
			} else {
				target.setRoom(room);
			}
		}
		
		String info = source.getInfo();
		if (info != null) {
			if (info.isEmpty()) {
				target.setInfo(null);
			} else {
				target.setInfo(info);
			}
		}
		
		return target;
	}

	@Override
	public Department convert(final DepartmentResource source) {
		return convert(source, new Department());
	}

}
