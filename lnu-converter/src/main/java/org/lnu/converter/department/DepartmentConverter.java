package org.lnu.converter.department;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityConverter;
import org.lnu.model.building.Building;
import org.lnu.model.building.map.BuildingMap;
import org.lnu.model.department.Department;
import org.lnu.model.unit.Unit;
import org.lnu.model.lecturer.Lecturer;
import org.lnu.resource.department.DepartmentResource;

/**
 * Converter, that converts from Department to DepartmentResource.
 * @author OlehZanevych
 */
@Converter("departmentConverter")
public class DepartmentConverter extends SecurityConverter<Department, DepartmentResource, Long> {

	@Override
	public DepartmentResource convert(final Department source, final DepartmentResource target) {
		
		super.convert(source, target);
		
		String name = source.getName();
		if (name != null) {
			target.setName(source.getName());
		}
		
		String abbreviation = source.getAbbreviation();
		if (abbreviation != null) {
			target.setAbbreviation(abbreviation);
		}
		
		Unit unit = source.getUnit();
		if (unit != null) {
			target.setUnitId(unit.getId());
		}
		
		String email = source.getEmail();
		if (email != null) {
			target.setEmail(email);
		}
		
		String phone = source.getPhone();
		if (phone != null) {
			target.setPhone(phone);
		}
		
		Lecturer head = source.getHead();
		if (head != null) {
			target.setHeadId(head.getId());
		}
		
		BuildingMap buildingMap = source.getBuildingMap();
		if (buildingMap != null) {
			target.setBuildingMapId(buildingMap.getId());
			Building building = buildingMap.getBuilding();
			if (building != null) {
				target.setBuildingId(building.getId());
			}
		}
		
		Short onMapId = source.getOnMapId();
		if (onMapId != null) {
			target.setOnMapId(onMapId);
		}
		
		String room = source.getRoom();
		if (room != null) {
			target.setRoom(room);
		}
		
		String info = source.getInfo();
		if (info != null) {
			target.setInfo(info);
		}
		
		return target;
	}

	@Override
	public DepartmentResource convert(final Department source) {
		return convert(source, new DepartmentResource());
	}

}
