package org.lnu.converter.lecturer;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityConverter;
import org.lnu.model.department.Department;
import org.lnu.model.enumtype.academicstatus.AcademicStatus;
import org.lnu.model.unit.Unit;
import org.lnu.model.lecturer.Lecturer;
import org.lnu.model.scientificdegree.ScientificDegree;
import org.lnu.resource.lecturer.LecturerResource;

/**
 * Converter, that converts from Lecturer to LecturerResource.
 * @author OlehZanevych
 */
@Converter("lecturerConverter")
public class LecturerConverter extends SecurityConverter<Lecturer, LecturerResource, Long> {

	@Override
	public LecturerResource convert(final Lecturer source, final LecturerResource target) {
		
		super.convert(source, target);
		
		String name = source.getName();
		if (name != null) {
			target.setName(name);
		}
		
		String surname = source.getSurname();
		if (surname != null) {
			target.setSurname(surname);
		}
		
		String firstName = source.getFirstName();
		if (firstName != null) {
			target.setFirstName(firstName);
		}
		
		String middleName = source.getMiddleName();
		if (middleName != null) {
			target.setMiddleName(middleName);
		}
		
		String email = source.getEmail();
		if (email != null) {
			target.setEmail(email);
		}
		
		String phone = source.getPhone();
		if (phone != null) {
			target.setPhone(phone);
		}
		
		String info = source.getInfo();
		if (info != null) {
			target.setInfo(info);
		}
		
		Department department = source.getDepartment();
		if (department != null) {
			target.setDepartmentId(department.getId());
			Unit unit = department.getUnit();
			if (unit != null) {
				target.setUnitId(unit.getId());
			}
		}
		
		ScientificDegree scientificDegree = source.getScientificDegree();
		if (scientificDegree != null) {
			target.setScientificDegreeId(scientificDegree.getId());
		}
		
		AcademicStatus academicStatus = source.getAcademicStatus();
		if (academicStatus != null) {
			target.setAcademicStatus(academicStatus);
		}
		
		return target;
	}

	@Override
	public LecturerResource convert(final Lecturer source) {
		return convert(source, new LecturerResource());
	}

}
