package org.lnu.converter.lecturer;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityResourceConverter;
import org.lnu.model.lecturer.Lecturer;
import org.lnu.model.scientificdegree.ScientificDegree;
import org.lnu.model.department.Department;
import org.lnu.model.enumtype.academicstatus.AcademicStatus;
import org.lnu.resource.lecturer.LecturerResource;

/**
 * Converter, that converts from LecturerResource to Lecturer.
 * @author OlehZanevych
 */
@Converter("lecturerResourceConverter")
public class LecturerResourceConverter extends SecurityResourceConverter<LecturerResource, Lecturer, Long> {

	@Override
	public Lecturer convert(final LecturerResource source, final Lecturer target) {
		
		super.convert(source, target);
		
		String surname = source.getSurname();
		if (surname != null) {
			if (surname.isEmpty()) {
				target.setSurname(null);
			} else {
				target.setSurname(surname);
			}
		}
		
		String firstName = source.getFirstName();
		if (firstName != null) {
			if (firstName.isEmpty()) {
				target.setFirstName(null);
			} else {
				target.setFirstName(firstName);
			}
		}
		
		String middleName = source.getMiddleName();
		if (middleName != null) {
			if (middleName.isEmpty()) {
				target.setMiddleName(null);
			} else {
				target.setMiddleName(middleName);
			}
		}
		
		String email = source.getEmail();
		if (email != null) {
			if (email.isEmpty()) {
				target.setEmail(null);
			} else {
				target.setEmail(email);
			}
		}
		
		String phone = source.getPhone();
		if (phone != null) {
			if (phone.isEmpty()) {
				target.setPhone(null);
			} else {
				target.setPhone(phone);
			}
		}
		
		String info = source.getInfo();
		if (info != null) {
			if (info.isEmpty()) {
				target.setInfo(null);
			} else {
				target.setInfo(info);
			}
		}
		
		Long departmentId = source.getDepartmentId();
		if (departmentId != null) {
			if (departmentId == 0) {
				target.setDepartment(null);
			} else {
				Department department = new Department(departmentId);
				target.setDepartment(department);
			}
		}
		
		Long scientificDegreeId = source.getScientificDegreeId();
		if (scientificDegreeId != null) {
			if (scientificDegreeId == 0) {
				target.setScientificDegree(null);
			} else {
				ScientificDegree scientificDegree = new ScientificDegree(scientificDegreeId);
				target.setScientificDegree(scientificDegree);
			}
		}
		
		AcademicStatus academicStatus = source.getAcademicStatus();
		if (academicStatus != null) {
			if (academicStatus == AcademicStatus.NULL) {
				target.setAcademicStatus(null);
			} else {
				target.setAcademicStatus(academicStatus);
			}
		}
		
		return target;
	}

	@Override
	public Lecturer convert(final LecturerResource source) {
		return convert(source, new Lecturer());
	}

}
