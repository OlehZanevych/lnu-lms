package org.lnu.converter.file;

import org.lnu.converter.SecurityConverter;
import org.lnu.model.file.FileModel;
import org.lnu.resource.FileAPIResource;

/**
 * Entity converter for common file properties.
 * @author OlehZanevych
 *
 * @param <SOURCE> Entity
 * @param <TARGET> Resource
 */
public abstract class FileConverter<SOURCE extends FileModel, TARGET extends FileAPIResource>
	extends SecurityConverter<SOURCE, TARGET, Long> {

	@Override
	public TARGET convert(final SOURCE source, final TARGET target) {
		super.convert(source, target);
		
		String fileURI = source.getFileURI();
		if (fileURI != null) {
			target.setFileURI(fileURI);
		}
		
		return target;
	}

}
