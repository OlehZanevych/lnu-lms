package org.lnu.converter.group;

import java.util.List;
import java.util.stream.Collectors;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityResourceConverter;
import org.lnu.model.group.Group;
import org.lnu.model.user.User;
import org.lnu.resource.group.GroupResource;

/**
 * Converter, that converts from GroupResource to Group.
 * @author OlehZanevych
 */
@Converter("groupResourceConverter")
public class GroupResourceConverter extends SecurityResourceConverter<GroupResource, Group, Long> {

	@Override
	public Group convert(final GroupResource source, final Group target) {
		
		super.convert(source, target);

		String name = source.getName();
		if (name != null) {
			if (name.isEmpty()) {
				target.setName(null);
			} else {
				target.setName(name);
			}
		}
		
		String abbreviation = source.getAbbreviation();
		if (abbreviation != null) {
			if (abbreviation.isEmpty()) {
				target.setAbbreviation(null);
			} else {
				target.setAbbreviation(abbreviation);
			}
		}
		
		String description = source.getDescription();
		if (description != null) {
			if (description.isEmpty()) {
				target.setDescription(null);
			}
			target.setDescription(description);
		}
		
		List<Long> userIds = source.getUserIds();
		if (userIds != null) {
			target.setUsers(userIds.stream().map(i -> new User(i))
					.collect(Collectors.toList()));
		}
		
		return target;
	}

	@Override
	public Group convert(final GroupResource source) {
		return convert(source, new Group());
	}

}
