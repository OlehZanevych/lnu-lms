package org.lnu.converter.group;

import java.util.List;
import java.util.stream.Collectors;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityConverter;
import org.lnu.model.group.Group;
import org.lnu.model.user.User;
import org.lnu.resource.group.GroupResource;

/**
 * Converter, that converts from Group to GroupResource.
 * @author OlehZanevych
 */
@Converter("groupConverter")
public class GroupConverter extends SecurityConverter<Group, GroupResource, Long> {

	@Override
	public GroupResource convert(final Group source, final GroupResource target) {
		
		super.convert(source, target);
		
		String name = source.getName();
		if (name != null) {
			target.setName(name);
		}
		
		String abbreviation = source.getAbbreviation();
		if (abbreviation != null) {
			target.setAbbreviation(abbreviation);
		}
		
		String description = source.getDescription();
		if (description != null) {
			target.setDescription(description);
		}
		
		List<User> users = source.getUsers();
		if (users != null) {
			target.setUserIds(users.stream().map(i -> i.getId())
					.collect(Collectors.toList()));
		}
		
		return target;
	}

	@Override
	public GroupResource convert(final Group source) {
		return convert(source, new GroupResource());
	}

}
