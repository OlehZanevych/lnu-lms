package org.lnu.converter.scientificdegree;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityConverter;
import org.lnu.model.scientificdegree.ScientificDegree;
import org.lnu.resource.scientificdegree.ScientificDegreeResource;

/**
 * Converter, that converts from ScientificDegree to ScientificDegreeResource.
 * @author OlehZanevych
 */
@Converter("scientificDegreeConverter")
public class ScientificDegreeConverter extends SecurityConverter<ScientificDegree, ScientificDegreeResource, Long> {

	@Override
	public ScientificDegreeResource convert(final ScientificDegree source, final ScientificDegreeResource target) {
		
		super.convert(source, target);
		
		String name = source.getName();
		if (name != null) {
			target.setName(name);
		}
		
		String abbreviation = source.getAbbreviation();
		if (abbreviation != null) {
			target.getAbbreviation();
		}
		
		return target;
	}

	@Override
	public ScientificDegreeResource convert(final ScientificDegree source) {
		return convert(source, new ScientificDegreeResource());
	}

}
