package org.lnu.converter.scientificdegree;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityResourceConverter;
import org.lnu.model.scientificdegree.ScientificDegree;
import org.lnu.resource.scientificdegree.ScientificDegreeResource;

/**
 * Converter, that converts from ScientificDegreeResource to ScientificDegree.
 * @author OlehZanevych
 */
@Converter("scientificDegreeResourceConverter")
public class ScientificDegreeResourceConverter extends SecurityResourceConverter<ScientificDegreeResource, ScientificDegree, Long> {

	@Override
	public ScientificDegree convert(final ScientificDegreeResource source, final ScientificDegree target) {
		
		super.convert(source, target);

		String name = source.getName();
		if (name != null) {
			if (name.isEmpty()) {
				target.setName(null);
			} else {
				target.setName(name);
			}
		}
		
		String abbreviation = source.getAbbreviation();
		if (abbreviation != null) {
			if (abbreviation.isEmpty()) {
				target.setAbbreviation(null);
			} else {
				target.setAbbreviation(abbreviation);
			}
		}
		
		return target;
	}

	@Override
	public ScientificDegree convert(final ScientificDegreeResource source) {
		return convert(source, new ScientificDegree());
	}

}
