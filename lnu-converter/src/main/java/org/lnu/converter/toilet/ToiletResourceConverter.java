package org.lnu.converter.toilet;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityResourceConverter;
import org.lnu.model.building.map.BuildingMap;
import org.lnu.model.enumtype.toilettype.ToiletType;
import org.lnu.model.toilet.Toilet;
import org.lnu.resource.toilet.ToiletResource;

/**
 * Converter, that converts from ToiletResource to Toilet.
 * @author OlehZanevych
 */
@Converter("toiletResourceConverter")
public class ToiletResourceConverter extends SecurityResourceConverter<ToiletResource, Toilet, Long> {

	@Override
	public Toilet convert(final ToiletResource source, final Toilet target) {
		super.convert(source, target);

		Long buildingMapId = source.getBuildingMapId();
		if (buildingMapId != null) {
			if (buildingMapId == 0) {
				target.setBuildingMap(null);
			} else {
				target.setBuildingMap(new BuildingMap(buildingMapId));
			}
		}
		
		Short onMapId = source.getOnMapId();
		if (onMapId != null) {
			if (onMapId == 0) {
				target.setOnMapId(null);
			} else {
				target.setOnMapId(onMapId);
			}
		}
		
		ToiletType toiletType = source.getToiletType();
		if (toiletType != null) {
			if (toiletType == ToiletType.NULL) {
				target.setToiletType(null);
			} else {
				target.setToiletType(toiletType);
			}
		}
		
		String info = source.getInfo();
		if (info != null) {
			if (info.isEmpty()) {
				target.setInfo(null);
			} else {
				target.setInfo(info);
			}
		}
		
		return target;
	}

	@Override
	public Toilet convert(final ToiletResource source) {
		return convert(source, new Toilet());
	}

}
