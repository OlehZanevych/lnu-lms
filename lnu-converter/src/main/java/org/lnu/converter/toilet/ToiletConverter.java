package org.lnu.converter.toilet;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityConverter;
import org.lnu.model.building.Building;
import org.lnu.model.building.map.BuildingMap;
import org.lnu.model.enumtype.toilettype.ToiletType;
import org.lnu.model.toilet.Toilet;
import org.lnu.resource.toilet.ToiletResource;

/**
 * Converter, that converts from Toilet to ToiletResource.
 * @author OlehZanevych
 */
@Converter("toiletConverter")
public class ToiletConverter extends SecurityConverter<Toilet, ToiletResource, Long> {

	@Override
	public ToiletResource convert(final Toilet source, final ToiletResource target) {
		super.convert(source, target);
		
		BuildingMap buildingMap = source.getBuildingMap();
		if (buildingMap != null) {
			target.setBuildingMapId(buildingMap.getId());
			Building building = buildingMap.getBuilding();
			if (building != null) {
				target.setBuildingId(building.getId());
			}
		}
		
		Short onMapId = source.getOnMapId();
		if (onMapId != null) {
			target.setOnMapId(onMapId);
		}
		
		ToiletType toiletType = source.getToiletType();
		if (toiletType != null) {
			target.setToiletType(toiletType);
		}
		
		String info = source.getInfo();
		if (info != null) {
			target.setInfo(info);
		}
		
		return target;
	}

	@Override
	public ToiletResource convert(final Toilet source) {
		return convert(source, new ToiletResource());
	}

}
