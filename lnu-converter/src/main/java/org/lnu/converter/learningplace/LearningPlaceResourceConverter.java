package org.lnu.converter.learningplace;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityResourceConverter;
import org.lnu.model.building.map.BuildingMap;
import org.lnu.model.enumtype.learningplacetype.LearningPlaceType;
import org.lnu.model.learningplace.LearningPlace;
import org.lnu.resource.learningplace.LearningPlaceResource;

/**
 * Converter, that converts from LearningPlaceResource to LearningPlace.
 * @author OlehZanevych
 */
@Converter("learningPlaceResourceConverter")
public class LearningPlaceResourceConverter extends SecurityResourceConverter<LearningPlaceResource, LearningPlace, Long> {

	@Override
	public LearningPlace convert(final LearningPlaceResource source, final LearningPlace target) {
		super.convert(source, target);
		
		String name = source.getName();
		if (name != null) {
			if (name.isEmpty()) {
				target.setName(null);
			} else {
				target.setName(name);
			}
		}
		
		Long buildingMapId = source.getBuildingMapId();
		if (buildingMapId != null) {
			if (buildingMapId == 0) {
				target.setBuildingMap(null);
			} else {
				target.setBuildingMap(new BuildingMap(buildingMapId));
			}
		}
		
		Short onMapId = source.getOnMapId();
		if (onMapId != null) {
			if (onMapId == 0) {
				target.setOnMapId(null);
			} else {
				target.setOnMapId(onMapId);
			}
		}
		
		LearningPlaceType learningLearningPlaceType = source.getLearningPlaceType();
		if (learningLearningPlaceType != null) {
			if (learningLearningPlaceType == LearningPlaceType.NULL) {
				target.setLearningPlaceType(null);
			} else {
				target.setLearningPlaceType(learningLearningPlaceType);
			}
		}
		
		Float square = source.getSquare();
		if (square != null) {
			if (square == 0) {
				target.setSquare(null);
			} else {
				target.setSquare(square);
			}
		}
		
		Byte roomsCount = source.getRoomsCount();
		if (roomsCount != null) {
			if (roomsCount == 0) {
				target.setRoomsCount(null);
			} else {
				target.setRoomsCount(roomsCount);
			}
		}
		
		Short seats = source.getSeats();
		if (seats != null) {
			if (seats == 0) {
				target.setSeats(null);
			} else {
				target.setSeats(seats);
			}
		}
		
		Short seatsOnTest = source.getSeatsOnTest();
		if (seatsOnTest != null) {
			if (seatsOnTest == 0) {
				target.setSeatsOnTest(null);
			} else {
				target.setSeatsOnTest(seatsOnTest);
			}
		}
		
		String info = source.getInfo();
		if (info != null) {
			if (info.isEmpty()) {
				target.setInfo(null);
			} else {
				target.setInfo(info);
			}
		}
		
		return target;
	}

	@Override
	public LearningPlace convert(final LearningPlaceResource source) {
		return convert(source, new LearningPlace());
	}

}
