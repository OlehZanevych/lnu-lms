package org.lnu.converter.learningplace;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityConverter;
import org.lnu.model.building.Building;
import org.lnu.model.building.map.BuildingMap;
import org.lnu.model.enumtype.learningplacetype.LearningPlaceType;
import org.lnu.model.learningplace.LearningPlace;
import org.lnu.resource.learningplace.LearningPlaceResource;

/**
 * Converter, that converts from LearningPlace to LearningPlaceResource.
 * @author OlehZanevych
 */
@Converter("learningPlaceConverter")
public class LearningPlaceConverter extends SecurityConverter<LearningPlace, LearningPlaceResource, Long> {
	
	@Override
	public LearningPlaceResource convert(final LearningPlace source, final LearningPlaceResource target) {
		super.convert(source, target);
		
		String name = source.getName();
		if (name != null) {
			target.setName(name);
		}
		
		BuildingMap buildingMap = source.getBuildingMap();
		if (buildingMap != null) {
			target.setBuildingMapId(buildingMap.getId());
			Building building = buildingMap.getBuilding();
			if (building != null) {
				target.setBuildingId(building.getId());
			}
		}
		
		Short onMapId = source.getOnMapId();
		if (onMapId != null) {
			target.setOnMapId(onMapId);
		}
		
		LearningPlaceType learningLearningPlaceType = source.getLearningPlaceType();
		if (learningLearningPlaceType != null) {
			target.setLearningPlaceType(learningLearningPlaceType);
		}
		
		Short seats = source.getSeats();
		if (seats != null) {
			target.setSeats(seats);
		}
		
		Float square = source.getSquare();
		if (square != null) {
			target.setSquare(square);
		}
		
		Byte roomsCount = source.getRoomsCount();
		if (roomsCount != null) {
			target.setRoomsCount(roomsCount);
		}
		
		Short seatsOnTest = source.getSeatsOnTest();
		if (seatsOnTest != null) {
			target.setSeatsOnTest(seatsOnTest);
		}
		
		String info = source.getInfo();
		if (info != null) {
			target.setInfo(info);
		}
		
		return target;
	}

	@Override
	public LearningPlaceResource convert(final LearningPlace source) {
		return convert(source, new LearningPlaceResource());
	}

}
