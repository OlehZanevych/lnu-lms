package org.lnu.converter.role;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityResourceConverter;
import org.lnu.converter.apicapability.ApiCapabilityResourceConverter;
import org.lnu.model.api.capability.APICapability;
import org.lnu.model.role.Role;
import org.lnu.resource.apicapability.ApiCapabilityResource;
import org.lnu.resource.role.RoleResource;

/**
 * Converter, that converts from RoleResource to Role.
 * @author OlehZanevych
 */
@Converter("roleResourceConverter")
public class RoleResourceConverter extends SecurityResourceConverter<RoleResource, Role, Long> {

	@Resource(name = "apiCapabilityResourceConverter")
	private ApiCapabilityResourceConverter apiCapabilityResourceConverter;
	
	@Override
	public Role convert(final RoleResource source, final Role target) {
		
		super.convert(source, target);

		String name = source.getName();
		if (name != null) {
			if (name.isEmpty()) {
				target.setName(null);
			} else {
				target.setName(name);
			}
		}
		
		String abbreviation = source.getAbbreviation();
		if (abbreviation != null) {
			if (abbreviation.isEmpty()) {
				target.setAbbreviation(null);
			} else {
				target.setAbbreviation(abbreviation);
			}
		}
		
		String description = source.getDescription();
		if (description != null) {
			if (description.isEmpty()) {
				target.setDescription(null);
			}
			target.setDescription(description);
		}
		
		List<ApiCapabilityResource> apiCapabilityResources = source.getApiCapabilities();
		if (apiCapabilityResources != null) {
			List<APICapability> apiCapabilities = target.getApiCapabilities();
			if (apiCapabilities == null) {
				apiCapabilities = new LinkedList<APICapability>();
				target.setApiCapabilities(apiCapabilities);
			}
			apiCapabilityResourceConverter.updateCollection(apiCapabilityResources,
					apiCapabilities, "role", target);
		}
		
		return target;
	}

	@Override
	public Role convert(final RoleResource source) {
		return convert(source, new Role());
	}

}
