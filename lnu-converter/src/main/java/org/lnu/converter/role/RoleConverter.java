package org.lnu.converter.role;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityConverter;
import org.lnu.converter.apicapability.ApiCapabilityConverter;
import org.lnu.model.api.capability.APICapability;
import org.lnu.model.role.Role;
import org.lnu.resource.role.RoleResource;

/**
 * Converter, that converts from Role to RoleResource.
 * @author OlehZanevych
 */
@Converter("roleConverter")
public class RoleConverter extends SecurityConverter<Role, RoleResource, Long> {
	
	@Resource(name = "apiCapabilityConverter")
	private ApiCapabilityConverter apiCapabilityConverter;

	@Override
	public RoleResource convert(final Role source, final RoleResource target) {
		
		super.convert(source, target);
		
		String name = source.getName();
		if (name != null) {
			target.setName(name);
		}
		
		String abbreviation = source.getAbbreviation();
		if (abbreviation != null) {
			target.setAbbreviation(abbreviation);
		}
		
		String description = source.getDescription();
		if (description != null) {
			target.setDescription(description);
		}
		
		List<APICapability> apiCapabilities = source.getApiCapabilities();
		if (apiCapabilities != null) {
			target.setApiCapabilities(apiCapabilities.stream()
					.map(i -> apiCapabilityConverter.convert(i)).collect(Collectors.toList()));
		}
		
		return target;
	}

	@Override
	public RoleResource convert(final Role source) {
		return convert(source, new RoleResource());
	}

}
