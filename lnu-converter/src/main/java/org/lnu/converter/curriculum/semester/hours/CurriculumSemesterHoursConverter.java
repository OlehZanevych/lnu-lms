package org.lnu.converter.curriculum.semester.hours;

import org.lnu.annotation.Converter;
import org.lnu.converter.BaseConverter;
import org.lnu.model.curriculum.semester.hours.CurriculumSemesterHours;
import org.lnu.model.enumtype.classtype.ClassType;
import org.lnu.resource.curriculum.semester.hours.CurriculumSemesterHoursResource;

/**
 * Converter, that converts from CurriculumSemesterHours to CurriculumSemesterHoursResource.
 * @author OlehZanevych
 */
@Converter("curriculumSemesterHoursConverter")
public class CurriculumSemesterHoursConverter extends BaseConverter<CurriculumSemesterHours, CurriculumSemesterHoursResource, Long> {

	@Override
	public CurriculumSemesterHoursResource convert(final CurriculumSemesterHours source, final CurriculumSemesterHoursResource target) {
		
		super.convert(source, target);
		
		ClassType classType = source.getClassType();
		if (classType != null) {
			target.setClassType(classType);
		}
		
		Short hours = source.getHours();
		if (hours != null) {
			target.setHours(hours);
		}
		
		Boolean isAcademicGroupDepartment = source.getIsAcademicGroupDepartment();
		if (isAcademicGroupDepartment != null) {
			target.setIsAcademicGroupDepartment(isAcademicGroupDepartment);
		}
		
		return target;
	}

	@Override
	public CurriculumSemesterHoursResource convert(final CurriculumSemesterHours source) {
		return convert(source, new CurriculumSemesterHoursResource());
	}

}
