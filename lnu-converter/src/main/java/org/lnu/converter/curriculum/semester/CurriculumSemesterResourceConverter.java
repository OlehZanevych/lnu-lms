package org.lnu.converter.curriculum.semester;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;

import org.lnu.annotation.Converter;
import org.lnu.converter.AbstractResourceConverter;
import org.lnu.converter.curriculum.semester.hours.CurriculumSemesterHoursResourceConverter;
import org.lnu.model.curriculum.semester.CurriculumSemester;
import org.lnu.model.curriculum.semester.hours.CurriculumSemesterHours;
import org.lnu.model.enumtype.report.Report;
import org.lnu.resource.curriculum.semester.CurriculumSemesterResource;
import org.lnu.resource.curriculum.semester.hours.CurriculumSemesterHoursResource;

/**
 * Converter, that converts from CurriculumSemesterResource to CurriculumSemester.
 * @author OlehZanevych
 */
@Converter("curriculumSemesterResourceConverter")
public class CurriculumSemesterResourceConverter extends AbstractResourceConverter<CurriculumSemesterResource, CurriculumSemester> {
	
	@Resource(name = "curriculumSemesterHoursResourceConverter")
	private CurriculumSemesterHoursResourceConverter curriculumSemesterHoursResourceConverter;

	@Override
	public CurriculumSemester convert(final CurriculumSemesterResource source, final CurriculumSemester target) {
		Byte semester = source.getSemester();
		if (semester != null) {
			if (semester == 0) {
				target.setSemester(null);
			} else {
				target.setSemester(semester);
			}
		}
		
		Short independentStudy = source.getIndependentStudy();
		if (independentStudy != null) {
			if (independentStudy == 0) {
				target.setIndependentStudy(null);
			} else {
				target.setIndependentStudy(independentStudy);
			}
		}
		
		Report report = source.getReport();
		if (report != null) {
			if (report == Report.NULL) {
				target.setReport(null);
			} else {
				target.setReport(source.getReport());
			}
		}
		
		List<CurriculumSemesterHoursResource> curriculumSemesterHoursResources = source.getCurriculumSemesterHours();
		if (curriculumSemesterHoursResources != null) {
			List<CurriculumSemesterHours> curriculumSemesterHours = target.getCurriculumSemesterHours();
			if (curriculumSemesterHours == null) {
				curriculumSemesterHours = new LinkedList<CurriculumSemesterHours>();
				target.setCurriculumSemesterHours(curriculumSemesterHours);
			}
			curriculumSemesterHoursResourceConverter.updateCollection(curriculumSemesterHoursResources,
					curriculumSemesterHours, "curriculumSemester", target);
		}
		
		return target;
	}

	@Override
	public CurriculumSemester convert(final CurriculumSemesterResource source) {
		return convert(source, new CurriculumSemester());
	}

}
