package org.lnu.converter.curriculum;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityConverter;
import org.lnu.converter.curriculum.course.CurriculumCourseConverter;
import org.lnu.converter.curriculum.semester.CurriculumSemesterConverter;
import org.lnu.model.curriculum.Curriculum;
import org.lnu.model.curriculum.course.CurriculumCourse;
import org.lnu.model.curriculum.semester.CurriculumSemester;
import org.lnu.model.enumtype.coursecycle.CourseCycle;
import org.lnu.model.enumtype.coursetype.CourseType;
import org.lnu.model.specialty.Specialty;
import org.lnu.resource.curriculum.CurriculumResource;

/**
 * Converter, that converts from Curriculum to CurriculumResource.
 * @author OlehZanevych
 */
@Converter("curriculumConverter")
public class CurriculumConverter extends SecurityConverter<Curriculum, CurriculumResource, Long> {
	
	@Resource(name = "curriculumCourseConverter")
	private CurriculumCourseConverter curriculumCourseConverter;

	@Resource(name = "curriculumSemesterConverter")
	private CurriculumSemesterConverter curriculumSemesterConverter;
	
	@Override
	public CurriculumResource convert(final Curriculum source, final CurriculumResource target) {
		super.convert(source, target);
		
		List<CurriculumCourse> curriculumCourses = source.getCurriculumCourses();
		if (curriculumCourses != null) {
			target.setCurriculumCourses(curriculumCourses.stream()
					.map(i -> curriculumCourseConverter.convert(i)).collect(Collectors.toList()));
		}
		
		CourseType courseType = source.getCourseType();
		if (courseType != null) {
			target.setCourseType(courseType);
		}
		
		CourseCycle courseCycle = source.getCourseCycle();
		if (courseCycle != null) {
			target.setCourseCycle(courseCycle);
		}
		
		List<Specialty> specialties = source.getSpecialties();
		if (specialties != null) {
			target.setSpecialtyIds(specialties.stream().map(i -> i.getId()).collect(Collectors.toList()));
		}
		
		List<CurriculumSemester> curriculumSemesters = source.getCurriculumSemesters();
		if (curriculumSemesters != null) {
			target.setCurriculumSemesters(curriculumSemesters.stream()
					.map(i -> curriculumSemesterConverter.convert(i)).collect(Collectors.toList()));
		}
		
		return target;
	}

	@Override
	public CurriculumResource convert(final Curriculum source) {
		return convert(source, new CurriculumResource());
	}

}
