package org.lnu.converter.curriculum;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.lnu.annotation.Converter;
import org.lnu.converter.SecurityResourceConverter;
import org.lnu.converter.curriculum.course.CurriculumCourseResourceConverter;
import org.lnu.converter.curriculum.semester.CurriculumSemesterResourceConverter;
import org.lnu.model.curriculum.Curriculum;
import org.lnu.model.curriculum.course.CurriculumCourse;
import org.lnu.model.curriculum.semester.CurriculumSemester;
import org.lnu.model.specialty.Specialty;
import org.lnu.model.enumtype.coursecycle.CourseCycle;
import org.lnu.resource.curriculum.CurriculumResource;
import org.lnu.resource.curriculum.course.CurriculumCourseResource;
import org.lnu.resource.curriculum.semester.CurriculumSemesterResource;

/**
 * Converter, that converts from CurriculumResource to Curriculum.
 * @author OlehZanevych
 */
@Converter("curriculumResourceConverter")
public class CurriculumResourceConverter extends SecurityResourceConverter<CurriculumResource, Curriculum, Long> {
	
	@Resource(name = "curriculumCourseResourceConverter")
	private CurriculumCourseResourceConverter curriculumCourseResourceConverter;
	
	@Resource(name = "curriculumSemesterResourceConverter")
	private CurriculumSemesterResourceConverter curriculumSemesterResourceConverter;
	
	@Override
	public Curriculum convert(final CurriculumResource source, final Curriculum target) {
		super.convert(source, target);

		List<CurriculumCourseResource> curriculumCourseResources = source.getCurriculumCourses();
		if (curriculumCourseResources != null) {
			List<CurriculumCourse> curriculumCourses = target.getCurriculumCourses();
			if (curriculumCourses == null) {
				curriculumCourses = new LinkedList<CurriculumCourse>();
				target.setCurriculumCourses(curriculumCourses);
			}
			curriculumCourseResourceConverter.updateCollection(curriculumCourseResources,
					curriculumCourses, "curriculum", target);
		}
		
		CourseCycle courseCycle = source.getCourseCycle();
		if (courseCycle != null) {
			if (courseCycle == CourseCycle.NULL) {
				target.setCourseCycle(null);
			} else {
				target.setCourseCycle(courseCycle);
			}
		}
		
		List<Long> specialtyIds = source.getSpecialtyIds();
		if (specialtyIds != null) {
			target.setSpecialties(specialtyIds.stream().map(i -> new Specialty(i)).collect(Collectors.toList()));
		}
		
		List<CurriculumSemesterResource> curriculumSemesterResources = source.getCurriculumSemesters();
		if (curriculumSemesterResources != null) {
			List<CurriculumSemester> curriculumSemesters = target.getCurriculumSemesters();
			if (curriculumSemesters == null) {
				curriculumSemesters = new LinkedList<CurriculumSemester>();
				target.setCurriculumSemesters(curriculumSemesters);
			}
			curriculumSemesterResourceConverter.updateCollection(curriculumSemesterResources,
					curriculumSemesters, "curriculum", target);
		}
		
		return target;
	}

	@Override
	public Curriculum convert(final CurriculumResource source) {
		return convert(source, new Curriculum());
	}

}
