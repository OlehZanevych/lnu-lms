package org.lnu.converter.curriculum.semester;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.lnu.annotation.Converter;
import org.lnu.converter.BaseConverter;
import org.lnu.converter.curriculum.semester.hours.CurriculumSemesterHoursConverter;
import org.lnu.model.curriculum.semester.CurriculumSemester;
import org.lnu.model.curriculum.semester.hours.CurriculumSemesterHours;
import org.lnu.model.enumtype.report.Report;
import org.lnu.resource.curriculum.semester.CurriculumSemesterResource;

/**
 * Converter, that converts from CurriculumSemester to CurriculumSemesterResource.
 * @author OlehZanevych
 */
@Converter("curriculumSemesterConverter")
public class CurriculumSemesterConverter extends BaseConverter<CurriculumSemester, CurriculumSemesterResource, Long> {

	@Resource(name = "curriculumSemesterHoursConverter")
	private CurriculumSemesterHoursConverter curriculumSemesterHoursConverter;
	
	@Override
	public CurriculumSemesterResource convert(final CurriculumSemester source, final CurriculumSemesterResource target) {
		super.convert(source, target);
		
		Byte semester = source.getSemester();
		if (semester != null) {
			target.setSemester(semester);
		}
		
		Byte year = source.getYear();
		if (year != null) {
			target.setYear(year);
		}
		
		Byte half = source.getHalf();
		if (half != null) {
			target.setHalf(half);
		}
		
		Float credits = source.getCredits();
		if (credits != null) {
			target.setCredits(credits);
		}
		
		Short allHours = source.getAllHours();
		if (allHours != null) {
			target.setAllHours(allHours);
		}
		
		Short independentStudy = source.getIndependentStudy();
		if (independentStudy != null) {
			target.setIndependentStudy(independentStudy);
		}
		
		Report report = source.getReport();
		if (report != null) {
			target.setReport(source.getReport());
		}
		
		List<CurriculumSemesterHours> curriculumSemesterHours = source.getCurriculumSemesterHours();
		if (curriculumSemesterHours != null) {
			target.setCurriculumSemesterHours(curriculumSemesterHours.stream()
					.map(i -> curriculumSemesterHoursConverter.convert(i)).collect(Collectors.toList()));
		}
		
		return target;
	}

	@Override
	public CurriculumSemesterResource convert(final CurriculumSemester source) {
		return convert(source, new CurriculumSemesterResource());
	}

}
