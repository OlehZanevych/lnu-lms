package org.lnu.converter.curriculum.course;

import org.lnu.annotation.Converter;
import org.lnu.converter.AbstractResourceConverter;
import org.lnu.model.course.Course;
import org.lnu.model.curriculum.course.CurriculumCourse;
import org.lnu.model.department.Department;
import org.lnu.resource.curriculum.course.CurriculumCourseResource;

/**
 * Converter, that converts from CurriculumCourseResource to CurriculumCourse.
 * @author OlehZanevych
 */
@Converter("curriculumCourseResourceConverter")
public class CurriculumCourseResourceConverter extends AbstractResourceConverter<CurriculumCourseResource, CurriculumCourse> {

	@Override
	public CurriculumCourse convert(final CurriculumCourseResource source, final CurriculumCourse target) {
		Long courseId = source.getCourseId();
		if (courseId != null) {
			if (courseId == 0) {
				target.setCourse(null);
			} else {
				target.setCourse(new Course(courseId));
			}
		}
		
		Long departmentId = source.getDepartmentId();
		if (departmentId != null) {
			if (departmentId == 0) {
				target.setDepartment(null);
			} else {
				target.setDepartment(new Department(departmentId));
			}
		}
		
		return target;
	}

	@Override
	public CurriculumCourse convert(final CurriculumCourseResource source) {
		return convert(source, new CurriculumCourse());
	}
	
}
