package org.lnu.converter.curriculum.course;

import org.lnu.annotation.Converter;
import org.lnu.converter.BaseConverter;
import org.lnu.model.course.Course;
import org.lnu.model.curriculum.course.CurriculumCourse;
import org.lnu.model.department.Department;
import org.lnu.resource.curriculum.course.CurriculumCourseResource;

/**
 * Converter, that converts from CurriculumCourse to CurriculumCourseResource.
 * @author OlehZanevych
 */
@Converter("curriculumCourseConverter")
public class CurriculumCourseConverter extends BaseConverter<CurriculumCourse, CurriculumCourseResource, Long> {
	
	@Override
	public CurriculumCourseResource convert(final CurriculumCourse source, final CurriculumCourseResource target) {
		super.convert(source, target);
		
		Course course = source.getCourse();
		if (course != null) {
			target.setCourseId(course.getId());
		}
		
		Department department = source.getDepartment();
		if (department != null) {
			target.setDepartmentId(department.getId());
		}
		
		return target;
	}

	@Override
	public CurriculumCourseResource convert(final CurriculumCourse source) {
		return convert(source, new CurriculumCourseResource());
	}
	
}
