package org.lnu.converter.curriculum.semester.hours;

import org.lnu.annotation.Converter;
import org.lnu.converter.AbstractResourceConverter;
import org.lnu.model.curriculum.semester.hours.CurriculumSemesterHours;
import org.lnu.model.enumtype.classtype.ClassType;
import org.lnu.resource.curriculum.semester.hours.CurriculumSemesterHoursResource;

/**
 * Converter, that converts from CurriculumSemesterHoursResource to CurriculumSemesterHours.
 * @author OlehZanevych
 */
@Converter("curriculumSemesterHoursResourceConverter")
public class CurriculumSemesterHoursResourceConverter extends AbstractResourceConverter<CurriculumSemesterHoursResource, CurriculumSemesterHours> {

	@Override
	public CurriculumSemesterHours convert(final CurriculumSemesterHoursResource source, final CurriculumSemesterHours target) {

		ClassType classType = source.getClassType();
		if (classType != null) {
			if (classType == ClassType.NULL) {
				target.setClassType(null);
			} else {
				target.setClassType(classType);
			}
		}
		
		Short hours = source.getHours();
		if (hours != null) {
			if (hours == 0) {
				target.setHours(null);
			} else {
				target.setHours(hours);
			}
		}
		
		Boolean isAcademicGroupDepartment = source.getIsAcademicGroupDepartment();
		if (isAcademicGroupDepartment != null) {
			target.setIsAcademicGroupDepartment(isAcademicGroupDepartment);
		}
		
		return target;
	}

	@Override
	public CurriculumSemesterHours convert(final CurriculumSemesterHoursResource source) {
		return convert(source, new CurriculumSemesterHours());
	}

}
