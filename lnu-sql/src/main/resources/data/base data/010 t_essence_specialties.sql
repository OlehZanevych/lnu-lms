INSERT INTO t_essence_specialties(id, is_private, name, code, unit_id, educational_degree) VALUES 
	(1, FALSE, 'Інформатика', '11234.1234.1234', 12, 'BACHELOR'), 
	(2, FALSE, 'Прикладна математика', '21234.1234.1234', 12, 'BACHELOR'), 
	(3, FALSE, 'Системний аналіз', '31234.1234.1234', 12, 'BACHELOR'),
	(4, FALSE, 'Інформатика. Магістри', '41234.1234.1234', 12, 'MASTER'),
	(5, FALSE, 'Прикладна інформатика', '51234.1234.1234', 12, 'MASTER'),
	(6, FALSE, 'Прикладна математика', '61234.1234.1234', 12, 'MASTER'),
	(7, FALSE, 'Системний аналіз', '71234.1234.1234', 12, 'MASTER'),
	(8, FALSE, 'Інформатика', '81234.1234.1234', 12, 'SPECIALIST'),
	(9, FALSE, 'Прикладна інформатика', '91234.1234.1234', 12, 'SPECIALIST'),
	(10, FALSE, 'Прикладна математика', '101234.1234.1234', 12, 'SPECIALIST'),
	(11, FALSE, 'Системний аналіз', '111234.1234.1234', 12, 'SPECIALIST');
SELECT setval('t_essence_specialties_id_seq', 11);
