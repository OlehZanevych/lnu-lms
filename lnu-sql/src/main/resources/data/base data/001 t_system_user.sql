INSERT INTO t_system_users(id, is_private, login, password_hash, surname, first_name, middle_name, email, phone, info) VALUES 
	(1, FALSE, 'admin', '$2a$10$ePg0csFsTK1l0lkYC.2d.u2Ru5Es7Gq8HBLcIVPYQkxCxauKNKn9u', 'Zanevych', 'Oleh', 'Bohdanovych', 'Oleh.Zanevych@gmail.com', NULL, NULL), 
	(2, FALSE, 'test', '$2a$10$IM24p5bo0Ensum.R7/uoN.UWWwT76hIGPKfeJq059ExG4YEFF4fbi', 'Test', 'Test', 'Testovych', 'Test@gmail.com', NULL, NULL);
SELECT setval('t_system_users_id_seq', 2);
