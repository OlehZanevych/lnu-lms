INSERT INTO t_essence_departments(id, is_private, name, abbreviation, unit_id, email, phone, building_map_id, on_map_id, room, info) VALUES 
	(1, FALSE, 'Програмування', 'Прог.', 12, 'test@mail', '911', 1, 1, 1, 'info'), 
	(2, FALSE, 'Інформаційних систем', 'Інф. сист.', 12, 'test@mail', '911', 1, 1, 1, 'info'), 
	(3, FALSE, 'Дискретного аналізу та інтелектуальних систем', 'ДАІС', 12, 'test@mail', '911', 1, 1, 1, 'info'),
	(4, FALSE, 'Обчислювальної математики', 'Обч. мат.', 12, 'test@mail', '911', 1, 1, 1, 'info'),
	(5, FALSE, 'Прикладної математики', 'Прик. мат.', 12, 'test@mail', '911', 1, 1, 1, 'info'),
	(6, FALSE, 'Теорії оптимальних процесів', 'ТОП', 12, 'test@mail', '911', 1, 1, 1, 'info'),
	(7, FALSE, 'Математичного моделювання соціально-економічних процесів', 'ММСЕП', 12, 'test@mail', '911', 1, 1, 1, 'info');
SELECT setval('t_essence_departments_id_seq', 7);
