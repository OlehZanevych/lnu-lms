INSERT INTO t_essence_courses (id, is_private, name, abbreviation, info) VALUES 
	(1, FALSE, 'Математичний аналіз', 'МА', 'info'),
	(2, FALSE, 'Алгебра', 'Алгебра', 'info'),
	(3, FALSE, 'Програмування', 'Програмування', 'info'),
	(4, FALSE, 'Чисельні методи', 'ЧМ', 'info');
SELECT setval('t_essence_courses_id_seq', 4);
