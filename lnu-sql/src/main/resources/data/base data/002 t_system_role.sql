INSERT INTO t_system_roles(id, is_private, name, abbreviation, description) VALUES 
	(1, FALSE, 'Admin', 'Admin', 'Role for system administrators'), 
	(2, FALSE, 'Student', 'Student', 'Role for students');
SELECT setval('t_system_roles_id_seq', 2);
